<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel=StyleSheet href="styles.css" type="text/css">
    <title>Our site is under maintenance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
    <div class="content-holder">
        <div class="content">
            <header class="header">
                <img src="images/logo.svg" alt="Aura by Tracie Ellis" width="142px" />
            </header>
            <h1>Our site is <br>under maintenance</h1>
            <h2>Customer service</h2>
            <p>For any concerns, please contact our customer service center on:</p>
            <div class="contact">
                <p>
                    <i class="fa fa-envelope-o"></i>Email <a href="mailto:hello@aurahome.com.au">hello@aurahome.com.au</a><br>
                </p>
                <p>
                    <i class="fa fa-phone"></i>Phone: 1300 304 269<br>
                    <span class="block">9am - 5pm<br>
                    Monday - Friday</span>
                </p>
            </div>
        </div>
    </div>
</body>
</html>
