<?php
// enable, adjust and copy this code for each store you run
// Store #0, default one
//if (isHttpHost("example.com")) {
//    $_SERVER["MAGE_RUN_CODE"] = "default";
//    $_SERVER["MAGE_RUN_TYPE"] = "store";
//}
function isHttpHost($host)
{
    if (!isset($_SERVER['HTTP_HOST'])) {
        return false;
    }
    return strpos(str_replace('---', '.', $_SERVER['HTTP_HOST']), $host) === 0;
}

//Linen House Australia
if (isHttpHost("www.linenhouse.com.au") || isHttpHost("linenhouse.com.au") || isHttpHost("www.linenhouse.com") || isHttpHost("linenhouse.com") || isHttpHost("staging.linenhouse.com")) {

    if (strpos($_SERVER['REQUEST_URI'], '/en-hk') === 0) {
        $_SERVER["MAGE_RUN_CODE"] = "linen_hk";
        $_SERVER["MAGE_RUN_TYPE"] = "website";
    } else {
        $_SERVER["MAGE_RUN_CODE"] = "linen_au";
        $_SERVER["MAGE_RUN_TYPE"] = "website";
    }
} else

//Linen House New Zealand
if (isHttpHost("www.linenhouse.co.nz") || isHttpHost("linenhouse.co.nz") || isHttpHost("staging.linenhouse.co.nz")) {
    $_SERVER["MAGE_RUN_CODE"] = "linen_nz";
    $_SERVER["MAGE_RUN_TYPE"] = "website";
} else

//Linen House Singapore
if (isHttpHost("www.linenhouse.sg") || isHttpHost("linenhouse.sg") || isHttpHost("staging.linenhouse.sg")) {
    $_SERVER["MAGE_RUN_CODE"] = "linen_sg";
    $_SERVER["MAGE_RUN_TYPE"] = "website";
} else

//Aura
if (isHttpHost("www.aurahome.com.au") || isHttpHost("aurahome.com.au") || isHttpHost("staging.aurahome.com.au")) {
    $_SERVER["MAGE_RUN_CODE"] = "aura_au";
    $_SERVER["MAGE_RUN_TYPE"] = "website";
} else

//Linen House B2B
if (isHttpHost("www.b2b.linenhouse.com.au") || isHttpHost("b2b.linenhouse.com.au") || isHttpHost("staging.b2b.linenhouse.com.au")) {
    $_SERVER["MAGE_RUN_CODE"] = "linen_au_b2b";
    $_SERVER["MAGE_RUN_TYPE"] = "website";
} else

//Linen House MY
if (isHttpHost("www.linenhouse.my") || isHttpHost("linenhouse.my") || isHttpHost("staging.linenhouse.my")) {
    $_SERVER["MAGE_RUN_CODE"] = "linen_my";
    $_SERVER["MAGE_RUN_TYPE"] = "website";
} else

//Linen House ID
if (isHttpHost("www.linenhouse.id") || isHttpHost("linenhouse.id") || isHttpHost("staging.linenhouse.id")) {
    $_SERVER["MAGE_RUN_CODE"] = "linen_id";
    $_SERVER["MAGE_RUN_TYPE"] = "website";
} else

//Dev env
if (isset($_SERVER['HTTP_HOST'])) {
    $data = explode('-dev-', $_SERVER['HTTP_HOST']);
    if (count($data) > 1 && !empty($data[0])) {
        $_SERVER["MAGE_RUN_CODE"] = str_replace('-', '_', $data[0]);
        $_SERVER["MAGE_RUN_TYPE"] = "website";
    }
}

