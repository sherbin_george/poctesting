<?php
namespace Balance\AwBlogCustomTags\Plugin\Balance\AwBlogCustomFields\Model;

/**
 * Class PostRepository.
 * Post repository model plugins.
 *
 * @package Balance\AwBlogCustomTags\Plugin\Balance\AwBlogCustomFields\Model
 */
class PostRepository
{
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder $_searchCriteriaBuilder Search criteria builder.
     */
    protected $_searchCriteriaBuilder;
    /**
     * @var \Balance\AwBlogCustomTags\Helper\Data $_helper Helper instance.
     */
    protected $_helper;
    /**
     * @var array $_customTagsAttributes Custom tags attributes data.
     */
    protected $_customTagsAttributes;

    /**
     * Post constructor.
     *
     * @param \Magento\Framework\Api\SearchCriteriaBuilder  $searchCriteriaBuilder Search criteria builder.
     * @param \Balance\AwBlogCustomTags\Helper\Data $helper                Helper.
     * @param array                                         $customTagsAttributes  Custom tags attributes.
     */
    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Balance\AwBlogCustomTags\Helper\Data $helper,
        $customTagsAttributes
    )
    {
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_helper = $helper;
        $this->_customTagsAttributes = $customTagsAttributes;
    }

    /**
     * Around save plugin.
     * NOTE: we use this plugin to process custom tags saving, when they are set as extension attributes.
     *
     * @param \Balance\AwBlogCustomFields\Model\PostRepository $subject Post repository.
     * @param \Closure                                                 $proceed Closure.
     * @param \Aheadworks\Blog\Api\Data\PostInterface                  $post    Post instance.
     *
     * @return void
     */
    public function aroundSave(
        \Balance\AwBlogCustomFields\Model\PostRepository $subject,
        \Closure $proceed,
        \Aheadworks\Blog\Api\Data\PostInterface $post
    )
    {
        unset($subject);

        /** @var \Aheadworks\Blog\Api\Data\PostExtension $extensionAttributes */
        $extensionAttributes = $post->getExtensionAttributes();
        $this->_extractCustomTags($extensionAttributes->__toArray());
        $proceed($post);
        $this->_processCustomTags($post);
    }

    /**
     * Extracts custom tags values from extension attributes to be processed later.
     *
     * @param array $extensionAttributes Extension attributes.
     *
     * @return void
     */
    private function _extractCustomTags($extensionAttributes)
    {
        foreach ($this->_customTagsAttributes as $attributeKey => $attributeData) {
            if (!isset($extensionAttributes[$attributeData['attribute_name']])) {
                continue;
            }

            $attributeValue = $extensionAttributes[$attributeData['attribute_name']];
            $this->_customTagsAttributes[$attributeKey]['attribute_value'] = $attributeValue;
        }
    }

    /**
     * Processes custom tags saving.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    private function _processCustomTags($post)
    {
        foreach ($this->_customTagsAttributes as $attributeKey => $attributeData) {
            if (!isset($attributeData['attribute_value'])
                || !isset($attributeData['attribute_save_processor'])
            ) {
                continue;
            }

            /**
             * @var \Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save\ProcessorInterface $attributeSaveProcessor
             */
            $attributeSaveProcessor = $attributeData['attribute_save_processor'];
            $attributeSaveProcessor->processValue($attributeData['attribute_value'], $post);
        }
    }

    /**
     * Around load extension attributes plugin.
     * NOTE: we use this plugin to process custom tags loading into a post instance.
     *
     * @param \Balance\AwBlogCustomFields\Model\PostRepository $subject Post repository.
     * @param \Closure                                                 $proceed Closure.
     * @param \Aheadworks\Blog\Api\Data\PostInterface                  $post    Post instance.
     *
     * @return void
     */
    public function aroundLoadExtensionAttributes(
        \Balance\AwBlogCustomFields\Model\PostRepository $subject,
        \Closure $proceed,
        \Aheadworks\Blog\Api\Data\PostInterface $post
    ) {
        unset($subject);

        $proceed($post);
        $this->_loadCustomTags($post);
    }

    /**
     * Loads custom tags values into a post instance.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    private function _loadCustomTags(\Aheadworks\Blog\Api\Data\PostInterface $post)
    {
        foreach ($this->_customTagsAttributes as $attributeData) {
            if (!isset($attributeData['attribute_name'])
                || !isset($attributeData['tag_repository'])
            ) {
                continue;
            }

            /** @var \Balance\AwBlogCustomTags\Api\TagRepositoryInterface $tagRepository */
            $tagRepository = $attributeData['tag_repository'];

            $this->_searchCriteriaBuilder->addFilter('post_ids', $post->getId());
            $tagItems = $tagRepository->getListByPostIds(
                $this->_searchCriteriaBuilder->create()
            );

            $this->_helper->populatePostsWithTagNames([$post], $tagItems, $attributeData['attribute_name']);
        }
    }
}