<?php
namespace Balance\AwBlogCustomTags\Setup;

use Magento\Framework\Phrase;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\ValidatorException;
use Balance\AwBlogCustomTags\Model\ResourceModel\Tag as AbstractTagResourceModel;

/**
 * Class InstallSchema.
 * Install module schema.
 *
 * @package Balance\AwBlogCustomTags\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var SchemaSetupInterface $_setup Setup model.
     */
    protected $_setup;

    /**
     * Install module schema.
     *
     * @param SchemaSetupInterface   $setup Setup model.
     * @param ModuleContextInterface $context Context model.
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->_setup = $setup;

        $this->_createProjectTagsTable();
        $this->_createProjectTagsRelationTable();

        $setup->endSetup();
    }

    /**
     * Creates project tags table.
     *
     * @return void
     */
    private function _createProjectTagsTable()
    {
        try {
            $this->_validateTableColumns(
                'aw_blog_tag',
                [
                    \Balance\AwBlogCustomTags\Api\Data\TagInterface::FIELD_ID,
                    \Balance\AwBlogCustomTags\Api\Data\TagInterface::FIELD_NAME,
                    \Balance\AwBlogCustomTags\Api\Data\TagInterface::FIELD_CREATED_AT,
                    \Balance\AwBlogCustomTags\Api\Data\TagInterface::FIELD_UPDATED_AT
                ]
            );
            $duplicatedTable = $this->_getTableDuplicate(
                'aw_blog_tag',
                \Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project::TABLE_NAME
            );
            $this->_createTable($duplicatedTable);
        } catch (AlreadyExistsException $exception) {
            return;
        }
    }

    /**
     * Creates project tags relation table.
     *
     * @return void
     *
     * @throws NotFoundException
     */
    private function _createProjectTagsRelationTable()
    {
        try {
            $this->_validateTableColumns(
                'aw_blog_post_tag',
                [
                    AbstractTagResourceModel::RELATION_TABLE_COLUMN_TAG_ID,
                    AbstractTagResourceModel::RELATION_TABLE_COLUMN_POST_ID
                ]
            );
            $duplicatedTable = $this->_getTableDuplicate(
                'aw_blog_post_tag',
                \Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project::RELATION_TABLE_NAME
            );
            $this->_createTable($duplicatedTable);
        } catch (AlreadyExistsException $exception) {
            return;
        }

        $connection = $this->_setup->getConnection();

        // We have to replace the foreign key to right one.
        $tagIdForeignKey = null;
        foreach ($connection->getForeignKeys($duplicatedTable->getName()) as $foreignKey) {
            if ($foreignKey['REF_TABLE_NAME'] == 'aw_blog_tag') {
                $connection->dropForeignKey($duplicatedTable->getName(), $foreignKey['FK_NAME']);
                $tagIdForeignKey = $foreignKey;
                break;
            }
        }

        if (is_null($tagIdForeignKey)) {
            throw new NotFoundException(
                new Phrase('Foreign key of ' . $duplicatedTable->getName() . ' table cannot be altered.')
            );
        }

        $connection->addForeignKey(
            $connection->getForeignKeyName(
                $tagIdForeignKey['TABLE_NAME'],
                $tagIdForeignKey['COLUMN_NAME'],
                \Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project::TABLE_NAME,
                $tagIdForeignKey['REF_COLUMN_NAME']
            ),
            $tagIdForeignKey['TABLE_NAME'],
            $tagIdForeignKey['COLUMN_NAME'],
            \Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project::TABLE_NAME,
            $tagIdForeignKey['REF_COLUMN_NAME'],
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
    }

    /**
     * Returns table duplicate (prepared for creating).
     *
     * @param string $sourceTableName      Source table name.
     * @param string $destinationTableName Destination table name.
     *
     * @return \Magento\Framework\DB\Ddl\Table
     *
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    private function _getTableDuplicate($sourceTableName, $destinationTableName)
    {
        $destinationTableName = $this->_setup->getTable($destinationTableName);
        if ($this->_setup->tableExists($destinationTableName)) {
            throw new AlreadyExistsException(
                new Phrase('Destination table ' . $destinationTableName . ' already exists.')
            );
        }

        $sourceTableName = $this->_setup->getTable($sourceTableName);
        if (!$this->_setup->tableExists($sourceTableName)) {
            throw new NoSuchEntityException(
                new Phrase('Source table ' . $sourceTableName . 'does not exist.')
            );
        }

        return $this->_setup->getConnection()->createTableByDdl($sourceTableName, $destinationTableName);
    }

    /**
     * Creates specified table.
     *
     * @param \Magento\Framework\DB\Ddl\Table $table Table instance.
     *
     * @return void
     */
    private function _createTable($table)
    {
        $this->_setup->getConnection()->createTable($table);
    }

    /**
     * Validates table columns.
     *
     * @param string $tableName Source table name.
     * @param array  $fieldList Expected fields list (table columns).
     *
     * @return void
     *
     * @throws ValidatorException
     */
    private function _validateTableColumns($tableName, $fieldList)
    {
        $tableColumns = array_keys($this->_setup->getConnection()->describeTable($tableName));
        $isEqual = count(array_intersect($tableColumns, $fieldList)) == count($tableColumns);
        if (!$isEqual) {
            throw new ValidatorException(
                new Phrase($tableName . ' table columns differs from specified field list.')
            );
        }
    }
}