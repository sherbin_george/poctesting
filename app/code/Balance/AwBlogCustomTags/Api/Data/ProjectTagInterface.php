<?php
namespace Balance\AwBlogCustomTags\Api\Data;

/**
 * Interface ProjectTagInterface.
 * Project tag interface.
 *
 * @package Balance\AwBlogCustomTags\Api\Data
 */
interface ProjectTagInterface extends TagInterface
{
}