<?php
namespace Balance\AwBlogCustomTags\Api\Data;

/**
 * Interface TagInterface.
 * Custom tag interface.
 *
 * @package Balance\AwBlogCustomTags\Api\Data
 */
interface TagInterface
{
    /**
     * @const string ID field name.
     */
    const FIELD_ID = 'id';
    /**
     * @const string Name field name.
     */
    const FIELD_NAME = 'name';
    /**
     * @const string Created at field name.
     */
    const FIELD_CREATED_AT = 'created_at';
    /**
     * @const string Updated at field name.
     */
    const FIELD_UPDATED_AT = 'updated_at';
    /**
     * @const string Related post IDs field value.
     */
    const FIELD_POST_IDS = 'post_ids';

    /**
     * Returns tag ID.
     *
     * @return int
     */
    public function getId();

    /**
     * Sets tag ID.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setId($value);

    /**
     * Returns tag name.
     *
     * @return string
     */
    public function getName();

    /**
     * Sets tag name.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setName($value);

    /**
     * Returns tag created at value.
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Sets tag created at value.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setCreatedAt($value);

    /**
     * Returns tag updated at value.
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Sets tag updated at value.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setUpdatedAt($value);

    /**
     * Returns related post IDs.
     *
     * @return int[]
     */
    public function getPostIds();

    /**
     * Sets related post IDs.
     *
     * @param string|array $value Value (string: add a new one, array: replace previous list).
     *
     * @return void
     */
    public function setPostIds($value);

    /**
     * Adds related post ID.
     *
     * @param string|array $value Value (string: add a new one, array: replace previous list).
     *
     * @return void
     */
    public function addPostId($value);
}