<?php
namespace Balance\AwBlogCustomTags\Api;

use Magento\Framework\Exception\LocalizedException;

/**
 * Interface TagRepository.
 * Custom tag repository interface.
 *
 * @package Balance\AwBlogCustomTags\Api
 */
interface TagRepositoryInterface
{
    /**
     * Returns tag instance by ID.
     *
     * @param int $tagId Tag ID.
     *
     * @return \Balance\AwBlogCustomTags\Api\Data\TagInterface
     */
    public function getById($tagId);

    /**
     * Returns tag list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return array
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Returns tag list bu specified post IDs.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return array
     */
    public function getListByPostIds(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Returns tag list by specified tag names.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return array
     */
    public function getListByTagNames(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Saves tag instance.
     *
     * @param \Balance\AwBlogCustomTags\Api\Data\TagInterface $tag Tag instance.
     *
     * @return void
     */
    public function save(\Balance\AwBlogCustomTags\Api\Data\TagInterface $tag);

    /**
     * Deletes tags by specified conditions.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return void
     *
     * @throws LocalizedException
     */
    public function deleteList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}