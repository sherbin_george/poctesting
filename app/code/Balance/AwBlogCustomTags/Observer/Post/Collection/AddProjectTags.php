<?php
namespace Balance\AwBlogCustomTags\Observer\Post\Collection;

/**
 * Class AddProjectTags.
 * Observer adds project tags to post collection items.
 * TODO: re-factor observer to process all custom tags, not only project.
 *
 * @package Balance\AwBlogCustomTags\Observer\Post\Collection
 */
class AddProjectTags implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Balance\AwBlogCustomTags\Model\Tag\Repository\Project $_projectTagRepository Tag repository.
     */
    protected $_projectTagRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder $_searchCriteriaBuilder Search criteria builder.
     */
    protected $_searchCriteriaBuilder;
    /**
     * @var \Balance\AwBlogCustomTags\Helper\Data $_helper Helper instance.
     */
    protected $_helper;

    /**
     * AddProjectTags constructor.
     *
     * @param \Balance\AwBlogCustomTags\Model\Tag\Repository\Project $projectTagRepository  Tag repository.
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                   $searchCriteriaBuilder Search criteria
     *                                                                                              builder.
     * @param \Balance\AwBlogCustomTags\Helper\Data                  $helper                Helper.
     */
    public function __construct(
        \Balance\AwBlogCustomTags\Model\Tag\Repository\Project $projectTagRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Balance\AwBlogCustomTags\Helper\Data $helper
    )
    {
        $this->_projectTagRepository = $projectTagRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_helper = $helper;
    }

    /**
     * Observer execute method.
     *
     * @param \Magento\Framework\Event\Observer $observer Observer instance.
     *
     * @return void
     *
     * @event add_project_tags_on_post_collection_load
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Aheadworks\Blog\Model\ResourceModel\Post\Collection $postCollection */
        $postCollection = $this->_extractEventObject($observer);
        if (is_null($postCollection)) {
            return;
        }

        $this->_searchCriteriaBuilder->addFilter(
            'post_ids',
            array_keys($postCollection->getItems())
        );

        $tagItems = $this->_projectTagRepository->getListByPostIds(
            $this->_searchCriteriaBuilder->create()
        );

        $this->_helper->populatePostsWithTagNames(
            $postCollection->getItems(),
            $tagItems,
            \Balance\AwBlogCustomTags\Model\Tag\Project::CUSTOM_TAGS_NAME
        );
    }

    /**
     * Returns event object instance.
     *
     * @param \Magento\Framework\Event\Observer $observer Observer instance.
     *
     * @return \Aheadworks\Blog\Model\ResourceModel\Post\Collection|null
     */
    private function _extractEventObject($observer)
    {
        $eventName = $observer->getEvent()->getName();
        $eventObjectName = str_replace(['aheadworks_blog_', '_load_after'], '', $eventName);

        return $observer->getData($eventObjectName);
    }
}