<?php
namespace Balance\AwBlogCustomTags\Observer\Post;

/**
 * Class DeleteProjectTags.
 * Observer cleans up outdated tags on post delete.
 * TODO: re-factor observer to process all custom tags, not only project.
 *
 * @package Balance\AwBlogCustomTags\Observer\Post
 */
class DeleteProjectTags implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Balance\AwBlogCustomTags\Model\Tag\Repository\Project $_projectTagRepository Tag repository.
     */
    protected $_projectTagRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder $_searchCriteriaBuilder Search criteria builder.
     */
    protected $_searchCriteriaBuilder;

    /**
     * DeleteProjectTags constructor.
     *
     * @param \Balance\AwBlogCustomTags\Model\Tag\Repository\Project $projectTagRepository  Tag repository.
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                   $searchCriteriaBuilder Search criteria
     *                                                                                              builder.
     */
    function __construct(
        \Balance\AwBlogCustomTags\Model\Tag\Repository\Project $projectTagRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->_projectTagRepository = $projectTagRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Observer execute method.
     *
     * @param \Magento\Framework\Event\Observer $observer Observer instance.
     *
     * @return void
     *
     * @event aheadworks_blog_api_data_postinterface_delete_after
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $post = $observer->getData('entity');
        if (is_null($post)) {
            return;
        }

        $this->_searchCriteriaBuilder->addFilter('post_ids', $post->getId(), 'IN');

        $this->_projectTagRepository->deleteList(
            $this->_searchCriteriaBuilder->create()
        );
    }
}