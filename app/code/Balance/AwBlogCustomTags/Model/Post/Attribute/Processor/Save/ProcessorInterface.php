<?php
namespace Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save;

/**
 * Interface ProcessorInterface.
 * Extension attribute value processor interface.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save
 */
interface ProcessorInterface
{
    /**
     * Processes custom tags attribute value saving.
     *
     * @param null|bool|int|string|array              $value   Attribute value.
     * @param \Aheadworks\Blog\Api\Data\PostInterface $context Context post model.
     *
     * @return void
     */
    public function processValue($value, $context);
}