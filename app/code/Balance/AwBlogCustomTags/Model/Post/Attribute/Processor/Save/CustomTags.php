<?php
namespace Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save;

/**
 * Class CustomTags.
 * Custom tags save processor abstract model.
 *
 * @package Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save
 */
abstract class CustomTags
    implements \Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save\ProcessorInterface
{
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder $_searchCriteriaBuilder Search criteria builder.
     */
    protected $_searchCriteriaBuilder;
    /**
     * @var \Balance\AwBlogCustomTags\Api\TagRepositoryInterface Tag repository.
     */
    protected $_tagRepository;
    /**
     * @var array $_addedTagItems Added tag items.
     */
    private $_addedTagItems = [];

    /**
     * CustomTags constructor.
     *
     * @param \Balance\AwBlogCustomTags\Api\TagRepositoryInterface $tagRepository         Tag repository.
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                 $searchCriteriaBuilder Search criteria
     *                                                                                            builder.
     */
    public function __construct(
        \Balance\AwBlogCustomTags\Api\TagRepositoryInterface $tagRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->_tagRepository = $tagRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Processes custom tags attribute value saving.
     *
     * @param null|bool|int|string|array              $value   Attribute value.
     * @param \Aheadworks\Blog\Api\Data\PostInterface $context Context post model.
     *
     * @return void
     */
    public function processValue($value, $context)
    {
        $tagNames = $value;
        $post = $context;

        // Retrieve existing tag items by specified tag names.
        $this->_searchCriteriaBuilder->addFilter('tag_names', $value);
        $tagItems = $this->_tagRepository->getListByTagNames(
            $this->_searchCriteriaBuilder->create()
        );

        $tagsToAdd = $this->_getNewTags($tagNames, $tagItems);
        $this->_addNewTags($post, $tagsToAdd);
        $this->_updateExistingTags($post, $tagItems);

        $this->_removeOutdatedTags(
            $post,
            array_merge($tagItems, $this->_addedTagItems)
        );
    }

    /**
     * Returns new tag list (to be added later).
     *
     * @param array $tagNames Tag names.
     * @param array $tagItems Existing tag items.
     *
     * @return array
     */
    protected function _getNewTags($tagNames, $tagItems)
    {
        if (is_array($tagNames)) {
            $tagNames = $this->_getTagNames($tagItems);
            if (is_array($tagNames)) {
                return array_diff($tagNames, $tagNames);
            }

            return $tagItems;
        }

        return [];
    }

    /**
     * Returns tag name list for specified tag items.
     *
     * @param array $tagItems Tag items.
     *
     * @return array
     */
    protected function _getTagNames($tagItems)
    {
        $tagNames = [];

        /** @var \Balance\AwBlogCustomTags\Api\Data\TagInterface $tagItem */
        foreach ($tagItems as $tagItem) {
            $tagNames[] = $tagItem->getName();
        }

        return $tagNames;
    }

    /**
     * Returns tag id list for specified tag items.
     *
     * @param array $tagItems Tag items.
     *
     * @return array
     */
    protected function _getTagIds($tagItems)
    {
        $tagNames = [];

        /** @var \Balance\AwBlogCustomTags\Api\Data\TagInterface $tagItem */
        foreach ($tagItems as $tagItem) {
            $tagNames[] = $tagItem->getId();
        }

        return $tagNames;
    }

    /**
     * Saves new tags.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post      Related post.
     * @param array                                   $tagsToAdd Tag list to be added.
     *
     * @return void
     */
    protected function _addNewTags($post, $tagsToAdd)
    {
        foreach ($tagsToAdd as $tagName) {
            /** @var \Balance\AwBlogCustomTags\Api\Data\TagInterface $tag */
            $tag = $this->_getTagFactory()->create();

            $tag->setId(null);
            $tag->setName($tagName);
            $tag->addPostId($post->getId());
            $this->_tagRepository->save($tag);
            $this->_addedTagItems[] = $tag;
        }
    }

    /**
     * Updates existing tag items.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post     Related post.
     * @param array                                   $tagItems Tag items to be updated.
     *
     * @return void
     */
    protected function _updateExistingTags($post, $tagItems)
    {
        /** @var \Balance\AwBlogCustomTags\Api\Data\TagInterface $tagItem */
        foreach ($tagItems as $tagItem) {
            $tagItem->addPostId($post->getId());
            $this->_tagRepository->save($tagItem);
        }
    }

    /**
     * Removes outdated tags (all tags except specified).
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post         Related post.
     * @param array                                   $existingTags Existing tag list.
     *
     * @return void
     */
    protected function _removeOutdatedTags($post, $existingTags)
    {
        $this->_searchCriteriaBuilder->addFilter('post_ids', $post->getId(), 'IN');
        $this->_searchCriteriaBuilder->addFilter(
            'tag_ids',
            implode(',', $this->_getTagIds($existingTags)),
            'NOT IN'
        );

        $this->_tagRepository->deleteList(
            $this->_searchCriteriaBuilder->create()
        );
    }

    /**
     * Returns tag factory.
     * Should be implemented in child classes.
     *
     * @return \Balance\AwBlogCustomTags\Api\Data\TagInterfaceFactory
     */
    abstract protected function _getTagFactory();
}