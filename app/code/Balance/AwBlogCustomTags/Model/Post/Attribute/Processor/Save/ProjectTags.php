<?php
namespace Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save;

use \Balance\AwBlogCustomTags\Model\Tag\Repository\Project as ProjectTagRepository;
use \Balance\AwBlogCustomTags\Api\Data\ProjectTagInterfaceFactory as ProjectTagFactory;

/**
 * Class ProjectTags.
 * Project tags save processor model.
 *
 * @package Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save
 */
class ProjectTags extends \Balance\AwBlogCustomTags\Model\Post\Attribute\Processor\Save\CustomTags
{
    /**
     * @var ProjectTagFactory $_tagFactory Tag factory.
     */
    protected $_tagFactory;

    /**
     * ProjectTags constructor.
     *
     * @param ProjectTagRepository                         $tagRepository         Tag repository.
     * @param ProjectTagFactory                            $tagFactory            Tag factory.
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder Search criteria builder.
     */
    function __construct(
        ProjectTagRepository $tagRepository,
        ProjectTagFactory $tagFactory,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->_tagFactory = $tagFactory;
        parent::__construct($tagRepository, $searchCriteriaBuilder);
    }

    /**
     * Returns tag factory.
     *
     * @return ProjectTagFactory
     */
    protected function _getTagFactory()
    {
        return $this->_tagFactory;
    }
}