<?php
namespace Balance\AwBlogCustomTags\Model\Aheadworks\Blog\ResourceModel\Post\Grid;

/**
 * Class Collection.
 * NOTE: we have to override the original collection to specify event prefix and event object name.
 *
 * @package Balance\AwBlogCustomTags\Model\Aheadworks\Blog\ResourceModel\Post\Grid
 */
class Collection extends \Aheadworks\Blog\Model\ResourceModel\Post\Grid\Collection
{
    /**
     * @var string $_eventPrefix Prefix of model events names.
     */
    protected $_eventPrefix = 'aheadworks_blog_post_grid_collection';
    /**
     * @var string $_eventObject Parameter name in event.
     */
    protected $_eventObject = 'post_grid_collection';
}