<?php
namespace Balance\AwBlogCustomTags\Model\Tag\Repository;

/**
 * Class Project.
 * Project tag repository.
 *
 * @package Balance\AwBlogCustomTags\Model\Tag\Repository
 */
class Project extends \Balance\AwBlogCustomTags\Model\TagRepository
{
    /**
     * Project constructor.
     *
     * @param \Magento\Framework\EntityManager\EntityManager                    $entityManager    Entity manager.
     * @param \Magento\Framework\Api\DataObjectHelper                           $dataObjectHelper Data object helper.
     * @param \Balance\AwBlogCustomTags\Model\Tag\ProjectFactory        $tagFactory       Tag factory.
     * @param \Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project $tagResourceModel Resource model.
     */
    function __construct(
        \Magento\Framework\EntityManager\EntityManager $entityManager,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Balance\AwBlogCustomTags\Model\Tag\ProjectFactory $tagFactory,
        \Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project $tagResourceModel
    )
    {
        $this->_tagFactory = $tagFactory;
        $this->_tagResourceModel = $tagResourceModel;

        parent::__construct($entityManager, $dataObjectHelper);
    }
}