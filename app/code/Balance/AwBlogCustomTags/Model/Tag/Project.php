<?php
namespace Balance\AwBlogCustomTags\Model\Tag;

use Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project as ProjectTagResourceModel;

/**
 * Class Project.
 * Project tag model.
 *
 * @package Balance\AwBlogCustomTags\Model\Tag
 */
class Project
    extends \Balance\AwBlogCustomTags\Model\Tag
    implements \Balance\AwBlogCustomTags\Api\Data\ProjectTagInterface
{
    /**
     * @const string Resource model class name.
     */
    const RESOURCE_MODEL = ProjectTagResourceModel::class;
    /**
     * @const string Project tags name.
     */
    const CUSTOM_TAGS_NAME = 'project_tags';
}