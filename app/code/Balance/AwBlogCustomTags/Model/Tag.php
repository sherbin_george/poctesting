<?php
namespace Balance\AwBlogCustomTags\Model;

/**
 * Class Tag.
 * Abstract custom tag model.
 *
 * @package Balance\AwBlogCustomTags\Model
 */
abstract class Tag
    extends \Magento\Framework\Model\AbstractModel
    implements \Balance\AwBlogCustomTags\Api\Data\TagInterface
{
    /**
     * @const string Resource model class name (must be overridden in child classes).
     */
    const RESOURCE_MODEL = '';
    /**
     * @const string Custom tags name.
     */
    const CUSTOM_TAGS_NAME = '';

    /**
     * Inner constructor.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function _construct()
    {
        if (empty(static::RESOURCE_MODEL)) {
            throw new \Exception('Custom tags resource model name is not set.');
        }

        if (empty(static::CUSTOM_TAGS_NAME)) {
            throw new \Exception('Custom tags name is not set.');
        }

        $this->_init(static::RESOURCE_MODEL);
    }

    /**
     * Returns tag ID.
     *
     * @return int
     */
    public function getId()
    {
        return (int) $this->getData(self::FIELD_ID);
    }

    /**
     * Sets tag ID.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setId($value)
    {
        $this->setData(self::FIELD_ID, $value);
    }

    /**
     * Returns tag name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::FIELD_NAME);
    }

    /**
     * Sets tag name.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setName($value)
    {
        $this->setData(self::FIELD_NAME, $value);
    }

    /**
     * Returns tag created at value.
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::FIELD_CREATED_AT);
    }

    /**
     * Sets tag created at value.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setCreatedAt($value)
    {
        $this->setData(self::FIELD_CREATED_AT, $value);
    }

    /**
     * Returns tag updated at value.
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::FIELD_UPDATED_AT);
    }

    /**
     * Sets tag updated at value.
     *
     * @param int $value Value.
     *
     * @return void
     */
    public function setUpdatedAt($value)
    {
        $this->setData(self::FIELD_UPDATED_AT, $value);
    }

    /**
     * Returns related post IDs.
     *
     * @return array
     */
    public function getPostIds()
    {
        if (!is_array($this->getData(self::FIELD_POST_IDS))) {
            // Initialize at first time.
            $this->setPostIds([]);
        }

        return $this->getData(self::FIELD_POST_IDS);
    }

    /**
     * Sets related post IDs.
     *
     * @param string|array $value Value (string: add a new one, array: replace previous list).
     *
     * @return void
     */
    public function setPostIds($value)
    {
        if (!is_array($value)) {
            $this->addPostId($value);
        }

        $this->setData(self::FIELD_POST_IDS, $value);
    }

    /**
     * Adds related post ID.
     *
     * @param string|array $value Value (string: add a new one, array: replace previous list).
     *
     * @return void
     */
    public function addPostId($value)
    {
        if (is_array($value)) {
            $this->setPostIds($value);
            return;
        }

        $postIds = $this->getPostIds();

        // Avoid IDs duplication.
        if (in_array($value, $postIds)) {
            return;
        }

        $postIds[] = $value;
        $this->setData(self::FIELD_POST_IDS, $postIds);
    }
}