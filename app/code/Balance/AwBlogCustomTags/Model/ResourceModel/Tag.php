<?php
namespace Balance\AwBlogCustomTags\Model\ResourceModel;

use Balance\AwBlogCustomTags\Api\Data\TagInterface;

/**
 * Class Tag.
 * Abstract custom tag resource model.
 *
 * @package Balance\AwBlogCustomTags\Model\ResourceModel
 */
abstract class Tag extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @const sting Related table name (must be overridden in child classes).
     */
    const TABLE_NAME = '';
    /**
     * @const sting Relation table name.
     */
    const RELATION_TABLE_NAME = '';
    /**
     * @const string Relation table tag ID column name.
     */
    const RELATION_TABLE_COLUMN_TAG_ID = 'tag_id';
    /**
     * @const string Relation table post ID column name.
     */
    const RELATION_TABLE_COLUMN_POST_ID = 'post_id';

    /**
     * Internal constructor.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function _construct()
    {
        if (empty(static::TABLE_NAME)) {
            throw new \Exception('Custom tags table name is not set.');
        }

        if (empty(static::RELATION_TABLE_NAME)) {
            throw new \Exception('Custom tags relation table name is not set.');
        }

        $this->_init(static::TABLE_NAME, TagInterface::FIELD_ID);
    }

    /**
     * Returns relation table name.
     *
     * @return string
     */
    public function getRelationTable()
    {
        return $this->getTable(static::RELATION_TABLE_NAME);
    }

    /**
     * Returns related post IDs for specified tag.
     *
     * @param int $tagId Tag ID.
     *
     * @return array
     */
    public function getRelatedPostIds($tagId)
    {
        $connection = $this->getConnection();
        /** @var \Magento\Framework\DB\Select $select */
        $select = $connection->select();
        $select->from($this->getRelationTable(), static::RELATION_TABLE_COLUMN_POST_ID);
        $select->where(
            sprintf('%s = ?', static::RELATION_TABLE_COLUMN_TAG_ID),
            (int) $tagId
        );

        return $connection->fetchCol($select);
    }

    /**
     * Adds a new 'tag-post' relation.
     *
     * @param int $tagId  Tag ID.
     * @param int $postId Post ID.
     *
     * @return void
     */
    public function saveRelation($tagId, $postId)
    {
        if ($this->isRelationExists($tagId, $postId)) {
            return;
        }

        $connection = $this->getConnection();
        $connection->insert(
            $this->getRelationTable(),
            [
                static::RELATION_TABLE_COLUMN_TAG_ID => (int) $tagId,
                static::RELATION_TABLE_COLUMN_POST_ID => (int) $postId
            ]
        );
    }

    /**
     * Determines whether 'tag-post' relation exists.
     *
     * @param int $tagId  Tag ID.
     * @param int $postId Post ID.
     *
     * @return bool
     */
    public function isRelationExists($tagId, $postId)
    {
        $connection = $this->getConnection();

        /** @var \Magento\Framework\DB\Select $select */
        $select = $connection->select();
        $select->from($this->getRelationTable());
        $select->where(
            sprintf('%s = ?', static::RELATION_TABLE_COLUMN_TAG_ID),
            (int) $tagId
        );
        $select->where(
            sprintf('%s = ?', static::RELATION_TABLE_COLUMN_POST_ID),
            (int) $postId
        );

        $result = $connection->fetchRow($select);

        return !empty($result);
    }

    /**
     * Deletes 'tag-post' relations.
     *
     * @param array  $tagIds               Tag IDs.
     * @param array  $postIds              Post IDs.
     * @param string $tagIdsConditionType  Tag IDs condition type (optional).
     * @param string $postIdsConditionType Post IDs condition type (optional).
     *
     * @return void
     */
    public function deleteRelations($tagIds, $postIds, $tagIdsConditionType = 'IN', $postIdsConditionType= 'IN')
    {
        $connection = $this->getConnection();

        /** @var \Magento\Framework\DB\Select $select */
        $select = $connection->select();
        $select->from($this->getRelationTable());

        if (!empty($tagIds)) {
            $select->where(
                sprintf('%s %s (?)', static::RELATION_TABLE_COLUMN_TAG_ID, $tagIdsConditionType),
                array_map('intval', (array) $tagIds)
            );
        }

        if (!empty($postIds)) {
            $select->where(
                sprintf('%s %s (?)', static::RELATION_TABLE_COLUMN_POST_ID, $postIdsConditionType),
                array_map('intval', (array) $postIds)
            );
        }

        $connection->query(
            $connection->deleteFromSelect($select, $this->getRelationTable())
        );
    }

    /**
     * Deletes tags that are not assigned to any post.
     *
     * @return void
     */
    public function cleanupTags()
    {
        $connection = $this->getConnection();

        /** @var \Magento\Framework\DB\Select $select */
        $select = $connection->select();
        $select->from($this->getMainTable(), TagInterface::FIELD_ID);
        $select->joinLeft(
            $this->getRelationTable(),
            sprintf(
                '%s.%s = %s.%s',
                $this->getMainTable(),
                TagInterface::FIELD_ID,
                $this->getRelationTable(),
                static::RELATION_TABLE_COLUMN_TAG_ID
            )
        );
        $select->where(
            sprintf('%s.%s IS NULL', $this->getRelationTable(), static::RELATION_TABLE_COLUMN_TAG_ID)
        );

        $connection->query(
            $connection->deleteFromSelect($select, $this->getMainTable())
        );
    }
}