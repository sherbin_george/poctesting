<?php
namespace Balance\AwBlogCustomTags\Model\ResourceModel\Tag;

/**
 * Class Project.
 * Project tag resource model.
 *
 * @package Balance\AwBlogCustomTags\Model\ResourceModel\Tag
 */
class Project extends \Balance\AwBlogCustomTags\Model\ResourceModel\Tag
{
    /**
     * @const sting Related table name.
     */
    const TABLE_NAME = 'aw_blog_tag_project';
    /**
     * @const sting Relation table name.
     */
    const RELATION_TABLE_NAME = 'aw_blog_post_tag_project';
}