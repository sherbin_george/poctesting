<?php
namespace Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project;

use Balance\AwBlogCustomTags\Model\Tag\Project as ProjectTagModel;
use Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project as ProjectTagResourceModel;

/**
 * Class Collection.
 * Project tag collection.
 *
 * @package Balance\AwBlogCustomTags\Model\ResourceModel\Tag\Project
 */
class Collection extends \Balance\AwBlogCustomTags\Model\ResourceModel\TagCollection
{
    /**
     * @const sting Model class name.
     */
    const MODEL = ProjectTagModel::class;
    /**
     * @const sting Resource model class name.
     */
    const RESOURCE_MODEL = ProjectTagResourceModel::class;
}