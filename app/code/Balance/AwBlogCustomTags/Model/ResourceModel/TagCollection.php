<?php
namespace Balance\AwBlogCustomTags\Model\ResourceModel;

use Balance\AwBlogCustomTags\Api\Data\TagInterface;
use Balance\AwBlogCustomTags\Model\ResourceModel\Tag as AbstractTagResourceModel;

/**
 * Class TagCollection.
 * Abstract custom tag collection.
 *
 * @package Balance\AwBlogCustomTags\Model\ResourceModel
 */
abstract class TagCollection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @const sting Model class name (must be overridden in child classes).
     */
    const MODEL = '';
    /**
     * @const sting Resource model class name (must be overridden in child classes).
     */
    const RESOURCE_MODEL = '';

    /**
     * Internal constructor.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function _construct()
    {
        if (empty(static::MODEL)) {
            throw new \Exception('Custom tags model name is not set.');
        }

        if (empty(static::RESOURCE_MODEL)) {
            throw new \Exception('Custom tags resource model name is not set.');
        }

        $this->_init(static::MODEL, static::RESOURCE_MODEL);
    }

    /**
     * Joins relation table to a collection.
     *
     * @return void
     */
    public function joinRelationTable()
    {
        /** @var \Balance\AwBlogCustomTags\Model\ResourceModel\Tag $resourceModel */
        $resourceModel = $this->getResource();

        $this->join(
            $resourceModel->getRelationTable(),
            sprintf(
                '%s.%s = %s.%s',
                'main_table',
                TagInterface::FIELD_ID,
                $resourceModel->getRelationTable(),
                AbstractTagResourceModel::RELATION_TABLE_COLUMN_TAG_ID
            )
        );
    }

    /**
     * Adds post IDs filter to a collection.
     *
     * @param \Magento\Framework\Api\Filter $filter Filter instance.
     *
     * @return void
     */
    public function addPostIdsFilter(\Magento\Framework\Api\Filter $filter)
    {
        $filterValue = $filter->getValue();
        if (!is_array($filterValue)) {
            $filterValue = [$filterValue];
        }

        /** @var \Balance\AwBlogCustomTags\Model\ResourceModel\Tag $resourceModel */
        $resourceModel = $this->getResource();

        $this->addFieldToFilter(
            sprintf(
                '%s.%s',
                $resourceModel->getRelationTable(),
                AbstractTagResourceModel::RELATION_TABLE_COLUMN_POST_ID
            ),
            array('IN' => $filterValue)
        );
    }

    /**
     * Adds tag names filter to a collection.
     *
     * @param \Magento\Framework\Api\Filter $filter Filter instance.
     *
     * @return void
     */
    public function addTagNamesFilter(\Magento\Framework\Api\Filter $filter)
    {
        $filterValue = $filter->getValue();
        if (!is_array($filterValue)) {
            $filterValue = [$filterValue];
        }

        $this->addFieldToFilter(
            sprintf(
                '%s.%s',
                'main_table',
                \Balance\AwBlogCustomTags\Api\Data\TagInterface::FIELD_NAME
            ),
            array('IN' => $filterValue)
        );
    }
}