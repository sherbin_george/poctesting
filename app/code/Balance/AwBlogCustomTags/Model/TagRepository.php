<?php
namespace Balance\AwBlogCustomTags\Model;

use Balance\AwBlogCustomTags\Api\Data\TagInterface;
use Balance\AwBlogCustomTags\Model\ResourceModel\Tag as AbstractTagResourceModel;
use Balance\AwBlogCustomTags\Model\ResourceModel\TagCollection as AbstractTagCollection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Phrase;

/**
 * Class TagRepository.
 * Abstract tag repository.
 *
 * @package Balance\AwBlogCustomTags\Model
 */
abstract class TagRepository implements \Balance\AwBlogCustomTags\Api\TagRepositoryInterface
{
    /**
     * @var \Magento\Framework\EntityManager\EntityManager $_entityManager Entity manager.
     */
    protected $_entityManager;
    /**
     * @var \Magento\Framework\Api\DataObjectHelper $_dataObjectManager Data object manager.
     */
    protected $_dataObjectManager;
    /**
     * @var \Balance\AwBlogCustomTags\Api\Data\TagFactory $_tagFactory Tag factory.
     */
    protected $_tagFactory;
    /**
     * @var \Balance\AwBlogCustomTags\Model\ResourceModel\Tag Tag resource model.
     */
    protected $_tagResourceModel;

    /**
     * TagRepository constructor.
     *
     * @param \Magento\Framework\EntityManager\EntityManager $entityManager    Entity manager.
     * @param \Magento\Framework\Api\DataObjectHelper        $dataObjectHelper Data object helper.
     */
    function __construct(
        \Magento\Framework\EntityManager\EntityManager $entityManager,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
    )
    {
        $this->_entityManager = $entityManager;
        $this->_dataObjectManager = $dataObjectHelper;

        $this->_validateTagCollection();
    }

    /**
     * Validated tag collection by type.
     *
     * @return void
     *
     * @throws ValidatorException
     */
    protected function _validateTagCollection()
    {
        if (!$this->_getTagCollection() instanceof AbstractTagCollection) {
            throw new ValidatorException(
                new Phrase('Tag collection does not match required type')
            );
        }
    }

    /**
     * Returns tag instance by ID.
     *
     * @param int $tagId Tag ID.
     *
     * @return \Balance\AwBlogCustomTags\Api\Data\TagInterface
     */
    public function getById($tagId)
    {
        /** @var  $tag \Balance\AwBlogCustomTags\Api\Data\TagInterface */
        $tag = $this->_tagFactory->create();
        $this->_entityManager->load($tag, $tagId);
        $relatedPostIds = $this->_tagResourceModel->getRelatedPostIds($tag->getId());
        $tag->addPostId($relatedPostIds);

        return $tag;
    }

    /**
     * Returns tag list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return array
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $tagItems = [];

        try {
            /** @var AbstractTagCollection $tagCollection */
            $tagCollection = $this->_getTagCollection();
            $tagCollection->joinRelationTable();
            $tagCollection->setCurPage($searchCriteria->getCurrentPage());
            $tagCollection->setPageSize($searchCriteria->getPageSize());

            $tagsData = $tagCollection->getConnection()->fetchAll(
                $tagCollection->getSelect()
            );

            $tagItems = $this->_getPopulatedTagItems($tagsData);
        } finally {
            return $tagItems;
        }
    }

    /**
     * Returns tag list by specified post IDs.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return array
     */
    public function getListByPostIds(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $tagItems = [];

        try {
            /** @var \Magento\Framework\Api\Filter $postIdsFilter */
            $postIdsFilter = $this->_extractFilter($searchCriteria, 'post_ids');

            /** @var AbstractTagCollection $tagCollection */
            $tagCollection = $this->_getTagCollection();
            $tagCollection->joinRelationTable();
            $tagCollection->addPostIdsFilter($postIdsFilter);
            $tagCollection->setCurPage($searchCriteria->getCurrentPage());
            $tagCollection->setPageSize($searchCriteria->getPageSize());

            $tagsData = $tagCollection->getConnection()->fetchAll(
                $tagCollection->getSelect()
            );

            $tagItems = $this->_getPopulatedTagItems($tagsData);
        } finally {
            return $tagItems;
        }
    }

    /**
     * Returns tag list by specified tag names.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return array
     */
    public function getListByTagNames(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $tagItems = [];

        try {
            /** @var \Magento\Framework\Api\Filter $tagNamesFilter */
            $tagNamesFilter = $this->_extractFilter($searchCriteria, 'tag_names');

            /** @var AbstractTagCollection $tagCollection */
            $tagCollection = $this->_getTagCollection();
            $tagCollection->joinRelationTable();
            $tagCollection->addTagNamesFilter($tagNamesFilter);
            $tagCollection->setCurPage($searchCriteria->getCurrentPage());
            $tagCollection->setPageSize($searchCriteria->getPageSize());

            $tagsData = $tagCollection->getConnection()->fetchAll(
                $tagCollection->getSelect()
            );

            $tagItems = $this->_getPopulatedTagItems($tagsData);
        } finally {
            return $tagItems;
        }
    }

    /**
     * Saves tag instance.
     *
     * @param TagInterface $tag Tag instance.
     *
     * @return void
     *
     * @throws NoSuchEntityException
     */
    public function save(TagInterface $tag)
    {
        $this->_validateTag($tag);
        if (is_null($this->_tagResourceModel)) {
            throw new NoSuchEntityException(
                new Phrase('Tag resource model is not initialized.')
            );
        }

        $this->_entityManager->save($tag);
        foreach ($tag->getPostIds() as $postId) {
            $this->_tagResourceModel->saveRelation($tag->getId(), $postId);
        }
    }

    /**
     * Deletes tags by specified conditions.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return void
     *
     * @throws LocalizedException
     */
    public function deleteList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        if (is_null($this->_tagResourceModel)) {
            throw new NoSuchEntityException(
                new Phrase('Tag resource model is not initialized.')
            );
        }

        try {
            /** @var \Magento\Framework\Api\Filter $postIdsFilter */
            $postIdsFilter = $this->_extractFilter($searchCriteria, 'post_ids');
            $postIds = explode(',', $postIdsFilter->getValue());
            $postIdsConditionType = $postIdsFilter->getConditionType();
        }
        catch (NotFoundException $exception) {
            // One of the filters may be optional.
            $postIds = [];
            $postIdsConditionType = '';
        }

        try {
            /** @var \Magento\Framework\Api\Filter $tagIdsFilter */
            $tagIdsFilter = $this->_extractFilter($searchCriteria, 'tag_ids');
            $tagIds = explode(',', $tagIdsFilter->getValue());
            $tagIdsConditionType = $tagIdsFilter->getConditionType();
        }
        catch (NotFoundException $exception) {
            // One of the filters may be optional.
            $tagIds = [];
            $tagIdsConditionType = '';
        }

        if (empty($postIds)
            && empty($tagIds)
        ) {
            throw new NotFoundException(
                new Phrase('No filter is set.')
            );
        }

        // Delete relations.
        $this->_tagResourceModel->deleteRelations($tagIds, $postIds, $tagIdsConditionType, $postIdsConditionType);
        // Delete tags if they are not assigned to any post.
        $this->_tagResourceModel->cleanupTags();
    }

    /**
     * Performs some basic validation on tag instance.
     *
     * @param TagInterface $tag Tag instance.
     *
     * @return void
     *
     * @throws ValidatorException
     */
    protected function _validateTag(TagInterface $tag)
    {
        $tagName = $tag->getName();
        $tagPostIds = $tag->getPostIds();

        if (empty($tagName)
            || empty($tagPostIds)
        ) {
            throw new ValidatorException(
                new Phrase('Tag data is not valid.')
            );
        }
    }

    /**
     * Extracts filter by name.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     * @param string                                         $filterName     Filter name.
     *
     * @return \Magento\Framework\Api\Filter
     *
     * @throws NotFoundException
     */
    protected function _extractFilter(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria, $filterName)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() == $filterName) {
                    return $filter;
                }
            }
        }

        throw new NotFoundException(
            new Phrase('Filter not found.')
        );
    }

    /**
     * Returns tag collection.
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     *
     * @throws NoSuchEntityException
     */
    protected function _getTagCollection()
    {
        if (is_null($this->_tagFactory)) {
            throw new NoSuchEntityException(
                new Phrase('Tag factory is not initialized.')
            );
        }

        /** @var \Balance\AwBlogCustomTags\Model\Tag $tagModel */
        $tagModel = $this->_tagFactory->create();
        return $tagModel->getCollection();
    }

    /**
     * Returns populated tag items.
     *
     * @param array $tagsData Tag items data.
     *
     * @return array
     */
    protected function _getPopulatedTagItems($tagsData)
    {
        $tagItems = [];

        foreach ($tagsData as $tagData) {
            if (!isset($tagData[TagInterface::FIELD_ID])) {
                continue;
            }

            $tagId = $tagData[TagInterface::FIELD_ID];
            $populatedTagItem = isset($tagItems[$tagId])
                ? $tagItems[$tagId]
                : $this->_getPopulatedTagItem($tagData);

            if (isset($tagData[AbstractTagResourceModel::RELATION_TABLE_COLUMN_POST_ID])) {
                $postId = $tagData[AbstractTagResourceModel::RELATION_TABLE_COLUMN_POST_ID];
                $populatedTagItem->addPostId($postId);
            }

            $tagItems[$tagId] = $populatedTagItem;
        }

        return $tagItems;
    }

    /**
     * Returns populated tag item.
     *
     * @param array $tagData Tag data.
     *
     * @return TagInterface
     */
    protected function _getPopulatedTagItem($tagData)
    {
        $tagItem = $this->_tagFactory->create();
        $this->_dataObjectManager->populateWithArray($tagItem, $tagData, TagInterface::class);

        return $tagItem;
    }
}