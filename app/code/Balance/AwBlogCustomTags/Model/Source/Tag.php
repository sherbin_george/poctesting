<?php
namespace Balance\AwBlogCustomTags\Model\Source;

/**
 * Class Tag.
 * Abstract custom tag source model.
 *
 * @package Balance\AwBlogCustomTags\Model\Source
 */
abstract class Tag implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Balance\AwBlogCustomTags\Api\TagRepositoryInterface $_tagRepository Tag repository.
     */
    protected $_tagRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder $_searchCriteriaBuilder Search criteria builder.
     */
    protected $_searchCriteriaBuilder;
    /**
     * @var \Balance\AwBlogCustomTags\Helper\Data $_helper Base helper.
     */
    protected $_helper;
    /**
     * @var array $_options Cached options.
     */
    private $_options;

    /**
     * Tag constructor.
     *
     * @param \Balance\AwBlogCustomTags\Api\TagRepositoryInterface $tagRepository         Tag repository.
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                 $searchCriteriaBuilder Search criteria
     *                                                                                            builder.
     * @param \Balance\AwBlogCustomTags\Helper\Data                $helper                base helper.
     */
    public function __construct(
        \Balance\AwBlogCustomTags\Api\TagRepositoryInterface $tagRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Balance\AwBlogCustomTags\Helper\Data $helper
    )
    {
        $this->_tagRepository = $tagRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_helper = $helper;
    }

    /**
     * Returns tag list as option array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (is_null($this->_options)) {
            $tagItems = $this->_tagRepository->getList(
                $this->_searchCriteriaBuilder->create()
            );

            $this->_options = $this->_helper->getTagsAsOptionArray($tagItems);
        }

        return $this->_options;
    }
}