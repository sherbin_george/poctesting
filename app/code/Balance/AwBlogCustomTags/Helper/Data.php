<?php
namespace Balance\AwBlogCustomTags\Helper;

/**
 * Class Data.
 * Base helper class.
 *
 * @package Balance\AwBlogCustomTags\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Populate post items with tag names.
     *
     * @param array  $postItems      Post instance list.
     * @param array  $tagItems       Tag instance list.
     * @param string $customTagsName Custom tags name.
     *
     * @return void
     */
    public function populatePostsWithTagNames(array $postItems, array $tagItems, $customTagsName)
    {
        /** @var \Aheadworks\Blog\Model\Post $postItem */
        foreach ($postItems as $postItem) {
            $postItem->setData(
                $customTagsName,
                $this->extractTagNamesForPost($postItem->getId(), $tagItems)
            );
        }
    }

    /**
     * Extracts tags for specified post.
     *
     * @param int   $postId   Post ID.
     * @param array $tagItems Tag item list.
     *
     * @return array
     */
    public function extractTagNamesForPost($postId, array $tagItems)
    {
        $tagNames = [];
        /** @var \Balance\AwBlogCustomTags\Api\Data\TagInterface $tagItem */
        foreach ($tagItems as $tagItem) {
            if (in_array($postId, $tagItem->getPostIds())) {
                $tagNames[] = $tagItem->getName();
            }
        }

        return $tagNames;
    }

    /**
     * Returns tag items as option array.
     *
     * @param array $tagItems Tag item list.
     *
     * @return array
     */
    public function getTagsAsOptionArray(array $tagItems)
    {
        $options = [];

        /** @var \Balance\AwBlogCustomTags\Api\Data\TagInterface $tagItem */
        foreach ($tagItems as $tagItem) {
            $options[] = [
                'value' => $tagItem->getId(),
                'label' => $tagItem->getName()
            ];
        }

        return $options;
    }
}