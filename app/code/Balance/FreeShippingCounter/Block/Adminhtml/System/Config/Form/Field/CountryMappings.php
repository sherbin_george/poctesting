<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\FreeShippingCounter\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Directory\Model\Config\Source\Country;
use Magento\Framework\Data\Form\Element\Factory;

/**
 * Class CountryMappings
 * @package Balance\FreeShippingCounter\Block\Adminhtml\System\Config\Form\Field
 */
class CountryMappings extends AbstractFieldArray
{
    /**
     * @var bool
     */
    protected $_addAfter = false;
    /**
     * @var Country
     */
    private $sourceCountry;
    /**
     * @var Factory
     */
    private $elementFactory;

    /**
     * CountryMappings constructor.
     * @param Country $countrySource
     * @param Factory $elementFactory
     * @param Context $context
     * @param array $data
     */
    public function __construct(Country $countrySource, Factory $elementFactory, Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->sourceCountry = $countrySource;
        $this->elementFactory = $elementFactory;
    }

    /**
     * Overwrite the internal constructor.
     *
     * Add the columns we need
     */
    protected function _construct()
    {
        $this->addColumn(
            'country_code',
            ['label' => __('Country')]
        );

        $this->addColumn(
            'amount',
            ['label' => __('Free Shipping Amount ($)'), 'class' => 'validate-number']
        );

        $this->_addButtonLabel = __('Add Mapping');

        parent::_construct();
    }

    /**
     * Render array cell for prototypeJS template
     *
     * @param string $columnName
     * @return string
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName == 'country_code' && isset($this->_columns[$columnName])) {
            /** @var $label \Magento\Framework\View\Design\Theme\Label */

            $options = $this->sourceCountry->toOptionArray(false);
            $element = $this->elementFactory->create('select');
            $element->setForm(
                $this->getForm()
            )->setName(
                $this->_getCellInputElementName($columnName)
            )->setHtmlId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setValues(
                $options
            );
            return str_replace("\n", '', $element->getElementHtml());
        }

        return parent::renderCellTemplate($columnName);
    }
}
