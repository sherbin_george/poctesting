<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\FreeShippingCounter\Block;

use Balance\FreeShippingCounter\Model\ShippingCounter as ShippingCounterModel;
use Magento\Framework\View\Element\Template;

/**
 * Class ShippingCounter
 * @package Balance\FreeShippingCounter\Block
 */
class ShippingCounter extends Template
{
    /**
     * @var \Balance\FreeShippingCounter\Model\ShippingCounter
     */
    private $shippingCounter;

    /**
     * ShippingCounter constructor.
     *
     * @param Template\Context      $context
     * @param ShippingCounterModel  $shippingCounter
     * @param array                 $data
     */
    public function __construct(
        Template\Context $context,
        ShippingCounterModel $shippingCounter,
        array $data
    ) {
        parent::__construct($context, $data);
        $this->shippingCounter = $shippingCounter;
    }

    /**
     * @return string
     */
    public function getAjaxRenderUrl()
    {
        return $this->getUrl('shipping_counter/ajax/render', ['isAjax' => true]);
    }

    /**
     * @return float|string
     */
    public function getRemainingCurrencyAmount()
    {
        return $this->shippingCounter->getRemainingCurrencyAmount();
    }

    /**
     * @return bool
     */
    public function canShow()
    {
        return $this->shippingCounter->getHelper()->isEnabled()
            && $this->shippingCounter->isNotEmptyQuote();
    }

    /**
     * @return bool
     */
    public function isFreeShipping()
    {
        return $this->shippingCounter->isFreeShipping();
    }

    /**
     * @return int|float
     */
    public function getMinimumTotal()
    {
        return $this->shippingCounter->getMinimumTotal();
    }

    /**
     * Get current currency rate
     *
     * @return float
     */
    public function getCurrentCurrencyRate()
    {
        return $this->shippingCounter->getCurrentCurrencyRate();
    }

    /**
     * Get current store currency code
     *
     * @return string
     */
    public function getCurrentCurrencyCode()
    {
        return $this->shippingCounter->getCurrentCurrencyCode();
    }

    /**
     * Get currency symbol for current locale and currency code
     *
     * @return string
     */
    public function getCurrentCurrencySymbol()
    {
        return $this->shippingCounter->getCurrentCurrencySymbol();
    }
}
