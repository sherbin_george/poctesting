/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        '../../model/shipping-rates-validator/freeshippingcounter',
        '../../model/shipping-rates-validation-rules/freeshippingcounter'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        freeshippingcounterShippingRatesValidator,
        freeshippingcounterShippingRatesValidationRules
    ) {
        "use strict";
        defaultShippingRatesValidator.registerValidator('freeshippingcounter', freeshippingcounterShippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('freeshippingcounter', freeshippingcounterShippingRatesValidationRules);
        return Component;
    }
);
