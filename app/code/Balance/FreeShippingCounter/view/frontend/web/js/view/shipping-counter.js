/*
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'jquery',
    'ko',
    'underscore',
    'sidebar'
], function (Component, customerData, $, ko, _) {
    'use strict';

    return Component.extend({
        isDebug: false,
        isInitialized: false,
        elements: {
            'wrapper': '.free-shipping-countdown',
            'content': '.shipping-counter > div'
        },
        shippingCounter: {},

        /**
         * @override
         */
        initialize: function () {
            var self = this,
                shippingCounterData = customerData.get('shipping-counter');

            this.update(shippingCounterData());
            shippingCounterData.subscribe(function (updatedContent) {
                this.isLoading(false);
                this.update(updatedContent);
                this.isInitialized = false;
                this.initSidebar();
            }, this);

            $('[data-block="shipping-counter"]').on('contentLoading', function (event) {
                self.isLoading(true);
            });

            if (shippingCounterData().website_id !== window.checkout.websiteId) {
                customerData.reload(['shipping-counter'], false);
            }

            return this._super();
        },
        isLoading: ko.observable(false),
        getFreeShippingHtml: function(){
            if(this.isFreeShipping() === false && this.getData('freeShippingHtml') === ''){
                return this.getData('remainingAmountHtml');
            }
            else {
                return this.getData('freeShippingHtml');
            }
        },
        /**
         * Init sidebar
         *
         * @return {Boolean}
         */
        initSidebar: function () {
            var self = this;

            $('[data-block="shipping-counter"]').trigger('contentUpdated');

            if (this.isInitialized) {
                return false;
            }
            this.isInitialized = true;

            if (this.getData('isFreeShipping') === false && this.getData('freeShippingHtml') === '') {
                $(self.elements.wrapper).find(self.elements.content).html(self.getData('remainingAmountHtml'));
            } else {
                $(self.elements.wrapper).find(self.elements.content).html(self.getData('freeShippingHtml'));
            }

            if (this.isDebug === true) {
                console.trace();
            }
        },

        /**
         * Update free shipping counter content.
         *
         * @param {Object} updatedContent
         * @returns void
         */
        update: function (updatedContent) {
            _.each(updatedContent, function (value, key) {
                if (!this.shippingCounter.hasOwnProperty(key)) {
                    this.shippingCounter[key] = ko.observable();
                }
                this.shippingCounter[key](value);
            }, this);
        },

        /**
         * Get shipping counter param by name.
         * @param {String} name
         * @returns {*}
         */
        getData: function (name) {
            if (!_.isUndefined(name)) {
                if (!this.shippingCounter.hasOwnProperty(name)) {
                    this.shippingCounter[name] = ko.observable();
                }
            }

            return this.shippingCounter[name]();
        },

        /**
         * Returns free shipping state
         *
         * @returns {Boolean}
         */
        isFreeShipping: function () {
            return this.getData('isFreeShipping');
        }
    });
});
