<?php

namespace Balance\FreeShippingCounter\Model\Adminhtml\System\Config;

use Magento\Framework\App\Config\Value;

/**
 * Class CountryMappings
 * @package Balance\FreeShippingCounter\Model\Adminhtml\System\Config
 */
class CountryMappings extends Value
{
    /**
     * CountryMappings constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param null $resource
     * @param null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        $resource = null,
        $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }


}
