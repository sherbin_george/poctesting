<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\FreeShippingCounter\Model;

use Magento\Checkout\Model\Session;
use Magento\Shipping\Model\Shipping;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Balance\FreeShippingCounter\Helper\Data as ShippingCounterHelper;

/**
 * Class ShippingCounter
 *
 * @package Balance\FreeShippingCounter\Model
 * @author  Toan Nguyen <me@nntoan.com>
 */
class ShippingCounter
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $currency;

    /**
     * @var \Balance\FreeShippingCounter\Helper\Data
     */
    private $shippingCounterHelper;


    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;
    /**
     * ShippingCounter constructor.
     *
     * @param Session                $checkoutSession
     * @param PricingHelper          $priceHelper
     * @param ShippingCounterHelper  $shippingCounterHelper
     * @param PriceCurrencyInterface $currency
     */
    public function __construct(
        Session $checkoutSession,
        PricingHelper $priceHelper,
        ShippingCounterHelper $shippingCounterHelper,
        PriceCurrencyInterface $currency,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManagerInterface
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->priceHelper = $priceHelper;
        $this->shippingCounterHelper = $shippingCounterHelper;
        $this->currency = $currency;
        $this->_cookieManager = $cookieManagerInterface;
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }

    /**
     * @return ShippingCounterHelper
     */
    public function getHelper()
    {
        return $this->shippingCounterHelper;
    }

    /**
     * @return bool
     */
    public function isNotEmptyQuote()
    {
        return $this->getQuote()->getSubtotal() > 0 && $this->getMinimumTotal() !== false;
    }

    /**
     * @return bool
     */
    public function isFreeShipping()
    {
        return ($this->getRemainingAmount() <= 0) ? true : false;
    }

    /**
     * @return float
     */
    public function getRemainingAmount()
    {

        $minimumOrder = $this->getMinimumTotal();
        $address = $this->getQuote()->getShippingAddress();
        $subTotalInclTax = $address->getSubtotalInclTax();
        //$discount = $address->getDiscountAmount();
        //$rewards = $this->getQuote()->getBaseRewardCurrencyAmount();

        //$subtotal = $subTotalInclTax + $discount - $rewards;

        //if($subtotal > 0){
        //    return max(0, $minimumOrder - $subtotal);
        //}
        //else{
        //    return max(0, $minimumOrder - 0);
        //}
        return max(0, $minimumOrder - $subTotalInclTax);

    }

    /**
     * @return float|string
     */
    public function getRemainingCurrencyAmount()
    {
        return $this->priceHelper->currency($this->getRemainingAmount());
    }

    /**
     * @return int|float
     */
    public function getMinimumTotal()
    {
        $address = $this->getQuote()->getShippingAddress();

        if($address->getCountryId() == ''){
            $countryCode = $this->_cookieManager->getCookie('country_code', false);
        }
        else{
            $countryCode = $address->getCountryId();
        }

        return $this->shippingCounterHelper->getMinimumTotal(
            $countryCode,
            $this->shippingCounterHelper->getCurrentStore()
        );
    }

    /**
     * @return int|null|string
     */
    public function getWebsiteId()
    {
        return $this->getQuote()->getStore()->getWebsiteId();
    }

    /**
     * Get current currency rate
     *
     * @return float
     */
    public function getCurrentCurrencyRate()
    {
        return $this->shippingCounterHelper->getCurrentStore()->getCurrentCurrencyRate();
    }

    /**
     * Get current store currency code
     *
     * @return string
     */
    public function getCurrentCurrencyCode()
    {
        return $this->shippingCounterHelper->getCurrentStore()->getCurrentCurrencyCode();
    }

    /**
     * Get currency symbol for current locale and currency code
     *
     * @return string
     */
    public function getCurrentCurrencySymbol()
    {
        return $this->currency->getCurrencySymbol(null, $this->currency->getCurrency());
    }
}
