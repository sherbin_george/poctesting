<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\FreeShippingCounter\CustomerData;

use Magento\Checkout\CustomerData\ItemPoolInterface;
use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Framework\App\ObjectManager;

/**
 * Class FreeShippingCounter
 *
 * @package Balance\FreeShippingCounter\CustomerData
 * @author  Toan Nguyen <me@nntoan.com>
 */
class ShippingCounter extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * @var \Balance\FreeShippingCounter\Model\ShippingCounter|null
     */
    protected $shippingCounter = null;

    /**
     * @var ItemPoolInterface
     */
    protected $itemPoolInterface;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $layout;

    /**
     * @param \Magento\Checkout\Model\Session         $checkoutSession
     * @param \Magento\Checkout\Helper\Data           $checkoutHelper
     * @param ItemPoolInterface                       $itemPoolInterface
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param array                                   $data
     *
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        ItemPoolInterface $itemPoolInterface,
        \Magento\Framework\View\LayoutInterface $layout,
        array $data = []
    ) {
        parent::__construct($data);
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
        $this->itemPoolInterface = $itemPoolInterface;
        $this->layout = $layout;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $shippingCounter = $this->getShippingCounter();
        return [
            'isFreeShipping' => $shippingCounter->isFreeShipping(),
            'freeShippingHtml' => ($shippingCounter->isFreeShipping())
                ? __('You qualify for <strong>FREE Shipping</strong>')
                : '',
            'currencyRate' => $shippingCounter->getCurrentCurrencyRate(),
            'currencyCode' => $shippingCounter->getCurrentCurrencyCode(),
            'currencySymbol' => $shippingCounter->getCurrentCurrencySymbol(),
            'miniTotal' => $shippingCounter->getMinimumTotal(),
            'remainingAmountHtml' => ($shippingCounter->isFreeShipping() === false)
                ? __('You are %2%1%3 away from %2Free Delivery%3', $shippingCounter->getRemainingCurrencyAmount(), '<strong>', '</strong>')
                : '',
            'remainingAmount' => $shippingCounter->getRemainingCurrencyAmount(),
            'website_id' => $shippingCounter->getWebsiteId()
        ];
    }

    /**
     * @return \Balance\FreeShippingCounter\Model\ShippingCounter|null
     */
    private function getShippingCounter()
    {
        if (null === $this->shippingCounter) {
            $this->shippingCounter = ObjectManager::getInstance()->get(\Balance\FreeShippingCounter\Model\ShippingCounter::class);
        }
        return $this->shippingCounter;
    }
}
