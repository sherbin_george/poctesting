<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\FreeShippingCounter\Helper;

use Balance\Core\Helper\Data as BalanceHelper;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\Store;

/**
 * Class Data
 * @package Balance\FreeShippingCounter\Helper
 */
class Data extends BalanceHelper
{
    /**
     * XML Path to country mapping
     */
    const XML_PATH_ENABLED = 'shipping/free_shipping_counter/active';
    /**
     * XML Path to country mapping
     */
    const XML_PATH_COUNTRY_MAPPING = 'shipping/free_shipping_counter/country_mapping';
    /**
     * XML Path to default threshold amount
     */
    const XML_PATH_DEFAULT_COUNTRY = 'shipping/free_shipping_counter/default_country';
    /**
     * XML Path to default threshold amount
     */
    const XML_PATH_DEFAULT_AMOUNT = 'shipping/free_shipping_counter/default_threshold';
    /**
     * @var array
     */
    private $countryMappings = [];
    /**
     * @var null|float
     */
    private $defaultCountryAmount;
    /**
     * @var null|string
     */
    private $defaultCountry;

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)$this->getConfig(
            self::XML_PATH_ENABLED
        );
    }

    /**
     * @param string|null          $country
     * @param StoreInterface|Store $store
     *
     * @return bool|float|mixed|null
     */
    public function getMinimumTotal($country, StoreInterface $store)
    {
        $this->initConfigurations();

        // If there is no country assigned to the quote, use the default country
        if ($country === null || mb_strlen((string)$country) === 0) {
            $country = $this->defaultCountry;
        }

        $amount = $this->defaultCountryAmount;
        // Use a mapped amount if the country exists and there is a mapping defined
        if ($country !== null && array_key_exists($country, $this->countryMappings)) {
            $amount = $this->countryMappings[$country];
        }

        if ($amount === null) {
            return false;
        }

        return $amount * $store->getCurrentCurrencyRate();
    }

    /**
     * Initialize the configurations needed.
     */
    private function initConfigurations()
    {
        $this->initCountryMappings();
        $this->initDefaultCountryAmount();
        $this->defaultCountry = $this->getConfig(
            self::XML_PATH_DEFAULT_COUNTRY
        );
    }

    /**
     * Initialize the default country amount
     */
    private function initDefaultCountryAmount()
    {
        $defaultAmount = $this->getConfig(
            self::XML_PATH_DEFAULT_AMOUNT
        );

        if (is_numeric($defaultAmount)) {
            $this->defaultCountryAmount = floatval($defaultAmount);
        } else {
            $this->defaultCountryAmount = null;
        }
    }

    /**
     * Retrieve the country mapping from configuration.
     */
    private function initCountryMappings()
    {
        $mappings = $this->getConfig(
            self::XML_PATH_COUNTRY_MAPPING
        );

        // This should be taken care of by the attribute backend, but isn't
        if (is_string($mappings)) {
            $mappings = \unserialize($mappings);
        }

        // Deal with a null object
        if (!is_array($mappings)) {
            $mappings = [];
        }

        foreach ($mappings as $mapping) {
            if (!array_key_exists('country_code', $mapping)) {
                continue;
            }
            if (!array_key_exists('amount', $mapping)) {
                continue;
            }
            $this->countryMappings[$mapping['country_code']] = floatval($mapping['amount']);
        }
    }
}
