<?php

namespace Balance\CustomHeader\Block\Account;

class AuthorizationLink extends \Magento\Customer\Block\Account\AuthorizationLink
{
    /**
     * @var \Magento\Customer\Model\Session
     */

    protected $session;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Customer\Model\Session $session
     * @param array $data
     */

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Customer\Model\Session $session,
        array $data = []
    ) {
        $this->session = $session;

        parent::__construct($context, $httpContext, $customerUrl, $postDataHelper);
    }

    /**
     * @return string
     */

    public function getCustomerFirstName()
    {
        return $this->session->getCustomer()->getFirstname();
    }
}