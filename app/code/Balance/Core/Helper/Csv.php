<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use \Exception;

/**
 * Class Csv
 *
 * @package Balance\Core\Helper
 * @author  Toan Nguyen <me@nntoan.com>
 */
class Csv extends AbstractHelper
{
    /**
     * @var int
     */
    protected $lineLength;
    /**
     * @var string
     */
    protected $delimiter;
    /**
     * @var string
     */
    protected $enclosure;

    /**
     * Csv constructor.
     *
     * @param Context $context Magento context
     */
    public function __construct(
        Context $context
    ) {
        $this->lineLength = 0;
        $this->delimiter = ',';
        $this->enclosure = '"';

        parent::__construct($context);
    }

    /**
     * Set max file line length
     *
     * @param   int $length Line length
     *
     * @return  \Balance\Core\Helper\Csv
     */
    public function setLineLength($length)
    {
        $this->lineLength = $length;
        return $this;
    }

    /**
     * Set CSV column delimiter
     *
     * @param   string $delimiter Column delimiter
     *
     * @return  \Balance\Core\Helper\Csv
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
        return $this;
    }

    /**
     * Set CSV column value enclosure
     *
     * @param   string $enclosure Column value enclosure
     *
     * @return  \Balance\Core\Helper\Csv
     */
    public function setEnclosure($enclosure)
    {
        $this->enclosure = $enclosure;
        return $this;
    }

    /**
     * Retrieve CSV file data as array
     *
     * @param   string $file File path
     *
     * @return array
     * @throws Exception
     */
    public function getData($file)
    {
        if (!file_exists($file)) {
            throw new Exception('File "' . $file . '" do not exists');
        }

        $fh = fopen($file, 'r');
        while ($rowData = fgetcsv($fh, $this->lineLength, $this->delimiter, $this->enclosure)) {
            yield $rowData;
        }
        fclose($fh);
    }

    /**
     * Retrieve CSV file data as pairs
     *
     * @param   string $file       File path
     * @param   int    $keyIndex   Key index
     * @param   int    $valueIndex Value index
     *
     * @return  array
     */
    public function getDataPairs($file, $keyIndex = 0, $valueIndex = 1)
    {
        $data = [];
        $csvData = $this->getData($file);
        foreach ($csvData as $rowData) {
            if (isset($rowData[$keyIndex])) {
                $data[$rowData[$keyIndex]] = isset($rowData[$valueIndex]) ? $rowData[$valueIndex] : null;
            }
        }
        return $data;
    }

    /**
     * Retrieve CSV file data with column names as array keys
     *
     * @param string $file        File path
     * @param bool   $columnNames Column names shift to array keys
     *
     * @return  array
     */
    public function parseData($file, $columnNames = true)
    {
        $data = [];
        $csvData = $this->getData($file);
        if ($columnNames) {
            $columns = null;
            foreach ($csvData as $keyIndex => $rowData) {
                if ($keyIndex === 0) {
                    $columns = $rowData;
                } else {
                    try {
                        $data[$keyIndex] = array_combine($columns, array_values($rowData));
                    } catch (\Exception $e) {
                        break;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Saving data row array into file
     *
     * @param   string $file File path
     * @param   array  $data Data
     *
     * @return  \Balance\Core\Helper\Csv
     */
    public function saveData($file, $data)
    {
        $fh = fopen($file, 'w');
        foreach ($data as $dataRow) {
            $this->_fputcsv($fh, $dataRow, $this->delimiter, $this->enclosure);
        }
        fclose($fh);
        return $this;
    }

    /**
     * Enhance default PHP method fputcsv
     *
     * @param resource $handle    File stream
     * @param array    $fields    Fields
     * @param string   $delimiter Delimiter
     * @param string   $enclosure Enclosure
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *
     * @return mixed
     */
    protected function _fputcsv(&$handle, $fields = [], $delimiter = ',', $enclosure = '"')
    {
        $str = '';
        $escapeChar = '\\';

        if (is_array($fields)) {
            foreach ($fields as $value) {
                if (strpos($value, $delimiter) !== false
                    || strpos($value, $enclosure) !== false
                    || strpos($value, "\n") !== false
                    || strpos($value, "\r") !== false
                    || strpos($value, "\t") !== false
                    || strpos($value, ' ') !== false
                ) {
                    $temp = $enclosure;
                    $escaped = 0;
                    $len = strlen($value);
                    for ($i = 0; $i < $len; $i++) {
                        if ($value[$i] === $escapeChar) {
                            $escaped = 1;
                        } else {
                            if (!$escaped && $value[$i] === $enclosure) {
                                $temp .= $enclosure;
                            } else {
                                $escaped = 0;
                            }
                        }
                        $temp .= $value[$i];
                    }
                    $temp .= $enclosure;
                    $str .= $temp . $delimiter;
                } else {
                    $str .= $enclosure . $value . $enclosure . $delimiter;
                }
            }
        }

        $str = substr($str, 0, -1);
        $str .= "\n";

        return fwrite($handle, $str);
    }
}
