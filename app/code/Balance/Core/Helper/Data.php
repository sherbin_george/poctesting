<?php
/**
 * Copyright © 2016 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Module\ResourceInterface as ModuleResourceInterface;

/**
 * Balance Internet general helper
 * - All helpers should be extend from this one instead of AbstractHelper
 *
 * @api
 * @package Balance\Core\Helper
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Data extends AbstractHelper
{
    /**#@+
     * Constants defined for keys of data array
     */
    const MODULE_NAME = 'Balance_Core';

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    /**
     * @var \Magento\Framework\Module\ResourceInterface
     */
    protected $moduleResource;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $defaultLogger;

    /**
     * @var string
     */
    protected $channelName;

    /**
     * @var string
     */
    protected $logFile;

    /**
     * Data constructor.
     *
     * @param Context                 $context        Context
     * @param ObjectManagerInterface  $objectManager  Object manager
     * @param StoreManagerInterface   $storeManager   Store manager
     * @param ModuleListInterface     $moduleList     Module list
     * @param ModuleResourceInterface $moduleResource Module resource
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        ModuleListInterface $moduleList,
        ModuleResourceInterface $moduleResource
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->objectManager = $objectManager;
        $this->moduleList = $moduleList;
        $this->moduleResource = $moduleResource;
        $this->defaultLogger = $context->getLogger();
        $this->channelName = 'balance-debug';
        $this->logFile = BP . '/var/log/balance.log';
    }

    /**
     * Retrieve base url of current store
     *
     * @param string $type (short syntax: m, s, l)
     *
     * @return string
     */
    public function getBaseUrl($type = UrlInterface::URL_TYPE_LINK)
    {
        switch ($type) {
            case 'm':
            case UrlInterface::URL_TYPE_MEDIA:
                return $this->getCurrentStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            case 's':
            case UrlInterface::URL_TYPE_STATIC:
                return $this->getCurrentStore()->getBaseUrl(UrlInterface::URL_TYPE_STATIC);
            case 'l':
            case UrlInterface::URL_TYPE_LINK:
            default:
                return $this->getCurrentStore()->getBaseUrl(UrlInterface::URL_TYPE_LINK);
        }
    }

    /**
     * Retrieve configuration
     *
     * @param string $configPath XML Path
     *
     * @return mixed
     */
    public function getConfig($configPath)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get model like the Magento 1
     * - Create new object instance
     *
     * @param string $model Instance basename
     * @param array  $agrs  Agruments
     *
     * @return mixed
     */
    public function getModel($model, array $agrs = [])
    {
        return $this->objectManager->create($model, $agrs);
    }

    /**
     * Get singleton like Magento 1
     * - Retrieve cached object instance
     *
     * @param string $model Instance basename
     *
     * @return mixed
     */
    public function getSingleton($model)
    {
        return $this->objectManager->get($model);
    }

    /**
     * Retrieve current store name
     *
     * @param int|null $storeId
     *
     * @return \Magento\Store\Api\Data\StoreInterface|\Magento\Store\Model\Store
     */
    public function getCurrentStore($storeId = null)
    {
        return $this->storeManager->getStore($storeId);
    }

    /**
     * Retrieve module version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->moduleList->getOne(static::MODULE_NAME)['setup_version'];
    }

    /**
     * Retrieve module version from database
     *
     * @return false|string
     */
    public function getDataVersion()
    {
        return $this->moduleResource->getDataVersion(static::MODULE_NAME);
    }

    /**
     * Create new logger instance with custom channel name
     * and log file name
     *
     * @param string      $type
     * @param string|null $channelName
     * @param string|null $logFile
     *
     * @return \Monolog\Logger|\Psr\Log\LoggerInterface
     */
    public function getLogger($type = 'default', $channelName = null, $logFile = null)
    {
        switch ($type) {
            case 'custom':
                $logger = $this->getCustomLogger($channelName, $logFile);
                break;
            case 'default':
            default:
                $logger = $this->getDefaultLogger();
                break;
        }

        return $logger;
    }

    /**
     * Return default logger instance
     *
     * @return \Psr\Log\LoggerInterface
     */
    protected function getDefaultLogger()
    {
        return $this->defaultLogger;
    }

    /**
     * Create new logger instance with custom channel name
     * and log file name
     *
     * @param string|null $channelName
     * @param string|null $logFile
     *
     * @return \Monolog\Logger
     */
    protected function getCustomLogger($channelName = null, $logFile = null)
    {
        if (!empty($channelName) && !empty($logFile)) {
            $this->channelName = $channelName;
            $this->logFile = BP . $logFile;
        }

        /** @var \Monolog\Logger $logger */
        $logger = $this->getModel('Monolog\Logger', [$this->channelName]);
        /** @var \Monolog\Handler\StreamHandler $streamHandler */
        $streamHandler = $this->getModel('Monolog\Handler\StreamHandler', [$this->logFile]);
        $logger->pushHandler($streamHandler);

        return $logger;
    }
}
