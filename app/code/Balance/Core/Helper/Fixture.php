<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Core\Helper;

/**
 * Helper Fixture
 *
 * Balance Internet fixture helper, this helper contains common functions to used while
 * installing fixture data
 *
 * @api
 * @package Balance\Core\Helper
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Fixture extends Data
{
    /**#@+
     * Constants defined for keys of data array
     */
    const MODULE_NAME = 'Balance_Core';
    const FIXTURES_MODE = false;
    const DEBUG_MODE = false;
    const FIXTURE_FILE_SUFFIX = '.html';

    /**
     * Retrieve module name
     *
     * @return string
     */
    public function getModuleName()
    {
        return static::MODULE_NAME;
    }

    /**
     * Check if using fixture to handle setup scripts
     *
     * @return bool
     */
    public function isFixtureModeActive()
    {
        return static::FIXTURES_MODE;
    }

    /**
     * Check if enable debug mode
     *
     * @return bool
     */
    public function isDebugModeActive()
    {
        return static::DEBUG_MODE;
    }

    /**
     * Load store data by code
     *
     * @param string $code Store code
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore($code)
    {
        return $this->getCurrentStore()->load($code);
    }
}
