<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Core\Model\File;

use League\Csv\Reader;
use League\Csv\Writer;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Directory\ReadFactory;

/**
 * Csv business logic model, provides fastest/lightweight way to interact with CSV files
 * Dependencies: league/csv: ^8.0
 *
 * @package Balance\Core\Model\File
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Csv
{
    const FILE_PREFIX = '';
    const FILE_NAME = 'customer_composite';
    const FILE_EXT = 'csv';

    /**
     * @var ReadFactory
     */
    private $readFactory;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * Csv constructor.
     *
     * @param ReadFactory   $readFactory
     * @param DirectoryList $directoryList
     */
    public function __construct(
        ReadFactory $readFactory,
        DirectoryList $directoryList
    ) {
        $this->readFactory = $readFactory;
        $this->directoryList = $directoryList;
    }

    /**
     * Get CSV reader object
     *
     * @param string $fileName
     *
     * @return \League\Csv\Reader
     */
    public function getReader($fileName)
    {
        $csvObj = Reader::createFromString($this->readFile($fileName));
        return $csvObj;
    }

    /**
     * Parse CSV data from file
     *
     * @param string $fileName
     *
     * @return \Iterator
     */
    public function getData($fileName)
    {
        $csvObj = Reader::createFromString($this->readFile($fileName));
        $csvObj->setDelimiter(',');
        $results = $csvObj->fetchAssoc();

        return $results;
    }

    /**
     * Write CSV data to file
     *
     * @param string $fileName File path to save CSV
     * @param array  $data     CSV data
     *
     * @return void
     */
    public function saveData($fileName, $data)
    {
        $csvObj = Writer::createFromPath($fileName, 'w');
        $csvObj->insertAll($data);
    }

    /**
     * Output the content of CSV
     *
     * @param string $fileName File path of CSV file
     *
     * @return void
     */
    public function output($fileName)
    {
        $csvObj = Reader::createFromPath($fileName);
        $csvObj->output();
    }

    /**
     * Return file name for downloading.
     *
     * @return string
     */
    public function getFileName()
    {
        $prefix = (self::FILE_PREFIX) ? self::FILE_PREFIX . '_' : '';
        return $prefix . self::FILE_NAME . '_' . time() . '.' . self::FILE_EXT;
    }

    /**
     * MIME-type for 'Content-Type' header.
     *
     * @return string
     */
    public function getContentType()
    {
        return 'text/csv';
    }

    /**
     * Read content file of CSV
     *
     * @param string $fileName
     *
     * @return mixed
     */
    protected function readFile($fileName)
    {
        $path = $this->directoryList->getRoot();
        $directoryRead = $this->readFactory->create($path);
        return $directoryRead->readFile($fileName);
    }
}
