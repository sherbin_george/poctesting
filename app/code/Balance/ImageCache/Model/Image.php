<?php
namespace Balance\ImageCache\Model;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Phrase;

/**
 * Class Image.
 * Cache image model.
 *
 * @package Balance\ImageCache\Model
 */
class Image implements ImageInterface
{
    /**
     * @var \Balance\ImageCache\Model\CacheInterface $_cache Cache instance.
     */
    protected $_cache;
    /**
     * @var \Balance\ImageCache\Model\Image\PathInterfaceFactory $pathFactory Path factory.
     */
    protected $_pathFactory;
    /**
     * @var \Balance\ImageCache\Helper\Data $_helper Helper instance.
     */
    protected $_helper;
    /**
     * @var \Balance\ImageCache\Model\Image\PathInterface $_sourcePath Source image path.
     */
    private $_sourcePath;
    /**
     * @var string $_preliminaryPath Preliminary destination path (before image is generated).
     */
    private $_preliminaryPath;
    /**
     * @var \Balance\ImageCache\Model\Image\PathInterface $_path Cache image path.
     */
    private $_path;
    /**
     * @var \Balance\ImageCache\Model\Image\SizeInterface $_size Image size.
     */
    private $_size;
    /**
     * @var bool $_isGenerated Determines whether destination file is generated.
     */
    private $_isGenerated;

    /**
     * Image constructor.
     *
     * @param \Balance\ImageCache\Model\Image\PathInterfaceFactory $pathFactory Path factory.
     * @param \Balance\ImageCache\Model\Image\SizeInterfaceFactory $sizeFactory Image size factory.
     * @param string                                                       $sourcePath  Source image path.
     * @param string                                                       $path        Cache image path.
     * @param array                                                        $size        Cache image size.
     */
    function __construct(
        \Balance\ImageCache\Model\Image\PathInterfaceFactory $pathFactory,
        \Balance\ImageCache\Model\Image\SizeInterfaceFactory $sizeFactory,
        \Balance\ImageCache\Helper\Data $helper,
        $sourcePath,
        $path,
        array $size
    )
    {
        $this->_helper = $helper;

        $this->_pathFactory = $pathFactory;
        $this->_sourcePath = $this->_pathFactory->create(['path' => $sourcePath]);
        $this->setPath($path);
        $this->_size = $sizeFactory->create(['size' => $size]);
    }

    /**
     * Sets related cache instance.
     *
     * @param \Balance\ImageCache\Model\CacheInterface $cache Cache instance.
     *
     * @return void
     */
    public function setCache(\Balance\ImageCache\Model\CacheInterface $cache)
    {
        $this->_cache = $cache;
    }

    /**
     * Returns cache base path.
     *
     * @return string
     *
     * @throws InputException
     */
    protected function _getCacheBasePath()
    {
        if (is_null($this->_cache)) {
            throw new InputException(
                new Phrase('Cache')
            );
        }

        return $this->_cache->getBasePath();
    }

    /**
     * Returns cache image url.
     *
     * @return string
     *
     * @throws NotFoundException
     */
    public function getUrl()
    {
        if (!$this->_isGenerated) {
            throw new NotFoundException(
                new Phrase('Image file not found.')
            );
        }

        $relativePath = ltrim(
            str_replace($this->_getCacheBasePath(), '', $this->getPath()),
            DIRECTORY_SEPARATOR
        );

        return $this->_helper->getMediaUrl()
            . str_replace(DIRECTORY_SEPARATOR, '/', $relativePath);
    }

    /**
     * Returns source image absolute path.
     *
     * @return string
     */
    public function getSourcePath()
    {
        return (string) $this->_sourcePath;
    }

    /**
     * Returns preliminary path when cache image does not exist, otherwise cache image path.
     *
     * @return string
     */
    public function getPreliminaryPath()
    {
        if ($this->_isGenerated) {
            return $this->getPath();
        }

        return $this->_preliminaryPath;
    }

    /**
     * Returns cache image absolute path.
     *
     * @return string
     *
     * @throws NotFoundException
     */
    public function getPath()
    {
        if (!$this->_isGenerated) {
            throw new NotFoundException(
                new Phrase('Image file not found.')
            );
        }

        return (string) $this->_path;
    }

    /**
     * Sets cache image absolute path.
     * Preliminary path is set when cache image does not exist.
     *
     * @param string $path Path to be set.
     *
     * @return void
     */
    public function setPath($path)
    {
        try {
            $this->_path = $this->_pathFactory->create(['path' => $path]);
            $this->_isGenerated = true;
        } catch (FileSystemException $exception) {
            $this->_preliminaryPath = $path;
            $this->_isGenerated = false;
        }
    }

    /**
     * Returns cache image size.
     *
     * @return \Balance\ImageCache\Model\Image\SizeInterface
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * Determines whether cache image was already generated
     *
     * @return bool
     */
    public function isGenerated()
    {
        return (bool) $this->_isGenerated;
    }
}