<?php
namespace Balance\ImageCache\Model;

/**
 * Interface CacheInterface.
 * Image cache interface.
 *
 * @package Balance\ImageCache\Model
 */
interface CacheInterface
{
    /**
     * Returns cache images base path.
     *
     * @return string
     */
    public function getBasePath();

    /**
     * Sets source images base path.
     *
     * @param string $path Base source path.
     *
     * @return void
     */
    public function setSourceBasePath($path);

    /**
     * Returns generated cache image.
     *
     * @param string $section Section name.
     * @param string $path    Absolute or relative source image path.
     * @param array  $size    Image size to be generated (width, height).
     *
     * @return \Balance\ImageCache\Model\ImageInterface
     */
    public function getImage($section, $path, array $size);

    /**
     * Generates cache image.
     *
     * @param string $section Section name.
     * @param string $path    Absolute or relative source image path.
     * @param array  $size    Image size to be generated (width, height).
     *
     * @return void
     */
    public function generateImage($section, $path, array $size);

    /**
     * Deletes cache image in all generated sizes.
     *
     * @param string $section Section name.
     * @param string $path    Absolute or relative source image path.
     *
     * @return void
     */
    public function deleteImage($section, $path);

    /**
     * Deletes all section related images.
     *
     * @param string $section Section name.
     *
     * @return void
     */
    public function deleteSection($section);
}