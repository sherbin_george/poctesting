<?php
namespace Balance\ImageCache\Model;

/**
 * Interface ImageInterface.
 * Cache image interface.
 *
 * @package Balance\ImageCache\Model
 */
interface ImageInterface
{
    /**
     * Returns cache image url.
     *
     * @return string
     */
    public function getUrl();

    /**
     * Returns source image absolute path.
     *
     * @return string
     */
    public function getSourcePath();

    /**
     * Returns preliminary path when cache image does not exist, otherwise cache image path.
     *
     * @return string
     */
    public function getPreliminaryPath();

    /**
     * Returns cache image absolute path.
     *
     * @return string
     */
    public function getPath();

    /**
     * Sets cache image absolute path.
     * Preliminary path is set when cache image does not exist.
     *
     * @param string $path Path to be set.
     *
     * @return void
     */
    public function setPath($path);

    /**
     * Returns cache image size.
     *
     * @return \Balance\ImageCache\Model\Image\SizeInterface
     */
    public function getSize();

    /**
     * Determines whether cache image was already generated
     *
     * @return bool
     */
    public function isGenerated();

    /**
     * Sets related cache instance.
     *
     * @param \Balance\ImageCache\Model\CacheInterface $cache Cache instance.
     *
     * @return void
     */
    public function setCache(\Balance\ImageCache\Model\CacheInterface $cache);
}