<?php
namespace Balance\ImageCache\Model\Image;

/**
 * Interface PathInterface.
 * Path data object interface.
 *
 * @package Balance\ImageCache\Model\Image
 */
interface PathInterface
{
    /**
     * Returns directory path.
     *
     * @return string
     */
    public function getDirectoryPath();

    /**
     * Returns file name.
     *
     * @return string
     */
    public function getFilename();

    /**
     * Returns stored path value.
     *
     * @return string
     */
    public function __toString();
}