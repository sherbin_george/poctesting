<?php
namespace Balance\ImageCache\Model\Image;

/**
 * Class Generator.
 * Cache image generator.
 *
 * @package Balance\ImageCache\Model\Image
 */
class Generator implements GeneratorInterface
{
    /**
     * @const int Default generated image quality.
     */
    const DEFAULT_IMAGE_QUALITY = 80;

    /**
     * @var \Magento\Framework\Image\Factory $_imageProcessorFactory Image processor factory.
     */
    protected $_imageProcessorFactory;
    /**
     * @var int $_imageQuality Generated image quality.
     */
    protected $_imageQuality;

    /**
     * Generator constructor.
     *
     * @param \Magento\Framework\Image\Factory $imageProcessorFactory Image processor factory.
     * @param null|int                         $imageQuality          Generate image quality.
     */
    public function __construct(
        \Magento\Framework\Image\Factory $imageProcessorFactory,
        $imageQuality = null
    )
    {
        $this->_imageProcessorFactory = $imageProcessorFactory;
        $this->_imageQuality = !is_null($imageQuality)
            ? (int) $imageQuality
            : self::DEFAULT_IMAGE_QUALITY;
    }

    /**
     * Generates cache image.
     *
     * @param \Balance\ImageCache\Model\ImageInterface $image Image instance.
     *
     * @return void
     */
    public function generateImage(\Balance\ImageCache\Model\ImageInterface $image)
    {
        /** @var \Magento\Framework\Image $imageProcessor */
        $imageProcessor = $this->_imageProcessorFactory->create($image->getSourcePath());
        $imageProcessor->keepAspectRatio(true);
        $imageProcessor->keepFrame(true);
        $imageProcessor->keepTransparency(true);
        $imageProcessor->constrainOnly(true);
        $imageProcessor->quality($this->_imageQuality);
        $imageProcessor->resize(
            $image->getSize()->getWidth(),
            $image->getSize()->getHeight()
        );

        $destinationPath = $image->getPreliminaryPath();
        $destinationPathInfo = pathinfo($destinationPath);
        $imageProcessor->save($destinationPathInfo['dirname'], $destinationPathInfo['basename']);

        $image->setPath($destinationPath);
    }
}