<?php
namespace Balance\ImageCache\Model\Image;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Phrase;

/**
 * Class Path.
 * Path data object.
 *
 * @package Balance\ImageCache\Model\Image
 */
final class Path implements PathInterface
{
    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadInterface $_readDirectory Readable directory.
     */
    private $_readDirectory;
    /**
     * @var string $_path Path.
     */
    private $_path;
    /**
     * @var string $_filename Filename.
     */
    private $_filename;

    /**
     * Path constructor.
     *
     * @param \Magento\Framework\Filesystem\Directory\ReadFactory $readDirectoryFactory Readable directory.
     * @param string                                              $path                 Path to be stored.
     *
     * @throws FileSystemException
     */
    public function __construct(
        \Magento\Framework\Filesystem\Directory\ReadFactory $readDirectoryFactory,
        $path
    )
    {
        $pathInfo = pathinfo($path);

        $this->_readDirectory = $readDirectoryFactory->create($pathInfo['dirname']);
        if (!$this->_readDirectory->isFile($pathInfo['basename'])) {
            throw new FileSystemException(
                new Phrase('Path is invalid or does not exist.')
            );
        }

        $this->_path = $path;
        $this->_filename = $pathInfo['basename'];
    }

    /**
     * Returns directory path.
     *
     * @return string
     */
    public function getDirectoryPath()
    {
        return $this->_readDirectory->getAbsolutePath();
    }

    /**
     * Returns file name.
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->_filename;
    }

    /**
     * Returns stored path value.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->_path;
    }
}