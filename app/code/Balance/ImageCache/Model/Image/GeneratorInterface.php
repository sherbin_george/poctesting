<?php
namespace Balance\ImageCache\Model\Image;

/**
 * Interface GeneratorInterface.
 * Cache image generator interface.
 *
 * @package Balance\ImageCache\Model\Image
 */
interface GeneratorInterface
{
    /**
     * Generates cache image.
     *
     * @param \Balance\ImageCache\Model\ImageInterface $image Image instance.
     *
     * @return void
     */
    public function generateImage(\Balance\ImageCache\Model\ImageInterface $image);
}