<?php
namespace Balance\ImageCache\Model\Image;

/**
 * Interface SizeInterface.
 * Size data object interface.
 *
 * @package Balance\ImageCache\Model\Image
 */
interface SizeInterface
{
    /**
     * Returns size width.
     *
     * @return int
     */
    public function getWidth();

    /**
     * Returns size height.
     *
     * @return int
     */
    public function getHeight();

    /**
     * Returns size values as an associative array.
     *
     * @return array
     */
    public function asArray();

    /**
     * Returns size values as a string.
     *
     * @return string
     */
    public function __toString();
}