<?php
namespace Balance\ImageCache\Model\Image;

/**
 * Class Size.
 * Size data object.
 *
 * @package Balance\ImageCache\Model\Image
 */
final class Size implements SizeInterface
{
    /**
     * @const string Size values default delimiter.
     */
    const DEFAULT_DELIMITER = 'x';

    /**
     * @var int $_width Size width.
     */
    private $_width;
    /**
     * @var int $_height Size height.
     */
    private $_height;
    /**
     * @var string $_delimiter Size values delimiter.
     */
    private $_delimiter;

    /**
     * Size constructor.
     *
     * @param array       $size      Size values (width, height) to be stored.
     * @param string|null $delimiter Size values delimiter.
     */
    public function __construct(array $size, $delimiter = null)
    {
        $this->_validate($size);
        $this->_setWidth($size[0]);
        $this->_setHeight($size[1]);

        $this->_delimiter = !is_null($delimiter)
            ? $delimiter
            : self::DEFAULT_DELIMITER;
    }

    /**
     * Validates size value before set.
     *
     * @param array $size Size values (width, height).
     *
     * @return void
     *
     * @throws \Exception
     */
    private function _validate(array $size)
    {
        if (count($size) != 2) {
            throw new \Exception('Invalid image size.');
        }
    }

    /**
     * Sets size width.
     *
     * @param int $width Size width.
     *
     * @return void
     *
     * @throws \Exception
     */
    private function _setWidth($width)
    {
        if (!is_int($width)
            || $width <= 0
        ) {
            throw new \Exception('Invalid image width.');
        }

        $this->_width = $width;
    }

    /**
     * Sets size height.
     *
     * @param int $height Size height.
     *
     * @return void
     *
     * @throws \Exception
     */
    private function _setHeight($height)
    {
        if (!is_int($height)
            || $height <= 0
        ) {
            throw new \Exception('Invalid image height.');
        }

        $this->_height = $height;
    }

    /**
     * Returns size width.
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * Returns size height.
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * Returns size values as an associative array.
     *
     * @return array
     */
    public function asArray()
    {
        return [
            'width' => $this->_width,
            'height' => $this->_height
        ];
    }

    /**
     * Returns size values as a string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->_width
            . $this->_delimiter
            . $this->_height;
    }
}