<?php
namespace Balance\ImageCache\Model;

/**
 * Class Cache.
 * Abstract image cache model.
 *
 * @package Balance\ImageCache\Model
 */
abstract class Cache implements CacheInterface
{
    /**
     * @var string $_sourceBasePath Base path for source image files.
     */
    protected $_sourceBasePath;
    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteFactory $_writeDirectoryFactory Writable directory factory.
     */
    protected $_writeDirectoryFactory;
    /**
     * @var \Balance\ImageCache\Model\Image\GeneratorInterface $_imageGenerator Image generator.
     */
    protected $_imageGenerator;
    /**
     * @var \Balance\ImageCache\Model\ImageInterfaceFactory $_imageFactory Image factory.
     */
    protected $_imageFactory;
    /**
     * @var \Balance\ImageCache\Model\Image\SizeInterfaceFactory $_imageSizeFactory Image size factory.
     */
    protected $_imageSizeFactory;
    /**
     * @var \Balance\ImageCache\Model\Cache\Registry\Size $_sizeRegistry Size registry.
     */
    protected $_sizeRegistry;
    /**
     * @var \Balance\ImageCache\Helper\Data $_helper Base helper instance.
     */
    protected $_helper;
    /**
     * @var \Magento\Framework\DataObjectFactory $dataObjectFactory Data object factory.
     */
    protected $_dataObjectFactory;

    /**
     * Cache constructor.
     *
     * @param \Magento\Framework\Filesystem\Directory\WriteFactory         $writeDirectoryFactory Writable directory
     *                                                                                            factory.
     * @param \Balance\ImageCache\Model\Image\GeneratorInterface   $imageGenerator        Image generator.
     * @param \Balance\ImageCache\Model\ImageInterfaceFactory      $imageFactory          Image factory.
     * @param \Balance\ImageCache\Model\Image\SizeInterfaceFactory $imageSizeFactory      Image size factory.
     * @param \Balance\ImageCache\Model\Cache\Registry\Size        $sizeRegistry          Size registry.
     * @param \Balance\ImageCache\Helper\Data                      $helper                Base helper instance.
     * @param \Magento\Framework\DataObjectFactory                         $dataObjectFactory     Data object factory.
     */
    public function __construct(
        \Magento\Framework\Filesystem\Directory\WriteFactory $writeDirectoryFactory,
        \Balance\ImageCache\Model\Image\GeneratorInterface $imageGenerator,
        \Balance\ImageCache\Model\ImageInterfaceFactory $imageFactory,
        \Balance\ImageCache\Model\Image\SizeInterfaceFactory $imageSizeFactory,
        \Balance\ImageCache\Model\Cache\Registry\Size $sizeRegistry,
        \Balance\ImageCache\Helper\Data $helper,
        \Magento\Framework\DataObjectFactory $dataObjectFactory
    )
    {
        $this->_writeDirectoryFactory = $writeDirectoryFactory;
        $this->_imageGenerator = $imageGenerator;
        $this->_imageFactory = $imageFactory;
        $this->_imageSizeFactory = $imageSizeFactory;

        $this->_sizeRegistry = $sizeRegistry;
        $this->_sizeRegistry->setCache($this);

        $this->_helper = $helper;
        $this->_dataObjectFactory = $dataObjectFactory;
    }

    /**
     * Returns cache images base path.
     *
     * @return string
     */
    public function getBasePath()
    {
        return $this->_getBasePath();
    }

    /**
     * Returns generated cache image.
     *
     * @param string $section Section name.
     * @param string $path    Absolute or relative source image path.
     * @param array  $size    Image size to be generated (width, height).
     *
     * @return \Balance\ImageCache\Model\ImageInterface
     */
    public function getImage($section, $path, array $size)
    {
        $section = $this->_prepareCacheSectionName($section);
        /** @var \Balance\ImageCache\Model\Image\SizeInterface $imageSize */
        $imageSize = $this->_imageSizeFactory->create(['size' => $size]);
        $cacheImagePath = $this->_getCacheImageAbsolutePath($section, $path, $imageSize);

        /** @var \Balance\ImageCache\Model\ImageInterface $image */
        $image = $this->_imageFactory->create(
            [
                'sourcePath' => $this->_getSourceImageAbsolutePath($path),
                'path' => $cacheImagePath,
                'size' => $size
            ]
        );
        $image->setCache($this);

        if (!$image->isGenerated()) {
            $this->_generateImage($image);
            $this->_registerNewSize($section, $imageSize);
        }

        return $image;
    }

    /**
     * Generates cache image.
     *
     * @param string $section Section name.
     * @param string $path    Absolute or relative source image path.
     * @param array  $size    Image size to be generated (width, height).
     *
     * @return void
     */
    public function generateImage($section, $path, array $size)
    {
        $section = $this->_prepareCacheSectionName($section);
        /** @var \Balance\ImageCache\Model\Image\SizeInterface $imageSize */
        $imageSize = $this->_imageSizeFactory->create(['size' => $size]);
        $cacheImagePath = $this->_getCacheImageAbsolutePath($section, $path, $imageSize);

        /** @var \Balance\ImageCache\Model\ImageInterface $image */
        $image = $this->_imageFactory->create(
            [
                'sourcePath' => $this->_getSourceImageAbsolutePath($path),
                'path' => $cacheImagePath,
                'size' => $size
            ]
        );
        $image->setCache($this);

        $this->_imageGenerator->generateImage($image);
        $this->_registerNewSize($section, $imageSize);
    }

    /**
     * Deletes cache image in all generated sizes.
     *
     * @param string $section Section name.
     * @param string $path    Absolute or relative source image path.
     *
     * @return void
     */
    public function deleteImage($section, $path)
    {
        $section = $this->_prepareCacheSectionName($section);
        /** @var \Magento\Framework\DataObject $data */
        $data = $this->_dataObjectFactory->create(
            [
                'data' => [
                    'cache_section' => $section
                ]
            ]
        );

        $relativePath = ltrim(
            str_replace($this->_getSourceBasePath(), '', $path),
            DIRECTORY_SEPARATOR
        );

        foreach ($this->_sizeRegistry->asArray($data) as $registeredSize) {
            /** @var \Magento\Framework\Filesystem\Directory\Write $writeFactory */
            $writeFactory = $this->_writeDirectoryFactory->create($registeredSize);
            $writeFactory->getDriver()->deleteFile(
                $writeFactory->getAbsolutePath($relativePath)
            );
        }
    }

    /**
     * Deletes all section related images.
     *
     * @param string $section Section name.
     *
     * @return void
     */
    public function deleteSection($section)
    {
        $section = $this->_prepareCacheSectionName($section);
        /** @var \Magento\Framework\Filesystem\Directory\Write $writeFactory */
        $writeFactory = $this->_writeDirectoryFactory->create(
            $this-> _getCacheSectionAbsolutePath($section)
        );

        $writeFactory->delete();
        $this->_deleteRegisteredSectionSizes($section);
    }

    /**
     * Generates cache image (for internal usage).
     *
     * @param \Balance\ImageCache\Model\ImageInterface $image Image instance.
     *
     * @return void
     */
    public function _generateImage(\Balance\ImageCache\Model\ImageInterface $image)
    {
        $this->_imageGenerator->generateImage($image);
    }

    /**
     * Prepares registry section name.
     *
     * @param string $section Section name.
     *
     * @return string
     */
    protected function _prepareCacheSectionName($section)
    {
        return strtolower(
            str_replace('_', DIRECTORY_SEPARATOR, $section)
        );
    }

    /**
     * Registers new size for section in size registry.
     *
     * @param string                                                $section   Section name.
     * @param \Balance\ImageCache\Model\Image\SizeInterface $imageSize Image size.
     *
     * @return void
     */
    protected function _registerNewSize($section, $imageSize)
    {
        /** @var \Magento\Framework\DataObject $itemData */
        $itemData = $this->_dataObjectFactory->create(
            [
                'data' => [
                    'cache_section' => $section,
                    'size' => $imageSize
                ]
            ]
        );

        $this->_sizeRegistry->addItem($itemData);
    }

    /**
     * Deletes registered section sizes from size registry.
     *
     * @param string $section Section name.
     *
     * @return void
     */
    protected function _deleteRegisteredSectionSizes($section)
    {
        /** @var \Magento\Framework\DataObject $itemData */
        $itemData = $this->_dataObjectFactory->create(
            [
                'data' => [
                    'cache_section' => $section
                ]
            ]
        );

        $this->_sizeRegistry->deleteAll($itemData);
    }

    /**
     * Sets source images base path.
     * By default, cache type base path is used as source images base path.
     *
     * @param string $path Base source path.
     *
     * @return void
     */
    public function setSourceBasePath($path)
    {
        $this->_sourceBasePath = $path;
    }

    /**
     * Returns source images base path.
     * By default, cache type base path is used as source images base path.
     *
     * @return string
     */
    protected function _getSourceBasePath()
    {
        if (is_null($this->_sourceBasePath)) {
            return $this->_getBasePath();
        }

        return $this->_sourceBasePath;
    }

    /**
     * Returns cache images base path.
     * Should be implemented in child classes.
     *
     * @return string
     */
    abstract protected function _getBasePath();

    /**
     * Returns source image absolute path.
     *
     * @param string $sourcePath Absolute or relative source image path.
     *
     * @return string
     */
    protected function _getSourceImageAbsolutePath($sourcePath)
    {
        return rtrim($this->_getSourceBasePath(), DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR
            . ltrim(
                str_replace($this->_getSourceBasePath(), '', $sourcePath),
                DIRECTORY_SEPARATOR
            );
    }

    /**
     * Returns cache image absolute path.
     *
     * @param string                                                $section    Section name.
     * @param string                                                $sourcePath Absolute or relative source image path.
     * @param \Balance\ImageCache\Model\Image\SizeInterface $size       Image size.
     *
     * @return string
     */
    protected function _getCacheImageAbsolutePath(
        $section,
        $sourcePath,
        \Balance\ImageCache\Model\Image\SizeInterface $size
    )
    {
        return $this-> _getCacheSectionAbsolutePath($section)
            . DIRECTORY_SEPARATOR
            . $size
            . DIRECTORY_SEPARATOR
            . ltrim(
                str_replace($this->_getSourceBasePath(), '', $sourcePath),
                DIRECTORY_SEPARATOR
            );
    }

    /**
     * Returns cache section absolute path.
     *
     * @param string $section Section name.
     *
     * @return string
     */
    protected function _getCacheSectionAbsolutePath($section)
    {
        return rtrim($this->_getBasePath(), DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR
            . $this->_helper->getBaseSectionPath()
            . DIRECTORY_SEPARATOR
            . $section;
    }
}