<?php
namespace Balance\ImageCache\Model\Cache\Registry;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Phrase;

/**
 * Class Size.
 * Size section cache registry model.
 *
 * @package Balance\ImageCache\Model\Cache\Registry
 */
class Size
    extends \Balance\ImageCache\Model\Cache\Registry
    implements \Balance\ImageCache\Model\Cache\RegistryInterface
{
    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadFactory $_readDirectoryFactory Readable directory factory.
     */
    protected $_readDirectoryFactory;
    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteFactory $_writeDirectoryFactory Writable directory factory.
     */
    protected $_writeDirectoryFactory;

    /**
     * Size constructor.
     *
     * @param \Balance\ImageCache\Helper\Data              $helper                Base helper instance.
     * @param \Magento\Framework\Filesystem\Directory\ReadFactory  $readDirectoryFactory  Readable directory factory.
     * @param \Magento\Framework\Filesystem\Directory\WriteFactory $writeDirectoryFactory Writable directory factory.
     * @param null|string                                          $registrySectionPath   Registry section path.
     */
    function __construct(
        \Balance\ImageCache\Helper\Data $helper,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readDirectoryFactory,
        \Magento\Framework\Filesystem\Directory\WriteFactory $writeDirectoryFactory,
        $registrySectionPath = null
    )
    {
        $this->_readDirectoryFactory = $readDirectoryFactory;
        $this->_writeDirectoryFactory = $writeDirectoryFactory;

        parent::__construct($helper, $registrySectionPath);
    }

    /**
     * Adds new item to registry.
     * NOTE: we add a symlink to original size folder as registry item.
     *
     * @param DataObject $data Item data.
     *
     * @return void
     */
    public function addItem(DataObject $data)
    {
        $this->_validateInputData($data);
        $registrySection = $this->_prepareRegistrySectionName($data->getData('cache_section'));

        /** @var \Magento\Framework\Filesystem\Directory\Write $writeDirectory */
        $writeDirectory = $this->_writeDirectoryFactory->create(
            $this->_getRegistryAbsolutePath($registrySection)
        );

        $writeDirectory->create();
        if ($writeDirectory->isExist($data->getData('size'))) {
            return;
        }

        $symlinkSource = $this->_getOriginalSizeFolder(
            $data->getData('cache_section'),
            $data->getData('size')
        );
        $symlinkDestination = $writeDirectory->getAbsolutePath(
            $data->getData('size')
        );

        $writeDirectory->getDriver()->symlink($symlinkSource, $symlinkDestination);
    }

    /**
     * Deletes item from registry.
     * NOTE: we delete a symlink to original size folder as registry item.
     *
     * @param DataObject $data Item data.
     *
     * @return void
     */
    public function deleteItem(DataObject $data)
    {
        $this->_validateInputData($data);
        $registrySection = $this->_prepareRegistrySectionName($data->getData('cache_section'));

        /** @var \Magento\Framework\Filesystem\Directory\Write $writeDirectory */
        $writeDirectory = $this->_writeDirectoryFactory->create(
            $this->_getRegistryAbsolutePath($registrySection)
        );

        $writeDirectory->getDriver()->deleteFile(
            $writeDirectory->getAbsolutePath($data->getData('size'))
        );
    }

    /**
     * Deletes all items from registry.
     * NOTE: we delete related section directory.
     *
     * @param DataObject $data Data object.
     *
     * @return void
     */
    public function deleteAll(DataObject $data)
    {
        if (is_null($data)
            || empty($data->getData('cache_section'))
        ) {
            return;
        }

        $registrySection = $this->_prepareRegistrySectionName($data->getData('cache_section'));

        /** @var \Magento\Framework\Filesystem\Directory\Write $writeDirectory */
        $writeDirectory = $this->_writeDirectoryFactory->create(
            $this->_getRegistryAbsolutePath($registrySection)
        );

        $writeDirectory->delete();
    }

    /**
     * Returns registry items as an array.
     * NOTE: symlink list to original size folders is returned.
     *
     * @param DataObject|null $data Data object.
     *
     * @return array
     */
    public function asArray(DataObject $data = null)
    {
        if (is_null($data)
            || empty($data->getData('cache_section'))
        ) {
            return [];
        }

        $registrySection = $this->_prepareRegistrySectionName($data->getData('cache_section'));

        /** @var \Magento\Framework\Filesystem\Directory\Read $readDirectory */
        $readDirectory = $this->_readDirectoryFactory->create(
            $this->_getRegistryAbsolutePath($registrySection)
        );

        $registryItems = [];
        foreach ($readDirectory->search('*') as $registryItem) {
            $registryItems[$registryItem] = $readDirectory->getAbsolutePath($registryItem);
        }

        return $registryItems;
    }

    /**
     * Validates input data.
     *
     * @param DataObject $data Item data.
     *
     * @throws InputException
     */
    protected function _validateInputData(DataObject $data)
    {
        $size = $data->getData('size');
        if (empty($size)) {
            throw new InputException(
                new Phrase('Image size')
            );
        }

        $cacheSection = $data->getData('cache_section');
        if (empty($cacheSection)) {
            throw new InputException(
                new Phrase('Cache section')
            );
        }
    }

    /**
     * Prepares registry section name.
     *
     * @param string $section Section name.
     *
     * @return string
     */
    protected function _prepareRegistrySectionName($section)
    {
        return strtolower(
            str_replace(DIRECTORY_SEPARATOR, '_', $section)
        );
    }

    /**
     * Returns original size image cache folder.
     *
     * @param string                                                       $cacheSection Cache section name.
     * @param \Balance\ImageCache\Model\Image\SizeInterface|string $size         Image size.
     *
     * @return string
     */
    protected function _getOriginalSizeFolder($cacheSection, $size)
    {
        return rtrim($this->_cache->getBasePath(), DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR
            . $this->_helper->getBaseSectionPath()
            . DIRECTORY_SEPARATOR
            . $cacheSection
            . DIRECTORY_SEPARATOR
            . $size;

    }
}