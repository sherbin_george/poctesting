<?php
namespace Balance\ImageCache\Model\Cache;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Phrase;

/**
 * Class Registry.
 * Cache registry abstract model.
 *
 * @package Balance\ImageCache\Model\Cache
 */
abstract class Registry implements RegistryInterface
{
    /**
     * @const string Default registry section path.
     */
    const DEFAULT_REGISTRY_SECTION_PATH = 'default';

    /**
     * @var \Balance\ImageCache\Model\CacheInterface $_cache Cache instance.
     */
    protected $_cache;
    /**
     * @var \Balance\ImageCache\Helper\Data $_helper Base helper instance.
     */
    protected $_helper;
    /**
     * @var string $_registrySectionPath Registry section path.
     */
    protected $_registrySectionPath;

    /**
     * Registry constructor.
     *
     * @param \Balance\ImageCache\Helper\Data $helper              Base helper instance.
     * @param null|string                             $registrySectionPath Registry section path.
     */
    function __construct(
        \Balance\ImageCache\Helper\Data $helper,
        $registrySectionPath = null
    )
    {
        $this->_helper = $helper;

        $this->_registrySectionPath = !is_null($registrySectionPath)
            ? $registrySectionPath
            : self::DEFAULT_REGISTRY_SECTION_PATH;
    }

    /**
     * Sets related cache instance.
     *
     * @param \Balance\ImageCache\Model\CacheInterface $cache Cache instance.
     *
     * @return void
     */
    public function setCache(\Balance\ImageCache\Model\CacheInterface $cache)
    {
        $this->_cache = $cache;
    }

    /**
     * Returns cache base path.
     *
     * @return string
     *
     * @throws InputException
     */
    protected function _getCacheBasePath()
    {
        if (is_null($this->_cache)) {
            throw new InputException(
                new Phrase('Cache')
            );
        }

        return $this->_cache->getBasePath();
    }

    /**
     * Returns cache registry absolute path.
     *
     * @param string $relativePath Relative registry path (optional).
     *
     * @return string
     */
    protected function _getRegistryAbsolutePath($relativePath = '')
    {
        return rtrim(
            rtrim($this->_getCacheBasePath(), DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR
            . $this->_helper->getBaseSectionPath()
            . DIRECTORY_SEPARATOR
            . $this->_helper->getRegistrySectionPath()
            . DIRECTORY_SEPARATOR
            . $this->_registrySectionPath
            . DIRECTORY_SEPARATOR
            . $relativePath,
            DIRECTORY_SEPARATOR
        );
    }
}