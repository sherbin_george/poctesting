<?php
namespace Balance\ImageCache\Model\Cache;

use Magento\Framework\DataObject;

/**
 * Interface RegistryInterface.
 * Cache registry interface.
 *
 * @package Balance\ImageCache\Model\Cache
 */
interface RegistryInterface
{
    /**
     * Sets related cache instance.
     *
     * @param \Balance\ImageCache\Model\CacheInterface $cache Cache instance.
     *
     * @return void
     */
    public function setCache(\Balance\ImageCache\Model\CacheInterface $cache);

    /**
     * Adds new item to registry.
     *
     * @param DataObject $data Item data.
     *
     * @return void
     */
    public function addItem(DataObject $data);

    /**
     * Deletes item from registry.
     *
     * @param DataObject $data Item data.
     *
     * @return void
     */
    public function deleteItem(DataObject $data);

    /**
     * Deletes all items from registry.
     *
     * @param DataObject $data Data object.
     *
     * @return void
     */
    public function deleteAll(DataObject $data);

    /**
     * Returns registry items as an array.
     *
     * @param DataObject|null $data Data object.
     *
     * @return array
     */
    public function asArray(DataObject $data = null);
}