<?php
namespace Balance\ImageCache\Model\Cache\Type;

/**
 * Class Media.
 * Media cache type (based on general 'media' folder).
 *
 * @package Balance\ImageCache\Model\Cache\Type
 */
class Media
    extends \Balance\ImageCache\Model\Cache
    implements \Balance\ImageCache\Model\CacheInterface
{
    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList $_directoryList Directory list model.
     */
    protected $_directoryList;

    /**
     * Cache constructor.
     *
     * @param \Magento\Framework\Filesystem\Directory\WriteFactory         $writeDirectoryFactory Writable directory
     *                                                                                            factory.
     * @param \Balance\ImageCache\Model\Image\GeneratorInterface   $imageGenerator        Image generator.
     * @param \Balance\ImageCache\Model\ImageInterfaceFactory      $imageFactory          Image factory.
     * @param \Balance\ImageCache\Model\Image\SizeInterfaceFactory $imageSizeFactory      Image size factory.
     * @param \Balance\ImageCache\Helper\Data                      $helper                Base helper instance.
     * @param \Magento\Framework\App\Filesystem\DirectoryList              $directoryList         Directory list.
     */
    public function __construct(
        \Magento\Framework\Filesystem\Directory\WriteFactory $writeDirectoryFactory,
        \Balance\ImageCache\Model\Image\GeneratorInterface $imageGenerator,
        \Balance\ImageCache\Model\ImageInterfaceFactory $imageFactory,
        \Balance\ImageCache\Model\Image\SizeInterfaceFactory $imageSizeFactory,
        \Balance\ImageCache\Model\Cache\Registry\Size $sizeRegistry,
        \Balance\ImageCache\Helper\Data $helper,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    )
    {
        $this->_directoryList = $directoryList;

        parent::__construct(
            $writeDirectoryFactory,
            $imageGenerator,
            $imageFactory,
            $imageSizeFactory,
            $sizeRegistry,
            $helper,
            $dataObjectFactory
        );
    }

    /**
     * Returns cache images base path.
     *
     * @return string
     */
    protected function _getBasePath()
    {
        return $this->_directoryList->getPath(
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );
    }
}