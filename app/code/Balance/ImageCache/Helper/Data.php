<?php
namespace Balance\ImageCache\Helper;

/**
 * Class Data.
 * Base helper class.
 *
 * @package Balance\ImageCache\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @const string XML path for base section path setting.
     */
    const XML_PATH_BASE_SECTION_PATH = 'system/image_cache/base_section';
    /**
     * @const string XML path for registry section path setting.
     */
    const XML_PATH_REGISTRY_SECTION_PATH = 'system/image_cache/registry_section';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface $_storeManager Store manager.
     */
    protected $_storeManager;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context      $context      Context model.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager Store manager.
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Returns base section path.
     *
     * @return string
     */
    public function getBaseSectionPath()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_BASE_SECTION_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
    }

    /**
     * Returns registry section path.
     *
     * @return string
     */
    public function getRegistrySectionPath()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_REGISTRY_SECTION_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
    }

    /**
     * Returns media url by store.
     *
     * @param \Magento\Store\Model\Store|null $store Store instance (optional).
     *
     * @return string
     */
    public function getMediaUrl($store = null)
    {
        if (is_null($store)) {
            /** @var \Magento\Store\Model\Store $store */
            $store = $this->_storeManager->getStore();
        }

        $mediaUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        return rtrim($mediaUrl, '/') . '/';
    }
}