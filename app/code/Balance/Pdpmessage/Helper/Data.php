<?php

namespace Balance\Pdpmessage\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_scopeConfig;
    /**
     * constructor.
     *
     * @param Magento\Framework\App\Helper\Context $context
     * @param Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ){
        parent::__construct($context);
        $this->_scopeConfig = $context->getScopeConfig();
    }
   
    /**
     * Get enable configuration
     *
     * @return array
     */
    public function getIsEnabled(){
        $val = $this->_scopeConfig->getValue('balance_pdpmessage/general/pdpmessage_enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if(!empty($val) && $val != 2) return 1;
        return 0;
    }
    /**
     * Get message configuration
     *
     * @return array
     */
    public function getMessage(){
        return $this->_scopeConfig->getValue('balance_pdpmessage/general/pdpmessage_message', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    public function showPdpMessage(){
        $pdp = '';
        if($this->getIsEnabled()){
            return $this->getMessage();
        }
        return $pdp;
    }

}