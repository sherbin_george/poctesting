<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\SampleData\Model;

use Magento\Cms\Model\Block;
use Magento\Cms\Model\Page;
use Magento\Store\Model\Store;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Setup\SampleData\Context as SampleDataContext;
use Balance\Core\Helper\Fixture as FixtureHelper;
use Balance\SampleData\Api\Data\SampleDataInterface;

/**
 * Abstract class to install sample data in a very elegant way
 *
 *
 * METHOD USAGE:
 *
 * - processSimple(array $fixtures, array $processArgs = [], $extraEntityArgs = [])
 * |___ This method will process and install data in simplest and effective way.
 *
 * - processContent(array $fixtures, array $processArgs = [], $extraEntityArgs = [])
 * |___ This method will process your data content in advanced. You are granted to use custom content file in fixtures
 *      directory.
 * |
 * |___ E.g (about fixture structure): fixtures/<entity>/<version>/<custom_path>/<data_file>.csv
 *
 * - processConverter(array $fixtures, array $processArgs = [], $extraEntityArgs = [])
 * |___ This method allow you do the same thing as processAdvanced(), but your data could be convert before save data
 * |
 * |___ E.g (about fixture structure): fixtures/<entity>/<version>/<custom_path>/<data_file>.csv
 *
 *
 * ARGUMENTS EXPLAINATION:
 *
 * - string[] $fixture (required)
 * |___ This is an array contains path to data files (CSV) under fixtures directory, it will be parse to another array
 *      of data to install
 *
 * - string[] $processArgs (required)
 * |___ This is the array you declare your entity and other things, check SampleDataInterface for more information
 *
 * - string[] $extraEntityArgs (optional)
 * |___ This is the array you declare data keys of extra entity you want to load and get data, then set it back to the
 *      current entity.
 *
 *
 * @package Balance\SampleData\Model
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
abstract class AbstractSampleData implements SampleDataInterface
{
    /**
     * @var \Magento\Framework\Setup\SampleData\FixtureManager
     */
    protected $fixtureManager;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvReader;

    /**
     * @var Assets\AssetsManagement
     */
    protected $assetManagement;

    /**
     * @var Fixture\FixtureManagement
     */
    protected $fixtureManagement;

    /**
     * @var \Balance\Core\Helper\Fixture
     */
    protected $helper;

    /**
     * AbstractSampleData constructor.
     *
     * @param SampleDataContext         $sampleDataContext
     * @param Assets\AssetsManagement   $assetsManagement
     * @param Fixture\FixtureManagement $fixtureManagement
     * @param FixtureHelper             $fixtureHelper
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        Assets\AssetsManagement $assetsManagement,
        Fixture\FixtureManagement $fixtureManagement,
        FixtureHelper $fixtureHelper
    ) {
        $this->fixtureManager = $sampleDataContext->getFixtureManager();
        $this->csvReader = $sampleDataContext->getCsvReader();
        $this->assetManagement = $assetsManagement;
        $this->fixtureManagement = $fixtureManagement;
        $this->helper = $fixtureHelper;
    }

    /**
     * Process data
     *
     * @param string[] $fixtures
     * @param string[] $processArgs
     * @param string[] $extraEntityArgs
     *
     * @return void
     */
    public function process($fixtures, $processArgs, $extraEntityArgs = [])
    {
        foreach ($fixtures as $fileName) {
            $fileName = $this->fixtureManager->getFixture($fileName);
            if (!$this->assetManagement->isExists($fileName)) {
                continue;
            }

            $rows = $this->csvReader->getData($fileName);
            $header = array_shift($rows);

            foreach ($rows as $row) {
                $data = [];
                foreach ($row as $key => $value) {
                    $data[$header[$key]] = $value;
                }

                $row = $data;
                $processData = $this->getProcessData($row, $processArgs);

                if (false !== $processData[static::IS_SAVEABLE]) {
                    if (!empty($processData[$processArgs[static::ENTITY]])) {
                        $processData = $processData[$processArgs[static::ENTITY]];
                    }
                    $entityObject = $this->saveEntityObjectData(
                        $processData,
                        $processArgs[static::ENTITY_CLASS],
                        $extraEntityArgs
                    );
                    $entityObject->unsetData();
                }
            }
        }

        if (isset($processArgs[static::PUBLISH_ASSETS]) && $processArgs[static::PUBLISH_ASSETS] === true) {
            $this->publishAssets();
        }
    }

    /**
     * Get the process data through types
     *
     * - CODE: 0x11 (processSimple)
     * |___ This method will process and install data in simplest and effective way.
     *
     * - CODE: 0x33 (processContent)
     * |___ This method will process your data content in advanced. You are granted to use custom content file in
     *      fixtures directory.
     *
     * - CODE: 0x44 (processConverter)
     * |___ This method allow you do the same thing as 0x33, but your data could be convert before save data
     *
     * @param array    $processData
     * @param string[] $processArgs
     *
     * @return array
     */
    private function getProcessData($processData, $processArgs)
    {
        if ($processArgs[static::PROCESS_TYPE] === static::PROCESS_SIMPLE) {
            $processData[static::IS_SAVEABLE] = true;
        }

        if ($processArgs[static::PROCESS_TYPE] === static::PROCESS_CONTENT) {
            $processData = $this->processContent($processData, $processArgs);
        }

        if ($processArgs[static::PROCESS_TYPE] === static::PROCESS_CONVERTER) {
            $processData = $this->processConverter($processData, $processArgs);
        }

        return $processData;
    }

    /**
     * Process data w/ custom content file
     *
     * @param array    $data
     * @param string[] $processArgs
     *
     * @return array
     */
    private function processContent(array $data, array $processArgs = [])
    {
        $filePath = $this->getPath($data);
        $content = $this->getContentData($data, $filePath, $processArgs);

        $data[static::CONTENT] = $content;
        $data[static::IS_SAVEABLE] = (!empty($content) && !empty($data[static::IS_ACTIVE]));

        return $data;
    }

    /**
     * Process data w/ custom content file + converter
     *
     * @param array    $data
     * @param string[] $processArgs
     *
     * @return array
     */
    private function processConverter(array $data, array $processArgs = [])
    {
        $convertData = $this->convertData($data, $processArgs[static::CONVERTER_CLASS]);
        $filePath = $this->getPath($data);
        $content = $this->getContentData($data, $filePath, $processArgs);

        $convertData[$processArgs[static::ENTITY]][static::CONTENT] = $content;
        $convertData[static::IS_SAVEABLE] = (!empty($content) && false !== $data[static::IS_ACTIVE]);

        return $convertData;
    }

    /**
     * Get current row content data
     *
     * @param array    $data
     * @param string   $filePath
     * @param string[] $processArgs
     *
     * @return string|bool|null
     */
    private function getContentData($data, $filePath, $processArgs = [])
    {
        $content = $data[static::CONTENT];
        if (empty($data[static::CONTENT])) {
            $contentFile = $data[static::IDENTIFIER] . FixtureHelper::FIXTURE_FILE_SUFFIX;
            $fixturePath = sprintf('%1$s%2$s%3$s',
                $this->helper->getModuleName(),
                $this->getFixturePath($processArgs[static::ENTITY]),
                (!empty($filePath)) ? $filePath . '/' : ''
            );
            $content = $this->fixtureManagement->getFixtureContent(
                $this->fixtureManager->getFixture($fixturePath . $contentFile),
                [
                    static::ENTITY => $processArgs[static::ENTITY],
                    static::PATH => (!empty($data[static::PATH])) ? $data[static::PATH] : '',
                    static::STORECODE => $this->getStoreIds($data),
                    static::HELPER => $this->helper
                ]
            );
        }

        return $content;
    }

    /**
     * Get path based on 'path' and 'storeviews' fields
     *
     * @param array $data Row data
     *
     * @return string
     */
    private function getPath($data)
    {
        $filePath = '';
        if (!empty($data[static::STORECODE])) {
            $filePath = $data[static::STORECODE];

            if (!empty($data[static::PATH])) {
                $filePath .= '/' . $data[static::PATH];
            }
        }

        return $filePath;
    }

    /**
     * Retrieve store ids array
     *
     * @param array $data Row data
     *
     * @return array
     */
    private function getStoreIds($data)
    {
        $storeIds = [Store::DEFAULT_STORE_ID];
        if (!empty($data[static::STORECODE])) {
            if (is_string($data[static::STORECODE])) {
                $storeIds = [$data[static::STORECODE]];
            }
        }

        return $storeIds;
    }

    /**
     * Save entity data which using multi-store and custom content file
     *
     * @param array  $data
     * @param string $entityClass
     * @param array  $extraEntityArgs
     *
     * @return object
     * @throws \Exception
     */
    private function saveEntityObjectData($data, $entityClass, $extraEntityArgs = [])
    {
        $storeIds = $this->getStoreIds($data);
        $storeCode = $data[static::STORECODE];
        $data = $this->unsetRedundantData($data);

        $entityObject = $this->helper->getModel($entityClass);
        if ($entityClass === Page::class || $entityClass === Block::class) {
            $entityObject->setStoreId($this->helper->getStore($storeCode)->getStoreId());
        }
        $entityObject->load($data[static::IDENTIFIER]);

        if (!$entityObject->getData()) {
            $entityObject->setData($data);
        } else {
            $entityObject->addData($data);
        }

        if (!empty($extraEntityArgs)) {
            $entityObject = $this->setEntityDataByExtraEntity(
                $extraEntityArgs[static::GET_DATA_KEY],
                $extraEntityArgs[static::LOAD_DATA_KEY],
                $extraEntityArgs[static::SET_DATA_KEY],
                $extraEntityArgs[static::EXTRA_ENTITY_CLASS],
                $entityObject
            );
        }

        $entityObject->setIsActive(true);
        $entityObject->setStores($this->convertCodesToIds($storeIds));

        try {
            $entityObject->save();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $entityObject;
    }

    /**
     * Set entity data from another entity
     *
     * @param string $getDataKey       Extra entity object key to extract data from
     * @param string $loadDataKey      Load extra entity object by using this key id
     * @param string $setDataKey       Column you want to set data for entity object
     * @param string $extraEntityClass Extra entity class name
     * @param object $entityObject     Entity object
     *
     * @return object
     */
    protected function setEntityDataByExtraEntity(
        $getDataKey,
        $loadDataKey,
        $setDataKey,
        $extraEntityClass,
        $entityObject
    ) {
        $extraEntityObject = $this->helper->getModel($extraEntityClass);
        $extraEntityObject->getResource()->load($extraEntityObject, $loadDataKey);

        if ($extraEntityObject->getData()) {
            $entityObject->setData($setDataKey, $extraEntityObject->getData($getDataKey));
        }

        return $entityObject;
    }

    /**
     * Unset redundant data before save
     *
     * @param array $data
     *
     * @return array
     */
    private function unsetRedundantData($data)
    {
        if (isset($data[static::STORECODE])) {
            unset($data[static::STORECODE]);
        }
        if (isset($data[static::PATH])) {
            unset($data[static::PATH]);
        }
        if (isset($data[static::CONTENT]) && empty($data[static::CONTENT])) {
            unset($data[static::CONTENT]);
        }
        if (isset($data[static::LAYOUT_UPDATE_XML]) && empty($data[static::LAYOUT_UPDATE_XML])) {
            unset($data[static::LAYOUT_UPDATE_XML]);
        }

        return $data;
    }

    /**
     * Convert store codes to store ids
     *
     * @param array $codes Store codes
     *
     * @return array
     */
    private function convertCodesToIds($codes)
    {
        $storeIds = $codes;
        $newStoreIds = [];
        foreach ($codes as $code) {
            if (is_string($code)) {
                $newStoreIds[] = $this->helper->getStore($code)->getStoreId();
            }
        }

        return (count($newStoreIds) > 0) ? $newStoreIds : $storeIds;
    }

    /**
     * Convert row data
     *
     * @param mixed  $data
     * @param string $converterClass
     *
     * @return mixed
     */
    private function convertData($data, $converterClass)
    {
        $converterObject = $this->helper->getModel($converterClass);
        return $converterObject->convertRow($data);
    }

    /**
     * Publish assets from app/code/Vendor/Package/pub folder into root/pub folder
     *
     * CODE: 0x000 - Everything is okay
     * CODE: 0x404 - No such file or directory
     * CODE: 0x999 - Exception throwed, check the log in var/log
     *
     * @return int
     */
    private function publishAssets()
    {
        $pubDirectory = $this->fixtureManager->getFixture($this->helper->getModuleName() . '::' . DirectoryList::PUB);
        return $this->assetManagement->moveFilesFromTo($pubDirectory, DirectoryList::PUB);
    }

    /**
     * Retrieve fixture path by entity name
     *
     * @param string $entity Entity name
     *
     * @return string
     */
    private function getFixturePath($entity)
    {
        return sprintf('::fixtures/%s/', $entity);
    }
}
