<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\SampleData\Model\Fixture;

use Magento\Framework\Filesystem\Glob;
use Balance\SampleData\Api\Data\SampleDataInterface;
use Balance\SampleData\Model\Assets\AssetsManagement;

class FixtureManagement
{
    /**#@+
     * Constants defined for keys of data array
     */
    const VERSION_COMPARE_GREATER = 1;
    const VERSION_COMPARE_LOWER = -1;
    const VERSION_COMPARE_EQUAL = 0;

    /**
     * @var \Balance\SampleData\Model\Assets\AssetsManagement
     */
    protected $assetManagement;

    /**
     * FixtureManagement constructor.
     *
     * @param AssetsManagement $assetsManagement
     */
    public function __construct(
        AssetsManagement $assetsManagement
    ) {
        $this->assetManagement = $assetsManagement;
    }

    /**
     * Retrieve file id content from fixture
     *
     * @param string $fileId
     * @param array  $args
     *
     * @return bool|string
     */
    public function getFixtureContent($fileId, $args = [])
    {
        $fileContent = false;
        $fileNameParts = $this->extractFixture($fileId);
        $helper = $args[SampleDataInterface::HELPER];
        if (count($fileNameParts) > 0) {
            $fileName = $this->assetManagement->getFilename(
                $this->extractFixture($fileId),
                $helper->getVersion(),
                $args
            );
            $this->getDebugInfo('BEFOREIF_' . __FUNCTION__, ['fileName' => $fileName], $helper);
            if (!$this->assetManagement->isExists($fileName) &&
                (($previous = $this->checkPreviousVersionContent($helper->getVersion(), $helper->getDataVersion(),
                        $fileId, $args)) !== false)
            ) {
                $this->getDebugInfo('INSIDEIF_' . __FUNCTION__, ['previousVersion' => $previous], $helper);
                foreach ($previous as $version) {
                    $fileName = $this->assetManagement->getFilename($this->extractFixture($fileId), $version, $args);
                    if ($this->assetManagement->isExists($fileName)) {
                        $fileContent = $this->assetManagement->fileGetContents($fileName);
                    }
                }
            } else {
                if ($this->assetManagement->isExists($fileName)) {
                    $fileContent = $this->assetManagement->fileGetContents($fileName);
                }
            }
        }

        return $fileContent;
    }

    /**
     * Check if any previous version has content, return array of those versions exists
     * or return false if can't found anything
     *
     * @param string $currentVersion
     * @param string $oldVersion
     * @param string $fileId
     * @param array  $args
     *
     * @return array|bool
     */
    protected function checkPreviousVersionContent($currentVersion, $oldVersion, $fileId, $args = [])
    {
        $fileNameParts = $this->processFileNameParts($fileId, $args);
        $fixtureDir = implode('/', $fileNameParts);
        $versions = Glob::glob($fixtureDir . DIRECTORY_SEPARATOR . "*", Glob::GLOB_ONLYDIR);
        $compareVersions = [];
        if (count($versions) > 1) {
            foreach ($versions as $version) {
                $_version = $this->assetManagement->getBasename($version);
                if (is_numeric(str_replace('.', '', $_version))) {
                    $this->getDebugInfo(
                        'INFOREACH_' . __FUNCTION__,
                        [
                            'version' => $_version,
                            'dataVersion' => $args[SampleDataInterface::HELPER]->getDataVersion()
                        ],
                        $args[SampleDataInterface::HELPER]
                    );
                    if (version_compare($currentVersion, $_version) !== self::VERSION_COMPARE_EQUAL) {
                        if (version_compare($oldVersion, $_version) === self::VERSION_COMPARE_LOWER) {
                            $compareVersions[] = $_version;
                        }
                    }
                }
            }
        }

        return (count($compareVersions) > 0) ? $compareVersions : false;
    }

    /**
     * Process file name parts
     *
     * @param string   $fileId
     * @param string[] $args
     *
     * @return array
     */
    protected function processFileNameParts($fileId, $args)
    {
        $fileNameParts = $this->extractFixture($fileId, true);
        if (!empty($args[SampleDataInterface::STORECODE]) && $args[SampleDataInterface::STORECODE][0] !== 0) {
            $fileNameParts = $this->extractFixture($fileId, true, 1);
        } else {
            if ((!empty($args[SampleDataInterface::STORECODE]) && $args[SampleDataInterface::STORECODE][0] !== 0)
                && !empty($args[SampleDataInterface::PATH])
            ) {
                $fileNameParts = $this->extractFixture($fileId, true, 2);
            }
        }

        return $fileNameParts;
    }

    /**
     * Extract fixture to array
     *
     * @param string   $fileId
     * @param bool     $isPopped
     * @param int|null $times
     *
     * @return array
     */
    protected function extractFixture($fileId, $isPopped = false, $times = null)
    {
        $fileNameParts = explode('/', $fileId);
        if ($isPopped) {
            array_pop($fileNameParts);
            if ($times === 1) {
                array_pop($fileNameParts);
            }
            if ($times === 2) {
                array_pop($fileNameParts);
                array_pop($fileNameParts);
            }
        }

        return $fileNameParts;
    }

    /**
     * Show debug info while turn on debug mode
     *
     * @param string $case   Section to print debug info
     * @param array  $args   Data of debug info
     * @param object $helper Helper object
     */
    private function getDebugInfo($case, $args = [], $helper)
    {
        if ($helper->isDebugModeActive()) {
            //@codingStandardsIgnoreStart
            switch ($case) {
                case 'BEFOREIF_getFixtureContent':
                    echo sprintf("\nfilename             :   %s", $args['fileName']);
                    echo sprintf("\nmodule.xml version   :   %s", $helper->getVersion());
                    echo sprintf("\nsetup_module version :   %s", $helper->getDataVersion());
                    break;
                case 'INSIDEIF_getFixtureContent':
                    if (!empty($args)) {
                        echo "\n\n";
                        print_r($args['previousVersion']);
                    }
                    break;
                case 'INFOREACH_checkPreviousVersionContent':
                    echo sprintf("\n%s : %d", $args['version'],
                        version_compare($args['dataVersion'], $args['version']));
            }
            //@codingStandardsIgnoreEnd
        }
    }
}