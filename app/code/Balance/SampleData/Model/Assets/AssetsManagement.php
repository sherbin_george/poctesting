<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\SampleData\Model\Assets;

use Balance\SampleData\Api\Data\SampleDataInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem\Io\File as FileIo;
use Magento\Framework\App\Filesystem\DirectoryList;

class AssetsManagement
{
    /**#@+
     * Constants defined for keys of data array
     */
    const FILESYS_NOTFOUND = 0x404;
    const FILESYS_OKAY = 0x000;
    const FILESYS_EXCEPTION = 0x999;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Write
     */
    protected $rootWrite;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Read
     */
    protected $rootRead;

    /**
     * AssetsManagement constructor.
     *
     * @param File       $file
     * @param FileIo     $fileIo
     * @param Filesystem $filesystem
     */
    public function __construct(
        File $file,
        FileIo $fileIo,
        Filesystem $filesystem
    ) {
        $this->file = $file;
        $this->fileIo = $fileIo;
        $this->rootWrite = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
        $this->rootRead = $filesystem->getDirectoryRead(DirectoryList::ROOT);
    }

    /**
     * Is file or directory exist in file system
     *
     * @param string $path
     *
     * @return bool
     */
    public function isExists($path)
    {
        return $this->file->isExists($path);
    }

    /**
     * Retrieve file contents from given path
     *
     * @param string        $path
     * @param string|null   $flag
     * @param resource|null $context
     *
     * @return string
     * @throws FileSystemException
     */
    public function fileGetContents($path, $flag = null, $context = null)
    {
        return $this->file->fileGetContents($path, $flag, $context);
    }

    /**
     * Move all files and directories from A to B
     *
     * @param string $fromPath
     * @param string $toPath
     *
     * @return int
     * @throws \Exception
     */
    public function moveFilesFromTo($fromPath, $toPath)
    {
        if ($this->file->isExists($fromPath)) {
            $returnCode = static::FILESYS_OKAY;
            $files = $this->rootRead->readRecursively($this->rootRead->getRelativePath($fromPath));
            foreach ($files as $file) {
                try {
                    $newFileName = str_replace($fromPath, $toPath, $file);
                    if ($this->rootRead->isFile($file)) {
                        $this->rootWrite->copyFile($file, $newFileName);
                        $this->rootWrite->changePermissions($newFileName, 0660);
                    } elseif ($this->rootRead->isDirectory($newFileName)) {
                        $this->rootWrite->changePermissions($newFileName, 0770);
                    }
                } catch (\Exception $ex) {
                    $returnCode = static::FILESYS_EXCEPTION;
                }
            }
        } else {
            $returnCode = static::FILESYS_NOTFOUND;
        }

        return $returnCode;
    }

    /**
     * Get basename from file path
     *
     * @param $filePath
     *
     * @return string
     */
    public function getBasename($filePath)
    {
        $fileInfo = $this->fileIo->getPathInfo($filePath);
        return $fileInfo['basename'];
    }

    /**
     * Get filename with version
     *
     * @param array  $fileNameParts
     * @param string $version
     * @param array  $args
     *
     * @return string
     */
    public function getFilename($fileNameParts, $version, $args = [])
    {
        if (isset($args[SampleDataInterface::ENTITY])) {
            $position = array_search($args[SampleDataInterface::ENTITY], $fileNameParts);
            array_splice($fileNameParts, (int)$position + 1, 0, $version);
        }
        return implode('/', $fileNameParts);
    }
}