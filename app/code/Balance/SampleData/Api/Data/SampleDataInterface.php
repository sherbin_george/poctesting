<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\SampleData\Api\Data;

/**
 * Interface SampleDataInterface
 *
 * @api
 * @package Balance\SampleData\Api\Data
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
interface SampleDataInterface
{
    /**#@+
     * Constants defined for keys of data array
     */
    const IDENTIFIER = 'identifier';
    const CONTENT = 'content';
    const LAYOUT_UPDATE_XML = 'layout_update_xml';
    const PATH = 'path';
    const STORECODE = 'storecode';
    const IS_ACTIVE = 'is_active';

    const ENTITY = 'entity';
    const ENTITY_CLASS = 'entity_class';
    const CONVERTER_CLASS = 'converter_class';
    const EXTRA_ENTITY_CLASS = 'extra_entity_class';
    const GET_DATA_KEY = 'get_data_key';
    const LOAD_DATA_KEY = 'load_data_key';
    const SET_DATA_KEY = 'set_data_key';

    const PROCESS_TYPE = 'type';
    const PROCESS_SIMPLE = 0x11;
    const PROCESS_CONTENT = 0x33;
    const PROCESS_CONVERTER = 0x44;
    const IS_SAVEABLE = 'is_saveable';
    const PUBLISH_ASSETS = 'publish_assets';
    const STORES_DELIMITER = '|';
    const HELPER = 'helper';

    /**
     * Install sample data by using fixtures
     *
     * @param string[] $fixtures Array of fixtures
     *
     * @return void
     */
    public function install($fixtures);
}
