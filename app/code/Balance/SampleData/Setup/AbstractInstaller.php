<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\SampleData\Setup;

use Balance\Core\Helper\Fixture as FixtureHelper;

/**
 * Class AbstractInstaller
 *
 * @package Balance\SampleData\Setup
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
class AbstractInstaller
{
    const ENTITIES = 'entities';
    const ENTITY_CLASSNAME = 'classname';
    const ENTITY_NAME = 'name';
    const PATH = '/';
    const FIXTURES_PREFIX = '::fixtures';
    const FIXTURE_DATA_FILE_SUFFIX = '.csv';

    /**
     * @var \Balance\Core\Helper\Fixture
     */
    protected $helper;

    /**
     * AbstractInstaller constructor.
     *
     * @param FixtureHelper $helper
     */
    public function __construct(FixtureHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Start invoke installing sample data
     *
     * @param array $args Array of agruments
     */
    protected function process($args = [])
    {
        if ($this->helper->isFixtureModeActive()) {
            $this->printDoubleNewLines();

            if (isset($args[static::ENTITIES]) && is_array($args[static::ENTITIES])) {
                foreach ($args[static::ENTITIES] as $entity => $data) {
                    $entityObject = $this->helper->getModel($data[static::ENTITY_CLASSNAME]);
                    $entityObject->install([$this->getFixturePath($entity)]);
                }
            }

            $this->printDoubleNewLines();
        }
    }

    /**
     * Retrieve fixture path
     *
     * @param string $name Entity name
     *
     * @return string
     */
    protected function getFixturePath($name)
    {
        $fixturePath = $this->helper->getModuleName();
        $fixturePath .= static::FIXTURES_PREFIX . static::PATH . $name . static::PATH;
        $fixturePath .= $name . static::FIXTURE_DATA_FILE_SUFFIX;

        return $fixturePath;
    }

    /**
     * Echo double new lines in console
     *
     * @codingStandardsIgnoreStart
     */
    protected function printDoubleNewLines()
    {
        if ($this->helper->isDebugModeActive()) {
            echo "\n\n";
        }
    }
}
