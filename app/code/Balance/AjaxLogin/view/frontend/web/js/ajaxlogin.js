define([
    "jquery",
    "jquery/ui"
], function($){
    "use strict";

    function main(config, element) {
        var $element         = $(element);
        var PostUrl          = config.PostUrl;
        var ValidateUrl      = config.ValidateUrl;
        var UpdateDobUrl     = config.UpdateDobUrl;
        var SignUpUrl        = config.SignUpUrl;
        var DateValidateUrl  = config.DateValidateUrl;

        var dataForm = $('#login-form');
        dataForm.mage('validation', {});

        $(document).on('click','.login-submit',function() {
            if($(this).hasClass( "authenticate" )) {
                authentication(PostUrl,dataForm);
            } else {
                validateEmail(ValidateUrl,dataForm);
            }
        });

        $(document).on('click','.update-dob', function () {
            var dobDataForm = $('#customer-dob-form');
            dobDataForm.mage('validation', {});
            updateDob(UpdateDobUrl, dobDataForm);

        });

        $(document).on('click','.tyanother', function () {
            showStep('form-login');
            hideStep('form-signup');
            return false;
        });

        $(document).on('click','.signup-submit', function () {
            var dobDataForm = $('#form-signup');
            dobDataForm.mage('validation', {});
            createCustomer(SignUpUrl, dobDataForm);
        });

        $(document).on('change','.date-input-validate', function () {
            var dobDataForm = $('.customer-dob-form');
            dobDataForm.mage('validation', {});
            validateDate(DateValidateUrl, dobDataForm);
        });

    };
    
    function showStep(element) {
        $('.'+element).show();
    }

    function hideStep(element) {
        $('.'+element).hide();
    }

    // Validate Email
    function validateEmail(ValidateUrl,dataForm) {
        $('.display-message').hide();
        $('.display-message').html('');
        if (dataForm.valid()){
            event.preventDefault();
            var param = dataForm.serialize();
            $.ajax({
                showLoader: true,
                url: ValidateUrl,
                data: param,
                type: "POST"
            }).done(function (data) {
                if(data.success !== 'false') {
                    showStep('exsiting-customer-welcome');
                    hideStep('login-page-note');
                    hideStep('block-title');
                    showStep('password-hidden');
                    showStep('forget-password');
                    hideStep('find-more');
                    showStep('terms-hidden');
                    // $('#login-form .username').attr('readonly', true);
                    $('.login-submit').addClass("authenticate");
                    return true;
                } else {
                    $('.form-signup .emailaddress').val($('.form-login .username').val());
                    // $('.form-signup .emailaddress').attr('readonly', true);
                    hideStep('block-title');
                    hideStep('form-login');
                    showStep('form-signup');
                }

            });
        }
    }

    // Customer Authentication
    function authentication(PostUrl,dataForm) {
        $('.form-login .display-message').hide();
        $('.form-login .display-message').html('');
        if (dataForm.valid()){
            event.preventDefault();
            var param = dataForm.serialize();
            $.ajax({
                showLoader: true,
                url: PostUrl,
                data: param,
                type: "POST"
            }).done(function (data) {
                if(data.success !== 'false') {
                    if(data.redirect === 'false') {
                        showStep('select-birthday');
                        hideStep('block-customer-login');
                        hideStep('user-promo');
                    } else {
                        window.location.href = data.url;
                    }
                    return true;
                } else {
                    $('.form-login .display-message').html(data.message);
                    $('.form-login .display-message').show();
                }

            });
        }
    }

    // DOB Update
    function updateDob(UpdateDobUrl,dataForm) {
        $('.select-birthday .display-message').hide();
        $('.select-birthday .display-message').html('');
        if (dataForm.valid()){
            event.preventDefault();
            var param = dataForm.serialize();
            $.ajax({
                showLoader: true,
                url: UpdateDobUrl,
                data: param,
                type: "POST"
            }).done(function (data) {
                if(data.success !== 'false') {
                    if(data.redirect !== 'false') {
                       $('body').trigger('processStart');
                       window.location.href = data.url;
                    }
                    return true;
                } else {
                    $('.select-birthday .display-message').removeClass('success');
                    $('.select-birthday .display-message').html(data.message);
                    $('.select-birthday .display-message').addClass("error");
                    $('.select-birthday .display-message').show();
                }

            });
        }
    }

    //Customer Create
    function createCustomer(SignUpUrl,dataForm) {
        $('.form-signup .display-message').hide();
        $('.form-signup .display-message').html('');
        if (dataForm.valid()){
            event.preventDefault();
            var param = dataForm.serialize();
            $.ajax({
                showLoader: true,
                url: SignUpUrl,
                data: param,
                type: "POST"
            }).done(function (data) {
                if(data.success !== 'false') {
                    showStep('select-birthday');
                    hideStep('block-customer-login');
                    hideStep('user-promo');
                    $('.select-birthday .display-message').html(data.message);
                    $('.select-birthday .display-message').addClass("success");
                    $('.select-birthday .display-message').show();
                    return true;
                } else {
                    $('.form-signup .display-message').html(data.message);
                    $('.form-signup .display-message').show();
                }

            });
        }
    }

    //Validate Date
    function validateDate(DateValidateUrl,dataForm) {
        $('.select-birthday .display-message').hide();
        $('.select-birthday .display-message').html('');
        if (dataForm.valid()){
            event.preventDefault();
            var param = dataForm.serialize();
            $.ajax({
                showLoader: true,
                url: DateValidateUrl,
                data: param,
                type: "POST"
            }).done(function (data) {
                if(data.success !== 'false') {
                    if(data.dates) {
                        updateDateOptions(data.dates, data.selectedate);
                    }
                    if(data.months) {
                        updateMonthOptions(data.months, data.selectedmonth);
                    }

                    return true;
                } else {
                    $('.select-birthday .display-message').html(data.message);
                    $('.select-birthday .display-message').show();
                }

            });
        }
    }

    //Update Date Options
    function updateDateOptions(newOptions, selectedOption) {
        var $el = $("#dob-date-selector");
        $el.empty(); // remove old options
        var i =1;
        while (i <= newOptions) {

            if(i == selectedOption) {
                $el.append($("<option></option>")
                    .attr('selected','selected')
                    .attr("value", i).text(i));
            } else {
                $el.append($("<option></option>")
                    .attr("value", i).text(i));
            }
            i++;
        }
        $el.niceSelect('update');
    }

    //Update Month Options
    function updateMonthOptions(newOptions, selectedOption) {
        var $el = $("#dob-month-selector");
        $el.empty(); // remove old options
        var i=1;
        while (i <= newOptions) {
            if(i == selectedOption) {
                $el.append($("<option></option>")
                    .attr('selected','selected')
                    .attr("value", i).text(i));
            } else {
                $el.append($("<option></option>")
                    .attr("value", i).text(i));
            }
            i++;
        }
        $el.niceSelect('update');
    }
    
    return main;
});