<?php
/**
 * @author     Balance Internet
 * @package    Balance_AjaxLogin
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Balance\AjaxLogin\Controller\Customer;

use Magento\Customer\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator;

class Validate extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Translate\Inline\ParserInterface
     */
    protected $inlineParser;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Session
     */
    protected $session;

    /** @var Validator */
    protected $formKeyValidator;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Translate\Inline\ParserInterface $inlineParser
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Translate\Inline\ParserInterface $inlineParser,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        Session $customerSession,
        Validator $formKeyValidator
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->inlineParser = $inlineParser;
        $this->customerRepository = $customerRepository;
        $this->session = $customerSession;
        $this->formKeyValidator = $formKeyValidator;
    }

    /**
     * Ajax action for validate email
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $post = $this->getRequest()->getPost('login');
        if ($this->session->isLoggedIn() || !$this->formKeyValidator->validate($this->getRequest()) && !$post) {
            $this->_redirect('*/*/');
            return;
        }
        $error = false;
        if (!\Zend_Validate::is(trim($post['username']), 'EmailAddress')) {
            $error = true;
        }
        if ($error) {
            throw new \Exception();
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        try {
            $customer = $this->customerRepository->get($post['username']);
            $customerId = $customer->getId();
            if((isset($customerId)) && ($customerId !='')) {
                $response = ['success' => 'true'];
            } else {
                throw new \Exception('Customer Email is not available.');
            }

        } catch (\Exception $e) {
            $response = ['success' => 'false', 'message' => 'Customer Email is not available.'];
        }
        return $resultJson->setData($response);
    }
}