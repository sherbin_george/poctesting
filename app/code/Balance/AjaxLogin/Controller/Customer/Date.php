<?php
/**
 * @author     Balance Internet
 * @package    Balance_AjaxLogin
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Balance\AjaxLogin\Controller\Customer;

use Magento\Customer\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator;

class Date extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * Date constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * Ajax action for validate date
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultJsonFactory->create();
            try {
                $year  = $this->getRequest()->getPost('year');
                $month = $this->getRequest()->getPost('month');
                $date  = $this->getRequest()->getPost('date');

                $numberOfMonths = 12;
                $numberOfDates = date('t', mktime(0, 0, 0, $month, 1, $year));


                if($numberOfDates < $date) {
                    $date = 1;
                }

                if ((isset($numberOfDates)) && (isset($numberOfMonths))) {
                    $response = ['success' => 'true', 'dates' => $numberOfDates, 'months' => $numberOfMonths, 'selectedate' => $date, 'selectedmonth' => $month ];
                } else {
                    throw new \Exception('Please select valid date.');
                }

            } catch (\Exception $e) {
                $response = ['success' => 'false', 'message' => $e->getMessage()];
            }
            return $resultJson->setData($response);
        }
    }
}