<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Model;

use Balance\VipStatusCalculator\Api\Data\VipStatusInterface;
use Balance\VipStatusCalculator\Model\GenericVipStatusCollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class VipStatus.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class VipStatus extends AbstractModel implements VipStatusInterface, IdentityInterface
{
    /**
     * @const Cache tag.
     */
    const CACHE_TAG = 'balance_vipstatus';
    
    const STATUS_ENABLED = 1;
    
    /**
     * @var GenericVipStatusCollectionFactory
     */
    private $genericVipStatusCollectionFactory;
    
    /**
     * VipStatus constructor.
     *
     * @param \Magento\Framework\Model\Context                             $context
     * @param \Magento\Framework\Registry                                  $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null           $resourceCollection
     * @param GenericVipStatusCollectionFactory|null                       $genericVipStatusCollectionFactory
     * @param array                                                        $data
     *
     * @return void
     */
    public function __construct(\Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        GenericVipStatusCollectionFactory $genericVipStatusCollectionFactory = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->genericVipStatusCollectionFactory = $genericVipStatusCollectionFactory
            ? : ObjectManager::getInstance()->get(GenericVipStatusCollectionFactory::class);
    }
    
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->_init('Balance\VipStatusCalculator\Model\ResourceModel\VipStatus');
    }
    
    /**
     * Get identities.
     *
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * Set status id.
     *
     * @param int $statusId
     *
     * @return $this
     */
    public function setStatusId($statusId)
    {
        $this->setData(self::STATUS_ID, $statusId);
        
        return $this;
    }
    
    /**
     * Get Status Id.
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->getData(self::STATUS_ID);
    }
    
    /**
     * Set customer group id.
     *
     * @param int $customerGroupId
     *
     * @return $this
     */
    public function setCustomerGroupId($customerGroupId)
    {
        $this->setData(self::CUSTOMER_GROUP_ID, $customerGroupId);
    
        return $this;
    }
    
    /**
     * Get Customer Group Id.
     *
     * @return int
     */
    public function getCustomerGroupId()
    {
        return $this->getData(self::CUSTOMER_GROUP_ID);
    }
    
    /**
     * Set value of points range from.
     *
     * @param int $pointsRangeFrom
     *
     * @return $this
     */
    public function setPointsRangeFrom($pointsRangeFrom)
    {
        $this->setData(self::POINTS_RANGE_FROM, $pointsRangeFrom);
    
        return $this;
    }
    
    /**
     * Get value of Points Range From
     *
     * @return int
     */
    public function getPointsRangeFrom()
    {
        return $this->getData(self::POINTS_RANGE_FROM);
    }
    
    /**
     * Set value of points range to.
     *
     * @param int $pointsRangeTo
     *
     * @return $this
     */
    public function setPointsRangeTo($pointsRangeTo)
    {
        $this->setData(self::POINTS_RANGE_TO, $pointsRangeTo);
    
        return $this;
    }
    
    /**
     * Get value of Points Range To
     *
     * @return int
     */
    public function getPointsRangeTo()
    {
        return $this->getData(self::POINTS_RANGE_TO);
    }
    
    /**
     * Receive page store ids
     *
     * @return int[]
     */
    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }
}
