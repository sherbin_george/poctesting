<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Model;

use Balance\VipStatusCalculator\Api\Data;
use Balance\VipStatusCalculator\Model\VipStatusFactory;
use Balance\VipStatusCalculator\Model\ResourceModel\VipStatus\CollectionFactory;
use Balance\VipStatusCalculator\Model\ResourceModel\VipStatus as ResourceVipStatus;

use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class VipStatusRepository.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class VipStatusRepository implements \Balance\VipStatusCalculator\Api\VipStatusRepositoryInterface
{
    /**
     * @var ResourceVipStatus
     */
    protected $resource;
    
    /**
     * VipStatus entity factory.
     *
     * @var VipStatusFactory
     */
    protected $vipStatusFactory;
    
    /**
     * VipStatus collection factory.
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;
    
    /**
     * @var \Balance\VipStatusCalculator\Api\Data\VipStatusSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    
    /**
     * VipStatusRepository constructor.
     *
     * @param ResourceVipStatus             $resource
     * @param VipStatusFactory              $vipStatusFactory
     * @param CollectionFactory             $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param StoreManagerInterface         $storeManager
     *
     * @return void
     */
    public function __construct(
        ResourceVipStatus $resource,
        VipStatusFactory $vipStatusFactory,
        CollectionFactory $collectionFactory,
        Data\VipStatusSearchResultsInterface $searchResultsFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->vipStatusFactory = $vipStatusFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->storeManager = $storeManager;
    }
    
    /**
     * Save vip status.
     *
     * @param \Balance\VipStatusCalculator\Api\Data\VipStatusInterface $vipStatus
     *
     * @return $this|\Balance\VipStatusCalculator\Api\Data\VipStatusInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(Data\VipStatusInterface $vipStatus)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $vipStatus->setStoreId($storeId);
        try {
            $this->resource->save($vipStatus);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        
        return $vipStatus;
    }
    
    /**
     * Get status by id.
     *
     * @param int $statusId
     *
     * @return VipStatus
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($statusId)
    {
        /** @var VipStatus $vipStatus */
        $vipStatus = $this->vipStatusFactory->create();
        $this->resource->load($vipStatus, $statusId);
        if (!$vipStatus->getId()) {
            throw new NoSuchEntityException(__('VIP Status with id "%1" does not exist.', $vipStatus));
        }
        
        return $vipStatus;
    }
    
    /**
     * Delete status.
     *
     * @param \Balance\VipStatusCalculator\Api\Data\VipStatusInterface $vipStatus
     *
     * @return $this|bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(Data\VipStatusInterface $vipStatus)
    {
        try {
            $this->resource->delete($vipStatus);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        
        return true;
    }
    
    /**
     * Delete status by id.
     *
     * @param int $statusId
     *
     * @return $this|\Balance\VipStatusCalculator\Model\VipStatusRepository|bool
     *
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteById($statusId)
    {
        return $this->delete($this->getById($statusId));
    }
    
    /**
     * Get statuses list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     *
     * @return \Balance\VipStatusCalculator\Api\VipStatusRepositoryInterface
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $collection = $this->collectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $objects = [];
        foreach ($collection as $objectModel) {
            $objects[] = $objectModel;
        }
        $searchResults->setItems($objects);
        
        return $searchResults;
    }
}
