<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Model\ResourceModel\VipStatus;

use Balance\VipStatusCalculator\Api\Data\VipStatusInterface;
use Balance\VipStatusCalculator\Model\ResourceModel\AbstractCollection;

/**
 * Class Collection.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'status_id';
    
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Balance\VipStatusCalculator\Model\VipStatus',
            'Balance\VipStatusCalculator\Model\ResourceModel\VipStatus'
        );
        $this->_map['fields']['store'] = 'store_table.store_id';
    }
    
    /**
     * Perform operations after collection load
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        $entityMetadata = $this->metadataPool->getMetadata(VipStatusInterface::class);
        
        $this->performAfterLoad('balance_vipstatus_store', $entityMetadata->getLinkField());
        
        return parent::_afterLoad();
    }
    
    /**
     * Join store relation table if there is store filter
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        try {
            $entityMetadata = $this->metadataPool->getMetadata(VipStatusInterface::class);
            $this->joinStoreRelationTable('balance_vipstatus_store', $entityMetadata->getLinkField());
        } catch (\Exception $exception) {}
    }
    
    /**
     * Returns pairs status_id - title
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('status_id', 'title');
    }
    
    /**
     * Add filter by store
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        $this->performAddStoreFilter($store, $withAdmin);
        
        return $this;
    }
}
