<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */

namespace Balance\VipStatusCalculator\Model\ResourceModel\VipStatus\Relation\Store;

use Magento\Framework\EntityManager\Operation\ExtensionInterface;
use Balance\VipStatusCalculator\Api\Data\VipStatusInterface;
use Balance\VipStatusCalculator\Model\ResourceModel\VipStatus;
use Magento\Framework\EntityManager\MetadataPool;

/**
 * Class SaveHandler.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class SaveHandler implements ExtensionInterface
{
    /**
     * @var MetadataPool
     */
    protected $metadataPool;
    
    /**
     * @var VipStatus
     */
    protected $resourceVipStatus;
    
    /**
     * @param MetadataPool $metadataPool
     * @param VipStatus $resourceVipStatus
     */
    public function __construct(
        MetadataPool $metadataPool,
        VipStatus $resourceVipStatus
    ) {
        $this->metadataPool = $metadataPool;
        $this->resourceVipStatus = $resourceVipStatus;
    }
    
    /**
     * @param object $entity
     * @param array $arguments
     * @return object
     * @throws \Exception
     */
    public function execute($entity, $arguments = [])
    {
        $entityMetadata = $this->metadataPool->getMetadata(VipStatusInterface::class);
        $linkField = $entityMetadata->getLinkField();
        
        $connection = $entityMetadata->getEntityConnection();
        
        $oldStores = $this->resourceVipStatus->lookupStoreIds((int)$entity->getId());
        $newStores = (array)$entity->getStores();
        
        $table = $this->resourceVipStatus->getTable('balance_vipstatus_store');
        
        $delete = array_diff($oldStores, $newStores);
        if ($delete) {
            $where = [
                $linkField . ' = ?' => (int)$entity->getData($linkField),
                'store_id IN (?)' => $delete,
            ];
            $connection->delete($table, $where);
        }
        
        $insert = array_diff($newStores, $oldStores);
        if ($insert) {
            $data = [];
            foreach ($insert as $storeId) {
                $data[] = [
                    $linkField => (int)$entity->getData($linkField),
                    'store_id' => (int)$storeId,
                ];
            }
            $connection->insertMultiple($table, $data);
        }
        
        return $entity;
    }
}
