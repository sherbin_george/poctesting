<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Model\ResourceModel;

use Balance\VipStatusCalculator\Api\Data\VipStatusInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\RapidFlow\Exception;

/**
 * Class VipStatus.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class VipStatus extends AbstractDb
{
    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * @var EntityManager
     */
    protected $entityManager;
    
    /**
     * @var MetadataPool
     */
    protected $metadataPool;
    
    /**
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param EntityManager $entityManager
     * @param MetadataPool $metadataPool
     * @param string $connectionName
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        $connectionName = null
    ) {
        $this->_storeManager = $storeManager;
        $this->entityManager = $entityManager;
        $this->metadataPool = $metadataPool;
        parent::__construct($context, $connectionName);
    }
    
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('balance_vipstatus', 'status_id');
    }
    
    /**
     * Perform operations before object save
     *
     * @param AbstractModel $object
     * @return $this
     * @throws LocalizedException
     */
    protected function _beforeSave(AbstractModel $object)
    {
        if (!$this->getIsUniqueVipStatusToStores($object)) {
            throw new LocalizedException(
                __('A vip status with the same properties already exists in the selected store.')
            );
        }
        return $this;
    }
    
    /**
     * Check is unique vip status to stores.
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return bool
     */
    protected function getIsUniqueVipStatusToStores(AbstractModel $object)
    {
        try {
            $entityMetadata = $this->metadataPool->getMetadata(VipStatusInterface::class);
            $linkField = $entityMetadata->getLinkField();
            
            if ($this->_storeManager->hasSingleStore()) {
                $stores = [Store::DEFAULT_STORE_ID];
            } else {
                $stores = (array)$object->getData('stores');
            }
        
            $select = $this->getConnection()->select()
                ->from(['cb' => $this->getMainTable()])
                ->join(
                    ['cbs' => $this->getTable('balance_vipstatus_store')],
                    'cbs.' . $linkField . ' = cb.' . $linkField,
                    []
                )
                ->where('cbs.store_id IN (?)', $stores);
        
            if ($object->getId()) {
                $select->where('cb.status_id <> ?', $object->getId());
            }
        
            if ($this->getConnection()->fetchRow($select)) {
                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }
    
        return true;
    }
    
    /**
     * Get min rewards points by customer group and store id.
     *
     * @param int $customerGroupId
     * @param int $storeId
     *
     * @return array|bool
     */
    public function getMinPointsByCustomerGroup(int $customerGroupId, int $storeId)
    {
        try {
            $connection = $this->getConnection();
        
            $entityMetadata = $this->metadataPool->getMetadata(VipStatusInterface::class);
            $linkField = $entityMetadata->getLinkField();
            $stores = [$storeId, Store::DEFAULT_STORE_ID];
        
            $select = $connection->select()
                ->from(
                    ['cbs' => $this->getTable('balance_vipstatus_store')],
                    'cb.points_range_from'
                )
                ->join(
                    ['cb' => $this->getMainTable()],
                    'cbs.' . $linkField . ' = cb.' . $linkField,
                    []
                )
                ->where('cb.customer_group_id = ?', $customerGroupId)
                ->where('cbs.store_id IN (?)', $stores);
            
            return $connection->fetchOne($select);
        } catch (\Exception $exception) {
            return false;
        }
    }
    
    /**
     * Get store ids to which specified item is assigned.
     *
     * @param $statusId
     *
     * @return array
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function lookupStoreIds($statusId)
    {
        $connection = $this->getConnection();
        
        $entityMetadata = $this->metadataPool->getMetadata(VipStatusInterface::class);
        $linkField = $entityMetadata->getLinkField();
        
        $select = $connection->select()
            ->from(['cbs' => $this->getTable('balance_vipstatus_store')], 'store_id')
            ->join(
                ['cb' => $this->getMainTable()],
                'cbs.' . $linkField . ' = cb.' . $linkField,
                []
            )
            ->where('cb.' . $entityMetadata->getIdentifierField()  . ' = :status_id');
        
        return $connection->fetchCol($select, ['status_id' => (int)$statusId]);
    }
    
    /**
     * @param AbstractModel $object
     * @param mixed $value
     * @param null $field
     * @return bool|int|string
     * @throws LocalizedException
     * @throws \Exception
     */
    private function getVipStatusId(AbstractModel $object, $value, $field = null)
    {
        $entityMetadata = $this->metadataPool->getMetadata(VipStatusInterface::class);
        if (!is_numeric($value) && $field === null) {
            $field = 'status_id';
        } elseif (!$field) {
            $field = $entityMetadata->getIdentifierField();
        }
        $entityId = $value;
        if ($field != $entityMetadata->getIdentifierField() || $object->getStoreId()) {
            $select = $this->_getLoadSelect($field, $value, $object);
            $select->reset(Select::COLUMNS)
                ->columns($this->getMainTable() . '.' . $entityMetadata->getIdentifierField())
                ->limit(1);
            $result = $this->getConnection()->fetchCol($select);
            $entityId = count($result) ? $result[0] : false;
        }
        return $entityId;
    }
    
    /**
     * Load an object
     *
     * @param \Balance\VipStatusCalculator\Model\VipStatus|AbstractModel $object
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     *
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function load(AbstractModel $object, $value, $field = null)
    {
        $vipStatusId = $this->getVipStatusId($object, $value, $field);
        if ($vipStatusId) {
            $this->entityManager->load($object, $vipStatusId);
        }
        
        return $this;
    }
    
    /**
     * @param AbstractModel $object
     * @return $this
     * @throws \Exception
     */
    public function save(AbstractModel $object)
    {
        $this->entityManager->save($object);
        
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    public function delete(AbstractModel $object)
    {
        $this->entityManager->delete($object);
        
        return $this;
    }
}
