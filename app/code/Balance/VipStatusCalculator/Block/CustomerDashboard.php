<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */

namespace Balance\VipStatusCalculator\Block;

use Magento\Backend\Block\Template\Context;
use \Magento\Customer\Model\Session;
use \Magento\Customer\Model\Group;

/**
 * Class CustomerDashboard.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class CustomerDashboard extends \Magento\Backend\Block\Template
{
    /**
     * @const Customer dashboard block name.
     */
    const CUSTOMER_DASHBOARD_BLOCK = 'customer_dashboard_%s';
    
    /**
     * @var Group
     */
    private $customerGroup;
    
    /**
     * @var Session
     */
    private $session;
    
    /**
     * @var string
     */
    protected $_vipMemberContent;
    
    /**
     * AccountMenu constructor.
     *
     * @param Context      $context
     * @param Session      $customerSession
     * @param Group        $customerGroup
     * @param BlockFactory $blockFactory
     * @param array        $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Group $customerGroup,
        array $data = []
    ) {
        $this->customerGroup = $customerGroup;
        $this->session = $customerSession;
        
        parent::__construct($context, $data);
    }
    
    /**
     * Get current customer.
     *
     * @return \Magento\Customer\Model\Customer
     */
    private function _getCustomer()
    {
        return $this->session->getCustomer();
    }
    
    /**
     * Get customer group name.
     *
     * @return string
     */
    public function getCustomerGroupName()
    {
        $currentGroupId = $this->_getCustomer()->getGroupId();
        $collection = $this->customerGroup->load($currentGroupId);
        
        return (string) $collection->getCustomerGroupCode();
    }
    
    /**
     * Get customer block identificator.
     *
     * @return string
     */
    public function getCustomerBlockId()
    {
        $customerGroupName = str_replace(' ', '_', strtolower($this->getCustomerGroupName()));
        
        return sprintf(self::CUSTOMER_DASHBOARD_BLOCK, $customerGroupName);
    }
}
