<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance_VipStatusCalculator\Index
 * @package   Balance_VipStatusCalculator\Index
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */

namespace Balance\VipStatusCalculator\Controller\Index;

use Balance\VipStatusCalculator\Model\ResourceModel\VipStatusFactory;
use \Magento\Customer\Model\Session;
use Mirasvit\Rewards\Helper\Balance;
use Mirasvit\Rewards\Model\TransactionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class AjaxCalculator.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class AjaxCalculator extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;
    
    /**
     * @var Session
     */
    private $session;
    
    /**
     * @var \Balance\VipStatusCalculator\Model\ResourceModel\VipStatusFactory
     */
    protected $vipStatusFactory;
    
    /**
     * @var \Mirasvit\Rewards\Helper\Data
     */
    protected $rewardsData;
    
    /**
     * @var \Mirasvit\Rewards\Helper\Balance
     */
    protected $rewardsBalance;
    
    /**
     * @var \Mirasvit\Rewards\Model\TransactionFactory
     */
    protected $transactionFactory;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Balance\EasyEdit\Helper\Data         $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Session $customerSession,
        Balance $rewardsBalance,
        TransactionFactory $transactionFactory,
        VipStatusFactory $vipStatusFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->session = $customerSession;
        $this->rewardsBalance = $rewardsBalance;
        $this->transactionFactory = $transactionFactory;
        $this->vipStatusFactory = $vipStatusFactory;
        $this->_storeManager = $storeManager;
    }
    
    /**
     * Get current customer.
     *
     * @return \Magento\Customer\Model\Customer
     */
    private function _getCustomer()
    {
        return $this->session->getCustomer();
    }
    
    /**
     * Get customer Rewards points.
     *
     * @return string
     */
    private function getCustomerRewardPoints()
    {
        return $this->rewardsBalance->getBalancePoints($this->_getCustomer());
    }
    
    /**
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getCurrentStore()
    {
        return $this->_storeManager->getStore();
    }
    
    /**
     * Get minimum of points by customer group.
     *
     * @return int
     */
    private function getMinPointsByCustomerGroup(): int
    {
        /**
         * @var \Balance\VipStatusCalculator\Model\ResourceModel\VipStatus $vipStatus
         */
        $vipStatus = $this->vipStatusFactory->create();
        
        $pointsRangeFrom = $vipStatus->getMinPointsByCustomerGroup(
            $this->_getCustomer()->getGroupId(),
            $this->getCurrentStore()->getId()
        );
        
        return (int) $pointsRangeFrom;
    }
    
    /**
     * Get expiration day of vip status.
     *
     * @return null|string
     */
    private function getVipStatusExpiredDay()
    {
        $customer = $this->_getCustomer();
        $collection = $this->transactionFactory->create()
            ->getCollection()
            ->addFieldToFilter('customer_id', $customer->getId())
            ->addFieldToFilter('is_expired', 0)
            ->setOrder('expires_at', 'ASC');
        
        $customerPoints = $this->getCustomerRewardPoints();
        $minPointsByCustomerGroup = $this->getMinPointsByCustomerGroup();
        
        $expirationDay = null;
        if ($customerPoints && $minPointsByCustomerGroup) {
            $minPoints = $customerPoints;
            
            foreach ($collection->getItems() as $transaction) {
                /*** @var \Mirasvit\Rewards\Model\Transaction $transaction */
                $expiresAt = $transaction->getData('expires_at');
                if (empty($expiresAt)) {
                    continue;
                }
                $amount = (int) $transaction->getAmount();
                $minPoints = ($amount > 0) ? $minPoints - $amount : $amount - $minPoints;
                if ($minPoints <= $minPointsByCustomerGroup) {
                    $expirationDay = (new \DateTime($expiresAt))->format('d/m/Y');
                    break;
                }
            }
        }
        
        return $expirationDay;
    }
    
    /**
     * Get vip status expire day.
     *
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            $response = [
                'success' => true,
                'expiration_date' => $this->getVipStatusExpiredDay()
            ];
        } else {
            $response = [
                'success' => false,
                'message' => __('Something went wrong with the calculation.'),
            ];
        }
    
        return $this->_resultJsonFactory->create()->setData($response);
    }
}
