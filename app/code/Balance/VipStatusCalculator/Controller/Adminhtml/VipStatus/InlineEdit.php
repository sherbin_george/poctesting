<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance_VipStatusCalculator\Adminhtml\VipStatus
 * @package   Balance_VipStatusCalculator\Adminhtml\VipStatus
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Controller\Adminhtml\VipStatus;

use Balance\VipStatusCalculator\Api\Data\VipStatusInterface;
use Balance\VipStatusCalculator\Api\VipStatusRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Psr\Log\LoggerInterface;

/**
 * Class InlineEdit.
 *
 * @category  Balance_VipStatusCalculator\Adminhtml\VipStatus
 * @package   Balance_VipStatusCalculator\Adminhtml\VipStatus
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Balance_VipStatusCalculator::manage';
    
    /** @var VipStatusRepositoryInterface */
    protected $vipStatusRepository;
    
    /** @var \Balance\VipStatusCalculator\Model\VipStatus */
    protected $_vipStatus;
    
    /** @var \Magento\Framework\Controller\Result\JsonFactory  */
    protected $resultJsonFactory;
    
    /** @var \Magento\Framework\Api\DataObjectHelper  */
    protected $dataObjectHelper;
    
    /** @var \Psr\Log\LoggerInterface */
    protected $logger;
    
    /**
     * @param Action\Context $context
     * @param VipStatusRepositoryInterface $vipStatusRepository
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Action\Context $context,
        VipStatusRepositoryInterface $vipStatusRepository,
        JsonFactory $resultJsonFactory,
        DataObjectHelper $dataObjectHelper,
        LoggerInterface $logger
    ) {
        $this->vipStatusRepository = $vipStatusRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->logger = $logger;
        parent::__construct($context);
    }
    
    /**
     * Set vip status.
     *
     * @param VipStatusInterface $vipStatus
     *
     * @return $this
     */
    private function _setVipStatus(VipStatusInterface $vipStatus)
    {
        $this->_vipStatus = $vipStatus;
        
        return $this;
    }
    
    /**
     * Receive vip status.
     *
     * @return \Balance\VipStatusCalculator\Model\VipStatus
     */
    private function _getVipStatus()
    {
        return $this->_vipStatus;
    }
    
    /**
     * Update vip data.
     *
     * @param array $data
     *
     * @return void
     */
    private function _updateVipStatus(array $data)
    {
        $vipStatus = $this->_getVipStatus();
        $vipStatusData = array_merge(
            $vipStatus->getData(),
            $data
        );
        $this->dataObjectHelper->populateWithArray(
            $vipStatus,
            $vipStatusData,
            '\Balance\VipStatusCalculator\Api\Data\VipStatusInterface'
        );
    }
    
    /**
     * Save vip status with error catching
     *
     * @param VipStatusInterface $vipStatus
     *
     * @return void
     */
    protected function _saveVipStatus(VipStatusInterface $vipStatus)
    {
        try {
            $this->vipStatusRepository->save($vipStatus);
        } catch (\Magento\Framework\Exception\InputException $e) {
            $this->getMessageManager()->addErrorMessage($this->getErrorWithVipStatusId($e->getMessage()));
            $this->logger->critical($e);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->getMessageManager()->addErrorMessage($this->getErrorWithVipStatusId($e->getMessage()));
            $this->logger->critical($e);
        } catch (\Exception $e) {
            $this->getMessageManager()->addErrorMessage(
                $this->getErrorWithVipStatusId('We can\'t save the vip status.')
            );
            $this->logger->critical($e);
        }
    }
    
    /**
     * Add page title to error message
     *
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithVipStatusId($errorText)
    {
        return '[Vip Status ID: ' . $this->_getVipStatus()->getId() . '] ' . __($errorText);
    }
    
    /**
     * Get array with errors
     *
     * @return array
     */
    protected function getErrorMessages()
    {
        $messages = [];
        foreach ($this->getMessageManager()->getMessages()->getItems() as $error) {
            $messages[] = $error->getText();
        }
        return $messages;
    }
    
    /**
     * Check if errors exists
     *
     * @return bool
     */
    protected function isErrorExists()
    {
        return (bool)$this->getMessageManager()->getMessages(true)->getCount();
    }
    
    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
    
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }
    
        foreach (array_keys($postItems) as $statusId) {
            $this->_setVipStatus($this->vipStatusRepository->getById($statusId));
    
            $this->_updateVipStatus($postItems[$statusId]);
            $this->_saveVipStatus($this->_getVipStatus());
        }
    
        return $resultJson->setData([
            'messages' => $this->getErrorMessages(),
            'error' => $this->isErrorExists()
        ]);
    }
}
