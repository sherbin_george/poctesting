<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Controller\Adminhtml\VipStatus;

use Balance\VipStatusCalculator\Model\VipStatus;
use Magento\Backend\App\Action;
use Balance\VipStatusCalculator\Model\Page;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @const Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Balance_VipStatusCalculator::vipstatus_save';
    
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    
    /**
     * Constructor.
     *
     * @param Action\Context         $context
     * @param DataPersistorInterface $dataPersistor
     *
     * @return void
     */
    public function __construct(Action\Context $context, DataPersistorInterface $dataPersistor)
    {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }
    
    /**
     * Save action.
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $statusId = $this->getRequest()->getParam('status_id');
            
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = VipStatus::STATUS_ENABLED;
            }
            if (empty($data['status_id'])) {
                $data['status_id'] = null;
            }
            
            /** @var \Balance\VipStatusCalculator\Model\VipStatus $model */
            $model = $this->_objectManager->create('Balance\VipStatusCalculator\Model\VipStatus')->load($statusId);
            if (!$model->getId() && $statusId) {
                $this->messageManager->addError(__('This VIP Status no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            
            $model->setData($data);
            
            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the vip status.'));
                $this->dataPersistor->clear('balance_vipstatus');
                
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['status_id' => $model->getId()]);
                }
                
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            
            $this->dataPersistor->set('balance_vipstatus', $data);
            
            return $resultRedirect->setPath('*/*/edit', ['status_id' => $this->getRequest()->getParam('status_id')]);
        }
        
        return $resultRedirect->setPath('*/*/');
    }
}
