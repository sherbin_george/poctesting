<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Controller\Adminhtml\VipStatus;

use Balance\VipStatusCalculator\Model\VipStatusFactory;
use Magento\Framework\App\RequestInterface;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\Registry;

/**
 * Class Builder.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class Builder
{
    /**
     * @var VipStatusFactory
     */
    private $vipStatusFactory;
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;
    
    /**
     * Constructor.
     *
     * @param VipStatusFactory $vipStatusFactory
     * @param Logger           $logger
     * @param Registry         $registry
     *
     * @return void
     */
    public function __construct(
        VipStatusFactory $vipStatusFactory,
        Logger $logger,
        Registry $registry
    ) {
        $this->vipStatusFactory = $vipStatusFactory;
        $this->logger = $logger;
        $this->registry = $registry;
    }
    
    /**
     * Build custom vip status based on user request.
     *
     * @param RequestInterface $request
     *
     * @return \Balance\VipStatusCalculator\Model\VipStatus
     */
    public function build(RequestInterface $request)
    {
        $vipStatusId = (int) $request->getParam('status_id');
        /** @var $vipStatus \Balance\VipStatusCalculator\Model\VipStatus */
        $vipStatus = $this->vipStatusFactory->create();
        
        if ($vipStatusId) {
            try {
                $vipStatus->load($vipStatusId);
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
        
        $this->registry->register('current_vipstatus', $vipStatus);
        
        return $vipStatus;
    }
}
