<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Api\Data;

/**
 * VIP Status interface.
 *
 * @api
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
interface VipStatusInterface
{
    /**#@+
     * Constants defined for keys of the data array. Identical to the name of the getter in snake case
     */
    const ID = 'id';
    const STATUS_ID = 'status_id';
    const CUSTOMER_GROUP_ID = 'customer_group_id';
    const POINTS_RANGE_FROM = 'points_range_from';
    const POINTS_RANGE_TO = 'points_range_to';
    /**#@-*/
    
    /**
     * Get vip status id
     *
     * @return int|null
     */
    public function getId();
    
    /**
     * Set vip status id
     *
     * @param int $id
     *
     * @return $this
     */
    public function setId($id);
    
    /**
     * Set status id.
     *
     * @param int $statusId
     *
     * @return $this
     */
    public function setStatusId($statusId);
    
    /**
     * Get Status Id.
     *
     * @return int
     */
    public function getStatusId();
    
    /**
     * Set customer group id.
     *
     * @param int $customerGroupId
     *
     * @return $this
     */
    public function setCustomerGroupId($customerGroupId);
    
    /**
     * Get Customer Group Id.
     *
     * @return int
     */
    public function getCustomerGroupId();
    
    /**
     * Set value of points range from.
     *
     * @param int $pointsRangeFrom
     *
     * @return $this
     */
    public function setPointsRangeFrom($pointsRangeFrom);
    
    /**
     * Get value of Points Range From
     *
     * @return int
     */
    public function getPointsRangeFrom();
    
    /**
     * Set value of points range to.
     *
     * @param int $pointsRangeTo
     *
     * @return $this
     */
    public function setPointsRangeTo($pointsRangeTo);
    
    /**
     * Get value of Points Range To
     *
     * @return int
     */
    public function getPointsRangeTo();
}
