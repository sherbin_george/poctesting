<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Api;

use Balance\VipStatusCalculator\Api\Data\VipStatusInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * VIP Status CRUD interface.
 *
 * @api
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
interface VipStatusRepositoryInterface
{
    /**
     * Save status.
     *
     * @param \Balance\VipStatusCalculator\Api\Data\VipStatusInterface $page
     *
     * @return VipStatusInterface
     */
    public function save(VipStatusInterface $page);
    
    /**
     * Get status by id.
     *
     * @param int $id
     *
     * @return VipStatusInterface
     */
    public function getById($id);
    
    /**
     * Get list of statuses.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     *
     * @return VipStatusRepositoryInterface
     */
    public function getList(SearchCriteriaInterface $criteria);
    
    /**
     * Delete status.
     *
     * @param \Balance\VipStatusCalculator\Api\Data\VipStatusInterface $page
     *
     * @return $this
     */
    public function delete(VipStatusInterface $page);
    
    /**
     * Delete status by id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function deleteById($id);
}
