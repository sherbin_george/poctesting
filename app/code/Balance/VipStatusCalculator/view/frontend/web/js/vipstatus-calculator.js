/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category Balance
 * @package Balance_VipStatusCalculator
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */
define([
    'jquery'
], function ($) {
    'use strict';
    
    /**
     * VIP Status Calculator ajax widget.
     */
    $.widget('Balance.vipStatusCalculatorAjax', {
        vipStatusCalculatorContentElement: '#vip-status-calculator-content',
        vipStatusExpirationElement: '.vip-status-expiration',
        
        /**
         * Widget options list.
         * May be overridden when widget is instantiated.
         */
        options: {
            url: null
        },
        
        /**
         * Initializes widget instance.
         *
         * @return void
         */
        _create: function() {
            var self = this;
            $(document).ready(function() {
                self._processLoadVIPStatusExpiration();
            });
        },
        
        /**
         * Load widget contend by ajax
         *
         * @return void
         */
        _processLoadVIPStatusExpiration: function () {
            var self = this;
            try {
                $.ajax(
                    self._getUrlAction(),
                    {
                        type: 'POST',
                        dataType: 'json',
                        data: {},
                        cache: false,
                        context: this,
                        showLoader: true,
                        success: function (response) {
                            if (response === null || typeof response.success === 'undefined') {
                                return;
                            }
                            if (!response.success) {
                                return;
                            }
                            if (typeof response.expiration_date !== 'undefined') {
                                if (response.expiration_date !== null) {
                                    $(self.vipStatusExpirationElement).html(response.expiration_date);
                                    $(self.vipStatusCalculatorContentElement).show();
                                }
                            }
                        }
                    }
                );
            } catch (e){
            }
        },

        /**
         * Returns form action.
         *
         * @return {string}
         */
        _getUrlAction: function() {
            return this.options.url;
        }
    });
    
    return $.Balance.vipStatusCalculatorAjax;
});
