<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Cms\Model\BlockFactory;
use Magento\Eav\Setup\EavSetup;

/**
 * Class UpgradeData.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;
    
    /**
     * @var BlockFactory
     */
    protected $blockFactory;
    
    /**
     * UpgradeData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param BlockFactory    $modelBlockFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory, BlockFactory $modelBlockFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->blockFactory = $modelBlockFactory;
    }
    
    /**
     * @param $blockData
     */
    private function _saveCmsBlock($blockData)
    {
        try {
            $cmsBlock = $this->blockFactory->create();
            $copyrightBlock = $cmsBlock->load($blockData['identifier'], 'identifier');
    
            if (!$copyrightBlock->getId()) {
                $cmsBlock->setData($blockData)->save();
            } else {
                $copyrightBlock->setContent($blockData['content']);
                $copyrightBlock->getResource()->save($copyrightBlock);
            }
        } catch (\Exception $exception) {
        }
    }
    
    /**
     * Create cms blocks for each customer vip status.
     *
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     */
    private function _createVipStatusCustomerCmsBlock(EavSetup $eavSetup)
    {
        $vipStatuses = ['general', 'silver', 'gold'];
        $blockTitle = 'Customer Dashboard %s Customer Group';
        $blockIdentifier = 'customer_dashboard_%s';
    
        foreach ($vipStatuses as $vipStatus) {
            $cmsBlock = [
                'title' => sprintf($blockTitle, ucfirst($vipStatus)),
                'identifier' => sprintf($blockIdentifier, $vipStatus),
                'content' => '',
                'is_active' => 1,
                'stores' => 0,
            ];
            $this->_saveCmsBlock($cmsBlock);
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $this->_createVipStatusCustomerCmsBlock($eavSetup);
        }
        
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $content = '<div class="customer-account-widget-content">'
                . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" </div>';
    
            $vipStatuses = ['general', 'silver', 'gold'];
            $blockIdentifier = 'customer_dashboard_%s';
    
            foreach ($vipStatuses as $vipStatus) {
                $cmsBlock = [
                    'identifier' => sprintf($blockIdentifier, $vipStatus),
                    'content' => $content,
                ];
                $this->_saveCmsBlock($cmsBlock);
            }
        }
    
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $content = [
                'general' => '<div class="customer-account-widget-content">'
                    . '<div style="float: left;">'
                    . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 10% OFF!</strong></p></div>'
                    . '<div style="float: right;">'
                    . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>',
                'silver' => '<div class="customer-account-widget-content">'
                    . '<div style="float: left;">'
                    . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 50% OFF!</strong></p></div>'
                    . '<div style="float: right;">'
                    . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>',
                'gold' => '<div class="customer-account-widget-content">'
                    . '<div style="float: left;">'
                    . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 100% OFF!</strong></p></div>'
                    . '<div style="float: right;">'
                    . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>'
            ];
            
            $vipStatuses = ['general', 'silver', 'gold'];
            $blockIdentifier = 'customer_dashboard_%s';
        
            foreach ($vipStatuses as $vipStatus) {
                $cmsBlock = [
                    'identifier' => sprintf($blockIdentifier, $vipStatus),
                    'content' => $content[$vipStatus],
                ];
                $this->_saveCmsBlock($cmsBlock);
            }
        }
    
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $content = [
                'general' => '<div class="customer-account-widget-content">'
                    . '<div class="align-left">'
                    . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 10% OFF!</strong></p></div>'
                    . '<div class="align-right">'
                    . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>',
                'silver' => '<div class="customer-account-widget-content">'
                    . '<div class="align-left">'
                    . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 50% OFF!</strong></p></div>'
                    . '<div class="align-right">'
                    . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>',
                'gold' => '<div class="customer-account-widget-content">'
                    . '<div class="align-left">'
                    . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 100% OFF!</strong></p></div>'
                    . '<div class="align-right">'
                    . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>'
            ];
    
            $vipStatuses = ['general', 'silver', 'gold'];
            $blockIdentifier = 'customer_dashboard_%s';
    
            foreach ($vipStatuses as $vipStatus) {
                $cmsBlock = [
                    'identifier' => sprintf($blockIdentifier, $vipStatus),
                    'content' => $content[$vipStatus],
                ];
                $this->_saveCmsBlock($cmsBlock);
            }
        }

        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $content = [
                'account_menu_' => [
                    'general' => '<div class="vip-status general">'
                        . '<p>Your VIP status</p>'
                        . '<p><strong>General</strong><br /><strong>Enjoy 10% OFF!</strong></p>'
                        . '</div>',
                    'silver' => '<div class="vip-status silver">'
                        . '<p>Your VIP status</p>'
                        . '<p><strong>Silver</strong><br /><strong>Enjoy 50% OFF!</strong></p>'
                        . '</div>',
                    'gold' => '<div class="vip-status gold">'
                        . '<p>Your VIP status</p>'
                        . '<p><strong>Gold</strong><br /><strong>Enjoy 100% OFF!</strong></p>'
                        . '</div>'
                ],
                'customer_dashboard_' => [
                    'general' => '<div class="customer-account-widget-content general">'
                        . '<p><strong>General</strong>'
                        . '<div class="align-left">'
                        . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 10% OFF!</strong></p></div>'
                        . '<div class="align-right">'
                        . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>',
                    'silver' => '<div class="customer-account-widget-content silver">'
                        . '<p><strong>Silver</strong>'
                        . '<div class="align-left">'
                        . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 50% OFF!</strong></p></div>'
                        . '<div class="align-right">'
                        . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>',
                    'gold' => '<div class="customer-account-widget-content gold">'
                        . '<p><strong>Gold</strong>'
                        . '<div class="align-left">'
                        . '<p>VIP Member Rewards Club status</p><p><strong>Enjoy 100% OFF!</strong></p></div>'
                        . '<div class="align-right">'
                        . '{{widget type="Balance\VipStatusCalculator\Block\Widget\VipStatusCalculator" }}</div></div>'
                ]
            ];

            $vipStatuses = ['general', 'gold', 'silver'];
            foreach ($vipStatuses as $vipStatus) {
                foreach ($content as $cmsIdentifier => $cmsBlockType) {
                    $this->_saveCmsBlock([
                        'identifier' => $cmsIdentifier . $vipStatus,
                        'content'    => $cmsBlockType[$vipStatus],
                        'title' => ucwords(str_replace('_', ' ', $cmsIdentifier) . $vipStatus),
                        'is_active' => 1,
                        'stores' => 0,
                    ]);
                }
            }
        }

        $setup->endSetup();
    }

}
