<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */

namespace Balance\VipStatusCalculator\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Create VIP Status To Store Linkage Table.
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     *
     * @return void
     */
    private function _createBalanceVipStatusStoreTable(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        
        try {
            $table = $connection->newTable(
                $setup->getTable('balance_vipstatus_store')
            )->addColumn(
                'status_id',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Status ID'
            )->addColumn(
                'store_id',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Store Id'
            )->addIndex(
                $setup->getIdxName('balance_vipstatus_store', ['status_id', 'store_id']),
                ['status_id', 'store_id']
            )->addForeignKey(
                $setup->getFkName(
                    'balance_vipstatus_store',
                    'status_id',
                    'balance_vipstatus',
                    'status_id'
                ),
                'status_id',
                $setup->getTable('balance_vipstatus'),
                'status_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $setup->getFkName(
                    'balance_vipstatus_store',
                    'store_id',
                    'store',
                    'store_id'
                ),
                'store_id',
                $setup->getTable('store'),
                'store_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'VIP Status To Store Linkage Table'
            );
        
            $connection->createTable($table);
        } catch (\Zend_Db_Exception $e) {
            echo "<pre>!!!!!!!!!!!";
                var_dump($e->getMessage());
                var_dump($e->getLine());
            echo "</pre>";
        }
    }
    
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
    
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $connection = $setup->getConnection();
    
            try {
                // change balance_vipstatus table
                $connection->changeColumn(
                    $setup->getTable('balance_vipstatus'),
                    'status_id',
                    'status_id',
                    [
                        'type' => Table::TYPE_SMALLINT,
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                        'comment' => 'Status ID'
                    ]
                );
                $connection->changeColumn(
                    $setup->getTable('balance_vipstatus'),
                    'customer_group_id',
                    'customer_group_id',
                    [
                        'type' => Table::TYPE_SMALLINT,
                        'unsigned' => true,
                        'nullable' => false,
                        'comment' => 'Customer Group Id'
                    ]
                );
                $connection->addIndex(
                    $setup->getTable('balance_vipstatus'),
                    $setup->getIdxName(
                        $setup->getTable('balance_vipstatus'),
                        ['status_id', 'customer_group_id', 'points_range_from', 'points_range_to']
                    ),
                    ['status_id', 'customer_group_id', 'points_range_from', 'points_range_to']
                );
                $connection->addForeignKey(
                    $setup->getFkName('balance_vipstatus', 'customer_group_id', 'customer_group', 'customer_group_id'),
                    $setup->getTable('balance_vipstatus'), 'customer_group_id',
                    $setup->getTable('customer_group'), 'customer_group_id'
                );
    
                // create a new balance_vipstatus_store table
                $this->_createBalanceVipStatusStoreTable($setup);
            } catch (\Zend_Db_Exception $e) {
                echo "<pre>!!!!!!!!!!!";
                var_dump($e->getMessage());
                var_dump($e->getLine());
                echo "</pre>";
            }
        }
    
        $setup->endSetup();
    }
}
