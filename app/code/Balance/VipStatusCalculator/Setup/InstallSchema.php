<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
namespace Balance\VipStatusCalculator\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema.
 *
 * @category  Balance
 * @package   Balance_VipStatusCalculator
 * @author    Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://balanceinternet.com.au/
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installation schema method.
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        //START table setup
        try {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('balance_vipstatus')
            )->addColumn(
                'status_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true,],
                'Entity ID'
            )->addColumn(
                'customer_group_id',
                Table::TYPE_SMALLINT,
                5,
                ['nullable' => false,],
                'Customer Group'
            )->addColumn(
                'points_range_from',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => 0],
                'Points Range From'
            )->addColumn(
                'points_range_to',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => 0],
                'Points Range To'
            )->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Creation Time'
            )->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Modification Time'
            );
            $installer->getConnection()->createTable($table);
        } catch (\Zend_Db_Exception $e) {
        }
        
        //END   table setup
        $installer->endSetup();
    }
}
