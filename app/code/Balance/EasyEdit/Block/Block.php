<?php

/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Balance\EasyEdit\Block;

/**
 * Cms block content block
 */
class Block extends \Magento\Cms\Block\Block
{
    /*
     * block template
     */
    const BLOCK_TEMPLATE = 'Balance_EasyEdit::links.phtml';

    /**
     * Url interface.
     *
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * Custom helper.
     *
     * @var \Balance\EasyEdit\Helper\Data
     */
    protected $_helper;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context    $context
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Cms\Model\BlockFactory            $blockFactory
     * @param array                                      $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Balance\EasyEdit\Helper\Data $helper,
        array $data = []
    ) {
        $this->_backendUrl = $backendUrl;
        $this->_helper = $helper;
        parent::__construct($context, $filterProvider, $storeManager, $blockFactory, $data);
    }

    /**
     * Prepare Content HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $blockId = $this->getBlockId();
        $html = '';

        if ($blockId) {
            $storeId = $this->_storeManager->getStore()->getId();
            /** @var \Magento\Cms\Model\Block $block */
            $block = $this->_blockFactory->create();
            $block->setStoreId($storeId)->load($blockId);
            if ($block->isActive()) {
                // Check permission t show edit link.
                $params = array(
                    'id' => $block->getId(),
                    'type' => 'block',
                    '_nosecret' => true
                );
                $url = $this->_backendUrl->getUrl("easyedit/index", $params);
                $html .= '<div class="wrap-easy-edit">';
                $html .= '<div style="display:none" class="easy-edit-pos easy-edit easy-edit-block easy-edit-type-' .
                    \Balance\EasyEdit\Helper\Data::BLOCK_TYPE . '">';
                $html .= $this->getLink($url);
                $html .= '</div>';
                $html .= $this->_filterProvider->getBlockFilter()->setStoreId($storeId)->filter($block->getContent());
                $html .= '</div>';
            }
        }
        return $html;
    }

    /**
     * Get link to back end.
     *
     * @param $url
     * @return string
     */
    protected function getLink($url)
    {
        $block = $this->_layout->createBlock('Magento\Framework\View\Element\Template');
        $block->setText(__('Edit'));
        $block->setEditUrl($url);
        $block->setTemplate(self::BLOCK_TEMPLATE);

        return $block->toHtml();
    }

}
