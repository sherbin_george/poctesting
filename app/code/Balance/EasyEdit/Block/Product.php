<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Block;

/**
 * Category Block
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class Product extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * Data constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Backend\Model\UrlInterface              $backendUrl
     * @param \Magento\Framework\Registry                      $registry
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_backendUrl = $backendUrl;
        $this->_registry = $registry;
    }

    /**
     * get admin url
     *
     * @return string
     */
    public function getAdminUrl()
    {
        $productId = $this->getCurrentProduct();
        if (!$productId->getId()) {
            return '';
        }
        $params = array(
            'id' => $productId->getId(),
            'type' => 'product',
            '_nosecret' => true
        );
        return $this->_backendUrl->getUrl("easyedit/index", $params);
    }

    /**
     * get current product
     *
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * get content type
     *
     * @return int
     */
    public function getContentType()
    {
        return \Balance\EasyEdit\Helper\Data::PRODUCT_TYPE;
    }

}
