<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Block;

/**
 * Category Block
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class Category extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * Data constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Backend\Model\UrlInterface              $backendUrl
     * @param \Magento\Framework\Registry                      $registry
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_backendUrl = $backendUrl;
        $this->_registry = $registry;
    }

    /**
     * get admin url
     *
     * @return string
     */
    public function getAdminUrl()
    {
        $category = $this->getCurrentCategory();
        if (!$category->getId()) {
            return '';
        }
        $params = array(
            'id' => $category->getId(),
            'type' => 'category',
            '_nosecret' => true
        );
        return $this->_backendUrl->getUrl("easyedit/index", $params);
    }

    /**
     * get current category
     *
     * @return mixed
     */
    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    /**
     * get content type
     *
     * @return int
     */
    public function getContentType()
    {
        return \Balance\EasyEdit\Helper\Data::CATEGORY_TYPE;
    }

}
