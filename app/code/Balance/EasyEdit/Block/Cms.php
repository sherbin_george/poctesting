<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Block;

/**
 * Cms Block
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class Cms extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var \Magento\Cms\Model\Page
     */
    protected $_page;

    /**
     * Data constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Backend\Model\UrlInterface              $backendUrl
     * @param \Magento\Cms\Model\Page                          $page
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Cms\Model\Page $page,
        array $data = []

    ) {
        parent::__construct($context, $data);
        $this->_backendUrl = $backendUrl;
        $this->_page = $page;
    }

    /**
     * get admin url
     *
     * @return string
     */
    public function getAdminUrl()
    {
        if (!$this->_page->getId()) {
            return '';
        }
        $params = array(
            'id' => $this->_page->getId(),
            'type' => 'cms',
            '_nosecret' => true
        );
        return $this->_backendUrl->getUrl("easyedit/index", $params);
    }

    /**
     * get content type
     *
     * @return int
     */
    public function getContentType()
    {
        return \Balance\EasyEdit\Helper\Data::CMS_TYPE;
    }

}
