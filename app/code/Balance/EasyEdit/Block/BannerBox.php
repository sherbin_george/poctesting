<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Block;

/**
 * Rewrite balance box banner block
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class BannerBox extends \Balance\Box\Block\BannerBox
{
    /*
     * box template
     */
    const BOX_TEMPLATE = 'Balance_EasyEdit::easy_edit_box.phtml';

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * _helper
     *
     * @var \Balance\EasyEdit\Helper\Data
     */
    protected $_helper;

    /**
     * Data constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context       $context
     * @param \Balance\Box\Model\BoxFactory                          $boxFactory
     * @param \Balance\Box\Model\GroupFactory                        $groupFactory
     * @param \Balance\Box\Model\ResourceModel\Box\CollectionFactory $boxCollectionFactory
     * @param \Magento\Framework\Registry                            $identifier
     * @param \Magento\Framework\Stdlib\DateTime                     $datetime
     * @param array                                                  $data
     * @param \Magento\Backend\Model\UrlInterface                    $backendUrl
     * @param \Balance\EasyEdit\Helper\Data                          $helper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Balance\Box\Model\BoxFactory $boxFactory,
        \Balance\Box\Model\GroupFactory $groupFactory,
        \Balance\Box\Model\ResourceModel\Box\CollectionFactory $boxCollectionFactory,
        \Magento\Framework\Registry $identifier,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        array $data = [],
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Balance\EasyEdit\Helper\Data $helper

    ) {
        parent::__construct($context, $boxFactory, $groupFactory, $boxCollectionFactory, $identifier, $datetime, $data);
        $this->_backendUrl = $backendUrl;
        $this->_helper = $helper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getBox()) {
            return '';
        }

        $easyEditElem = '';
        $params = array(
            'id' => $this->getBox()->getId(),
            'type' => 'box',
            '_nosecret' => true
        );
        $easyEditElem = $this->setEasyEditTemplate($this->_backendUrl->getUrl("easyedit/index", $params));
        $id = $this->escapeQuote($this->getBox()->getIdentifier());
        $script = "<script class='easy-edit-script'>
        require([
            'jquery'
        ], function ($) {
            $(document).ready(function(){
                var easyEditElem = `$easyEditElem`;
                var id = '$id';
                $('#js__categorybanner-balance-box-' + id).addClass('wrap-easy-edit').prepend(easyEditElem);
            });
        });
        </script>";
        $html = parent::_toHtml() . $script;

        return $html;
    }


    /**
     * set easy edit template
     *
     * @param $url
     * @return string
     */
    protected function setEasyEditTemplate($url)
    {
        $block = $this->_layout->createBlock('Magento\Framework\View\Element\Template');
        $block->setText(__('Edit'));
        $block->setEasyEditClass('easy-edit-box easy-edit-pos');
        $block->setAdminUrl($url);
        $block->setTemplate(self::BOX_TEMPLATE);

        return $block->toHtml();
    }

}