<?php

/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Balance\EasyEdit\Block\Widget;

/**
 * Cms block content block
 */
class Block extends \Magento\Cms\Block\Widget\Block
{
    /*
     * block template
     */
    const BLOCK_TEMPLATE = 'Balance_EasyEdit::links.phtml';

    /**
     * Url interface.
     *
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * Custom helper.
     *
     * @var \Balance\EasyEdit\Helper\Data
     */
    protected $_helper;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context    $context
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Cms\Model\BlockFactory            $blockFactory
     * @param array                                      $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Balance\EasyEdit\Helper\Data $helper,
        array $data = []
    ) {
        $this->_backendUrl = $backendUrl;
        $this->_helper = $helper;
        parent::__construct($context, $filterProvider, $blockFactory, $data);
    }

    /**
     * Prepare block text and determine whether block output enabled or not
     * Prevent blocks recursion if needed
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();

        $blockId = $this->getData('block_id');
        $blockHash = get_class($this) . $blockId;

        if (isset(self::$_widgetUsageMap[$blockHash])) {
            return $this;
        }
        self::$_widgetUsageMap[$blockHash] = true;

        if ($blockId) {
            $storeId = $this->_storeManager->getStore()->getId();
            /** @var \Magento\Cms\Model\Block $block */
            $block = $this->_blockFactory->create();
            $block->setStoreId($storeId)->load($blockId);
            if ($block->isActive()) {
                $html = '';
                // Check permission t show edit link.
                $params = array(
                    'id' => $blockId,
                    'type' => 'block',
                    '_nosecret' => true
                );
                $url = $this->_backendUrl->getUrl("easyedit/index", $params);
                $html .= '<div class="wrap-easy-edit">';
                $html .= '<div class="easy-edit-pos easy-edit easy-edit-block easy-edit-type-' .
                    \Balance\EasyEdit\Helper\Data::BLOCK_TYPE . '">';
                $html .= $this->getLink($url);
                $html .= '</div>';
                $html .= $this->_filterProvider->getBlockFilter()->setStoreId($storeId)->filter($block->getContent());
                $html .= '</div>';
                $this->setText(
                    $html
                );
            }
        }

        unset(self::$_widgetUsageMap[$blockHash]);
        return $this;
    }

    /**
     * Get link to back end.
     *
     * @param $url
     * @return string
     */
    protected function getLink($url)
    {
        $block = $this->_layout->createBlock('Magento\Framework\View\Element\Template');
        $block->setText(__('Edit'));
        $block->setEditUrl($url);
        $block->setTemplate(self::BLOCK_TEMPLATE);

        return $block->toHtml();
    }

}
