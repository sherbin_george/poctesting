<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Block\Widget;

/**
 * Rewrite balance box widget group
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class Group extends \Balance\Box\Block\Widget\Group
{
    /*
     * box template
     */
    const BOX_TEMPLATE = 'Balance_EasyEdit::easy_edit_box.phtml';

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * _helper
     *
     * @var \Balance\EasyEdit\Helper\Data
     */
    protected $_helper;

    /**
     * Data constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context       $context
     * @param \Balance\Box\Model\BoxFactory                          $boxFactory
     * @param \Balance\Box\Model\GroupFactory                        $groupFactory
     * @param \Balance\Box\Model\ResourceModel\Box\CollectionFactory $boxCollectionFactory
     * @param \Magento\Framework\Registry                            $identifier
     * @param \Magento\Framework\Stdlib\DateTime                     $datetime
     * @param array                                                  $data
     * @param \Magento\Backend\Model\UrlInterface                    $backendUrl
     * @param \Balance\EasyEdit\Helper\Data                          $helper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Balance\Box\Model\BoxFactory $boxFactory,
        \Balance\Box\Model\GroupFactory $groupFactory,
        \Balance\Box\Model\ResourceModel\Box\CollectionFactory $boxCollectionFactory,
        \Magento\Framework\Registry $identifier,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        array $data = [],
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Balance\EasyEdit\Helper\Data $helper

    ) {
        parent::__construct($context, $boxFactory, $groupFactory, $boxCollectionFactory, $identifier, $datetime, $data);
        $this->_backendUrl = $backendUrl;
        $this->_helper = $helper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getGroup() || !$this->getBoxes()) {
            return '';
        }

        $easyEditElem = '';
        $script = '';
        //group
        $params = array(
            'id' => $this->getGroup()->getId(),
            'type' => 'box_group',
            '_nosecret' => true
        );
        $easyEditElem = $this->setEasyEditTemplate($this->_backendUrl->getUrl("easyedit/index", $params));

        //box
        $borderColor = $this->_helper->getConfig(\Balance\EasyEdit\Helper\Data::XML_PATH_BORDER_COLOR);
        $textColor = $this->_helper->getConfig(\Balance\EasyEdit\Helper\Data::XML_PATH_TEXT_COLOR);
        $backgroundColor = $this->_helper->getConfig(\Balance\EasyEdit\Helper\Data::XML_PATH_BACKGROUND_COLOR);
        $text = __('Edit');
        $boxIds = json_encode($this->getBoxIds());
        $params = array(
            'type' => 'box',
            '_nosecret' => true
        );
        $url = $this->_backendUrl->getUrl("easyedit/index", $params);
        $id = $this->escapeQuote($this->getGroup()->getIdentifier());
        $script = "<script class=easy-edit-script>
        require([
            'jquery'
        ], function ($) {
            $(document).ready(function(){
                var boxIds = $boxIds;
                var url = '$url';
                var text = '$text';
                var borderColor = '$borderColor';
                var textColor = '$textColor';
                var backgroundColor = '$backgroundColor';
                var id = '$id';
                $('#js__balance-box-group-' + id + ' .balance-box-group-slide').each(function(i) {
                    $(this).addClass('wrap-easy-edit');
                    var easyEditElem = '<div class=\"" . "easy-edit easy-edit-box easy-edit-pos easy-edit-type-" .
                        \Balance\EasyEdit\Helper\Data::BALANCE_BOX_TYPE . "\">' +
                        '<a class=link-button href=' + url +
                        'id/' + boxIds[i] + '/' + ' target=_blank' +
                        ' style=border-color:' + borderColor + ';' +  'color:' + textColor + ';' +
                        'background-color:' + backgroundColor + ';>' +
                        '<span class=i-name>' + text + '</span></a><div class=clear-both></div></div>';
                    $(this).prepend(easyEditElem);
                });
            });
        });
        </script>";
        $html = '<div class="wrap-easy-edit">' . $easyEditElem . parent::_toHtml() . '</div>' . $script;

        return $html;
    }

    /**
     * get box ids
     *
     * @return array
     */
    protected function getBoxIds()
    {
        $boxIds = [];
        foreach ($this->getBoxes() as $box) {
            array_push($boxIds, $box->getId());
        }
        return $boxIds;
    }

    /**
     * set easy edit template
     *
     * @param $url
     * @return string
     */
    protected function setEasyEditTemplate($url)
    {
        $block = $this->_layout->createBlock('Magento\Framework\View\Element\Template');
        $block->setText(__('Edit'));
        $block->setEasyEditClass('box-group');
        $block->setAdminUrl($url);
        $block->setTemplate(self::BOX_TEMPLATE);

        return $block->toHtml();
    }

}