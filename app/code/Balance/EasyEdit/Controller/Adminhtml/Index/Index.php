<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Controller\Adminhtml\Index;

/**
 * Index
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $_responseFactory;

    /**
     * @var \Balance\EasyEdit\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\App\ResponseFactory $responseFactory
     * @param \Balance\EasyEdit\Helper\Data          $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Balance\EasyEdit\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->_responseFactory = $responseFactory;
        $this->_helper = $helper;
    }

    /**
     * set redirect url
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id', '0');
        $type = $this->getRequest()->getParam('type', '');

        $redirectUrl = $this->_helper->getUrl($id, $type);
        $this->_responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
        exit();
    }
}