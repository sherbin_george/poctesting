<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Controller\Index;

/**
 * Index
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Balance\EasyEdit\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Balance\EasyEdit\Helper\Data         $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Balance\EasyEdit\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->_helper = $helper;
        $this->_resultJsonFactory = $resultJsonFactory;
    }

    /**
     * check permission type to load js again
     *
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $permissionType = array();
        $contentTypes = $this->_helper->getConfig(\Balance\EasyEdit\Helper\Data::XML_PATH_CONTENT_TYPES);
        $contentTypes = explode(',', $contentTypes);
        foreach ($contentTypes as $type) {
            if ($this->_helper->isEnabled($type)) {
                array_push($permissionType, $type);
            }
        }
        $result = $this->_resultJsonFactory->create();
        return $result->setData($permissionType);
    }
}
