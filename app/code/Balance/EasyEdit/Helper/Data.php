<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Helper;

/**
 * EasyEdit helper
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * easyedit config path
     */
    const XML_PATH_EASYEDIT = 'easyedit/easyedit/active';

    /**
     * content type config path
     */
    const XML_PATH_CONTENT_TYPES = 'easyedit/easyedit/content_types';

    /**
     * color text config path
     */
    const XML_PATH_BORDER_COLOR = 'easyedit/easyedit/border_color';

    /**
     * color text config path
     */
    const XML_PATH_TEXT_COLOR = 'easyedit/easyedit/text_color';

    /**
     * background color config path
     */
    const XML_PATH_BACKGROUND_COLOR = 'easyedit/easyedit/background_color';

    /**
     * Admin session lifetime config path
     */
    const XML_PATH_ADMIN_SESSION_LIFETIME = 'admin/security/session_lifetime';

    /**
     * Home page config path
     */
    const XML_PATH_HOME_PAGE = 'web/default/cms_home_page';

    /**
     * No route page config path
     */
    const XML_PATH_NO_ROUTE = 'web/default/cms_no_route';


    /**
     * easy edit resource
     */
    const EASYEDIT_RESOURCE = 'Balance_EasyEdit::config_easyedit';

    /**
     * easy edit resource
     */
    const ADMIN_PERMISSION = 'admin-permission';

    /**
     * cms type
     */
    const CMS_TYPE = 1;

    /**
     * block type
     */
    const BLOCK_TYPE = 2;

    /**
     * category type
     */
    const CATEGORY_TYPE = 3;

    /**
     * product type
     */
    const PRODUCT_TYPE = 4;

    /**
     * balance box type
     */
    const BALANCE_BOX_TYPE = 5;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var Magento\Backend\Helper\Data
     */
    protected $_helperBackend;

    /**
     * Data constructor
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Backend\Helper\Data $helperBackend
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_cookieManager = $cookieManager;
        $this->_backendUrl = $backendUrl;
        $this->_helperBackend = $helperBackend;
    }

    /**
     * Check whether easyedit module is enabled in system config
     *
     * @return boolean
     */
    public function isEnabled($type)
    {
        if (!$this->_cookieManager->getCookie(self::ADMIN_PERMISSION)) {
            return false;
        }

        if (!$this->getConfig(self::XML_PATH_EASYEDIT)) {
            return false;
        }

        $contentTypes = $this->getConfig(self::XML_PATH_CONTENT_TYPES);
        $contentTypes = explode(',', $contentTypes);
        if (!in_array("0", $contentTypes) && !in_array($type, $contentTypes)) {
            return false;
        }
        return true;
    }

    /**
     * get config
     *
     * @param $configPath
     * @return mixed
     */
    public function getConfig($configPath)
    {
        return $this->_scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get url for each type
     *
     * @param $id
     * @param $type
     * @return string
     */
    public function getUrl($id, $type)
    {
        switch ($type) {
            case 'cms':
                $url = $this->_backendUrl->getUrl('cms/page/edit', array('page_id' => $id));
                break;
            case 'block':
                $url = $this->_backendUrl->getUrl('cms/block/edit', array('block_id' => $id));
                break;
            case 'box_group':
                $url = $this->_backendUrl->getUrl('box/group/edit', array('group_id' => $id));
                break;
            case 'box':
                $url = $this->_backendUrl->getUrl('box/box/edit', array('box_id' => $id));
                break;
            case 'category':
                $url = $this->_backendUrl->getUrl('catalog/category/edit', array('id' => $id));
                break;
            case 'product':
                $url = $this->_backendUrl->getUrl('catalog/product/edit', array('id' => $id));
                break;
            default:
                $url = $this->_helperBackend->getHomePageUrl();
                break;
        }
        return $url;
    }
}
