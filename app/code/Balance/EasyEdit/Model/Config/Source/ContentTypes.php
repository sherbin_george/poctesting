<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Model\Config\Source;

/**
 * Content type source
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class ContentTypes implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * content type value
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'label' => __('-- Please select --'),
                'value' => '0',
            ],
            [
                'label' => __('CMS Pages'),
                'value' => \Balance\EasyEdit\Helper\Data::CMS_TYPE,
            ],
            [
                'label' => __('Blocks'),
                'value' => \Balance\EasyEdit\Helper\Data::BLOCK_TYPE,
            ],
            [
                'label' => __('Categories'),
                'value' => \Balance\EasyEdit\Helper\Data::CATEGORY_TYPE,
            ],
            [
                'label' => __('Product Pages'),
                'value' => \Balance\EasyEdit\Helper\Data::PRODUCT_TYPE,
            ],
            [
                'label' => __('Balance Box'),
                'value' => \Balance\EasyEdit\Helper\Data::BALANCE_BOX_TYPE,
            ],
        ];
        return $options;
    }
}