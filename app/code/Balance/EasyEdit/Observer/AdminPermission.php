<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\EasyEdit\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * AdminPermission observer
 *
 * @author      Tuan Nguyen <tuan@balanceinternet.com.au>
 */
class AdminPermission implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_authorization = $authorization;
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
    }

    /**
     * save cookie permission
     * (non-PHPdoc)
     * @see \Magento\Framework\Event\ObserverInterface::execute()
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $adminSessionLifetime = $this->_scopeConfig->getValue(
            \Balance\EasyEdit\Helper\Data::XML_PATH_ADMIN_SESSION_LIFETIME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $metadata = $this->_cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($adminSessionLifetime)
            ->setPath('/');
        $this->_cookieManager->setPublicCookie(
            \Balance\EasyEdit\Helper\Data::ADMIN_PERMISSION,
            $this->_authorization->isAllowed(\Balance\EasyEdit\Helper\Data::EASYEDIT_RESOURCE) ? 1 : 0,
            $metadata
        );
        return;
    }
}