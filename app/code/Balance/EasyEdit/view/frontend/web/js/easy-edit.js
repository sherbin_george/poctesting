require([
    'jquery'
], function ($) {
    $(document).ready(function(){
        /**
         * show the button for content types which have permission
         * @private
         */
        showLink();
    });

    /**
     * show link button
     */
    function showLink()
    {
        $.ajax({
            url: '/easyedit',
            type: 'POST',
            cache: true,
            success: function (response) {
                if (typeof response !== 'undefined' && response.length > 0) {
                    if ($.inArray("0", response) !== -1) {
                        $('div[class*=easy-edit-type-]').show();
                    } else {
                        for (var i = 0; i < response.length; i++) {
                            if ($('.easy-edit-type-' + response[i]).length > 0) {
                                $('.easy-edit-type-' + response[i]).show();
                            }
                        }
                        var removeScript = false;
                        $('div[class*=easy-edit-type-]').each(function(i){
                            var index = $(this).attr('class').substr($(this).attr('class').lastIndexOf("-") + 1);
                            if ($.inArray(index, response) === -1) {
                                if (index == '2') {
                                    removeWrap($(this).closest('.wrap-easy-edit'));
                                } else if (index == '5') {
                                    if (!removeScript) {
                                        $('.easy-edit-script').remove();
                                        removeScript = true;
                                    }
                                    if ($(this).hasClass('box-group')) {
                                        removeWrap($(this).closest('.wrap-easy-edit'));
                                    } else {
                                        $(this).closest('.wrap-easy-edit').removeClass('wrap-easy-edit');
                                    }
                                }
                                $(this).remove();
                            }
                        });
                        if ($(window).width() <= 770) {
                            $('.columns').addClass('display-block');
                        }
                    }
                    $('.easy-edit a.link-button').css({
                            'float': 'right',
                            'text-align': 'right',
                            'padding': '0px 6px',
                            'border': '1.5px solid',
                            'border-radius': '4px',
                            'line-height': '1.42857143',
                            'min-width': '0',
                            'text-transform': 'none',
                            'text-decoration': 'none'
                    }).hover(function() {
                        $(this).css('text-decoration', 'none');
                        $(this).closest('.wrap-easy-edit').addClass('easy-edit-border');
                    }, function() {
                        $(this).closest('.wrap-easy-edit').removeClass('easy-edit-border');
                    });
                } else {
                    $('.easy-edit-type-2, .easy-edit-type-5').each(function(){
                        if ($(this).is('.easy-edit-type-2') ||
                            ($(this).is('.easy-edit-type-5') && $(this).hasClass('easy-edit-box-group'))) {
                            removeWrap($(this).closest('.wrap-easy-edit'));
                        } else {
                            $(this).closest('.wrap-easy-edit').removeClass('wrap-easy-edit');
                        }
                    });
                    $('div[class*=easy-edit-type-]').remove();
                    $('.easy-edit-script').remove();
                    $('.columns').removeClass('display-block');
                }
            }
        });
    }

    /**
     * remove wrap easy edit
     */
    function removeWrap(elem)
    {
        if (typeof elem !== 'undefined' && elem.length > 0) {
            elem.replaceWith(function() {
                return elem.children(':not(.easy-edit)');
            });
        }
    }
});
