## Balance Box (Magento 2)

Custom content module for flexible and responsive content areas.

### Installation

__via composer__
```shell
composer require "balance/box2"
```

__via git__
```shell
cd <YOUR_PROJECT_DIR>/src
mkdir -p app/code/Balance && cd "$_"
git clone git@bitbucket.org:balanceinternet/balance_box-m2 Box
```

### Usage

For any Balance Internet project, we will need to create a directory in `app/code/<project_name>`
Now we will extend all setup scripts (fixtures) from `Balance\Box` by doing the following steps.

- Create `<project_name>\Box` extension in `app/code`
- Create `di.xml` and overwrite the Fixture helper: `Balance\Box\Helper\Fixture`
```xml
<?xml version="1.0"?>
<!--
  ~ Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
  ~ See COPYING.txt for license details.
  -->

<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config.xsd">
    <preference for="Balance\Box\Helper\Fixture" type="Projectname\Box\Helper\Fixture" />
</config>
```
- Create `module.xml` and makes sure it depends on `Balance\Box` extension
```xml
<?xml version="1.0"?>
<!--
  ~ Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
  ~ See COPYING.txt for license details.
  -->

<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
    <module name="Projectname_Box" setup_version="0.1.0">
        <sequence>
            <module name="Balance_Box"/>
        </sequence>
    </module>
</config>
```
- Create the helper class `Projectname\Box\Helper\Fixture` which overwrite `Balance\Box` one
```php
<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Projectname\Box\Helper;

/**
 * Class Fixture
 *
 * @package Projectname\Box\Helper
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 * 
 *
 * Set this to false if you don't want to use fixture yet
 * @const   FIXTURES_MODE [default: false]
 *
 * Set this to true if you want to test code of setup script \
 * any time you run setup:upgrade without change your setup version
 * @const   DEBUG_MODE    [default: false]
 */
class Fixture extends \Balance\Box\Helper\Fixture
{
    /**#@+
     * Constants defined for keys of data array
     */
    const MODULE_NAME = 'Projectname_Box';
    const FIXTURES_MODE = true;
    const DEBUG_MODE = false;
}
```
- Now you can add your setup data to `fixtures` or images to `pub` in `app/code/<project_name>/Box`.
- Take a look into an example project https://bitbucket.org/balanceinternet/windsorsmith-m2/src
which currently using this, to know more about how it works in real life.

Happy coding!