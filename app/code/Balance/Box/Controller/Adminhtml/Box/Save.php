<?php

namespace Balance\Box\Controller\Adminhtml\Box;

use Balance\Box\Model\Box\Image;
use Magento\Backend\App\Action;
use Magento\Framework\Stdlib\DateTime\Filter\Date as DateFilter;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Save extends Action
{
    /**
     * @var UploaderFactory
     */
    protected $uploader;

    /**
     * @var Image
     */
    protected $imageModel;

    /**
     * Date filter instance
     *
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $dateFilter;

    /**
     * @param Action\Context  $context
     * @param UploaderFactory $uploader
     * @param Image           $imageModel
     */
    public function __construct(
        Action\Context $context,
        UploaderFactory $uploader,
        DateFilter $dateFilter,
        Image $imageModel
    ) {
        $this->uploader = $uploader;
        $this->dateFilter = $dateFilter;
        $this->imageModel = $imageModel;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Balance\Box\Model\Box $model */
            $model = $this->_objectManager->create('Balance\Box\Model\Box');

            $id = $this->getRequest()->getParam('box_id');
            if ($id) {
                $model->load($id);
            }

            $validateResult = $model->validateData(new \Magento\Framework\DataObject($data));
            if ($validateResult !== true) {
                foreach ($validateResult as $errorMessage) {
                    $this->messageManager->addErrorMessage($errorMessage);
                }
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', ['box_id' => $model->getId()]);
                return;
            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'box_box_prepare_save',
                ['box' => $model, 'request' => $this->getRequest()]
            );

            try {
                $desktopImageName = $this->uploadFileAndGetName(
                    'desktop_image',
                    $this->imageModel->getBaseDir().'desktop',
                    $data
                );
                $tabletImageName = $this->uploadFileAndGetName(
                    'tablet_image',
                    $this->imageModel->getBaseDir().'tablet',
                    $data
                );
                $mobileImageName = $this->uploadFileAndGetName(
                    'mobile_image',
                    $this->imageModel->getBaseDir().'mobile',
                    $data
                );
                $model->setDesktopImage($desktopImageName);
                $model->setTabletImage($tabletImageName);
                $model->setMobileImage($mobileImageName);
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved this Box.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['box_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the box.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['box_id' => $this->getRequest()->getParam('box_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    public function uploadFileAndGetName($input, $destinationFolder, $data)
    {
        try {
            if (isset($data[$input]['delete'])) {
                return '';
            } else {
                $uploader = $this->uploader->create(['fileId' => $input]);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                $result = $uploader->save($destinationFolder);
                return $result['file'];
            }
        } catch (\Exception $e) {
            if ($e->getCode() != \Magento\Framework\File\Uploader::TMP_NAME_EMPTY) {
                $this->messageManager->addError($e->getMessage());
            } else {
                if (isset($data[$input]['value'])) {
                    return $data[$input]['value'];
                }
            }
        }
        return '';
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Balance_Box::save');
    }
}
