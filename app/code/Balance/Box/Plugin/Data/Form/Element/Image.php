<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Plugin\Data\Form\Element;

use Magento\Framework\Data\Form\Element\Image as Subject;

class Image
{
    /**
     * Image element value became an array if an exception throwed
     *
     * @param Subject  $subject
     * @param \Closure $proceed
     *
     * @return mixed
     */
    public function aroundGetElementHtml(Subject $subject, \Closure $proceed)
    {
        if (!is_string($subject->getValue())) {
            if (is_array($subject->getValue())) {
                $subject->setValue(implode($subject->getValue()));
            }
        }

        return $proceed();
    }
}