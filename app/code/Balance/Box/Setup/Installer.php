<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\Box\Setup;

use Magento\Framework\Setup;
use Balance\SampleData\Setup\AbstractInstaller;
use Balance\Box\Helper\Fixture as FixtureHelper;

/**
 * Class Installer
 *
 * @package Balance\Box\Setup
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Installer extends AbstractInstaller implements Setup\SampleData\InstallerInterface
{
    /**
     * @var \Balance\Box\Helper\Fixture
     */
    protected $helper;

    /**
     * BoxInstaller constructor.
     *
     * @param FixtureHelper $helper
     */
    public function __construct(FixtureHelper $helper)
    {
        parent::__construct($helper);
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function install()
    {
        $this->process(
            [
                self::ENTITIES => [
                    'groups' => [
                        self::ENTITY_NAME => 'groups',
                        self::ENTITY_CLASSNAME => \Balance\Box\Model\SampleData\Group::class
                    ],
                    'boxes' => [
                        self::ENTITY_NAME => 'boxes',
                        self::ENTITY_CLASSNAME => \Balance\Box\Model\SampleData\Box::class
                    ]
                ]
            ]
        );
    }
}