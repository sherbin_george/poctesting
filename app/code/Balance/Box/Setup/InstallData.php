<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\Box\Setup;

use Magento\Framework\Setup;

/**
 * Class InstallData
 *
 * @package Balance\Box\Setup
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class InstallData implements Setup\InstallDataInterface
{
    /**
     * @var Setup\SampleData\Executor
     */
    protected $executor;

    /**
     * @var Installer
     */
    protected $installer;

    /**
     * InstallData constructor.
     *
     * @param Setup\SampleData\Executor $executor
     * @param Installer                 $installer
     */
    public function __construct(Setup\SampleData\Executor $executor, Installer $installer)
    {
        $this->executor = $executor;
        $this->installer = $installer;
    }

    /**
     * {@inheritdoc}
     */
    public function install(Setup\ModuleDataSetupInterface $setup, Setup\ModuleContextInterface $moduleContext)
    {
        $this->executor->exec($this->installer);
    }
}
