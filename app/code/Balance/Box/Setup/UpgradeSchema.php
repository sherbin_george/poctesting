<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Setup;

use Balance\Box\Api\Data\BoxInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Add "Show Text Block", "Block Background" and "Text Block Colour" columns to Balance Box table
 *
 * @package Balance\Box\Setup
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const TABLE_NAME = 'balance_box_single';

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addExtendedColumns($setup);
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::TYPE)) {
                $setup->getConnection()
                    /**
                     * Add 'type' column to 'balance_box_single'
                     *
                     * @param SchemaSetupInterface $setup
                     */
                    ->addColumn(
                        $setup->getTable(self::TABLE_NAME),
                        BoxInterface::TYPE,
                        [
                            'type' => Table::TYPE_TEXT,
                            'nullable' => false,
                            'default' => 'default',
                            'length' => 25,
                            'comment' => 'Type of Box(default,youtube)'
                        ]
                    );
            }
            if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::TABLET_IMAGE)) {
                $setup->getConnection()
                    /**
                     * Add 'tablet_image' column to 'balance_box_single'
                     *
                     * @param SchemaSetupInterface $setup
                     */
                    ->addColumn(
                        $setup->getTable(self::TABLE_NAME),
                        BoxInterface::TABLET_IMAGE,
                        [
                            'type' => Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' => 'Box Tablet Image'
                        ]
                    );
            }
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::VIDEO_LINK)) {
                $setup->getConnection()
                    /**
                     * Add 'type' column to 'balance_box_single'
                     *
                     * @param SchemaSetupInterface $setup
                     */
                    ->addColumn(
                        $setup->getTable(self::TABLE_NAME),
                        BoxInterface::VIDEO_LINK,
                        [
                            'type' => Table::TYPE_TEXT,
                            'nullable' => false,
                            'length' => '64K',
                            'comment' => 'Video Link'
                        ]
                    );
            }
        }
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $this->addFullTextIndex($setup);
        }
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::GROUP_IDENTIFIER)) {
                $setup->getConnection()
                    /**
                     * Add 'group_identifier' column to 'balance_box_single'
                     *
                     * @param SchemaSetupInterface $setup
                     */
                    ->addColumn(
                        $setup->getTable(self::TABLE_NAME),
                        BoxInterface::GROUP_IDENTIFIER,
                        [
                            'type' => Table::TYPE_TEXT,
                            'nullable' => false,
                            'length' => '64K',
                            'comment' => 'Group Identifier'
                        ]
                    );
            }
        }
        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::IS_TRACKABLE)) {
                $setup->getConnection()
                    /**
                     * Add 'is_trackable' column to 'balance_box_single'
                     *
                     * @param SchemaSetupInterface $setup
                     */
                    ->addColumn(
                        $setup->getTable(self::TABLE_NAME),
                        BoxInterface::IS_TRACKABLE,
                        [
                            'type' => Table::TYPE_SMALLINT,
                            'nullable' => false,
                            'default' => 0,
                            'comment' => 'Is Trackable (GA)'
                        ]
                    );
            }
        }
        $setup->endSetup();
    }

    /**
     * Add "Show Text Block", "Block Background" and "Text Block Colour" columns to 'balance_box_single'
     *
     * @param SchemaSetupInterface $setup
     */
    protected function addExtendedColumns(SchemaSetupInterface $setup)
    {
        if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::SHOW_TEXT_BLOCK)) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_NAME),
                BoxInterface::SHOW_TEXT_BLOCK,
                [
                    'type' => Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'default' => 1,
                    'comment' => 'Show Text Block?'
                ]
            );
        }

        if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::BLOCK_BACKGROUND)) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_NAME),
                BoxInterface::BLOCK_BACKGROUND,
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => false,
                    'default' => 'transparent',
                    'length' => 25,
                    'comment' => 'Block Background Colour'
                ]
            );
        }

        if (!$setup->getConnection()->tableColumnExists(self::TABLE_NAME, BoxInterface::TEXT_BLOCK_COLOUR)) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_NAME),
                BoxInterface::TEXT_BLOCK_COLOUR,
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Text Block Colour'
                ]
            );
        }
    }

    /**
     * addFullTextIndex
     *
     * @param SchemaSetupInterface $setup
     */
    protected function addFullTextIndex(SchemaSetupInterface $setup)
    {
        $tableName = self::TABLE_NAME;
        $fullTextIndex = ['identifier', 'title'];
        $setup->getConnection()->addIndex(
            $tableName,
            $setup->getIdxName(
                $tableName,
                $fullTextIndex,
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            $fullTextIndex,
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );

    }
}
