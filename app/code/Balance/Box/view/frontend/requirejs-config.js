var config = {
    'map': {
        '*': {
            'balance/box': 'Balance_Box/js/box',
            'balance/video': 'Balance_Box/js/video'
        }
    },
    'shim': {
        'flickity': ['jquery']
    },
    'paths': {
        // flickity.pkgd.orig.js as supplied by vendor, below file
        // is patched for Magneto. Diff these to see patch.
        'flickity': 'Balance_Box/js/vendor/flickity.pkgd'
    }
};
