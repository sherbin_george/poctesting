define([
    'jquery',
    'flickity',
    'jquery/ui'
], function ($, Flickity) {
    'use strict';

    var debounce = function (func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
    var playerDebug = function(_func,_var) {
        if(false) {
            console.log(_func);
            console.log(_var);
        }
    };

    $.widget('balance.box', {
        options: {
            imgSelector: 'img',
            mobileBreakpoint: 320,
            tabletBreakpoint: 770,
            desktopBreakpoint: 1240,
            videoOptions: {
                videoSelector: '',
                useImageVideo: false,   //example: video id is AkyQgpqRyBY, then image of video has link http://img.youtube.com/vi/AkyQgpqRyBY/0.jpg
                bpWidth: {
                    mobile: { width: 300 },
                    tablet: { width: 730 },
                    desktop: { width: 1200 }
                },
                apiOptions: {
                    id: 'player',   // id of place holder|iframe
                    config: {
                        height: '244',
                        width: '434',
                        // videoId: '',    //need to add when iframe is not already defined
                        playerVars: {
                            'autoplay': 0,
                            'rel': 0,
                            'showinfo': 0
                        },
                        events: {
                            onReady: function(){},
                            onStateChange: function(){},
                            onPlaybackQualityChange: function(){},
                            onPlaybackRateChange: function(){},
                            onError: function(){},
                            onApiChange: function(){}
                        }
                    }
                },
                thumbnail: 'http://img.youtube.com/vi/',   //placeholder
                iframeAlready: false,
                apiUrl: '//www.youtube.com/iframe_api'
            }
        },

        _create: function () {
            this._super();
            this._image = this.element.find(this.options.imgSelector);
            this._desktopImage = this._getImage([this._image.attr('data-desktop-image'),this._image.attr('data-tablet-image'),this._image.attr('data-mobile-image')]);
            this._tabletImage = this._getImage([this._image.attr('data-tablet-image'),this._image.attr('data-desktop-image'),this._image.attr('data-mobile-image')]);
            this._mobileImage = this._getImage([this._image.attr('data-mobile-image'),this._image.attr('data-tablet-image'),this._image.attr('data-desktop-image')]);
            this._isLoaded = false;
            this._setSrc();
            this._videoAPILoader();
            this._listen();
        },
        _getImage: function (listImg) {
            var i = 0;
            while(!listImg[i] && (listImg.length > i)) i++;
            return listImg[i];
        },
        _setImageSrc: function (screen) {
            this._image.attr('src', this['_' + screen + 'Image']);
        },
        _setSrc: function () {
            if (window.innerWidth <= this.options.mobileBreakpoint) {
                this._setImageSrc('mobile');
            } else if (window.innerWidth <= this.options.tabletBreakpoint) {
                this._setImageSrc('tablet');
            } else {
                this._setImageSrc('desktop');
            }
        },
        _listen: function () {
            // fix problem bottom spacing redundant when change desktop => mobile and mobile => tablet
            var ths = this;
            if(typeof Flickity.prototype.resize === "function") {
                var originalOnresize = Flickity.prototype.resize;
                Flickity.prototype.resize = function() {
                    //window.addEventListener('resize', debounce(function () {
                        ths._setSrc();
                    //}, 200));
                    originalOnresize.apply(this, arguments);
                };
            } else {
                window.addEventListener('resize', debounce(function () {
                    ths._setSrc();
                }, 200));
            }
        },
        _guid: function(prefix) {
            return prefix+Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        },
        _videoAPILoader: function () {
            var ths = this;
            this.element.find(this.options.videoOptions.videoSelector).each(function(i,e){
                var iframeId = ($(e).attr("id"))?$(e).attr("id"):ths._guid('iframe');
                var player = $.extend({}, ths.options.videoOptions.apiOptions, {id: iframeId});
                $(e).attr("id",iframeId);
                $(e).data("player",player);
            });
            ths._run();
        },
        _loadAPI: function (api) {
            var tag = document.createElement('script');
            tag.src = api;
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        },
        _createPlayer: function(player) {
            playerDebug('_createPlayer:player', player);
            return new YT.Player(player.id, player.config);
        },
        _run: function() {
            var ths = this;
            // load the api however you like
            ths._loadAPI(ths.options.videoOptions.apiUrl);

            window.onYouTubeIframeAPIReady = function() {
                ths._isLoaded = true;
                $(".balance-box-video").each(function(){
                    var player = $(this).data('player');
                    player = $.extend({},$(this).data('player'),{obj: ths._createPlayer(player)});
                    $(this).data('player', player);
                });
            }
        }
    });

    $.widget('balance.boxSlider', {
        options: {
            boxSelector: '',
            boxOptions: {
                imgSelector: 'img'
            },
            flickityOptions: {
                imagesLoaded: true,
                autoPlay: 6000
            }
        },

        _create: function () {
            this._super();
            this._initImages();
            this._initSlider();
        },

        _initImages: function () {
            var ths = this;
            ths.element.find(ths.options.boxSelector).each(function (i, e) {
                $.balance.box(ths.options.boxOptions, $(e));
            });
        },

        _initSlider: function () {
            var ths = this;
            ths.flickity = new Flickity(ths.element.get(0), ths.options.flickityOptions);
            console.log($(this.element));
            $(this.element).data('flickity', ths.flickity);
        }
    });

    return {
        'box': $.balance.box,
        'slider': $.balance.boxSlider
    };
});
