<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model\ResourceModel;

/**
 * Box mysql resource
 */
class Box extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Construct
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime       $date
     * @param string|null                                       $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('balance_box_single', 'box_id');
    }

    /**
     * Process box data before saving
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $fromDate = $toDate = null;

        if (!$this->isValidBoxIdentifier($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The box identifier contains capital letters or disallowed symbols.')
            );
        }

        if ($this->isNumericBoxIdentifier($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The box identifier cannot be made of only numbers.')
            );
        }

        if (!$object->getGroupId()) {
            $object->setGroupId(null);
        }

        if ($object->hasFromDate()) {
            $fromDate = $object->getFromDate();
            $fromDate = strtotime($fromDate);

            $object->setFromDate($this->formatDate($fromDate));
        }

        if ($object->hasToDate()) {
            $toDate = $object->getToDate();
            $toDate = strtotime($toDate);

            $object->setToDate($this->formatDate($toDate));
        }

        if ($object->isObjectNew() && !$object->hasCreationTime()) {
            $object->setCreationTime($this->_date->gmtDate());
        }

        $object->setUpdateTime($this->_date->gmtDate());

        return parent::_beforeSave($object);
    }

    /**
     * Load an object using 'identifier' field if there's no field specified and value is not numeric
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param mixed                                  $value
     * @param string                                 $field
     *
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'identifier';
        }

        return parent::load($object, $value, $field);
    }

    /**
     * Retrieve load select with filter by identifier and activity
     *
     * @param string $identifier
     * @param int    $isActive
     *
     * @return \Magento\Framework\DB\Select
     */
    protected function _getLoadByIdentifierSelect($identifier, $isActive = null)
    {
        $select = $this->getConnection()->select()->from(
            ['bbs' => $this->getMainTable()]
        )->where(
            'bbs.identifier = ?',
            $identifier
        );

        if (!is_null($isActive)) {
            $select->where('bbs.is_active = ?', $isActive);
        }

        return $select;
    }

    /**
     *  Check whether box identifier is numeric
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return bool
     */
    protected function isNumericBoxIdentifier(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[0-9]+$/', $object->getData('identifier'));
    }

    /**
     *  Check whether box identifier is valid
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return bool
     */
    protected function isValidBoxIdentifier(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[a-z0-9_-]+$/', $object->getData('identifier'));
    }

    /**
     * Check if box identifier exists
     * return box id if box exists
     *
     * @param string $identifier
     *
     * @return int
     */
    public function checkIdentifier($identifier)
    {
        $select = $this->_getLoadByIdentifierSelect($identifier, 1);
        $select->reset(\Zend_Db_Select::COLUMNS)->columns('bbs.box_id')->limit(1);

        return $this->getConnection()->fetchOne($select);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($boxId = $object->getData('box_id')) {
            $storeIds = $object->getData('store_ids');
            if (!empty($object->getData('stores'))) {
                $storeIds = $object->getData('stores');
            }
            $connection = $this->getConnection();
            $tableName = $this->getTable('balance_box_store');
            if (count($storeIds) > 0) {
                $connection->delete(
                    $tableName,
                    'box_id = ' . $boxId
                );

                $insertData = [];
                foreach ($storeIds as $storeId) {
                    $insertData[] = ['box_id' => $boxId, 'store_id' => $storeId];
                }

                if ($insertData > 0) {
                    try {
                        $connection->beginTransaction();
                        $connection->insertMultiple($tableName, $insertData);
                        $connection->commit();
                    } catch (\Exception $e) {
                        $connection->rollBack();
                    }
                }
            }
        }
        return $this;
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $boxId
     *
     * @return array
     */
    public function lookupStoreIdsByBoxId($boxId)
    {
        $connection = $this->getConnection();
        $linkField = 'box_id';

        $select = $connection->select()
            ->from(['cps' => $this->getTable('balance_box_store')], 'store_id')
            ->join(
                ['cp' => $this->getMainTable()],
                'cps.' . $linkField . ' = cp.' . $linkField,
                []
            )
            ->where('cp.box_id = :box_id');

        return $connection->fetchCol($select, ['box_id' => (int)$boxId]);
    }

    /**
     * @param $boxId
     * @param $storeId
     */
    public function setCustomStoreId($boxId, $storeId)
    {
        if ($boxId > 0 && $storeId >= 0) {
            $connection = $this->getConnection();
            $tableName = $this->getTable('balance_box_store');
            $connection->delete($tableName, ['box_id = ?', $boxId]);
            $insertData = ['box_id' => $boxId, 'store_id' => $storeId];
            $connection->insert($tableName, $insertData);
        }
    }

    /**
     * Prepare date for save in DB
     *
     * string format used from input fields (all date input fields need apply locale settings)
     * int value can be declared in code (this meen whot we use valid date)
     *
     * @param string|int|\DateTime $date
     *
     * @return string
     */
    public function formatDate($date)
    {
        if (empty($date)) {
            return null;
        }

        // unix timestamp given - simply instantiate date object
        if (is_scalar($date) && preg_match('/^[0-9]+$/', $date)) {
            $date = (new \DateTime())->setTimestamp($date);
        } elseif (!($date instanceof \DateTime)) {
            // normalized format expecting Y-m-d[ H:i:s]  - time is optional
            $date = new \DateTime($date);
        }

        return $date->format('Y-m-d H:i:s');
    }
}
