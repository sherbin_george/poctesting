<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model;

use Balance\Box\Api\Data\BoxInterface;
use Balance\Box\Model\Box\Image;
use Balance\Box\Model\Box\Layout;
use Balance\Box\Model\Box\Type;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Class Box
 *
 * @package Balance\Box\Model
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Box extends AbstractModel implements BoxInterface, IdentityInterface
{
    /**#@+
     * Box's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'box_single';

    /**
     * @var string
     */
    protected $_cacheTag = 'box_single';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'box_single';

    /**
     * @var Image
     */
    protected $_boxImageModel;

    /**
     * @var Layout
     */
    protected $_boxLayoutModel;

    /**
     * @var Type
     */
    protected $_boxTypeModel;

    /**
     * @param Image            $boxImageModel
     * @param Layout           $boxLayoutModel
     * @param Type             $boxTypeModel
     * @param Context          $context
     * @param Registry         $registry
     * @param AbstractResource $resource
     * @param AbstractDb       $resourceCollection
     * @param Image            $boxImageModel
     * @param array            $data
     */
    public function __construct(
        Image $boxImageModel,
        Layout $boxLayoutModel,
        Type $boxTypeModel,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_boxImageModel = $boxImageModel;
        $this->_boxLayoutModel = $boxLayoutModel;
        $this->_boxTypeModel = $boxTypeModel;
    }

    /**
     * Check if box identifier exists
     * return box id if box exists
     *
     * @param string $identifier
     *
     * @return int
     */
    public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }

    /**
     * Prepare box's statuses.
     * Available event box_single_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::BOX_ID);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     * Get group ID
     *
     * @return int|null
     */
    public function getGroupId()
    {
        return $this->getData(self::GROUP_ID);
    }

    /**
     * Get group identifier
     *
     * @return string|null
     */
    public function getGroupIdentifier()
    {
        return $this->getData(self::GROUP_IDENTIFIER);
    }

    /**
     * Get desktop image
     *
     * @return string|null
     */
    public function getDesktopImage()
    {
        return $this->getData(self::DESKTOP_IMAGE);
    }

    /**
     * Get tablet image
     *
     * @return string|null
     */
    public function getTabletImage()
    {
        return $this->getData(self::TABLET_IMAGE);
    }

    /**
     * Get mobile image
     *
     * @return string|null
     */
    public function getMobileImage()
    {
        return $this->getData(self::MOBILE_IMAGE);
    }

    /**
     * Get alt text
     *
     * @return string|null
     */
    public function getAltText()
    {
        return $this->getData(self::ALT_TEXT);
    }

    /**
     * Get heading
     *
     * @return string|null
     */
    public function getHeading()
    {
        return $this->getData(self::HEADING);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get link
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->getData(self::LINK);
    }

    /**
     * Get button text
     *
     * @return string|null
     */
    public function getButtonText()
    {
        return $this->getData(self::BUTTON_TEXT);
    }

    /**
     * Get show text block
     *
     * @return bool|null
     */
    public function getShowTextBlock()
    {
        return $this->getData(self::SHOW_TEXT_BLOCK);
    }

    /**
     * Get block background
     *
     * @return string|null
     */
    public function getBlockBackground()
    {
        return $this->getData(self::BLOCK_BACKGROUND);
    }

    /**
     * Get text block colour
     *
     * @return int|null
     */
    public function getTextBlockColour()
    {
        return $this->getData(self::TEXT_BLOCK_COLOUR);
    }

    /**
     * Get layout
     *
     * @return string|null
     */
    public function getLayout()
    {
        return $this->getData(self::LAYOUT);
    }

    /**
     * Get type
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * Get video link
     *
     * @return string|null
     */
    public function getVideoLink()
    {
        return $this->getData(self::VIDEO_LINK);
    }

    /**
     * Get from date
     *
     * @return string|null
     */
    public function getFromDate()
    {
        return $this->getData(self::FROM_DATE);
    }

    /**
     * Get to date
     *
     * @return string|null
     */
    public function getToDate()
    {
        return $this->getData(self::TO_DATE);
    }

    /**
     * Get position
     *
     * @return int|null
     */
    public function getPosition()
    {
        return $this->getData(self::POSITION);
    }

    /**
     * Is trackable
     *
     * @return bool|null
     */
    public function isTrackable()
    {
        return (bool)$this->getData(self::IS_TRACKABLE);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool)$this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setId($id)
    {
        return $this->setData(self::BOX_ID, $id);
    }

    /**
     * Set title
     *
     * @param int $title
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set identifier
     *
     * @param int $identifier
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setIdentifier($identifier)
    {
        return $this->setData(self::IDENTIFIER, $identifier);
    }

    /**
     * Set group ID
     *
     * @param $group_id
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setGroupId($group_id)
    {
        return $this->setData(self::GROUP_ID, $group_id);
    }

    /**
     * Set group identifier
     *
     * @param $group_identifier
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setGroupIdentifier($group_identifier)
    {
        return $this->setData(self::GROUP_IDENTIFIER, $group_identifier);
    }

    /**
     * Set desktop image
     *
     * @param $desktop_image
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setDesktopImage($desktop_image)
    {
        return $this->setData(self::DESKTOP_IMAGE, $desktop_image);
    }

    /**
     * Set tablet image
     *
     * @param $tablet_image
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setTabletImage($tablet_image)
    {
        return $this->setData(self::TABLET_IMAGE, $tablet_image);
    }

    /**
     * Set mobile image
     *
     * @param $mobile_image
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setMobileImage($mobile_image)
    {
        return $this->setData(self::MOBILE_IMAGE, $mobile_image);
    }

    /**
     * Set alt text
     *
     * @param $alt_text
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setAltText($alt_text)
    {
        return $this->setData(self::ALT_TEXT, $alt_text);
    }

    /**
     * Set heading
     *
     * @param $heading
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setHeading($heading)
    {
        return $this->setData(self::HEADING, $heading);
    }

    /**
     * Set content
     *
     * @param $content
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set link
     *
     * @param $link
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setLink($link)
    {
        return $this->setData(self::LINK, $link);
    }

    /**
     * Set button text
     *
     * @param $button_text
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setButtonText($button_text)
    {
        return $this->setData(self::BUTTON_TEXT, $button_text);
    }

    /**
     * Set show text block
     *
     * @param $show_text_block
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setShowTextBlock($show_text_block)
    {
        return $this->setData(self::SHOW_TEXT_BLOCK, $show_text_block);
    }

    /**
     * Set block background
     *
     * @param $block_background
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setBlockBackground($block_background)
    {
        return $this->setData(self::BLOCK_BACKGROUND, $block_background);
    }

    /**
     * Set text block colour
     *
     * @param $text_block_colour
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setTextBlockColour($text_block_colour)
    {
        return $this->setData(self::TEXT_BLOCK_COLOUR, $text_block_colour);
    }

    /**
     * Set layout
     *
     * @param $layout
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setLayout($layout)
    {
        return $this->setData(self::LAYOUT, $layout);
    }

    /**
     * Set Type
     *
     * @param $type
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * Set video link
     *
     * @param $videolink
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setVideoLink($videolink)
    {
        return $this->setData(self::VIDEO_LINK, $videolink);
    }

    /**
     * Set from date
     *
     * @param $from_date
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setFromDate($from_date)
    {
        return $this->setData(self::FROM_DATE, $from_date);
    }

    /**
     * Set to date
     *
     * @param $to_date
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setToDate($to_date)
    {
        return $this->setData(self::TO_DATE, $to_date);
    }

    /**
     * Set position
     *
     * @param $position
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setPosition($position)
    {
        return $this->setData(self::POSITION, $position);
    }

    /**
     * Set position
     *
     * @param $isTrackable
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setIsTrackable($isTrackable)
    {
        return $this->setData(self::IS_TRACKABLE, $isTrackable);
    }

    /**
     * Set creation time
     *
     * @param string $creation_time
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }

    /**
     * Set custom store id
     *
     * @param int $boxId
     * @param int $storeId
     */
    public function setCustomStoreId($boxId, $storeId)
    {
        $this->_getResource()->setCustomStoreId($boxId, $storeId);
    }

    /**
     * Get desktop image url
     *
     * @return string|null
     */
    public function getDesktopImageUrl()
    {
        return $this->getImageUrlFromScope(self::DESKTOP_IMAGE, self::DESKTOP_DIR);
    }

    /**
     * Get tablet image url
     *
     * @return string|null
     */
    public function getTabletImageUrl()
    {
        return $this->getImageUrlFromScope(self::TABLET_IMAGE, self::TABLET_DIR);
    }

    /**
     * Get mobile image url
     *
     * @return string|null
     */
    public function getMobileImageUrl()
    {
        return $this->getImageUrlFromScope(self::MOBILE_IMAGE, self::MOBILE_DIR);
    }

    /**
     * Get layout class
     *
     * @return string|null
     */
    public function getLayoutClass()
    {
        return $this->_boxLayoutModel->getClass($this->getData(self::LAYOUT));
    }

    /**
     * Get image url from scope
     *
     * @param string $scope
     * @param string $device_dir
     *
     * @return string
     */
    protected function getImageUrlFromScope($scope = self::DESKTOP_IMAGE, $device_dir = self::DESKTOP_DIR)
    {
        if($this->getData($scope)){
            return $this->_boxImageModel->getBaseUrl() . $device_dir . $this->getData($scope);
        }
        return;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Balance\Box\Model\ResourceModel\Box');
    }

    /**
     * {@inheritdoc}
     */
    public function validateData(\Magento\Framework\DataObject $dataObject)
    {
        $result = [];
        $fromDate = $toDate = null;

        if ($dataObject->hasFromDate() && $dataObject->hasToDate()) {
            $fromDate = $dataObject->getFromDate();
            $toDate = $dataObject->getToDate();
        }

        if ($fromDate && $toDate) {
            $fromDate = strtotime($fromDate);
            $toDate = strtotime($toDate);

            if ($fromDate > $toDate) {
                $result[] = __('End Date must follow Start Date.');
            }
        }

        if ($dataObject->hasStoreIds()) {
            $websiteIds = $dataObject->getStoreIds();
            if (empty($websiteIds)) {
                $result[] = __('Please specify a website.');
            }
        }

        return !empty($result) ? $result : true;
    }
}
