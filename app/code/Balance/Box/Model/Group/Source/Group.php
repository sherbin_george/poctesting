<?php

namespace Balance\Box\Model\Group\Source;

use \Balance\Box\Model\Group as GroupModel;

class Group implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var \Balance\Box\Model\ResourceModel\Group\Collection
     */
    protected $groupCollection;


    /**
     * @param \Balance\Box\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
     */
    public function __construct (
        \Balance\Box\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
    ) {
        $this->groupCollection = $groupCollectionFactory->create();
    }


    /**
     * Return array of Groups as value/label pairs
     *
     * @return array
     */
    public function getAllOptions ()
    {
        if (!$this->options) {
            foreach($this->groupCollection as $group) {
                $this->options[] = [
                    'label' => $group->getData(GroupModel::TITLE),
                    'value' => $group->getData(GroupModel::GROUP_ID),
                ];
            }
        }
        return $this->options;
    }


    /**
     * Return array of Groups as value/label pairs
     *
     * @return array
     */
    public function toOptionArray ()
    {
        return $this->getAllOptions();
    }


    /**
     * Return flat array of Groups
     *
     * @return array
     */
    public function toFlatArray ($nullValue = false)
    {
        $flat = [];
        if ($nullValue) {
            $flat[''] = __('None');
        }
        foreach ($this->toOptionArray() as $option) {
            $flat[$option['value']] = $option['label'];
        }
        return $flat;
    }

}
