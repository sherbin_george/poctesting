<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model\SampleData;

use Balance\SampleData\Api\Data\SampleDataInterface;
use Balance\SampleData\Model\AbstractSampleData;
use Balance\SampleData\Model\Assets\AssetsManagement;
use Balance\SampleData\Model\Fixture\FixtureManagement;
use Balance\Box\Helper\Fixture as FixtureHelper;
use Magento\Framework\Setup\SampleData\Context as SampleDataContext;

/**
 * Class Box
 *
 * @package Balance\Box\Model\SampleData
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Box extends AbstractSampleData
{
    /**
     * @var \Balance\Box\Helper\Fixture
     */
    protected $helper;

    /**
     * Box constructor.
     *
     * @param SampleDataContext $sampleDataContext
     * @param AssetsManagement  $assetsManagement
     * @param FixtureManagement $fixtureManagement
     * @param FixtureHelper     $fixtureHelper
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        AssetsManagement $assetsManagement,
        FixtureManagement $fixtureManagement,
        FixtureHelper $fixtureHelper
    ) {
        parent::__construct($sampleDataContext, $assetsManagement, $fixtureManagement, $fixtureHelper);
        $this->helper = $fixtureHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function install($fixtures)
    {
        $this->process(
            $fixtures,
            [
                SampleDataInterface::PROCESS_TYPE => SampleDataInterface::PROCESS_CONVERTER,
                SampleDataInterface::ENTITY => 'boxes',
                SampleDataInterface::PUBLISH_ASSETS => true,
                SampleDataInterface::ENTITY_CLASS => \Balance\Box\Model\Box::class,
                SampleDataInterface::CONVERTER_CLASS => \Balance\Box\Model\SampleData\Box\Converter::class
            ],
            [
                SampleDataInterface::GET_DATA_KEY => \Balance\Box\Model\Box::GROUP_ID,
                SampleDataInterface::LOAD_DATA_KEY => \Balance\Box\Model\Box::GROUP_IDENTIFIER,
                SampleDataInterface::SET_DATA_KEY => \Balance\Box\Model\Box::GROUP_ID,
                SampleDataInterface::EXTRA_ENTITY_CLASS => \Balance\Box\Model\Group::class
            ]
        );
    }
}
