<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model\SampleData;

use Balance\Box\Helper\Fixture as FixtureHelper;
use Balance\SampleData\Api\Data\SampleDataInterface;
use Balance\SampleData\Model\AbstractSampleData;
use Balance\SampleData\Model\Assets\AssetsManagement;
use Balance\SampleData\Model\Fixture\FixtureManagement;
use Magento\Framework\Setup\SampleData\Context as SampleDataContext;

/**
 * Class Group
 *
 * @package Balance\Box\Model\SampleData
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Group extends AbstractSampleData
{
    /**
     * @var \Balance\Box\Helper\Fixture
     */
    protected $helper;

    /**
     * Group constructor.
     *
     * @param SampleDataContext $sampleDataContext
     * @param AssetsManagement  $assetsManagement
     * @param FixtureManagement $fixtureManagement
     * @param FixtureHelper     $fixtureHelper
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        AssetsManagement $assetsManagement,
        FixtureManagement $fixtureManagement,
        FixtureHelper $fixtureHelper
    ) {
        parent::__construct($sampleDataContext, $assetsManagement, $fixtureManagement, $fixtureHelper);
        $this->helper = $fixtureHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function install($fixtures)
    {
        $this->process(
            $fixtures,
            [
                SampleDataInterface::PROCESS_TYPE => SampleDataInterface::PROCESS_SIMPLE,
                SampleDataInterface::ENTITY => 'groups',
                SampleDataInterface::ENTITY_CLASS => \Balance\Box\Model\Group::class
            ]
        );
    }
}
