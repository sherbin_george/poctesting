<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model\SampleData\Box;

use Balance\SampleData\Model\Converter\AbstractConverter;

/**
 * Class Converter
 *
 * @package Balance\Box\Model\SampleData\Box
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Converter extends AbstractConverter
{
    const CONVERT_KEY = 'content';
    const ENTITY = 'boxes';
}
