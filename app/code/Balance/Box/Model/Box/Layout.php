<?php
/**
 * Copyright © 2016 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model\Box;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class Layout extends AbstractModel
{
    /**
     * @var array
     */
    protected $_layouts;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Context              $context
     * @param Registry             $registry
     * @param AbstractResource     $resource
     * @param AbstractDb           $resourceCollection
     * @param array                $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getLabel($code)
    {
        return $this->getLayoutAttribute($code, 'label');
    }

    /**
     * @return mixed
     */
    protected function getLayoutAttribute($code, $attribute)
    {
        $layouts = $this->getAll();
        if (isset($layouts[$code]) && isset($layouts[$code][$attribute])) {
            return $layouts[$code][$attribute];
        }
    }

    /**
     * @return array
     */
    public function getAll()
    {
        if (!$this->_layouts) {
            $this->_layouts = $this->_scopeConfig->getValue('balance_box/layouts');
        }
        return $this->_layouts;
    }

    /**
     * @return string
     */
    public function getClass($code)
    {
        return $this->getLayoutAttribute($code, 'class');
    }
}
