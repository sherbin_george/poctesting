<?php
/**
 * Copyright © 2016 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model\Box;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class Type extends AbstractModel
{
    /**
     * @var array
     */
    protected $_types;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Context              $context
     * @param Registry             $registry
     * @param AbstractResource     $resource
     * @param AbstractDb           $resourceCollection
     * @param array                $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getLabel($code)
    {
        return $this->getTypeAttribute($code, 'label');
    }

    /**
     * @return mixed
     */
    protected function getTypeAttribute($code, $attribute)
    {
        $layouts = $this->getAll();
        if (isset($layouts[$code]) && isset($layouts[$code][$attribute])) {
            return $layouts[$code][$attribute];
        }
    }

    /**
     * @return array
     */
    public function getAll()
    {
        if (!$this->_types) {
            $this->_types = $this->_scopeConfig->getValue('balance_box/type');
        }
        return $this->_types;
    }

    /**
     * @return string
     */
    public function getClass($code)
    {
        return $this->getTypeAttribute($code, 'class');
    }
}
