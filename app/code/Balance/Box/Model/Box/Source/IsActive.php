<?php

namespace Balance\Box\Model\Box\Source;

use Magento\Framework\Data\OptionSourceInterface;

class IsActive implements OptionSourceInterface
{
    /**
     * @var \Balance\Box\Model\Box
     */
    protected $box;

    /**
     * Constructor
     *
     * @param \Balance\Box\Model\Box $box
     */
    public function __construct(\Balance\Box\Model\Box $box)
    {
        $this->box = $box;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->box->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
