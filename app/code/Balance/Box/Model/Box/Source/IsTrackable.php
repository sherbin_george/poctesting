<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Model\Box\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsTrackable
 *
 * @package Balance\Box\Model\Box\Source
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class IsTrackable implements OptionSourceInterface
{
    /**
     * @var \Balance\Box\Model\Box
     */
    protected $box;

    /**
     * Constructor
     *
     * @param \Balance\Box\Model\Box $box
     */
    public function __construct(\Balance\Box\Model\Box $box)
    {
        $this->box = $box;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->box->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
