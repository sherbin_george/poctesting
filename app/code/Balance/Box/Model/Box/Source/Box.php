<?php

namespace Balance\Box\Model\Box\Source;

use \Balance\Box\Model\Box as BoxModel;

class Box implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var \Balance\Box\Model\ResourceModel\Box\Collection
     */
    protected $boxCollection;


    /**
     * @param \Balance\Box\Model\ResourceModel\Box\CollectionFactory $boxCollectionFactory
     */
    public function __construct (
        \Balance\Box\Model\ResourceModel\Box\CollectionFactory $boxCollectionFactory
    ) {
        $this->boxCollection = $boxCollectionFactory->create();
    }


    /**
     * Return array of Boxes as value/label pairs
     *
     * @return array
     */
    public function getAllOptions ()
    {
        if (!$this->options) {
            foreach($this->boxCollection as $box) {
                $this->options[] = [
                    'label' => $box->getData(BoxModel::TITLE),
                    'value' => $box->getData(BoxModel::BOX_ID),
                ];
            }
        }
        return $this->options;
    }


    /**
     * Return array of Boxes as value/label pairs
     *
     * @return array
     */
    public function toOptionArray ()
    {
        return $this->getAllOptions();
    }


    /**
     * Return flat array of Boxes
     *
     * @return array
     */
    public function toFlatArray ($nullValue = false)
    {
        $flat = [];
        if ($nullValue) {
            $flat[''] = __('None');
        }
        foreach ($this->toOptionArray() as $option) {
            $flat[$option['value']] = $option['label'];
        }
        return $flat;
    }

}
