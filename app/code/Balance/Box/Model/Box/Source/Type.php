<?php
namespace Balance\Box\Model\Box\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Type implements OptionSourceInterface
{
    /**
     * @var \Balance\Box\Model\Box\Type
     */
    protected $_typeModel;

    public function __construct(
        \Balance\Box\Model\Box\Type $typeModel
    ) {
        $this->_typeModel = $typeModel;
    }

    /**
     * @return array
     */
    public function toFlatArray($empty = false)
    {
        $flat = [];

        if ($empty) {
            $flat[''] = '';
        }

        foreach ($this->toOptionArray() as $option) {
            $flat[$option['value']] = $option['label'];
        }

        return $flat;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    /**
     * @return array
     */
    protected function getAllOptions()
    {
        $options = [];

        foreach ($this->_typeModel->getAll() as $code => $type) {
            $options[] = [
                'value' => $code,
                'label' => $type['label'],
            ];
        }

        return $options;
    }
}

