<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block;

use Balance\Box\Model\Box as BoxModel;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Class Box
 *
 * @package Balance\Box\Block
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Box extends AbstractBox implements IdentityInterface
{
    /**#@+
     * Block template
     */
    const DEFAULT_TEMPLATE = 'Balance_Box::single.phtml';

    /**
     * Get box data
     *
     * @return \Balance\Box\Model\Box
     */
    public function getBox()
    {
        if (!$this->hasData('box')) {
            /** @var \Balance\Box\Model\Box $box */
            $box = $this->_boxFactory->create();
            if ($identifier = $this->getData(BoxModel::IDENTIFIER)) {
                $box->load($identifier, BoxModel::IDENTIFIER);
                if ($box->isActive()) {
                    $isPublished = true;
                    if ($box->getFromDate() && $box->getToDate()) {
                        $isPublished =  $this->isPublished($box->getFromDate(), $box->getToDate());
                    }

                    if ($isPublished) {
                        $this->setData('box', $box);
                    }
                }
            } elseif ($id = $this->getData(BoxModel::BOX_ID)) {
                $box->load($id, BoxModel::BOX_ID);
                if ($box->isActive()) {
                    $this->setData('box', $box);
                }
            }
        }
        return $this->getData('box');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        $cacheTag = BoxModel::CACHE_TAG;
        if ($box = $this->getBox()) {
            $cacheTag .= '_' . $box->getId();
        }
        return [$cacheTag];
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getBox()) {
            return '';
        }
        return parent::_toHtml();
    }
}
