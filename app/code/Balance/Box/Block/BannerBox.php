<?php
/**
 * Copyright © 2016 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block;

use Balance\Box\Model\Box as BoxModel;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Class BannerBox
 *
 * @package Balance\Box\Block
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class BannerBox extends AbstractBox implements IdentityInterface
{
    const DEFAULT_TEMPLATE = 'Balance_Box::category-banner.phtml';
    const CACHE_TAG = 'box_banner';

    /**
     * Get box
     *
     * @return \Balance\Box\Model\Box
     */
    public function getBox()
    {
        if (!$this->hasData('box')) {
            /** @var \Balance\Box\Model\Box $box */
            $box = $this->_boxFactory->create();
            $this->addCategoryName($this->getCategoryName(), '_banner');
            if ($identifier = $this->getData(BoxModel::IDENTIFIER)) {
                $box->load($identifier, BoxModel::IDENTIFIER);
                if ($box->isActive()) {
                    $isPublished = true;
                    if ($box->getFromDate() && $box->getToDate()) {
                        $isPublished =  $this->isPublished($box->getFromDate(), $box->getToDate());
                    }

                    if ($isPublished) {
                        $this->setData('box', $box);
                    }
                }
            }
        }
        return $this->getData('box');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        $cacheTag = self::CACHE_TAG;
        if ($this->getBox()) {
            $cacheTag .= '_' . $this->getData(BoxModel::IDENTIFIER);
        }
        return [$cacheTag];
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getBox()) {
            return '';
        }
        return parent::_toHtml();
    }
}
