<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block;

use Balance\Box\Model\Box as BoxModel;
use Balance\Box\Model\Group as GroupModel;
use Balance\Box\Model\ResourceModel\Box\Collection as BoxCollection;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Class Group
 *
 * @package Balance\Box\Block
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Group extends AbstractBox implements IdentityInterface
{
    /**#@+
     * Block template
     */
    const DEFAULT_TEMPLATE = 'Balance_Box::group.phtml';

    /**
     * Get group data
     *
     * @return \Balance\Box\Model\Group
     */
    public function getGroup()
    {
        if (!$this->hasData('group')) {
            /** @var \Balance\Box\Model\Group $group */
            $group = $this->_groupFactory->create();
            if ($identifier = $this->getData(GroupModel::IDENTIFIER)) {
                $group->load($identifier, GroupModel::IDENTIFIER);
                $this->setData('group', $group);
            } elseif ($id = $this->getData(GroupModel::GROUP_ID)) {
                $group->load($id, GroupModel::GROUP_ID);
                $this->setData('group', $group);
            }
        }
        return $this->getData('group');
    }

    /**
     * Get boxes
     *
     * @return \Balance\Box\Model\ResourceModel\Box\Collection
     */
    public function getBoxes()
    {
        if (!$this->hasData('boxes')) {
            if ($group = $this->getGroup()) {
                $storeId = $this->_storeManager->getStore()->getId();
                $currentTime = new \DateTime();
                /** @var \Balance\Box\Model\ResourceModel\Box\Collection $boxes */
                $boxes = $this->_boxCollectionFactory->create();
                $boxes->addFieldToFilter([BoxModel::GROUP_ID, BoxModel::GROUP_IDENTIFIER],
                    [
                        ['eq' => $group->getId()],
                        ['eq' => $group->getIdentifier()]
                    ])
                    ->addStoreFilter($storeId, true)
                    ->addFieldToFilter(BoxModel::FROM_DATE, [
                        ['datetime' => true, 'to' => $currentTime->format('Y-m-d H:i:s')],
                        ['is' => new \Zend_Db_Expr('null')]
                    ])
                    ->addFieldToFilter(BoxModel::TO_DATE, [
                        ['datetime' => true, 'from' => $currentTime->format('Y-m-d H:i:s')],
                        ['is' => new \Zend_Db_Expr('null')]
                    ])
                    ->addFieldToFilter(BoxModel::IS_ACTIVE, BoxModel::STATUS_ENABLED)
                    ->addOrder(BoxModel::POSITION, BoxCollection::SORT_ORDER_ASC);
                if ($boxes->count()) {
                    $this->setData('boxes', $boxes);
                }
            }
        }
        return $this->getData('boxes');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        $cacheTag = GroupModel::CACHE_TAG;
        if ($group = $this->getGroup()) {
            $cacheTag .= '_' . $group->getId();
        }
        if ($boxes = $this->getBoxes()) {
            foreach ($boxes as $box) {
                $cacheTag .= '_' . $box->getId();
            }
        }
        return [$cacheTag];
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getGroup() || !$this->getBoxes()) {
            return '';
        }
        return parent::_toHtml();
    }
}
