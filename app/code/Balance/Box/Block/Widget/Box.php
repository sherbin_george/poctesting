<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block\Widget;

class Box
    extends \Balance\Box\Block\Box
    implements \Magento\Widget\Block\BlockInterface
{
}
