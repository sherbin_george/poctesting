<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block\Widget\Category;

use Balance\Box\Model\Box;
use Balance\Box\Model\ResourceModel\Box\Collection as BoxCollection;

/**
 * Class Landing
 *
 * @package Balance\Box\Block\Widget\Category
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Landing extends \Balance\Box\Block\Group implements \Magento\Widget\Block\BlockInterface
{
    /**#@+
     * Block template
     */
    const DEFAULT_TEMPLATE = 'Balance_Box::category/landing.phtml';

    /**
     * @var  string[]
     */
    protected $boxIdParameters = [
        'box_1_id',
        'box_2_id',
        'box_3_id',
        'box_4_id',
    ];

    /**
     * @var  string
     */
    protected $uniqueId;

    /**
     * @var  int[]
     */
    protected $boxIds;

    /**
     * @return  int
     */
    protected function getBoxCount()
    {
        switch ($this->getData('layout')) {
            case 'one':
                return 1;
            case 'two-even':
            case 'two-left':
            case 'two-right':
                return 2;
            case 'three':
                return 3;
            case 'four':
            default:
                return 4;
        }
    }

    /**
     * @return  int[]
     */
    protected function getBoxIds()
    {
        if (!$this->boxIds) {
            $boxIds = [];
            $i = 0;
            $boxCount = $this->getBoxCount();
            foreach ($this->boxIdParameters as $boxIdParameter) {
                $boxIds[] = intval($this->getData($boxIdParameter));
                $i++;
                if ($i >= $boxCount) {
                    break;
                }
            }
            $this->boxIds = array_unique($boxIds);
        }
        return $this->boxIds;
    }


    /**
     * @return  string
     */
    public function getUniqueId()
    {
        if (!$this->uniqueId) {
            $this->uniqueId = mt_rand(1000, 9999);
        }
        return $this->uniqueId;
    }


    /**
     * @return  \Balance\Box\Model\ResourceModel\Box\Collection
     */
    public function getBoxes()
    {
        if (!$this->hasData('boxes')) {
            $storeId = $this->_storeManager->getStore()->getId();
            /** @var \Balance\Box\Model\ResourceModel\Box\Collection $boxes */
            $boxes = $this->_boxCollectionFactory->create();
            $boxes->addFieldToFilter(Box::BOX_ID, ['in' => $this->getBoxIds()])
                ->addStoreFilter($storeId, true)
                ->addFieldToFilter(Box::IS_ACTIVE, Box::STATUS_ENABLED)
                ->addOrder(Box::POSITION, BoxCollection::SORT_ORDER_ASC);
            if ($boxes->count()) {
                $this->setData('boxes', $boxes);
            }
        }
        return $this->getData('boxes');
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getBoxes()) {
            return '';
        }
        return parent::_toHtml();
    }
}
