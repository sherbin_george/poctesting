<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block\Widget;

class Group
    extends \Balance\Box\Block\Group
    implements \Magento\Widget\Block\BlockInterface
{
}
