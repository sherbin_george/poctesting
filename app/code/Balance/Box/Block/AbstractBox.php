<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block;

use Balance\Box\Model\Box;
use Balance\Box\Model\BoxFactory;
use Balance\Box\Model\GroupFactory;
use Balance\Box\Model\ResourceModel\Box\CollectionFactory as BoxCollectionFactory;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;

/**
 * Class AbstractBox
 *
 * @package Balance\Box\Block
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class AbstractBox extends Template
{
    /**
     * @var \Balance\Box\Model\BoxFactory
     */
    protected $_boxFactory;

    /**
     * @var \Balance\Box\Model\GroupFactory
     */
    protected $_groupFactory;

    /**
     * @var \Balance\Box\Model\ResourceModel\Box\CollectionFactory
     */
    protected $_boxCollectionFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $datetime;

    /**#@+
     * Block template
     */
    const DEFAULT_TEMPLATE = '';

    /**
     * AbstractBox constructor.
     *
     * @param Context              $context
     * @param BoxFactory           $boxFactory
     * @param GroupFactory         $groupFactory
     * @param BoxCollectionFactory $boxCollectionFactory
     * @param Registry             $identifier
     * @param DateTime             $datetime
     * @param array                $data
     */
    public function __construct(
        Context $context,
        BoxFactory $boxFactory,
        GroupFactory $groupFactory,
        BoxCollectionFactory $boxCollectionFactory,
        Registry $identifier,
        DateTime $datetime,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_boxFactory = $boxFactory;
        $this->_groupFactory = $groupFactory;
        $this->_boxCollectionFactory = $boxCollectionFactory;
        $this->_coreRegistry = $identifier;
        $this->datetime = $datetime;

        if (!$this->hasData('template') && !empty(static::DEFAULT_TEMPLATE)) {
            $this->setTemplate(static::DEFAULT_TEMPLATE);
        }
    }

    /**
     * Swap all youtube urls to embed
     *
     * @param string $url
     *
     * @return string
     */
    public function convertYoutubeUrl($url)
    {
        return $this->escapeXssInUrl(
            preg_replace(
                '#(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))#x',
                'https://www.youtube.com/embed/$2',
                $url
            ) . '?enablejsapi=1'
        );
    }

    /**
     * Added suffix to normalised category to identifiers
     *
     * @param string $name
     * @param string $suffix
     *
     * @return void
     */
    public function addCategoryName($name, $suffix = '_banner')
    {
        $this->setData(Box::IDENTIFIER, $name . $suffix);
    }

    /**
     * Get category name from registry
     *
     * @return string
     */
    public function getCategoryName()
    {
        if ($categoryName = $this->_coreRegistry->registry('current_category')->getName()) {
            $categoryName = $this->normaliseCategoryName($categoryName);
        }

        return $categoryName;
    }

    /**
     * Check if using url for the whole image
     *
     * @param \Balance\Box\Model\Box $box
     *
     * @return bool
     */
    public function isUsingImageUrl(Box $box)
    {
        return (empty($box->getButtonText()) && $box->getLink()) ? true : false;
    }

    /**
     * Check if using url for button only
     *
     * @param \Balance\Box\Model\Box $box
     *
     * @return bool
     */
    public function isUsingButtonUrl(Box $box)
    {
        return (!empty($box->getButtonText()) && $box->getLink()) ? true : false;
    }

    /**
     * Check if box trackable through GA
     *
     * @param Box    $box
     * @param string $element
     *
     * @return string
     */
    public function getTrackingHtml(Box $box, $element = 'a')
    {
        $html = '';
        if ($box->isTrackable()) {
            switch ($element) {
                case 'a':
                    $html = 'onclick="';
                    $html .= "ga('send', 'event', 'Banner', 'Clicks', '" . $box->getTitle() . "', 1.00, {'nonInteraction': 1});";
                    $html .= '"';
                    break;
                case 'img':
                    $html = '<img width=0 height=0 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" onload="';
                    $html .= "ga('send', 'event', 'Banner', 'Impressions', '" . $box->getTitle() . "', 2.00, {'nonInteraction': 1});";
                    $html .= '" />';
                    break;
            }
        }

        return $html;
    }

    /**
     * Normalise category name by:
     * - Replaces all spaces with hyphens
     * - Removes special chars
     * - Replaces multiple hyphens with single one
     *
     * @param string $string
     *
     * @return string
     */
    protected function normaliseCategoryName($string)
    {
        $categoryName = strtolower(str_replace(' ', '-', $string));
        $categoryName = preg_replace('/[^A-Za-z0-9\-]/', '', $categoryName);

        return preg_replace('/-/', '-', $categoryName);
    }

    /**
     * Check if current box is published
     *
     * @param string $fromDate
     * @param string $toDate
     *
     * @return bool
     */
    protected function isPublished($fromDate, $toDate)
    {
        $isPublished = true;
        $currentTime = $this->datetime->timestamp();

        if ($currentTime <= $fromDate || $currentTime >= $toDate) {
            $isPublished = false;
        }

        return $isPublished;
    }
}
