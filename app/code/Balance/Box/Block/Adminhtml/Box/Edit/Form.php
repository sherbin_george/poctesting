<?php
/**
 * Copyright © 2016 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Block\Adminhtml\Box\Edit;

use Balance\Box\Model\Box\Source\Group;
use Balance\Box\Model\Box\Source\Layout;
use Balance\Box\Model\Box\Source\Type;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Cms\Model\Wysiwyg\Config;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\Store\Model\System\Store;

/**
 * Adminhtml box edit form
 */
class Form extends Generic
{
    /**
     * @var Config
     */
    protected $_wysiwygConfig;

    /**
     * @var Store
     */
    protected $_systemStore;

    /**
     * @var Group
     */
    protected $_groupSource;

    /**
     * @var Layout
     */
    protected $_layoutSource;

    /**
     * @var Type
     */
    protected $_typeSource;

    /**
     * @param Context     $context
     * @param Registry    $registry
     * @param FormFactory $formFactory
     * @param Config      $wysiwygConfig
     * @param Store       $systemStore
     * @param Group       $groupSource
     * @param Layout      $layoutSource
     * @param Type        $typeSource
     * @param array       $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        Store $systemStore,
        Group $groupSource,
        Layout $layoutSource,
        Type $typeSource,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_systemStore = $systemStore;
        $this->_groupSource = $groupSource;
        $this->_layoutSource = $layoutSource;
        $this->_typeSource = $typeSource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('box_form');
        $this->setTitle(__('Box Information'));
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareForm()
    {
        /** @var \Balance\Box\Model\Box $model */
        $model = $this->_coreRegistry->registry('box_box');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );

        $form->setHtmlIdPrefix('box_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('General Information'),
                'class' => 'fieldset-wide'
            ]
        );

        $fieldsetImages = $form->addFieldSet(
            'images_fieldset',
            [
                'legend' => __('Images'),
                'class' => 'fieldset-wide'
            ]
        );

        $fieldsetImages->addType('image', '\Balance\Box\Block\Adminhtml\Box\Helper\Image');

        $fieldsetContent = $form->addFieldset(
            'content_fieldset',
            [
                'legend' => __('Content'),
                'class' => 'fieldset-wide'
            ]
        );

        $fieldsetPublishing = $form->addFieldset(
            'publishing_fieldset',
            [
                'legend' => __('Publishing'),
                'class' => 'fieldset-wide'
            ]
        );

        if ($model->getId()) {
            $fieldset->addField('box_id', 'hidden', ['name' => 'box_id']);
        }

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Box Title'),
                'title' => __('Box Title'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'identifier',
            'text',
            [
                'name' => 'identifier',
                'label' => __('Identifier'),
                'title' => __('Identifier'),
                'required' => true,
                'class' => 'validate-xml-identifier'
            ]
        );

        $fieldset->addField(
            'group_id',
            'select',
            [
                'name' => 'group_id',
                'label' => __('Group'),
                'title' => __('Group'),
                'options' => $this->_groupSource->toFlatArray(true),
            ]
        );

        if (!$this->_storeManager->isSingleStoreMode()) {
            $field = $fieldset->addField(
                'store_ids',
                'multiselect',
                [
                    'name' => 'store_ids[]',
                    'label' => __('Store Views'),
                    'title' => __('Store Views'),
                    'required' => true,
                    'values' => $this->_systemStore->getStoreValuesForForm(false, true)
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                'Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element'
            );
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField(
                'store_ids',
                'hidden',
                ['name' => 'store_ids[]', 'value' => $this->_storeManager->getStore(true)->getId()]
            );
            $model->setStoreId($this->_storeManager->getStore(true)->getId());
        }

        $fieldset->addField(
            'group_identifier',
            'text',
            [
                'name' => 'group_identifier',
                'label' => __('Group Identifer'),
                'title' => __('Group Identifer')
            ]
        );

        $fieldset->addField(
            'position',
            'text',
            [
                'name' => 'position',
                'label' => __('Position'),
                'title' => __('Position'),
                'type' => 'number',
                'class' => 'validate-number'
            ]
        );

        $fieldset->addField(
            'is_trackable',
            'select',
            [
                'label' => __('GA Tracking'),
                'title' => __('GA Tracking'),
                'name' => 'is_trackable',
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );

        $fieldsetImages->addField(
            'desktop_image',
            'image',
            [
                'name' => 'desktop_image',
                'label' => __('Desktop Image'),
                'title' => __('Desktop Image'),
                'data_type' => \Balance\Box\Model\Box::DESKTOP_DIR
            ]
        );

        $fieldsetImages->addField(
            'tablet_image',
            'image',
            [
                'name' => 'tablet_image',
                'label' => __('Tablet Image'),
                'title' => __('Tablet Image'),
                'data_type' => \Balance\Box\Model\Box::TABLET_DIR
            ]
        );

        $fieldsetImages->addField(
            'mobile_image',
            'image',
            [
                'name' => 'mobile_image',
                'label' => __('Mobile Image'),
                'title' => __('Mobile Image'),
                'data_type' => \Balance\Box\Model\Box::MOBILE_DIR
            ]
        );

        $fieldsetImages->addField(
            'alt_text',
            'text',
            [
                'name' => 'alt_text',
                'label' => __('Alt Text'),
                'title' => __('Alt Text')
            ]
        );

        $fieldsetContent->addField(
            'type',
            'select',
            [
                'name' => 'type',
                'label' => __('Type'),
                'title' => __('Type'),
                'options' => $this->_typeSource->toFlatArray(true),
            ]
        );

        $fieldsetContent->addField(
            'video_link',
            'text',
            [
                'name' => 'video_link',
                'label' => __('Video Link'),
                'title' => __('Video Link'),
                'note' => 'Excluding base URL'
            ]
        );

        $showTextBlock = $fieldsetContent->addField(
            'show_text_block',
            'select',
            [
                'name' => 'show_text_block',
                'label' => __('Show Text Block?'),
                'title' => __('Show Text Block?'),
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );

        /**
         * START: Show Text Block value will affect to all fields below
         */
        $blockBgColour = $fieldsetContent->addField(
            'block_background',
            'text',
            [
                'name' => 'block_background',
                'class' => 'box-color {required:false,hash:true}',
                'label' => __('Block Background'),
                'title' => __('Block Background')
            ]
        );

        $textBlockColour = $fieldsetContent->addField(
            'text_block_colour',
            'select',
            [
                'name' => 'text_block_colour',
                'label' => __('Text Block Colour'),
                'title' => __('Text Block Colour'),
                'options' => [
                    'box-txt-white' => __('White'),
                    'box-txt-black' => __('Black')
                ]
            ]
        );

        $layout = $fieldsetContent->addField(
            'layout',
            'select',
            [
                'name' => 'layout',
                'label' => __('Layout'),
                'title' => __('Layout'),
                'options' => $this->_layoutSource->toFlatArray(true),
            ]
        );

        $heading = $fieldsetContent->addField(
            'heading',
            'text',
            [
                'name' => 'heading',
                'label' => __('Heading'),
                'title' => __('Heading')
            ]
        );

        $link = $fieldsetContent->addField(
            'link',
            'text',
            [
                'name' => 'link',
                'label' => __('Link'),
                'title' => __('Link'),
                'note' => 'Excluding base URL'
            ]
        );

        $buttonTxt = $fieldsetContent->addField(
            'button_text',
            'text',
            [
                'name' => 'button_text',
                'label' => __('Button Text'),
                'title' => __('Button Text')
            ]
        );

        //@TODO: Remove content field from dependence block because of default Magento 2 bug with editor
        $contentTextBlock = $fieldsetContent->addField(
            'content',
            'editor',
            [
                'name' => 'content',
                'label' => __('Content'),
                'title' => __('Content'),
                'style' => 'height:36em',
                'comment' => 'The content of text block depends on "Show Text Block" option',
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );
        /**
         * END: Show Text Block value will affect to all fields above
         */

        $fieldsetPublishing->addField(
            'from_date',
            'date',
            [
                'name' => 'from_date',
                'label' => __('From Date'),
                'title' => __('From Date'),
                'input_format' => DateTime::DATETIME_PHP_FORMAT,
                'date_format' => 'yyyy-MM-dd',
                'time_format' => 'hh:mm:ss'
            ]
        );

        $fieldsetPublishing->addField(
            'to_date',
            'date',
            [
                'name' => 'to_date',
                'label' => __('To Date'),
                'title' => __('To Date'),
                'input_format' => DateTime::DATE_INTERNAL_FORMAT,
                'date_format' => 'yyyy-MM-dd',
                'time_format' => 'hh:mm:ss'
            ]
        );

        $fieldsetPublishing->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );

        if (!$model->getId()) {
            $model->setIsActive(false);
        }

        /** @var \Magento\Backend\Block\Widget\Form\Element\Dependence $dependenceBlock */
        $dependenceBlock = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Form\Element\Dependence');
        $dependenceBlock->addFieldMap(
            $showTextBlock->getHtmlId(),
            $showTextBlock->getName()
        )->addFieldMap(
            $blockBgColour->getHtmlId(),
            $blockBgColour->getName()
        )->addFieldMap(
            $textBlockColour->getHtmlId(),
            $textBlockColour->getName()
        )->addFieldMap(
            $layout->getHtmlId(),
            $layout->getName()
        )->addFieldMap(
            $heading->getHtmlId(),
            $heading->getName()
        )->addFieldMap(
            $link->getHtmlId(),
            $link->getName()
        )->addFieldMap(
            $buttonTxt->getHtmlId(),
            $buttonTxt->getName()
        )->addFieldDependence(
            $blockBgColour->getName(),
            $showTextBlock->getName(),
            '1'
        )->addFieldDependence(
            $textBlockColour->getName(),
            $showTextBlock->getName(),
            '1'
        )->addFieldDependence(
            $layout->getName(),
            $showTextBlock->getName(),
            '1'
        )->addFieldDependence(
            $heading->getName(),
            $showTextBlock->getName(),
            '1'
        )->addFieldDependence(
            $link->getName(),
            $showTextBlock->getName(),
            '1'
        )->addFieldDependence(
            $buttonTxt->getName(),
            $showTextBlock->getName(),
            '1'
        );
        $this->setChild('form_after', $dependenceBlock);

        $dataForm = $model->getData();
        if ($boxId = $model->getBoxId()) {
            $storeIds = $model->getResource()->lookupStoreIdsByBoxId($boxId);
            if (count($storeIds) > 0) {
                $dataForm['store_ids'] = $storeIds;
            }
        }
        $form->setValues($dataForm);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
