<?php
/**
 * Copyright © 2016 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Api\Data;

interface BoxInterface
{
    const BOX_ID = 'box_id';
    const TITLE = 'title';
    const IDENTIFIER = 'identifier';
    const GROUP_ID = 'group_id';
    const GROUP_IDENTIFIER = 'group_identifier';
    const DESKTOP_IMAGE = 'desktop_image';
    const TABLET_IMAGE = 'tablet_image';
    const MOBILE_IMAGE = 'mobile_image';
    const DESKTOP_DIR = 'desktop';
    const TABLET_DIR = 'tablet';
    const MOBILE_DIR = 'mobile';
    const ALT_TEXT = 'alt_text';
    const HEADING = 'heading';
    const CONTENT = 'content';
    const LINK = 'link';
    const BUTTON_TEXT = 'button_text';
    const SHOW_TEXT_BLOCK = 'show_text_block';
    const BLOCK_BACKGROUND = 'block_background';
    const TEXT_BLOCK_COLOUR = 'text_block_colour';
    const TYPE = 'type';
    const VIDEO_LINK = 'video_link';
    const LAYOUT = 'layout';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const POSITION = 'position';
    const IS_TRACKABLE = 'is_trackable';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME = 'update_time';
    const IS_ACTIVE = 'is_active';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get identifier
     *
     * @return string|null
     */
    public function getIdentifier();

    /**
     * Get group ID
     *
     * @return int|null
     */
    public function getGroupId();

    /**
     * Get group identifier
     *
     * @return string|null
     */
    public function getGroupIdentifier();

    /**
     * Get desktop image
     *
     * @return string|null
     */
    public function getDesktopImage();

    /**
     * Get tablet image
     *
     * @return string|null
     */
    public function getTabletImage();

    /**
     * Get mobile image
     *
     * @return string|null
     */
    public function getMobileImage();

    /**
     * Get alt text
     *
     * @return string|null
     */
    public function getAltText();

    /**
     * Get heading
     *
     * @return string|null
     */
    public function getHeading();

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get link
     *
     * @return string|null
     */
    public function getLink();

    /**
     * Get button text
     *
     * @return string|null
     */
    public function getButtonText();

    /**
     * Get show text block
     *
     * @return bool|null
     */
    public function getShowTextBlock();

    /**
     * Get block background
     *
     * @return string|null
     */
    public function getBlockBackground();

    /**
     * Get text block colour
     *
     * @return int|null
     */
    public function getTextBlockColour();

    /**
     * Get layout
     *
     * @return string|null
     */
    public function getLayout();

    /**
     * Get type
     *
     * @return string|null
     */
    public function getType();

    /**
     * Get from date
     *
     * @return string|null
     */
    public function getFromDate();

    /**
     * Get to date
     *
     * @return string|null
     */
    public function getToDate();

    /**
     * Get position
     *
     * @return int|null
     */
    public function getPosition();

    /**
     * Is trackable
     *
     * @return bool|null
     */
    public function isTrackable();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setId($id);

    /**
     * Set title
     *
     * @param int $title
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setTitle($title);

    /**
     * Set identifier
     *
     * @param int $identifier
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setIdentifier($identifier);

    /**
     * Set group ID
     *
     * @param $group_id
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setGroupId($group_id);

    /**
     * Set group identifer
     *
     * @param $group_identifier
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setGroupIdentifier($group_identifier);

    /**
     * Set desktop image
     *
     * @param $desktop_image
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setDesktopImage($desktop_image);

    /**
     * Set tablet image
     *
     * @param $tablet_image
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setTabletImage($tablet_image);

    /**
     * Set mobile image
     *
     * @param $mobile_image
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setMobileImage($mobile_image);

    /**
     * Set alt text
     *
     * @param $alt_text
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setAltText($alt_text);

    /**
     * Set heading
     *
     * @param $heading
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setHeading($heading);

    /**
     * Set content
     *
     * @param $content
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setContent($content);

    /**
     * Set link
     *
     * @param $link
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setLink($link);

    /**
     * Set button text
     *
     * @param $button_text
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setButtonText($button_text);

    /**
     * Set show text block
     *
     * @param $show_text_block
     *
     * @return BoxInterface
     */
    public function setShowTextBlock($show_text_block);

    /**
     * Set block background
     *
     * @param $block_background
     *
     * @return BoxInterface
     */
    public function setBlockBackground($block_background);

    /**
     * Set text block colour
     *
     * @param $text_block_colour
     *
     * @return BoxInterface
     */
    public function setTextBlockColour($text_block_colour);

    /**
     * Set layout
     *
     * @param $layout
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setLayout($layout);

    /**
     * Set type
     *
     * @param $type
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setType($type);

    /**
     * Set from date
     *
     * @param $from_date
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setFromDate($from_date);

    /**
     * Set to date
     *
     * @param $to_date
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setToDate($to_date);

    /**
     * Set position
     *
     * @param $position
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setPosition($position);

    /**
     * Set is trackable
     *
     * @param $isTrackable
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setIsTrackable($isTrackable);

    /**
     * Set creation time
     *
     * @param string $creationTime
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     *
     * @return \Balance\Box\Api\Data\BoxInterface
     */
    public function setIsActive($isActive);
}
