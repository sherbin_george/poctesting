<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Box\Helper;

/**
 * Fixture helper
 *
 * @package Balance\Box\Helper
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Fixture extends \Balance\Core\Helper\Fixture
{
    /**#@+
     * Constants defined for keys of data array
     */
    const MODULE_NAME = 'Balance_Box';
    const FIXTURES_MODE = false;
    const DEBUG_MODE = false;
}