<?php
namespace Balance\AwBlogCustomFields\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class UpgradeSchema.
 * Upgrade module schema.
 *
 * @package Balance\AwBlogCustomFields\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var SchemaSetupInterface $_setup Setup model.
     */
    protected $_setup;
    /**
     * @var string $_setupVersion Current setup version.
     */
    private $_setupVersion;

    /**
     * Upgrade module schema.
     *
     * @param SchemaSetupInterface   $setup   Setup model.
     * @param ModuleContextInterface $context Context model.
     *
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // Share current setup version with all class functionality.
        $this->_setupVersion = $context->getVersion();

        $setup->startSetup();
        $this->_setup = $setup;
        $this->_addLayoutTypeColumn();
        $this->_addMediaContentColumn();
        $this->_addPostAttributeColumn();
        $setup->endSetup();
    }

    /**
     * Adds 'layout_type' column to 'aw_blog_post' table.
     *
     * @return void
     *
     * @throws NoSuchEntityException
     */
    private function _addLayoutTypeColumn()
    {
        if (version_compare($this->_setupVersion, '1.0.1') < 0) {
            $postTableName = $this->_setup->getTable('aw_blog_post');
            if (!$this->_setup->tableExists($postTableName)) {
                throw new NoSuchEntityException('\'aw_blog_post\' table does not exist.');
            }

            $connection = $this->_setup->getConnection();
            $connection->addColumn(
                $postTableName,
                'layout_type',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Custom Layout Type'
                ]
            );
        }
    }

    /**
     * Adds 'media_content' column to 'aw_blog_post' table.
     *
     * @return void
     *
     * @throws NoSuchEntityException
     */
    private function _addMediaContentColumn()
    {
        if (version_compare($this->_setupVersion, '1.0.2') < 0) {
            $postTableName = $this->_setup->getTable('aw_blog_post');
            if (!$this->_setup->tableExists($postTableName)) {
                throw new NoSuchEntityException('\'aw_blog_post\' table does not exist.');
            }

            $connection = $this->_setup->getConnection();
            $connection->addColumn(
                $postTableName,
                'media_content',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '2M',
                    'nullable' => true,
                    'comment' => 'Media content',
                    'after' => 'short_content'
                ]
            );
        }
    }

    /**
     * Adds 'post_attribute' column to 'aw_blog_post' table.
     *
     * @return void
     *
     * @throws NoSuchEntityException
     */
    private function _addPostAttributeColumn()
    {
        if (version_compare($this->_setupVersion, '1.0.3', '<')) {
            if (!$this->_setup->getConnection()->tableColumnExists('aw_blog_post', 'post_attribute')) {
                $this->_setup->getConnection()->addColumn(
                    $this->_setup->getTable('aw_blog_post'),
                    'post_attribute',
                    [
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'after'    => 'content',
                        'comment'  => 'post attribute'
                    ]
                );
            }
        }
    }
}