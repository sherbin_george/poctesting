<?php
namespace Balance\AwBlogCustomFields\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class InstallSchema.
 * Install module schema.
 *
 * @package Balance\AwBlogCustomFields\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var SchemaSetupInterface $_setup Setup model.
     */
    protected $_setup;

    /**
     * Install module schema.
     *
     * @param SchemaSetupInterface   $setup   Setup model.
     * @param ModuleContextInterface $context Context model.
     *
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->_setup = $setup;
        $this->_addThumbnailColumn();

        $setup->endSetup();
    }

    /**
     * Adds 'thumbnail' column to 'aw_blog_post' table.
     *
     * @return void
     *
     * @throws NoSuchEntityException
     */
    private function _addThumbnailColumn()
    {
        $postTableName = $this->_setup->getTable('aw_blog_post');
        if (!$this->_setup->tableExists($postTableName)) {
            throw new NoSuchEntityException('\'aw_blog_post\' table does not exist.');
        }

        $connection = $this->_setup->getConnection();
        $connection->addColumn(
            $postTableName,
            'thumbnail',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'Post Thumbnail'
            ]
        );
    }
}