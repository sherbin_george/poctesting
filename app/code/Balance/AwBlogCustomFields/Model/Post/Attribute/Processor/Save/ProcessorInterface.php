<?php
namespace Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save;

/**
 * Interface ProcessorInterface.
 * Extension attribute value processor interface.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save
 */
interface ProcessorInterface
{
    /**
     * Prepares extension attribute value before save.
     *
     * @param null|bool|int|string|array                     $value   Attribute value.
     * @param \Balance\AwBlogCustomFields\Model\Post $context Context post model.
     *
     * @return null|bool|int|string|array
     */
    public function prepareValue($value, $context);
}
