<?php
namespace Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Delete;

/**
 * Class Dummy.
 * Dummy extension attribute value processor.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Delete
 */
class Dummy implements ProcessorInterface
{
    /**
     * Skips extension attribute value processing.
     *
     * @param null|bool|int|string|array                     $value   Attribute value.
     * @param \Balance\AwBlogCustomFields\Model\Post $context Context post model.
     *
     * @return void
     */
    public function executeAfterDelete($value, $context)
    {
        unset($context);
        unset($value);
    }
}