<?php
namespace Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Delete;

/**
 * Class Thumbnail.
 * 'Thumbnail' extension attribute value processor.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save
 */
class Thumbnail implements ProcessorInterface
{
    /**
     * @var \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory Uploader factory.
     */
    protected $_uploaderFactory;

    /**
     * Thumbnail constructor.
     *
     * @param \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory Uploader
     *                                                                                                  factory.
     */
    function __construct(
        \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory
    )
    {
        $this->_uploaderFactory = $uploaderFactory;
    }

    /**
     * Performs stuff after 'thumbnail' extension attribute value is deleted.
     *
     * @param null|bool|int|string|array                     $value   Attribute value.
     * @param \Balance\AwBlogCustomFields\Model\Post $context Context post model.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function executeAfterDelete($value, $context)
    {
        if (empty($value)
            || is_null($context->getId())
        ) {
            return;
        }

        /** @var \Balance\AwBlogCustomFields\Model\Post\Thumbnail\Uploader $uploader */
        $uploader = $this->_uploaderFactory->create();

        // Flag controls whether parent directory should be deleted.
        $isPostIdIncluded = $this->_isIncludedInPath($context->getId(), dirname($value));
        $uploader->deleteUploadedFile($value, $isPostIdIncluded);

        // Check for possible errors.
        $uploaderLastError = $uploader->getLastError();
        if (!empty($uploaderLastError)) {
            throw new \Exception($uploaderLastError);
        }
    }

    /**
     * Determines whether specified part is included in the path as the last one.
     *
     * @param string $part Path part.
     * @param string $path Path.
     *
     * @return bool
     */
    protected function _isIncludedInPath($part, $path)
    {
        $parts = explode(DIRECTORY_SEPARATOR, $path);
        return $part == array_pop($parts);
    }
}