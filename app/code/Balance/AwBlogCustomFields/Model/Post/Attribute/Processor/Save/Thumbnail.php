<?php
namespace Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save;

/**
 * Class Thumbnail.
 * 'Thumbnail' extension attribute value processor.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save
 */
class Thumbnail implements ProcessorInterface
{
    /**
     * @var \Balance\AwBlogCustomFields\Helper\Data $_helper Helper instance.
     */
    protected $_helper;
    /**
     * @var \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory Uploader factory.
     */
    protected $_uploaderFactory;

    /**
     * Thumbnail constructor.
     *
     * @param \Balance\AwBlogCustomFields\Helper\Data                          $helper          Helper instance.
     * @param \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory Uploader
     *                                                                                                  factory.
     */
    function __construct(
        \Balance\AwBlogCustomFields\Helper\Data $helper,
        \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory
    )
    {
        $this->_helper = $helper;
        $this->_uploaderFactory = $uploaderFactory;
    }

    /**
     * Prepares 'thumbnail' extension attribute value before save.
     *
     * @param array|string                                   $value   Thumbnail attribute value.
     * @param \Balance\AwBlogCustomFields\Model\Post $context Context post model.
     *
     * @return string
     *
     * @throws \Exception
     */
    public function prepareValue($value, $context)
    {
        if (empty($value)) {
            // Usually means that thumbnail is deleted or was not uploaded at all.
            $this->_deleteThumbnailFile($context);
            return $value;
        }

        if (!is_array($value)) {
            // Usually means that thumbnail is not uploaded but file path is re-saved.
            return $value;
        }

        $fileInfo = array_shift($value);

        if (!isset($fileInfo['path'])
            || !isset($fileInfo['name'])
        ) {
            return '';
        }

        $this->_processThumbnailFile($fileInfo, $context->getId());

        // We have to re-save extension attribute value anyway, because it is overridden by main update request.
        return $this->_helper->getPathWithoutMediaDirectory($fileInfo['path'])
            . DIRECTORY_SEPARATOR
            . $fileInfo['name'];
    }

    /**
     * Deletes thumbnail file (folder).
     *
     * @param \Balance\AwBlogCustomFields\Model\Post $context Context post model.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function _deleteThumbnailFile($context)
    {
        if (is_null($context->getId())) {
            return;
        }

        /** @var \Balance\AwBlogCustomFields\Model\Post\Thumbnail\Uploader $uploader */
        $uploader = $this->_uploaderFactory->create();

        // For now we can't get original thumbnail file path from DB, it may be overridden by main update request.
        $thumbnailPath = $uploader->getBaseDirectoryPath()
            . DIRECTORY_SEPARATOR
            . $context->getId()
            . DIRECTORY_SEPARATOR
            . '*';

        $uploader->deleteUploadedFile($thumbnailPath, true);

        // Check for possible errors.
        $uploaderLastError = $uploader->getLastError();
        if (!empty($uploaderLastError)) {
            throw new \Exception($uploaderLastError);
        }
    }

    /**
     * Processed thumbnail file info.
     * Updates file info when thumbnail was processed successfully.
     *
     * @param array $fileInfo Thumbnail file info.
     * @param int   $postId   Related post ID.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function _processThumbnailFile(&$fileInfo, $postId)
    {
        if ($this->_isFileProcessed($fileInfo)) {
            // If file was already processed we can avoid its re-processing.
            return;
        }

        /** @var \Balance\AwBlogCustomFields\Model\Post\Thumbnail\Uploader $uploader */
        $uploader = $this->_uploaderFactory->create();
        $uploader->moveFileFromTmpDirectory($fileInfo, $postId);

        // Check for possible errors.
        $uploaderLastError = $uploader->getLastError();
        if (!empty($uploaderLastError)) {
            throw new \Exception($uploaderLastError);
        }

        $fileInfo = $uploader->getUploadedFileInfo();
    }

    /**
     * Determines whether file was already processed (already saved).
     *
     * @param array $fileInfo Thumbnail file info.
     *
     * @return bool
     */
    protected function _isFileProcessed($fileInfo)
    {
        return !empty($fileInfo['skip_processing']);
    }
}