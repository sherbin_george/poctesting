<?php
namespace Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Delete;

/**
 * Interface ProcessorInterface.
 * Extension attribute value processor interface.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Delete
 */
interface ProcessorInterface
{
    /**
     * Performs stuff after extension attribute value is deleted.
     *
     * @param null|bool|int|string|array                     $value   Attribute value.
     * @param \Balance\AwBlogCustomFields\Model\Post $context Context post model.
     *
     * @return void
     */
    public function executeAfterDelete($value, $context);
}
