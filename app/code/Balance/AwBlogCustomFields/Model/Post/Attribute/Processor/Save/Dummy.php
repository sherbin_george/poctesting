<?php
namespace Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save;

/**
 * Class Dummy.
 * Dummy extension attribute value processor.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save
 */
class Dummy implements ProcessorInterface
{
    /**
     * Skips extension attribute value processing.
     *
     * @param null|bool|int|string|array                     $value   Attribute value.
     * @param \Balance\AwBlogCustomFields\Model\Post $context Context post model.
     *
     * @return null|bool|int|string|array
     */
    public function prepareValue($value, $context)
    {
        unset($context);
        return $value;
    }
}