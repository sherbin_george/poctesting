<?php
namespace Balance\AwBlogCustomFields\Model\Post\LayoutType;

/**
 * Interface HandleInterface.
 * Layout type handle interface.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\LayoutType
 */
interface HandleInterface
{
    /**
     * Returns handle ID.
     *
     * @return null|string
     */
    public function getId();

    /**
     * Returns handle title.
     *
     * @return null|string
     */
    public function getTitle();

    /**
     * Returns layout update handle.
     *
     * @return null|string
     */
    public function getLayoutUpdateHandle();
}