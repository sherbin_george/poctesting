<?php
namespace Balance\AwBlogCustomFields\Model\Post\LayoutType;

/**
 * Class HandlesProvider.
 * Layout type handles provider model.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\LayoutType
 */
class HandlesProvider implements HandlesProviderInterface
{
    /**
     * @var array $_layoutTypeHandles Layout type handles.
     */
    protected $_layoutTypeHandles = [];

    /**
     * HandlesProvider constructor.
     *
     * @param array $layoutTypeHandles Layout type handles.
     */
    public function __construct($layoutTypeHandles)
    {
        $this->_addLayoutTypeHandles($layoutTypeHandles);
    }

    /**
     * Adds layout type handles.
     *
     * @param array $layoutTypeHandles Layout type handles.
     *
     * @return void
     */
    protected function _addLayoutTypeHandles($layoutTypeHandles)
    {
        if (!is_array($layoutTypeHandles)) {
            return;
        }

        foreach ($layoutTypeHandles as $handleItem) {
            if (isset($handleItem['visible'])
                && $handleItem['visible'] === false
                || !isset($handleItem['handle'])
            ) {
                continue;
            }

            $this->_validateLayoutTypeHandle($handleItem['handle']);
            $this->_layoutTypeHandles[] = $handleItem['handle'];
        }
    }

    /**
     * Validates layout type handle.
     *
     * @param HandleInterface $handle Layout type handle.
     *
     * @return void
     */
    protected function _validateLayoutTypeHandle(HandleInterface $handle)
    {
        // For now we just validate the type.
    }

    /**
     * Returns layout type handle list.
     *
     * @return array
     */
    public function getHandles()
    {
        return $this->_layoutTypeHandles;
    }

    /**
     * Returns handle instance by ID.
     *
     * @param string $handleId Handle ID.
     *
     * @return null|HandleInterface
     */
    public function getHandleById($handleId)
    {
        /** @var HandleInterface $handle */
        foreach ($this->getHandles() as $handle) {
            if ($handle->getId() == $handleId) {
                return $handle;
            }
        }

        return null;
    }
}