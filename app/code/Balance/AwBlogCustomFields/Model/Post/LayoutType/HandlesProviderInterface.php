<?php
namespace Balance\AwBlogCustomFields\Model\Post\LayoutType;

/**
 * Interface HandlesProviderInterface.
 * Layout type handles provider interface.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\LayoutType
 */
interface HandlesProviderInterface
{
    /**
     * Returns layout type handle list.
     *
     * @return array
     */
    public function getHandles();

    /**
     * Returns handle instance by ID.
     *
     * @param string $handleId Handle ID.
     *
     * @return null|HandleInterface
     */
    public function getHandleById($handleId);
}