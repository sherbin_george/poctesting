<?php
namespace Balance\AwBlogCustomFields\Model\Post\LayoutType\Handle;

/**
 * Class DefaultHandle.
 * Layout type default handle model.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\LayoutType\Handle
 */
class DefaultHandle
    extends AbstractHandle
    implements \Balance\AwBlogCustomFields\Model\Post\LayoutType\HandleInterface
{
    /**
     * @const string Handle ID.
     */
    const ID = 'default';
    /**
     * @const string Handle title.
     */
    const TITLE = 'Default';
    /**
     * @const string Layout update handle.
     */
    const LAYOUT_UPDATE_HANDLE = 'default';
}