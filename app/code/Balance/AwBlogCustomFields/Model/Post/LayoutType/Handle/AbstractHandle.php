<?php
namespace Balance\AwBlogCustomFields\Model\Post\LayoutType\Handle;

/**
 * Class AbstractHandle.
 * Layout type handle abstract model.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\LayoutType\Handle
 */
class AbstractHandle implements \Balance\AwBlogCustomFields\Model\Post\LayoutType\HandleInterface
{
    /**
     * @const null|string Default handle ID.
     */
    const ID = null;
    /**
     * @const null|string Default handle title.
     */
    const TITLE = null;
    /**
     * @const null|string Default layout update handle.
     */
    const LAYOUT_UPDATE_HANDLE = null;
    /**
     * @const string Default layout update handle prefix.
     */
    const LAYOUT_UPDATE_HANDLE_PREFIX = 'aw_blog_post_view_%s';

    /**
     * Returns handle ID.
     *
     * @return null|string
     */
    public function getId()
    {
        return static::ID;
    }

    /**
     * Returns handle title.
     *
     * @return null|string
     */
    public function getTitle()
    {
        return __(static::TITLE);
    }

    /**
     * Returns layout update handle.
     *
     * @return null|string
     */
    public function getLayoutUpdateHandle()
    {
        return !is_null(static::LAYOUT_UPDATE_HANDLE)
            ? sprintf(self::LAYOUT_UPDATE_HANDLE_PREFIX, static::LAYOUT_UPDATE_HANDLE)
            : static::LAYOUT_UPDATE_HANDLE;
    }
}