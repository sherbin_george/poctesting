<?php
namespace Balance\AwBlogCustomFields\Model\Post\Thumbnail;

/**
 * Class Uploader.
 * Uploads and handles thumbnail file.
 *
 * @package Balance\AwBlogCustomFields\Model\Post\Thumbnail
 */
class Uploader
{
    /**
     * @const string File ID.
     */
    const UPLOADED_FILE_ID = 'thumbnail';

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory $_uploaderFactory Uploader factory.
     */
    protected $_uploaderFactory;
    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteFactory $_directoryFactory Writable directory factory.
     */
    protected $_directoryFactory;
    /**
     * @var \Balance\AwBlogCustomFields\Helper\Data $_helper Helper instance.
     */
    protected $_helper;
    /**
     * @var string $_baseTmpDirectory Base tmp directory for thumbnails.
     */
    protected $_baseTmpDirectory;
    /**
     * @var string $_baseDirectory Base directory for thumbnails.
     */
    protected $_baseDirectory;
    /**
     * @var array $_allowedExtensions Allowed extension list.
     */
    protected $_allowedExtensions;
    /**
     * @var array Uploaded file info.
     */
    protected $_uploadedFileInfo = [];
    /**
     * @var string $_lastError Last error message.
     */
    protected $_lastError;

    /**
     * Uploader constructor.
     *
     * @param \Magento\MediaStorage\Model\File\UploaderFactory     $uploaderFactory   Uploaded factory.
     * @param \Magento\Framework\Filesystem\Directory\WriteFactory $directoryFactory  Directory factory.
     * @param \Balance\AwBlogCustomFields\Helper\Data      $helper            Helper instance.
     * @param string                                               $baseTmpDirectory  Base tmp directory.
     * @param string                                               $baseDirectory     Base directory.
     * @param array                                                $allowedExtensions Allowed extension list.
     */
    public function __construct(
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Filesystem\Directory\WriteFactory $directoryFactory,
        \Balance\AwBlogCustomFields\Helper\Data $helper,
        $baseTmpDirectory,
        $baseDirectory,
        $allowedExtensions = []
    )
    {
        $this->_uploaderFactory = $uploaderFactory;
        $this->_directoryFactory = $directoryFactory;
        $this->_helper = $helper;
        $this->_baseTmpDirectory = $baseTmpDirectory;
        $this->_baseDirectory = $baseDirectory;
        $this->_allowedExtensions = $allowedExtensions;
    }

    /**
     * Saves uploaded file to tmp directory.
     *
     * @return void
     */
    public function saveFileToTmpDirectory()
    {
        $this->_clearLastError();

        try {
            /** @var \Magento\MediaStorage\Model\File\Uploader $uploader */
            $uploader = $this->_uploaderFactory->create(
                ['fileId' => self::UPLOADED_FILE_ID]
            );
            $uploader->setAllowRenameFiles(true);
            $uploader->setAllowedExtensions($this->_allowedExtensions);

            $baseTmpDirectoryPath = $this->_getAbsoluteDirectoryPath($this->_baseTmpDirectory);
            $this->_uploadedFileInfo = $uploader->save($baseTmpDirectoryPath);
            $this->_addUrlToUploadedFile();
        } catch (\Exception $exception) {
            $this->_lastError = $exception->getMessage();
        }
    }

    /**
     * Moves uploaded file from tmp directory to general one.
     *
     * @param array       $fileInfo     Uploaded file info.
     * @param null|string $subDirectory Subdirectory name (optional).
     *
     * @return void
     */
    public function moveFileFromTmpDirectory($fileInfo, $subDirectory = null)
    {
        $this->_clearLastError();

        if (!isset($fileInfo['path'])
            || !isset($fileInfo['file'])
            || !isset($fileInfo['name'])
        ) {
            $this->_lastError = 'Cannot move the uploaded file from tmp directory. File data is missed.';
            return;
        }

        try {
            $destinationDirectoryPath = $this->_getAbsoluteDirectoryPath($this->_baseDirectory);
            $destinationDirectoryPath .= !empty($subDirectory) ? DIRECTORY_SEPARATOR . $subDirectory : '';

            /** @var \Magento\Framework\Filesystem\Directory\Write $destinationDirectory */
            $destinationDirectory = $this->_directoryFactory->create($destinationDirectoryPath);
            $destinationDirectory->create();

            /** @var \Magento\Framework\Filesystem\Directory\Write $tmpDirectory */
            $tmpDirectory = $this->_directoryFactory->create($fileInfo['path']);
            $tmpDirectory->copyFile(
                $fileInfo['file'],
                $fileInfo['name'],
                $destinationDirectory
            );
            $tmpDirectory->delete($fileInfo['file']);

            $fileInfo['path'] = $destinationDirectoryPath;
            $this->_uploadedFileInfo = $fileInfo;
        } catch (\Exception $exception) {
            $this->_lastError = $exception->getMessage();
        }
    }

    /**
     * Deletes uploaded file.
     *
     * @param string $filePath                  File path to be deleted.
     * @param bool   $deleteParentDirectoryFlag Determines whether parent directory should be deleted.
     *
     * @return void
     */
    public function deleteUploadedFile($filePath, $deleteParentDirectoryFlag = false)
    {
        $this->_clearLastError();

        if (empty($filePath)) {
            return;
        }

        try {
            $pathInfo = pathinfo($this->_getAbsoluteDirectoryPath($filePath));

            /** @var \Magento\Framework\Filesystem\Directory\Write $writableDirectory */
            $writableDirectory = $this->_directoryFactory->create(
                $pathInfo['dirname']
            );

            $writableDirectory->delete($pathInfo['basename']);
            if ($deleteParentDirectoryFlag) {
                $writableDirectory->delete();
            }
        }
        catch(\Exception $exception) {
            $this->_lastError = $exception->getMessage();
        }
    }

    /**
     * Returns uploaded file info.
     *
     * @return array
     */
    public function getUploadedFileInfo()
    {
        return $this->_uploadedFileInfo;
    }

    /**
     * Returns last error message.
     *
     * @return string
     */
    public function getLastError()
    {
        return $this->_lastError;
    }

    /**
     * Returns base directory path.
     *
     * @return string
     */
    public function getBaseDirectoryPath()
    {
        return $this->_baseDirectory;
    }

    /**
     * Clear last error.
     *
     * @return void
     */
    protected function _clearLastError()
    {
        $this->_lastError = '';
    }

    /**
     * Returns absolute path within 'media' directory.
     *
     * @param string $path Relative path.
     *
     * @return string
     */
    protected function _getAbsoluteDirectoryPath($path)
    {
        return $this->_helper->getMediaDirectoryPath($path);
    }

    /**
     * Adds url to uploaded file info.
     *
     * @return void
     */
    protected function _addUrlToUploadedFile()
    {
        if (!isset($this->_uploadedFileInfo['file'])) {
            return;
        }

        $this->_uploadedFileInfo['url'] = $this->_helper->getMediaUrl()
            . $this->_baseTmpDirectory
            . DIRECTORY_SEPARATOR
            . $this->_uploadedFileInfo['file'];
    }
}