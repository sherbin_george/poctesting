<?php
namespace Balance\AwBlogCustomFields\Model\ResourceModel;

/**
 * Class Post.
 * Extended post entity resource model.
 *
 * @package Balance\AwBlogCustomFields\Model\ResourceModel
 */
class Post extends \Aheadworks\Blog\Model\ResourceModel\Post
{
    /**
     * Returns extension attributes values for specified post.
     *
     * @param int   $postId              Post ID.
     * @param array $extensionAttributes Extension attribute list to be loaded.
     *
     * @return array
     */
    public function getExtensionAttributesValues($postId, $extensionAttributes = [])
    {
        $connection = $this->getConnection();

        /** @var \Magento\Framework\DB\Select $select */
        $select = $connection->select();
        $select->from($this->getMainTable(), $extensionAttributes);
        $select->where(
            sprintf('%s = ?', $this->getIdFieldName()),
            (int) $postId
        );

        return $connection->fetchRow($select);
    }

    /**
     * Saves post extension attributes.
     *
     * @param \Balance\AwBlogCustomFields\Model\Post $post Post instance.
     *
     * @return void
     */
    public function saveExtensionAttributes(\Balance\AwBlogCustomFields\Model\Post $post)
    {
        if (is_null($post->getId())) {
            return;
        }

        $this->updateObject($post);
    }
}
