<?php
namespace Balance\AwBlogCustomFields\Model\Aheadworks\Blog;

/**
 * Class Post.
 * Rewrite for original post model to specify model event prefix and object name.
 *
 * @package Balance\AwBlogCustomFields\Model\Aheadworks\Blog
 */
class Post extends \Aheadworks\Blog\Model\Post
{
    /**
     * @var string $_eventPrefix Prefix of model events names.
     */
    protected $_eventPrefix = 'aheadworks_blog_post';
    /**
     * @var string $_eventObject Parameter name in event.
     */
    protected $_eventObject = 'post';
}