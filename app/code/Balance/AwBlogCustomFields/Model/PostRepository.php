<?php
namespace Balance\AwBlogCustomFields\Model;

use \Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Class Repository.
 * Extended post entity repository model (for extension attributes handling).
 *
 * @package Balance\AwBlogCustomFields\Model\Post
 */
class PostRepository implements \Balance\AwBlogCustomFields\Api\PostRepositoryInterface
{
    /**
     * @var \Balance\AwBlogCustomFields\Model\PostFactory $_postFactory Post factory.
     */
    protected $_postFactory;
    /**
     * @var \Magento\Framework\Api\DataObjectHelper $_dataObjectHelper Data object helper.
     */
    protected $_dataObjectHelper;
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor $_dataObjectProcessor Data object processor.
     */
    protected $_dataObjectProcessor;
    /**
     * @var \Balance\AwBlogCustomFields\Model\ResourceModel\Post $_postResource Post resource model.
     */
    protected $_postResource;
    /**
     * @var array $_extensionAttributes Extension attribute list.
     */
    protected $_extensionAttributes;

    /**
     * PostRepository constructor.
     *
     * @param \Balance\AwBlogCustomFields\Model\PostFactory        $postFactory         Post factory.
     * @param \Magento\Framework\Api\DataObjectHelper                      $dataObjectHelper    Data object helper.
     * @param \Magento\Framework\Reflection\DataObjectProcessor            $dataObjectProcessor Data object processor.
     * @param \Balance\AwBlogCustomFields\Model\ResourceModel\Post $postResource        Data resource.
     * @param array                                                        $extensionAttributes Extension attribute
     *                                                                                          list.
     */
    public function __construct(
        \Balance\AwBlogCustomFields\Model\PostFactory $postFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Balance\AwBlogCustomFields\Model\ResourceModel\Post $postResource,
        $extensionAttributes = []
    )
    {
        $this->_postFactory = $postFactory;
        $this->_dataObjectProcessor = $dataObjectProcessor;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_postResource = $postResource;
        $this->_extensionAttributes = $extensionAttributes;
    }

    /**
     * Saves post extension attributes.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    public function save(\Aheadworks\Blog\Api\Data\PostInterface $post)
    {
        try {
            $objectToSave = $this->_getPostToSave($post);
            $this->_postResource->saveExtensionAttributes($objectToSave);
        } catch (\Exception $exception) {
            // Exceptions handling.
        }
    }

    /**
     * Returns new post instance with extension attributes only.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return \Balance\AwBlogCustomFields\Model\Post
     *
     * @throws \Exception
     */
    protected function _getPostToSave(\Aheadworks\Blog\Api\Data\PostInterface $post)
    {
        $postData = $this->_dataObjectProcessor->buildOutputDataArray(
            $post,
            \Aheadworks\Blog\Api\Data\PostInterface::class
        );

        if (!isset($postData[\Aheadworks\Blog\Api\Data\PostInterface::ID])) {
            throw new \Exception('Post ID should be specified for additional attributes saving.');
        }

        if (!isset($postData[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY])) {
            throw new \Exception('No post extension attributes to save.');
        }

        /** @var \Balance\AwBlogCustomFields\Model\Post $newPost */
        $newPost = $this->_postFactory->create();
        $newPost->setId(
            $postData[\Aheadworks\Blog\Api\Data\PostInterface::ID]
        );
        $newPost->addExtensionAttributes(
            $postData[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY]
        );

        return $newPost;
    }

    /**
     * Loads extension attributes to post instance.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    public function loadExtensionAttributes(\Aheadworks\Blog\Api\Data\PostInterface $post)
    {
        $extensionAttributesValues = $this->_postResource->getExtensionAttributesValues(
            $post->getId(),
            $this->_extensionAttributes
        );

        if (empty($extensionAttributesValues)) {
            return;
        }

        // TODO: check already set extension attributes values overriding.
        $this->_dataObjectHelper->populateWithArray(
            $post,
            [ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY => $extensionAttributesValues],
            \Aheadworks\Blog\Api\Data\PostInterface::class
        );
    }
}