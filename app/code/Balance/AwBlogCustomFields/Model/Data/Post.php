<?php
namespace Balance\AwBlogCustomFields\Model\Data;

/**
 * Class Post.
 * Extended post entity data model.
 *
 * @package Balance\AwBlogCustomFields\Model\Data
 */
class Post
    extends \Aheadworks\Blog\Model\Data\Post
    implements \Balance\AwBlogCustomFields\Api\Data\PostInterface
{
    /**
     * Returns relative path for thumbnail.
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->_get(self::THUMBNAIL);
    }

    /**
     * Sets relative path for thumbnail.
     *
     * @param string $thumbnail Relative path for thumbnail.
     *
     * @return $this
     */
    public function setThumbnail($thumbnail)
    {
        return $this->setData(self::THUMBNAIL, $thumbnail);
    }

    /**
     * Returns media content.
     *
     * @return string
     */
    public function getMediaContent()
    {
        return $this->_get(self::MEDIA_CONTENT);
    }

    /**
     * Sets media content.
     *
     * @param string $mediaContent Media content data.
     *
     * @return $this
     */
    public function setMediaContent($mediaContent)
    {
        return $this->setData(self::MEDIA_CONTENT, $mediaContent);
    }

    /**
     * Returns layout type value.
     *
     * @return string|null
     */
    public function getLayoutType()
    {
        return $this->_get(self::LAYOUT_TYPE);
    }

    /**
     * Sets layout type value.
     *
     * @param string $layoutType Layout type value.
     *
     * @return $this
     */
    public function setLayoutType($layoutType)
    {
        return $this->setData(self::LAYOUT_TYPE, $layoutType);
    }

    /**
     * Returns post attribute.
     *
     * @return string
     */
    public function getPostAttribute()
    {
        return $this->_get(self::POST_ATTRIBUTE);
    }

    /**
     * Sets post attribute.
     *
     * @param string $postAttribute post attribute data.
     *
     * @return $this
     */
    public function setPostAttribute($postAttribute)
    {
        return $this->setData(self::POST_ATTRIBUTE, $postAttribute);
    }
}