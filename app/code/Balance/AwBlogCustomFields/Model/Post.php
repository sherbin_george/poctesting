<?php
namespace Balance\AwBlogCustomFields\Model;

/**
 * Class Post.
 * Extended post entity model.
 *
 * @package Balance\AwBlogCustomFields\Model
 */
class Post
    extends \Aheadworks\Blog\Model\Post
    implements \Balance\AwBlogCustomFields\Api\Data\PostInterface
{
    /**
     * @var \Magento\Framework\Api\DataObjectHelper $_dataObjectHelper Data object helper.
     */
    protected $_dataObjectHelper;
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor $_dataObjectProcessor Data object processor.
     */
    protected $_dataObjectProcessor;
    /**
     * @var array $_attributeSaveProcessors Attribute save processor list.
     */
    protected $_attributeSaveProcessors;
    /**
     * @var array $_attributeDeleteProcessors Attribute delete processor list.
     */
    protected $_attributeDeleteProcessors;

    /**
     * Post constructor.
     *
     * @param \Magento\Framework\Model\Context                              $context                   Context model.
     * @param \Magento\Framework\Registry                                   $registry                  Registry model.
     * @param \Aheadworks\Blog\Model\Rule\ProductFactory                    $productRuleFactory        Product rule
     *                                                                                                 factory.
     * @param \Aheadworks\Blog\Model\Converter\Condition                    $conditionConverter        Condition
     *                                                                                                 converter.
     * @param \Aheadworks\Blog\Model\ResourceModel\Validator\UrlKeyIsUnique $urlKeyIsUnique            Url key
     *                                                                                                 validator.
     * @param \Magento\Framework\Api\DataObjectHelper                       $dataObjectHelper          Data object
     *                                                                                                 helper.
     * @param \Magento\Framework\Reflection\DataObjectProcessor             $dataObjectProcessor       Data object
     * @param null                                                          $resource                  Resource model.
     * @param null                                                          $resourceCollection        Collection
     *                                                                                                 model.
     * @param array                                                         $data                      Data array.
     * @param array                                                         $attributeSaveProcessors   Attribute save
     *                                                                                                 processors.
     * @param array                                                         $attributeDeleteProcessors Attribute delete
     *                                                                                                 processors.
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Aheadworks\Blog\Model\Rule\ProductFactory $productRuleFactory,
        \Aheadworks\Blog\Model\Converter\Condition $conditionConverter,
        \Aheadworks\Blog\Model\ResourceModel\Validator\UrlKeyIsUnique $urlKeyIsUnique,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        $resource = null,
        $resourceCollection = null,
        array $data = [],
        array $attributeSaveProcessors = [],
        array $attributeDeleteProcessors = []
    )
    {
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_dataObjectProcessor = $dataObjectProcessor;
        $this->_attributeSaveProcessors = $attributeSaveProcessors;
        $this->_attributeDeleteProcessors = $attributeDeleteProcessors;

        parent::__construct(
            $context,
            $registry,
            $productRuleFactory,
            $conditionConverter,
            $urlKeyIsUnique,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Returns relative path for thumbnail.
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->getData(self::THUMBNAIL);
    }

    /**
     * Sets relative path for thumbnail.
     *
     * @param string $thumbnail Relative path for thumbnail.
     *
     * @return $this
     */
    public function setThumbnail($thumbnail)
    {
        return $this->setData(self::THUMBNAIL, $thumbnail);
    }

    /**
     * Returns media content data.
     *
     * @return string
     */
    public function getMediaContent()
    {
        return $this->getData(self::MEDIA_CONTENT);
    }

    /**
     * Sets media content data.
     *
     * @param string $mediaContent Media content data.
     *
     * @return $this
     */
    public function setMediaContent($mediaContent)
    {
        return $this->setData(self::MEDIA_CONTENT, $mediaContent);
    }

    /**
     * Returns layout type value.
     *
     * @return string|null
     */
    public function getLayoutType()
    {
        return $this->getData(self::LAYOUT_TYPE);
    }

    /**
     * Sets layout type value.
     *
     * @param string $layoutType Layout type value.
     *
     * @return $this
     */
    public function setLayoutType($layoutType)
    {
        return $this->setData(self::LAYOUT_TYPE, $layoutType);
    }

    /**
     * Returns post attribute data.
     *
     * @return string
     */
    public function getPostAttribute()
    {
        return $this->getData(self::POST_ATTRIBUTE);
    }

    /**
     * Sets post attribute data.
     *
     * @param string $postAttribute post attribute data.
     *
     * @return $this
     */
    public function setPostAttribute($postAttribute)
    {
        return $this->setData(self::POST_ATTRIBUTE, $postAttribute);
    }

    /**
     * Populates post instance with extension attributes.
     *
     * @param array $extensionAttributes Extension attribute list.
     *
     * @return void
     */
    public function addExtensionAttributes($extensionAttributes)
    {
        $this->_dataObjectHelper->populateWithArray(
            $this,
            $this->_prepareExtensionAttributes($extensionAttributes),
            \Balance\AwBlogCustomFields\Api\Data\PostInterface::class
        );
    }

    /**
     * Returns extension attributes prepared for saving.
     *
     * @param array $extensionAttributes Extension attribute list.
     *
     * @return array
     */
    protected function _prepareExtensionAttributes($extensionAttributes)
    {
        $preparedExtensionAttributes = [];
        foreach ($extensionAttributes as $attributeName => $attributeValue) {
            try {
                $processor = $this->_getAttributeSaveProcessor($attributeName);
                $preparedExtensionAttributes[$attributeName] = $processor->prepareValue($attributeValue, $this);
            } catch (\Exception $exception) {
                continue;
            }
        }

        return $preparedExtensionAttributes;
    }

    /**
     * Returns extension attribute value processor.
     *
     * @param string $attributeName Attribute name to be processed.
     *
     * @return \Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Save\ProcessorInterface
     *
     * @throws \Exception
     */
    protected function _getAttributeSaveProcessor($attributeName)
    {
        if (isset($this->_attributeSaveProcessors[$attributeName])) {
            return $this->_attributeSaveProcessors[$attributeName];
        }

        if (isset($this->_attributeSaveProcessors['dummy'])) {
            return $this->_attributeSaveProcessors['dummy'];
        }

        throw new \Exception('Extension attribute processor not found.');
    }

    /**
     * Processing manipulation after main transaction commit.
     * Performs extension attributes delete processors execution.
     *
     * @return void
     */
    public function afterDeleteCommit()
    {
        parent::afterDeleteCommit();

        /** @var \Aheadworks\Blog\Api\Data\PostExtensionInterface $extensionAttributes */
        $extensionAttributes = $this->getExtensionAttributes();
        if (is_null($extensionAttributes)) {
            return;
        }

        $extensionAttributeList = $this->_dataObjectProcessor->buildOutputDataArray(
            $extensionAttributes,
            \Aheadworks\Blog\Api\Data\PostExtensionInterface::class
        );

        foreach ($extensionAttributeList as $attributeName => $attributeValue) {
            try {
                $processor = $this->_getAttributeDeleteProcessor($attributeName);
                $processor->executeAfterDelete($attributeValue, $this);
            } catch (\Exception $exception) {
                continue;
            }
        }
    }

    /**
     * Returns extension attribute delete processor.
     *
     * @param string $attributeName Attribute name to be processed.
     *
     * @return \Balance\AwBlogCustomFields\Model\Post\Attribute\Processor\Delete\ProcessorInterface
     *
     * @throws \Exception
     */
    protected function _getAttributeDeleteProcessor($attributeName)
    {
        if (isset($this->_attributeDeleteProcessors[$attributeName])) {
            return $this->_attributeDeleteProcessors[$attributeName];
        }

        if (isset($this->_attributeDeleteProcessors['dummy'])) {
            return $this->_attributeDeleteProcessors['dummy'];
        }

        throw new \Exception('Extension attribute processor not found.');
    }
}