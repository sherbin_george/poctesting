<?php
namespace Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Model\ResourceModel;

use \Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Class PostRepository.
 *
 * @package Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Model\ResourceModel
 */
class PostRepository
{
    /**
     * @var \Magento\Framework\Api\DataObjectHelper $_dataObjectHelper Data object helper.
     */
    protected $_dataObjectHelper;
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor $_dataObjectProcessor Data object processor.
     */
    protected $_dataObjectProcessor;
    /**
     * @var \Aheadworks\Blog\Api\Data\PostInterfaceFactory $_postFactory Post factory.
     */
    protected $_postFactory;
    /**
     * @var \Aheadworks\Blog\Model\PostRegistry $_postRegistry Post registry.
     */
    protected $_postRegistry;
    /**
     * @var \Balance\AwBlogCustomFields\Api\PostRepositoryInterface $_postRepository Post repository.
     */
    protected $_postRepository;
    /**
     * @var \Aheadworks\Blog\Api\Data\PostExtensionInterface $_extensionAttributes Post extension attributes.
     */
    private $_extensionAttributes;

    /**
     * PostRepository constructor.
     *
     * @param \Magento\Framework\Api\DataObjectHelper                         $dataObjectHelper    Data object helper.
     * @param \Magento\Framework\Reflection\DataObjectProcessor               $dataObjectProcessor Data object
     *                                                                                             processor.
     * @param \Aheadworks\Blog\Api\Data\PostInterfaceFactory                  $postFactory         Post factory.
     * @param \Aheadworks\Blog\Model\PostRegistry                             $postRegistry        Post registry.
     * @param \Balance\AwBlogCustomFields\Api\PostRepositoryInterface $postRepository      Post repository.
     */
    public function __construct(
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Aheadworks\Blog\Api\Data\PostInterfaceFactory $postFactory,
        \Aheadworks\Blog\Model\PostRegistry $postRegistry,
        \Balance\AwBlogCustomFields\Api\PostRepositoryInterface $postRepository
    )
    {
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_dataObjectProcessor = $dataObjectProcessor;
        $this->_postFactory = $postFactory;
        $this->_postRegistry = $postRegistry;
        $this->_postRepository = $postRepository;
    }

    /**
     * Around get plugin.
     *
     * @param \Aheadworks\Blog\Api\PostRepositoryInterface $subject Post repository.
     * @param \Closure                                     $proceed Closure.
     * @param int                                          $postId  Post ID.
     *
     * @return \Aheadworks\Blog\Api\Data\PostInterface
     */
    public function aroundGet(
        \Aheadworks\Blog\Api\PostRepositoryInterface $subject,
        \Closure $proceed,
        $postId
    )
    {
        // Variable is not used for now.
        unset($subject);

        $isPresentInRegistry = !is_null($this->_postRegistry->retrieve($postId));
        $result = $proceed($postId);
        if (!$isPresentInRegistry) {
            $this->_postRepository->loadExtensionAttributes($result);
        }

        return $result;
    }

    /**
     * Around get list plugin.
     *
     * @param \Aheadworks\Blog\Api\PostRepositoryInterface   $subject Post repository.
     * @param \Closure                                       $proceed Closure.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria.
     *
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function aroundGetList(
        \Aheadworks\Blog\Api\PostRepositoryInterface $subject,
        \Closure $proceed,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    )
    {
        // Variable is not used for now.
        unset($subject);

        /** @var \Magento\Framework\Api\SearchResultsInterface $result */
        $result = $proceed($searchCriteria);
        foreach ($result->getItems() as $post) {
            $this->_postRepository->loadExtensionAttributes($post);
        }

        return $result;
    }

    /**
     * Around save plugin.
     * NOTE: here is a workaround for original post repository model
     * to prevent fatal errors during post instance with extension attributes saving.
     *
     * @param \Aheadworks\Blog\Api\PostRepositoryInterface $subject Post repository.
     * @param \Closure                                     $proceed Closure.
     * @param \Aheadworks\Blog\Api\Data\PostInterface      $post    Processed post instance.
     *
     * @return \Aheadworks\Blog\Api\Data\PostInterface
     */
    public function aroundSave(
        \Aheadworks\Blog\Api\PostRepositoryInterface $subject,
        \Closure $proceed,
        \Aheadworks\Blog\Api\Data\PostInterface $post
    )
    {
        // Variable is not used for now.
        unset($subject);

        $post = $this->_extractExtensionAttributes($post);
        $result = $proceed($post);
        $this->_restoreExtensionAttributes($result);
        $this->_saveExtensionAttributes($result);

        return $result;
    }

    /**
     * Saves post extension attributes (custom fields).
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    private function _saveExtensionAttributes($post)
    {
        $this->_postRepository->save($post);
    }

    /**
     * Extracts extension attributes from the instance, new instance is returned.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return \Aheadworks\Blog\Api\Data\PostInterface
     */
    private function _extractExtensionAttributes($post)
    {
        $this->_extensionAttributes = $post->getExtensionAttributes();
        if (is_null($this->_extensionAttributes)) {
            return $post;
        }

        $postData = $this->_dataObjectProcessor->buildOutputDataArray(
            $post,
            \Aheadworks\Blog\Api\Data\PostInterface::class
        );

        unset($postData[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY]);

        /** @var \Aheadworks\Blog\Api\Data\PostInterface $newPost */
        $newPost = $this->_postFactory->create();
        $this->_dataObjectHelper->populateWithArray(
            $newPost,
            $postData,
            \Aheadworks\Blog\Api\Data\PostInterface::class
        );

        $post = null;

        return $newPost;
    }

    /**
     * Restores extension attributes within the instance.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    private function _restoreExtensionAttributes($post)
    {
        if (is_null($this->_extensionAttributes)) {
            return;
        }

        $post->setExtensionAttributes($this->_extensionAttributes);
    }
}