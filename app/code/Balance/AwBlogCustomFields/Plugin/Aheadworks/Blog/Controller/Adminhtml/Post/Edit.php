<?php
namespace Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Controller\Adminhtml\Post;

use \Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Class Edit.
 * Plugin class for post edit action.
 *
 * @package Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Controller\Adminhtml\Post
 */
class Edit
{
    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface $_dataPersistor Data persistor.
     */
    protected $_dataPersistor;

    /**
     * Edit constructor.
     *
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor Data persistor.
     */
    function __construct(\Magento\Framework\App\Request\DataPersistorInterface $dataPersistor)
    {
        $this->_dataPersistor = $dataPersistor;
    }

    /**
     * Before execute plugin.
     * Remove extension attribute list on edit page reloading (an error has been occurred).
     *
     * @return void
     */
    public function beforeExecute(\Aheadworks\Blog\Controller\Adminhtml\Post\Edit $subject)
    {
        unset($subject);

        $params = $this->_dataPersistor->get('aw_blog_post');
        if (!isset($params[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY])
            || !is_array($params[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY])
        ) {
            return;
        }

        unset($params[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY]);
        $this->_dataPersistor->set('aw_blog_post', $params);
    }
}