<?php
namespace Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Controller\Adminhtml\Post;

use \Magento\Framework\Api\ExtensibleDataInterface;
use Balance\AwBlogCustomFields\Api\Data\PostInterface;

/**
 * Class Save.
 * Plugin class for post save action.
 *
 * @package Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Controller\Adminhtml\Post
 */
class Save
{
    /**
     * @var array $_extensionAttributes Extension attribute list.
     */
    protected $_extensionAttributes;

    /**
     * Save constructor.
     *
     * @param array $extensionAttributes Extension attributes list.
     */
    public function __construct($extensionAttributes = [])
    {
        $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Sets extension attributes after simple check.
     *
     * @param array $extensionAttributes Extension attributes.
     *
     * @return void
     */
    private function _setExtensionAttributes($extensionAttributes)
    {
        if (!is_array($extensionAttributes)) {
            $this->_extensionAttributes = [];
        }

        foreach ($extensionAttributes as $attributeName => $attributeData) {
            if (!isset($attributeData['attribute_name'])
                || !isset($attributeData['default_value'])
            ) {
                continue;
            }

            $this->_extensionAttributes[$attributeName] = $attributeData;
        }
    }

    /**
     * Before execute plugin.
     * Stores extension attributes values into extension attribute list to be processed by post repository.
     *
     * @return void
     */
    public function beforeExecute(\Aheadworks\Blog\Controller\Adminhtml\Post\Save $subject)
    {
        $controller = $subject;

        $params = $controller->getRequest()->getPostValue();
        foreach ($this->_extensionAttributes as $attribute) {
            $attributeName = $attribute['attribute_name'];
            $defaultValue = $attribute['default_value'];

            $attributeValue = isset($params[$attributeName])
                ? $params[$attributeName]
                : $defaultValue;

            $params[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY][$attributeName] = $attributeValue;

            if (isset($attribute['depends_on'])
                && empty($params[$attribute['depends_on']])
            ) {
                // Use default value when dependency is empty.
                $params[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY][$attributeName] = $defaultValue;
            }

            unset($params[$attribute['attribute_name']]);
        }

        $controller->getRequest()->setPostValue($params);
    }
}