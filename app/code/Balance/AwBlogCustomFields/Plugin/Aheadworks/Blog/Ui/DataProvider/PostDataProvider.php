<?php
namespace Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Ui\DataProvider;
use Balance\AwBlogCustomFields\Api\Data\PostInterface;

/**
 * Class PostDataProvider.
 * Plugin class for post data provider model.
 *
 * @package Balance\AwBlogCustomFields\Plugin\Aheadworks\Blog\Ui\DataProvider
 */
class PostDataProvider
{
    /**
     * @var array $_attributeProcessors Attribute processor list.
     */
    protected $_attributeProcessors;

    /**
     * PostDataProvider constructor.
     *
     * @param array $attributeProcessors Attribute processor list.
     */
    public function __construct(
        $attributeProcessors
    )
    {
        $this->_attributeProcessors = $attributeProcessors;
    }

    /**
     * After getData plugin.
     * Adds prepared extension attributes values to result.
     *
     * @param \Aheadworks\Blog\Ui\DataProvider\PostDataProvider $subject Data provider model.
     * @param array                                             $result  Result data.
     *
     * @return array
     */
    public function afterGetData(
        \Aheadworks\Blog\Ui\DataProvider\PostDataProvider $subject,
        array $result
    )
    {
        unset($subject);

        foreach ($result as $documentId => $documentData) {
            $preparedExtensionAttributes = $this->_getPreparedExtensionAttributes($documentData);

            if (empty($preparedExtensionAttributes)) {
                continue;
            }

            $result[$documentId] = array_merge(
                $result[$documentId],
                $preparedExtensionAttributes
            );
        }

        return $result;
    }

    /**
     * Returns prepared extension attributes values for specified data.
     *
     * @param array $data Data to be processed.
     *
     * @return array
     */
    protected function _getPreparedExtensionAttributes($data)
    {
        $preparedExtensionAttributes = [];

        foreach ($this->_attributeProcessors as $attributeName => $attributeProcessor) {
            // In some cases we should inject new attributes, so there may be no initial value.
            $value = isset($data[$attributeName])
                ? $data[$attributeName]
                : null;

            try {
                $preparedExtensionAttributes[$attributeName] = $attributeProcessor->prepareValue($value, $data);
            } catch (\Exception $exception) {
                continue;
            }
        }

        return $preparedExtensionAttributes;
    }
}