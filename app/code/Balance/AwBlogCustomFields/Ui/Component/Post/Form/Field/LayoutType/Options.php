<?php
namespace Balance\AwBlogCustomFields\Ui\Component\Post\Form\Field\LayoutType;

use Balance\AwBlogCustomFields\Model\Post\LayoutType\HandleInterface;
use Balance\AwBlogCustomFields\Model\Post\LayoutType\HandlesProviderInterface;

/**
 * Class Options.
 * Provides layout type attribute options.
 *
 * @package Balance\AwBlogCustomFields\Ui\Component\Post\Form\Field\LayoutType
 */
class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var HandlesProviderInterface $_layoutTypeHandlesProvider Layout type handles provider.
     */
    protected $_layoutTypeHandlesProvider;

    /**
     * Options constructor.
     *
     * @param HandlesProviderInterface $layoutTypeHandlesProvider Layout type handles provider.
     */
    function __construct(HandlesProviderInterface $layoutTypeHandlesProvider)
    {
        $this->_layoutTypeHandlesProvider = $layoutTypeHandlesProvider;
    }

    /**
     * Return layout type attribute options.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $layoutTypeHandles = [];

        /** @var HandleInterface $layoutTypeHandle */
        foreach ($this->_layoutTypeHandlesProvider->getHandles() as $layoutTypeHandle) {
            $layoutTypeHandles[] = [
                'value' => $layoutTypeHandle->getId(),
                'label' => $layoutTypeHandle->getTitle()
            ];
        }

        return $layoutTypeHandles;
    }
}