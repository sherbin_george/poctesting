<?php
namespace Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor;

use Magento\Framework\Exception\InputException;

/**
 * Class Thumbnail.
 * 'Thumbnail' extension attribute value processor.
 *
 * @package Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor
 */
class Thumbnail implements ProcessorInterface
{
    /**
     * @var \Balance\AwBlogCustomFields\Helper\Data $_helper Helper instance.
     */
    protected $_helper;

    /**
     * Thumbnail constructor.
     *
     * @param \Balance\AwBlogCustomFields\Helper\Data $helper Helper instance.
     */
    function __construct(
        \Balance\AwBlogCustomFields\Helper\Data $helper
    )
    {
        $this->_helper = $helper;
    }

    /**
     * Prepares 'thumbnail' extension attribute value on UI component rendering.
     *
     * @param null|string                $value       Attribute value.
     * @param null|bool|int|string|array $relatedData Related data.
     *
     * @return array
     *
     * @throws InputException
     */
    public function prepareValue($value, $relatedData)
    {
        if (is_null($value)) {
            throw new InputException();
        }

        $thumbnailFullPath = $this->_helper->getMediaDirectoryPath($value);

        $pathParts = pathinfo($thumbnailFullPath);
        $thumbnailData['path'] = $pathParts['dirname'];
        $thumbnailData['name'] = $pathParts['basename'];

        $thumbnailData['size'] = filesize($thumbnailFullPath);
        $thumbnailData['type'] = mime_content_type($thumbnailFullPath);
        $thumbnailData['url'] = $this->_helper->getMediaUrl() . $value;

        // Flag determines whether thumbnail should be processed on post save.
        $thumbnailData['skip_processing'] = true;

        return [$thumbnailData];
    }
}