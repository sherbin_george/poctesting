<?php
namespace Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor;

use Balance\AwBlogCustomFields\Api\Data\PostInterface;

/**
 * Class HasMediaContent.
 * 'Has Media Content' extension attribute value processor.
 *
 * @package Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor
 */
class HasMediaContent implements ProcessorInterface
{
    /**
     * Prepares 'media' extension attribute value on UI component rendering.
     *
     * @param null|string                $value       Attribute value.
     * @param null|bool|int|string|array $relatedData Related data.
     *
     * @return bool
     */
    public function prepareValue($value, $relatedData)
    {
        return !empty($relatedData[PostInterface::MEDIA_CONTENT]);
    }
}