<?php
namespace Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor;

/**
 * Interface ProcessorInterface.
 * Extension attribute value processor interface.
 *
 * @package Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor
 */
interface ProcessorInterface
{
    /**
     * Prepares extension attribute value on UI component rendering.
     *
     * @param null|bool|int|string|array $value       Attribute value.
     * @param null|bool|int|string|array $relatedData Related data.
     *
     * @return null|bool|int|string|array
     *
     * @throws \Magento\Framework\Exception\InputException
     */
    public function prepareValue($value, $relatedData);
}