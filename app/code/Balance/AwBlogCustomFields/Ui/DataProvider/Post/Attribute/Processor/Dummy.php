<?php
namespace Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor;

use Magento\Framework\Exception\InputException;

/**
 * Class Dummy.
 * Dummy extension attribute value processor.
 *
 * @package Balance\AwBlogCustomFields\Ui\DataProvider\Post\Attribute\Processor
 */
class Dummy implements ProcessorInterface
{
    /**
     * Skips extension attribute value processing.
     *
     * @param null|bool|int|string|array $value       Attribute value.
     * @param null|bool|int|string|array $relatedData Related data.
     *
     * @return null|bool|int|string|array
     *
     * @throws InputException
     */
    public function prepareValue($value, $relatedData)
    {
        unset($relatedData);

        if (is_null($value)) {
            throw new InputException();
        }

        return $value;
    }
}