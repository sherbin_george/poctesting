<?php
namespace Balance\AwBlogCustomFields\Helper;

/**
 * Class Data.
 * Base helper class.
 *
 * @package Balance\AwBlogCustomFields\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList $_directoryList Directory list model.
     */
    protected $_directoryList;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface $_storeManager Store manager.
     */
    protected $_storeManager;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context           $context       Context model.
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList Directory list model.
     * @param \Magento\Store\Model\StoreManagerInterface      $storeManager  Store manager.
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_directoryList = $directoryList;
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * Returns absolute path within media directory.
     *
     * @param null|string $path Relative path (optional).
     *
     * @return string
     */
    public function getMediaDirectoryPath($path = null)
    {
        $mediaDirectory = $this->_directoryList->getPath(
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );

        $mediaDirectory .= !empty($path) ? DIRECTORY_SEPARATOR . $path : '';

        return $mediaDirectory;
    }

    /**
     * Returns relative path (without media directory path).
     *
     * @param string $path Absolute path within media directory.
     *
     * @return string
     */
    public function getPathWithoutMediaDirectory($path)
    {
        $mediaDirectory = $this->_directoryList->getPath(
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );

        return str_replace($mediaDirectory . DIRECTORY_SEPARATOR, '', $path);
    }

    /**
     * Returns media url by store.
     *
     * @param \Magento\Store\Model\Store|null $store Store instance (optional).
     *
     * @return string
     */
    public function getMediaUrl($store = null)
    {
        if (is_null($store)) {
            /** @var \Magento\Store\Model\Store $store */
            $store = $this->_storeManager->getStore();
        }

        return $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
}