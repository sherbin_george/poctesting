<?php
namespace Balance\AwBlogCustomFields\Controller\Post;

use Aheadworks\Blog\Api\CategoryRepositoryInterface;
use Aheadworks\Blog\Api\Data\PostInterface;
use Aheadworks\Blog\Api\PostRepositoryInterface;
use Aheadworks\Blog\Api\TagRepositoryInterface;
use Aheadworks\Blog\Model\Config;
use Aheadworks\Blog\Model\Source\Post\Status;
use Aheadworks\Blog\Model\Url;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\Controller\Result\Redirect as ResultRedirect;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\Forward as ResultForward;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Result\Page as ResultPage;
use Magento\Store\Model\StoreManagerInterface;
use Balance\AwBlogCustomFields\Model\Post\LayoutType\HandleInterface;
use Balance\AwBlogCustomFields\Model\Post\LayoutType\HandlesProviderInterface;

/**
 * Class View.
 * Post view controller action class.
 * NOTE: the controller action is overridden to make it possible to add custom layout update handles.
 *
 * @package Aheadworks\Blog\Controller\Post
 */
class View extends \Aheadworks\Blog\Controller\Action
{
    /**
     * @var HandlesProviderInterface $_layoutTypeHandlesProvider Layout type handles provider.
     */
    protected $_layoutTypeHandlesProvider;

    /**
     * View constructor.
     *
     * @param Context                     $context                   Context model.
     * @param PageFactory                 $resultPageFactory         Result page factory.
     * @param ForwardFactory              $resultForwardFactory      Result forward factory.
     * @param StoreManagerInterface       $storeManager              Store manager.
     * @param Registry                    $coreRegistry              Core registry.
     * @param CategoryRepositoryInterface $categoryRepository        Category repository.
     * @param PostRepositoryInterface     $postRepository            Post repository.
     * @param TagRepositoryInterface      $tagRepository             Tag repository.
     * @param Config                      $config                    Config model.
     * @param Url                         $url                       Url model.
     * @param HandlesProviderInterface    $layoutTypeHandlesProvider Layout type handles provider.
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        StoreManagerInterface $storeManager,
        Registry $coreRegistry,
        CategoryRepositoryInterface $categoryRepository,
        PostRepositoryInterface $postRepository,
        TagRepositoryInterface $tagRepository,
        Config $config,
        Url $url,
        HandlesProviderInterface $layoutTypeHandlesProvider
    )
    {
        $this->_layoutTypeHandlesProvider = $layoutTypeHandlesProvider;

        parent::__construct(
            $context,
            $resultPageFactory,
            $resultForwardFactory,
            $storeManager,
            $coreRegistry,
            $categoryRepository,
            $postRepository,
            $tagRepository,
            $config,
            $url
        );
    }

    /**
     * Execute method.
     *
     * @return ResultPage|ResultForward|ResultRedirect
     */
    public function execute()
    {
        try {
            $post = $this->postRepository->get(
                $this->getRequest()->getParam('post_id')
            );

            if ($post->getStatus() != Status::PUBLICATION
                || strtotime($post->getPublishDate()) > time()
                || (!in_array($this->getStoreId(), $post->getStoreIds())
                    && !in_array(0, $post->getStoreIds()))
            ) {
                /**  @var ResultForward $forward */
                $forward = $this->resultForwardFactory->create();
                $forward->setModule('cms');
                $forward->setController('noroute');
                $forward->forward('index');

                return $forward;
            }
            if ($this->getRequest()->getParam('blog_category_id')) {
                // Forced redirect to post url without category id
                /** @var ResultRedirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setUrl($this->url->getPostUrl($post));

                return $resultRedirect;
            }

            $resultPage = $this->resultPageFactory->create();
            $this->_addCustomLayoutUpdateHandles($resultPage, $post);

            $pageConfig = $resultPage->getConfig();
            $pageConfig->getTitle()->set($post->getTitle());
            $pageConfig->setMetadata('description', $post->getMetaDescription());
            $pageConfig->setMetadata('og:description', $this->getMetaOgDescription($post));

            return $resultPage;
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->goBack();
        }
    }

    /**
     * Adds custom layout update handle if it was set for the post.
     *
     * @param ResultPage                              $resultPage Result page instance.
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post       Post instance.
     *
     * @return void
     */
    protected function _addCustomLayoutUpdateHandles($resultPage, $post)
    {
        /** @var \Aheadworks\Blog\Api\Data\PostExtensionInterface $extensionAttributes */
        $extensionAttributes = $post->getExtensionAttributes();
        if (is_null($extensionAttributes)) {
            return;
        }

        $layoutType = $extensionAttributes->getLayoutType();
        if (is_null($layoutType)) {
            return;
        }

        /** @var HandleInterface $handle */
        $handle = $this->_layoutTypeHandlesProvider->getHandleById($layoutType);
        if (is_null($handle)) {
            return;
        }

        $resultPage->addHandle($handle->getLayoutUpdateHandle());
    }

    /**
     * Retrieve og:description meta tag from post.
     *
     * @param PostInterface $post Post instance.
     *
     * @return string
     */
    private function getMetaOgDescription($post)
    {
        $content = $this->getClearContent($post->getShortContent());
        if (strlen($content) == 0) {
            $content = $this->getClearContent($post->getContent());
        }

        return $content;
    }

    /**
     * Retrieve clear content.
     *
     * @param string $content Content.
     *
     * @return string
     */
    private function getClearContent($content)
    {
        $lenContent = 256;
        $content = trim(strip_tags($content));

        return strlen($content) > $lenContent
            ? substr($content, 0, $lenContent)
            : $content;
    }
}