<?php
namespace Balance\AwBlogCustomFields\Controller\Adminhtml\Post\Thumbnail;

use Magento\Framework\View\Result\PageFactory;

/**
 * Class Upload.
 *
 * @package Balance\AwBlogCustomFields\Controller\Adminhtml\Post\Thumbnail
 */
class Upload extends \Magento\Backend\App\Action
{
    /**
     * @const string Authorization level of a basic admin session.
     */
    const ADMIN_RESOURCE = 'Aheadworks_Blog::posts';

    /**
     * @var \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory Uploader factory.
     */
    protected $_uploaderFactory;
    /**
     * @var \Psr\Log\LoggerInterface $_logger Logger instance.
     */
    protected $_logger;

    /**
     * Upload constructor.
     *
     * @param \Magento\Backend\App\Action\Context                                      $context         Context model.
     * @param \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory Uploader
     *                                                                                                  factory.
     * @param \Psr\Log\LoggerInterface                                                 $logger          Logger instance.
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Balance\AwBlogCustomFields\Model\Post\Thumbnail\UploaderFactory $uploaderFactory,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_uploaderFactory = $uploaderFactory;
        $this->_logger = $logger;

        parent::__construct($context);
    }

    /**
     * Execute method.
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Balance\AwBlogCustomFields\Model\Post\Thumbnail\Uploader $uploader */
        $uploader = $this->_uploaderFactory->create();
        $uploader->saveFileToTmpDirectory();
        $uploadedFileInfo = $uploader->getUploadedFileInfo();

        // Check for possible errors.
        $uploaderLastError = $uploader->getLastError();
        if (!empty($uploaderLastError)) {
            $this->_logger->error($uploaderLastError);
            $uploadedFileInfo['error'] = $uploaderLastError;
        }

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $result->setData($uploadedFileInfo);

        return $result;
    }
}