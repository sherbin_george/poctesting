<?php
namespace Balance\AwBlogCustomFields\Observer\Post;

/**
 * Class LoadExtensionAttributes.
 * Observer loads extension attributes into a post on corresponding event.
 *
 * @package Balance\AwBlogCustomFields\Observer\Post
 */
class LoadExtensionAttributes implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Balance\AwBlogCustomFields\Api\PostRepositoryInterface $_postRepository Post repository.
     */
    protected $_postRepository;

    /**
     * LoadExtensionAttributes constructor.
     *
     * @param \Balance\AwBlogCustomFields\Api\PostRepositoryInterface $postRepository Post repository.
     */
    function __construct(
        \Balance\AwBlogCustomFields\Api\PostRepositoryInterface $postRepository
    )
    {
        $this->_postRepository = $postRepository;
    }

    /**
     * Observer execute method.
     * Loads extension attributes into a post on corresponding event.
     *
     * @param \Magento\Framework\Event\Observer $observer Observer instance.
     *
     * @return void
     *
     * @event aheadworks_blog_api_data_postinterface_delete_before
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $post = $observer->getData('entity');
        if (is_null($post)) {
            return;
        }

        $this->_postRepository->loadExtensionAttributes($post);
    }
}