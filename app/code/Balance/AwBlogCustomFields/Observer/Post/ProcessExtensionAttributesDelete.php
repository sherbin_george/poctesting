<?php
namespace Balance\AwBlogCustomFields\Observer\Post;

/**
 * Class ProcessExtensionAttributesDelete.
 * Observer triggers extension attributes processing after post is deleted.
 *
 * @package Balance\AwBlogCustomFields\Observer\Post
 */
class ProcessExtensionAttributesDelete implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Balance\AwBlogCustomFields\Model\PostFactory $_postFactory Post factory.
     */
    protected $_postFactory;

    /**
     * ProcessExtensionAttributesDelete constructor.
     *
     * @param \Balance\AwBlogCustomFields\Model\PostFactory $postFactory Post factory.
     */
    function __construct(
        \Balance\AwBlogCustomFields\Model\PostFactory $postFactory
    )
    {
        $this->_postFactory = $postFactory;
    }

    /**
     * Observer execute method.
     * Triggers extension attributes processing after post is deleted.
     *
     * @param \Magento\Framework\Event\Observer $observer Observer instance.
     *
     * @return void
     *
     * @event aheadworks_blog_post_delete_commit_after
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Aheadworks\Blog\Api\Data\PostInterface $post */
        $post = $observer->getData('post');
        if (is_null($post)) {
            return;
        }

        /** @var \Aheadworks\Blog\Api\Data\PostExtensionInterface $extensionAttributes */
        $extensionAttributes = $post->getExtensionAttributes();
        if (is_null($extensionAttributes)) {
            return;
        }

        /** @var \Balance\AwBlogCustomFields\Model\Post $extendedPost */
        $extendedPost = $this->_postFactory->create();
        $extendedPost->setId($post->getId());
        $extendedPost->setExtensionAttributes($extensionAttributes);
        $extendedPost->afterDeleteCommit();
    }
}