<?php
namespace Balance\AwBlogCustomFields\Api;

/**
 * Interface PostRepositoryInterface (additional interface for extension attributes saving).
 *
 * @package Balance\AwBlogCustomFields\Api
 */
interface PostRepositoryInterface
{
    /**
     * Saves post extension attributes.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    public function save(\Aheadworks\Blog\Api\Data\PostInterface $post);

    /**
     * Loads extension attributes to post instance.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return void
     */
    public function loadExtensionAttributes(\Aheadworks\Blog\Api\Data\PostInterface $post);
}