<?php
namespace Balance\AwBlogCustomFields\Api\Data;

/**
 * Interface PostInterface (additional interface for extension attributes).
 *
 * @package Balance\AwBlogCustomFields\Api\Data
 */
interface PostInterface
{
    /**
     * @const string Thumbnail field name.
     */
    const THUMBNAIL = 'thumbnail';
    /**
     * @const string Layout type field name.
     */
    const LAYOUT_TYPE = 'layout_type';
    /**
     * @const string Media content field name.
     */
    const MEDIA_CONTENT = 'media_content';

    /**
     * @const string Post attribute field name.
     */
    const POST_ATTRIBUTE = 'post_attribute';

    /**
     * Returns post ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Sets post ID.
     *
     * @param int $id Post ID.
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Returns relative path for thumbnail.
     *
     * @return string|null
     */
    public function getThumbnail();

    /**
     * Sets relative path for thumbnail.
     *
     * @param string $thumbnail Relative path for thumbnail.
     *
     * @return $this
     */
    public function setThumbnail($thumbnail);

    /**
     * Returns layout type value.
     *
     * @return string|null
     */
    public function getLayoutType();

    /**
     * Sets layout type value.
     *
     * @param string $layoutType Layout type value.
     *
     * @return $this
     */
    public function setLayoutType($layoutType);

    /**
     * Returns media content.
     *
     * @return string
     */
    public function getMediaContent();

    /**
     * Sets media content.
     *
     * @param string $mediaContent Media content data.
     *
     * @return $this
     */
    public function setMediaContent($mediaContent);

    /**
     * Sets post attribute.
     *
     * @param string $postAttribute post attribute data.
     *
     * @return $this
     */
    public function setPostAttribute($postAttribute);

    /**
     * Returns media content.
     *
     * @return string
     */
    public function getPostAttribute();
}