<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\AlgoliaSearch\Helper;

use  Algolia\AlgoliaSearch\Helper\Image as AlgoliaImage;

class Image extends AlgoliaImage
{
    public function getProductListingThumbnail($product)
    {
        return $this->init($product, 'product_listing_thumbnail')->getUrl();
    }
}