<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\AlgoliaSearch\Helper\Entity;

use Algolia\AlgoliaSearch\Helper\Entity\ProductHelper as AlgoliaProductHelper;
use Magento\Directory\Model\Currency;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Algolia\AlgoliaSearch\Helper\AlgoliaHelper;
use Algolia\AlgoliaSearch\Helper\ConfigHelper;
use Algolia\AlgoliaSearch\Helper\Logger;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Helper\Stock;
use Magento\CatalogRule\Model\ResourceModel\Rule;
use Magento\Directory\Model\Currency as CurrencyHelper;
use Magento\Eav\Model\Config;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Helper\Data;
use Algolia\AlgoliaSearch\Helper\Entity\CategoryHelper;
use Algolia\AlgoliaSearch\Helper\Image;
use Magento\Catalog\Model\Product;

/**
 * Class ProductHelper
 * @package Balance\AlgoliaSearch\Helper\Entity
 */
class ProductHelper extends AlgoliaProductHelper
{
    /**
     * @var Image $imageHelper
     */
    private $imageHelper;

    /**
     * @var ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * @var ConfigHelper
     */
    private $_configHelper;

    /**
     * @var PriceCurrencyInterface
     */
    private $_priceCurrency;

    /**
     * @var Rule
     */
    private $_rule;

    /**
     * @var CatalogHelper
     */
    private $_catalogHelper;


    /**
     * ProductHelper constructor.
     *
     * @param Config $eavConfig
     * @param ConfigHelper $configHelper
     * @param AlgoliaHelper $algoliaHelper
     * @param Logger $logger
     * @param StoreManagerInterface $storeManager
     * @param ManagerInterface $eventManager
     * @param Visibility $visibility
     * @param Stock $stockHelper
     * @param Data $taxHelper
     * @param StockRegistryInterface $stockRegistry
     * @param ObjectManagerInterface $objectManager
     * @param CatalogHelper $catalogHelper
     * @param CurrencyHelper $currencyManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param Rule $rule
     * @param CategoryHelper $categoryHelper
     */
    public function __construct(
        Config $eavConfig,
        ConfigHelper $configHelper,
        AlgoliaHelper $algoliaHelper,
        Logger $logger,
        StoreManagerInterface $storeManager,
        ManagerInterface $eventManager,
        Visibility $visibility,
        Stock $stockHelper,
        Data $taxHelper,
        StockRegistryInterface $stockRegistry,
        ObjectManagerInterface $objectManager,
        CatalogHelper $catalogHelper,
        CurrencyHelper $currencyManager,
        PriceCurrencyInterface $priceCurrency,
        Rule $rule,
        CategoryHelper $categoryHelper
    )
    {
        parent::__construct(
            $eavConfig,
            $configHelper,
            $algoliaHelper,
            $logger,
            $storeManager,
            $eventManager,
            $visibility,
            $stockHelper,
            $taxHelper,
            $stockRegistry,
            $objectManager,
            $catalogHelper,
            $currencyManager,
            $priceCurrency,
            $rule,
            $categoryHelper
        );

        $this->imageHelper = $objectManager->create(
            'Algolia\AlgoliaSearch\Helper\Image',
            [
                'options' => [
                    'shouldRemovePubDir' => $configHelper->shouldRemovePubDirectory(),
                ]
            ]
        );

        $this->_objectManager = $objectManager;
        $this->_configHelper = $configHelper;
        $this->_priceCurrency = $priceCurrency;
        $this->_rule = $rule;
        $this->_catalogHelper = $catalogHelper;
    }

    /**
     * Add custom parameter to image data.
     *
     * @param array $customData
     * @param \Magento\Catalog\Model\Product $product
     * @param $additionalAttributes
     * @return array
     */
    protected function addImageData(array $customData, Product $product, $additionalAttributes)
    {
        return ['thumbnail_url' => isset($customData['thumbnail_url']) ?: $this->imageHelper->getProductListingThumbnail($product)]
            + parent::addImageData($customData, $product, $additionalAttributes);
    }

    /**
     * Add original price to configurable products
     *
     * @param Product $product
     * @param $subProducts
     * @param $customData
     */
    protected function handlePrice(Product &$product, $subProducts, &$customData)
    {
        $store = $product->getStore();
        $type = $product->getTypeId();

        $fields = $this->getFields($store);

        $areCustomersGroupsEnabled = $this->_configHelper->isCustomerGroupsEnabled($product->getStoreId());

        $currencies = $store->getAvailableCurrencyCodes();
        $baseCurrencyCode = $store->getBaseCurrencyCode();

        $groups = $this->_objectManager->create('Magento\Customer\Model\ResourceModel\Group\Collection');

        if (!$areCustomersGroupsEnabled) {
            $groups->addFieldToFilter('main_table.customer_group_id', 0);
        }

        foreach ($fields as $field => $withTax) {
            $customData[$field] = [];

            foreach ($currencies as $currencyCode) {
                $customData[$field][$currencyCode] = [];

                $price = $product->getPrice();
                if ($currencyCode !== $baseCurrencyCode) {
                    $price = $this->_priceCurrency->convert($price, $store, $currencyCode);
                }

                $price = (double) $this->_catalogHelper->getTaxPrice($product, $price, $withTax, null, null, null, $product->getStore(), null);

                $customData[$field][$currencyCode]['default'] = $this->_priceCurrency->round($price);
                $customData[$field][$currencyCode]['default_formated'] = $this->_priceCurrency->format($price, false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode);

                $specialPrices   = [];
                $specialPrice    = [];
                foreach ($groups as $group) {
                    $groupId = (int) $group->getData('customer_group_id');
                    $specialPrices[$groupId] = [];
                    $specialPrices[$groupId][] = (double) $this->_rule->getRulePrice(new \DateTime(), $store->getWebsiteId(), $groupId, $product->getId()); // The price with applied catalog rules
                    $specialPrices[$groupId][] = $product->getFinalPrice(); // The product's special price

                    $specialPrices[$groupId] = array_filter($specialPrices[$groupId], function ($price) {
                        return $price > 0;
                    });

                    $specialPrice[$groupId] = false;
                    if (!empty($specialPrices[$groupId])) {
                        $specialPrice[$groupId] = min($specialPrices[$groupId]);
                    }

                    if ($specialPrice[$groupId]) {
                        if ($currencyCode !== $baseCurrencyCode) {
                            $specialPrice[$groupId] = $this->_priceCurrency->convert($specialPrice[$groupId], $store, $currencyCode);
                            $specialPrice[$groupId] = $this->_priceCurrency->round($specialPrice[$groupId]);
                        }

                        $specialPrice[$groupId] = (double) $this->_catalogHelper->getTaxPrice($product, $specialPrice[$groupId], $withTax, null, null, null, $product->getStore(), null);
                    }
                }

                if ($areCustomersGroupsEnabled) {
                    /** @var \Magento\Customer\Model\Group $group */
                    foreach ($groups as $group) {
                        $groupId = (int) $group->getData('customer_group_id');

                        $product->setData('customer_group_id', $groupId);

                        $discountedPrice = $product->getPriceModel()->getFinalPrice(1, $product);
                        if ($currencyCode !== $baseCurrencyCode) {
                            $discountedPrice = $this->_priceCurrency->convert($discountedPrice, $store, $currencyCode);
                        }

                        if ($discountedPrice !== false) {
                            $customData[$field][$currencyCode]['group_' . $groupId] = (double) $this->_catalogHelper->getTaxPrice($product, $discountedPrice, $withTax, null, null, null, $product->getStore(), null);
                            $customData[$field][$currencyCode]['group_' . $groupId . '_formated'] = $this->_priceCurrency->format($customData[$field][$currencyCode]['group_' . $groupId], false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode);

                            if ($customData[$field][$currencyCode]['default'] > $customData[$field][$currencyCode]['group_' . $groupId]) {
                                $customData[$field][$currencyCode]['group_'.$groupId.'_original_formated'] = $customData[$field][$currencyCode]['default_formated'];
                            }
                        } else {
                            $customData[$field][$currencyCode]['group_' . $groupId] = $customData[$field][$currencyCode]['default'];
                            $customData[$field][$currencyCode]['group_' . $groupId . '_formated'] = $customData[$field][$currencyCode]['default_formated'];
                        }
                    }

                    $product->setData('customer_group_id', null);
                }

                $customData[$field][$currencyCode]['special_from_date'] = strtotime($product->getSpecialFromDate());
                $customData[$field][$currencyCode]['special_to_date'] = strtotime($product->getSpecialToDate());

                if ($areCustomersGroupsEnabled) {
                    foreach ($groups as $group) {
                        $groupId = (int) $group->getData('customer_group_id');

                        if ($specialPrice[$groupId] && $specialPrice[$groupId] < $customData[$field][$currencyCode]['group_' . $groupId]) {
                            $customData[$field][$currencyCode]['group_' . $groupId] = $specialPrice[$groupId];
                            $customData[$field][$currencyCode]['group_' . $groupId . '_formated'] = $this->_priceCurrency->format($specialPrice[$groupId], false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode);

                            if ($customData[$field][$currencyCode]['default'] > $customData[$field][$currencyCode]['group_' . $groupId]) {
                                $customData[$field][$currencyCode]['group_'.$groupId.'_original_formated'] = $customData[$field][$currencyCode]['default_formated'];
                            }
                        }
                    }
                } else {
                    if ($specialPrice[0] && $specialPrice[0] < $customData[$field][$currencyCode]['default']) {
                        $customData[$field][$currencyCode]['default_original_formated'] = $customData[$field][$currencyCode]['default_formated'];

                        $customData[$field][$currencyCode]['default'] = $this->_priceCurrency->round($specialPrice[0]);
                        $customData[$field][$currencyCode]['default_formated'] = $this->_priceCurrency->format($specialPrice[0], false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode);
                    }
                }

                if ($type == 'configurable' || $type == 'grouped' || $type == 'bundle') {
                    /**
                     * Start rewrite
                     */
                    $min = $minOriginal = PHP_INT_MAX;
                    /**
                     * End rewrite
                     */
                    $max = 0;

                    $dashedFormat = '';

                    if ($type == 'bundle') {
                        /** @var \Magento\Bundle\Model\Product\Price $priceModel */
                        $priceModel = $product->getPriceModel();
                        list($min, $max) = $priceModel->getTotalPrices($product, null, $withTax, true);
                    }

                    if ($type == 'grouped' || $type == 'configurable') {
                        if (count($subProducts) > 0) {
                            /** @var Product $subProduct */
                            foreach ($subProducts as $subProduct) {
                                $price = (double) $this->_catalogHelper->getTaxPrice($product, $subProduct->getFinalPrice(), $withTax, null, null, null, $product->getStore(), null);

                                /**
                                 * Start rewrite
                                 *
                                 * Sub product original price will be used only
                                 * when this product haves minimal final price
                                 */
                                $originalPrice = $subProduct->getPrice();
                                $originalPrice = $this->_priceCurrency->round(
                                    (double) $this->_catalogHelper->getTaxPrice($subProduct, $originalPrice, $withTax, null, null, null, $subProduct->getStore(), null)
                                );
                                if ($originalPrice < $minOriginal) {
                                    $minOriginal = $originalPrice;
                                }
                                /**
                                 * End rewrite
                                 */

                                $min = min($min, $price);
                                $max = max($max, $price);
                            }
                        } else {
                            /**
                             * Start rewrite
                             */
                            $min = $minOriginal = $max;
                            /**
                             * End rewrite
                             */
                        } // avoid to have PHP_INT_MAX in case of no subproducts (Corner case of visibility and stock options)
                    }

                    /**
                     * Start rewrite
                     */
                    /*
                    if ($min != $max) {
                        if ($currencyCode !== $baseCurrencyCode) {
                            $min = $this->_priceCurrency->convert($min, $store, $currencyCode);
                        }

                        if ($currencyCode !== $baseCurrencyCode) {
                            $max = $this->_priceCurrency->convert($max, $store, $currencyCode);
                        }

                        $dashedFormat =
                            $this->_priceCurrency->format($min, false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode)
                            . ' - ' .
                            $this->_priceCurrency->format($max, false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode);

                        if (isset($customData[$field][$currencyCode]['default_original_formated']) === false
                            || $min <= $customData[$field][$currencyCode]['default']) {
                            $customData[$field][$currencyCode]['default_formated'] = $dashedFormat;

                            //// Do not keep special price that is already taken into account in min max
                            unset($customData['price']['special_from_date']);
                            unset($customData['price']['special_to_date']);
                            unset($customData['price']['default_original_formated']);

                            $customData[$field][$currencyCode]['default'] = 0; // will be reset just after
                        }

                        if ($areCustomersGroupsEnabled) {
                            foreach ($groups as $group) {
                                $groupId = (int) $group->getData('customer_group_id');

                                if ($min != $max && $min <= $customData[$field][$currencyCode]['group_' . $groupId]) {
                                    $customData[$field][$currencyCode]['group_' . $groupId] = 0;
                                    $customData[$field][$currencyCode]['group_' . $groupId . '_formated'] = $dashedFormat;
                                }
                            }
                        }
                    }
                    */
                    /**
                     * End rewrite
                     */

                    if ($customData[$field][$currencyCode]['default'] == 0) {
                        $customData[$field][$currencyCode]['default'] = $min;

                        /**
                         * Start rewrite
                         */
//                        if ($min === $max) {
                            if ($currencyCode !== $baseCurrencyCode) {
                                $min = $this->_priceCurrency->convert($min, $store, $currencyCode);
                                $minOriginal = $this->_priceCurrency->convert($minOriginal, $store, $currencyCode);
                            }

                            $customData[$field][$currencyCode]['default'] = $min;
                            $customData[$field][$currencyCode]['default_formated'] = $this->_priceCurrency->format($min, false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode);

                            if ($minOriginal != $min) {
                                $customData[$field][$currencyCode]['default_original_formated'] = $this->_priceCurrency->format($minOriginal, false, PriceCurrencyInterface::DEFAULT_PRECISION, $store, $currencyCode);
                            }
//                        }
                        /**
                         * End rewrite
                         */
                    }

                    if ($areCustomersGroupsEnabled) {
                        foreach ($groups as $group) {
                            $groupId = (int) $group->getData('customer_group_id');

                            if ($customData[$field][$currencyCode]['group_' . $groupId] == 0) {
                                $customData[$field][$currencyCode]['group_' . $groupId] = $min;

                                if ($min === $max) {
                                    $customData[$field][$currencyCode]['group_' . $groupId . '_formated'] = $customData[$field][$currencyCode]['default_formated'];
                                } else {
                                    $customData[$field][$currencyCode]['group_' . $groupId . '_formated'] = $dashedFormat;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}