<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AheadworksBlog\Controller\Block;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Translate\InlineInterface;
use \Magento\Framework\App\Action\Action;
use \Aheadworks\Blog\Model\CategoryFactory;
use \Balance\AheadworksBlog\Block\PostList\Widget\Filtered\Post;
use \Balance\AheadworksBlog\Block\PostList\Widget\Filtered\Category;

class Renderer extends Action
{

    /**
     * @var InlineInterface
     */
    private $translateInline;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * Renderer constructor.
     * @param Context $context
     * @param InlineInterface $translateInline
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(
        Context $context,
        InlineInterface $translateInline,
        CategoryFactory $categoryFactory
    )
    {
        parent::__construct($context);
        $this->translateInline = $translateInline;
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        return $this->getRequest()->isAjax() ? $this->processAjaxRequest() : true;
    }

    /**
     * @return mixed
     */
    private function processAjaxRequest()
    {
        $layout = $this->_view->getLayout();

        $categoriesBlock = $layout->createBlock(Category::class)
            ->setTemplate('Balance_AheadworksBlog::post/widget/current_category.phtml');
        $postListBlock = $layout->createBlock(Post::class)
            ->setTemplate('Balance_AheadworksBlog::post/widget/list.phtml');

        $data = [
            'postList' => $postListBlock->toHtml(),
            'categories' => $categoriesBlock->toHtml(),
            'canShowMore' => $postListBlock->canShowMore()
        ] ;

        $this->translateInline->processResponseBody($data);

        return $this->getResponse()->appendBody(json_encode($data));
    }
}