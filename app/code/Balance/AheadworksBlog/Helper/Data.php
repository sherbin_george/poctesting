<?php
namespace Balance\AheadworksBlog\Helper;

/**
 * Class Data.
 * Base helper class.
 *
 * @package Balance\AheadworksBlog\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @const string Image cache blog section name.
     */
    const IMAGE_CACHE_SECTION_BLOG = 'blog';

    /**
     * @var \Balance\ImageCache\Model\CacheInterface $_imageCache Image cache model.
     */
    protected $_imageCache;
    /**
     * @var \Magento\Framework\Config\View $_viewConfig View config.
     */
    protected $_viewConfig;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context            $context           Context model.
     * @param \Balance\ImageCache\Model\CacheInterface $imageCache        Image cache model.
     * @param \Magento\Framework\Config\ViewFactory            $viewConfigFactory View config factory.
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Balance\ImageCache\Model\CacheInterface $imageCache,
        \Magento\Framework\Config\ViewFactory $viewConfigFactory
    )
    {
        $this->_imageCache = $imageCache;
        $this->_viewConfig = $viewConfigFactory->create();

        parent::__construct($context);
    }

    /**
     * Returns blog re-sized image url.
     *
     * @param string $path Path to original blog image (source image).
     * @param array  $size Image size (width, height).
     *
     * @return string
     */
    public function getBlogImageUrl($path, array $size)
    {
        try {
            $cacheImage = $this->_imageCache->getImage(
                self::IMAGE_CACHE_SECTION_BLOG,
                $path,
                array_map('intval', array_values($size))
            );

            return $cacheImage->getUrl();
        } catch (\Exception $exception) {
            return '';
        }
    }

    /**
     * Returns view config value.
     *
     * @param string $path Config value path.
     *
     * @return array|false|string
     */
    public function getViewConfigValue($path)
    {
        return $this->_viewConfig->getVarValue(
            'Balance_AheadworksBlog',
            $path
        );
    }
}