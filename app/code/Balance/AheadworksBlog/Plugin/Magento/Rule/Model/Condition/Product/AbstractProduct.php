<?php
namespace Balance\AheadworksBlog\Plugin\Magento\Rule\Model\Condition\Product;

/**
 * Class AbstractProduct.
 * Abstract product rule condition model plugins.
 *
 * @package Balance\AheadworksBlog\Plugin\Magento\Rule\Model\Condition\Product
 */
class AbstractProduct
{
    /**
     * @var \Magento\Framework\App\RequestInterface $_request Request model.
     */
    protected $_request;

    /**
     * AbstractProduct constructor.
     *
     * @param \Magento\Framework\App\RequestInterface $request            Request model.
     * @param array                                   $attributesToRemove Attributes to remove.
     */
    function __construct(
        \Magento\Framework\App\RequestInterface $request,
        array $attributesToRemove = []
    )
    {
        $this->_request = $request;
        $this->_attributesToRemove = $attributesToRemove;
    }

    /**
     * After load attribute options plugin.
     * NOTE: we use this plugin to filter product rule condition attribute options. The thing is that
     * "Amasty_Rules" module may add several additional options that may cause SQL error.
     *
     * @param \Magento\Rule\Model\Condition\Product\AbstractProduct $subject Product rule condition model.
     *
     * @return \Magento\Rule\Model\Condition\Product\AbstractProduct
     */
    public function afterLoadAttributeOptions(
        \Magento\Rule\Model\Condition\Product\AbstractProduct $subject
    ) {
        if ($this->_request->getModuleName() != 'aw_blog_admin'
            || !in_array($this->_request->getActionName(), ['edit', 'newConditionHtml'])
        ) {
            return $subject;
        }

        $attributeOptions = $subject->getData('attribute_option');
        if (!is_array($attributeOptions)) {
            return $subject;
        }

        $this->_filterAttributeOptions($attributeOptions);
        $subject->setData('attribute_option', $attributeOptions);

        return $subject;
    }

    /**
     * Filters attribute option list.
     *
     * @param array $attributeOptions Attribute options to be removed.
     *
     * @return void
     */
    private function _filterAttributeOptions(&$attributeOptions)
    {
        foreach ($this->_attributesToRemove as $attributeName) {
            unset($attributeOptions[$attributeName]);
        }
    }
}