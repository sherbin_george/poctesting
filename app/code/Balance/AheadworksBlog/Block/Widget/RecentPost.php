<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Balance\AheadworksBlog\Block\Widget;

use Magento\Widget\Block\BlockInterface;

/**
 * Tag Cloud Widget
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RecentPost extends \Aheadworks\Blog\Block\Sidebar\Recent implements BlockInterface
{
    /**
     * @var string
     */
    const WIDGET_NAME_PREFIX = 'widget_recent_post_';

    /**
     * Path to template file in theme
     * @var string
     */
    protected $_template = 'Balance_AheadworksBlog::widget/list.phtml';


    protected $_postBlockClass = 'Balance\AheadworksBlog\Block\Post';


    /**
     * {@inheritdoc}
     */
    public function getPosts($numberToDisplay = null)
    {
        return parent::getPosts($this->getData('number_to_display'));
    }

    /**
     * Checks blog is enabled or not
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isBlogEnabled();
    }

    /**
     * Retrieve widget encode data
     *
     * @return string
     */
    public function getWidgetEncodeData()
    {
        return base64_encode(
            serialize(
                [
                    'name' => $this->getNameInLayout(),
                    'number_to_display' => $this->getData('number_to_display'),
                    'title' => $this->getData('title'),
                    'template' => $this->getTemplate()
                ]
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getNameInLayout()
    {
        return self::WIDGET_NAME_PREFIX . parent::getNameInLayout();
    }


    public function getItemHtml(\Aheadworks\Blog\Model\Post $post)
    {
        $postBlockClass = $this->_postBlockClass;
        $postBlockTemplate = 'post/list/item.phtml';
        if (empty($postBlockClass)
            || empty($postBlockTemplate)
        ) {
            return '';
        }

        /** @var \Balance\AheadworksBlog\Block\Post $block */
        $block = $this->getLayout()->createBlock(
            $postBlockClass,
            '',
            [
                'data' => [
                    'template' => $postBlockTemplate,
                    'post' => $post,
                    'mode' => \Aheadworks\Blog\Block\Post::MODE_LIST_ITEM
                ]
            ]
        );

        return $block->toHtml();
    }
}
