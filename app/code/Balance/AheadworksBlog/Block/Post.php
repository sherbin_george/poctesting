<?php
namespace Balance\AheadworksBlog\Block;

/**
 * Class Post.
 * Extended post item block template.
 *
 * @package Balance\AheadworksBlog\Block
 */
class Post extends \Aheadworks\Blog\Block\Post
{
    /**
     * @const string Xml path for view config post thumbnail size value.
     */
    const XML_PATH_POST_THUMBNAIL_SIZE = 'post/images/thumbnail';

    /**
     * @var string
     */
    protected $_template = 'Aheadworks_Blog::post.phtml';

    /**
     * @var \Balance\AheadworksBlog\Helper\Data $_helper Base helper instance.
     */
    protected $_helper;
    /**
     * @var \Aheadworks\Blog\Model\Template\FilterProvider $templateFilterProvider Filter provider.
     */
    protected $_templateFilterProvider;

    /**
     * Post constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context                Context model.
     * @param \Aheadworks\Blog\Api\PostRepositoryInterface     $postRepository         Post repository.
     * @param \Aheadworks\Blog\Api\CategoryRepositoryInterface $categoryRepository     Category repository.
     * @param \Aheadworks\Blog\Api\TagRepositoryInterface      $tagRepository          Tag repository.
     * @param \Magento\Framework\Api\SearchCriteriaBuilder     $searchCriteriaBuilder  Search criteria builder.
     * @param \Aheadworks\Blog\Model\Config                    $config                 Config model.
     * @param \Aheadworks\Blog\Block\LinkFactory               $linkFactory            Link blok factory.
     * @param \Aheadworks\Blog\Model\Url                       $url                    Url model.
     * @param \Aheadworks\Blog\Model\Template\FilterProvider   $templateFilterProvider Template filter provider.
     * @param \Balance\AheadworksBlog\Helper\Data      $helper                 Helper instance.
     * @param array                                            $data                   Data array.
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Aheadworks\Blog\Api\PostRepositoryInterface $postRepository,
        \Aheadworks\Blog\Api\CategoryRepositoryInterface $categoryRepository,
        \Aheadworks\Blog\Api\TagRepositoryInterface $tagRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Aheadworks\Blog\Model\Config $config,
        \Aheadworks\Blog\Block\LinkFactory $linkFactory,
        \Aheadworks\Blog\Model\Url $url,
        \Aheadworks\Blog\Model\Template\FilterProvider $templateFilterProvider,
        \Balance\AheadworksBlog\Helper\Data $helper,
        array $data = []
    )
    {
        $this->_helper = $helper;
        $this->_templateFilterProvider = $templateFilterProvider;

        parent::__construct(
            $context,
            $postRepository,
            $categoryRepository,
            $tagRepository,
            $searchCriteriaBuilder,
            $config,
            $linkFactory,
            $url,
            $templateFilterProvider,
            $data
        );
    }

    /**
     * Return thumbnail url, if it was specified for the post.
     *
     * @param \Aheadworks\Blog\Model\Post $post Post instance.
     *
     * @return string
     */
    public function getThumbnailUrl($post)
    {
        // Check directly set data at first (for instance, when collection is used).
        $thumbnailPath = $post->getData('thumbnail');
        if (!is_null($thumbnailPath)) {
            return $this->_getThumbnailUrl($thumbnailPath);
        }

        /** @var \Aheadworks\Blog\Api\Data\PostExtensionInterface $extensionAttributes */
        $extensionAttributes = $post->getExtensionAttributes();
        if (!is_null($extensionAttributes)) {
            $thumbnailPath = $extensionAttributes->getThumbnail();
            return $this->_getThumbnailUrl($thumbnailPath);
        }

        return '';
    }

    /**
     * Returns thumbnail url by specified path.
     *
     * @param string $thumbnailPath Thumbnail path.
     *
     * @return string
     */
    protected function _getThumbnailUrl($thumbnailPath)
    {
        if (empty($thumbnailPath)) {
            return '';
        }

        $thumbnailConfigSize = $this->_helper->getViewConfigValue(self::XML_PATH_POST_THUMBNAIL_SIZE);
        if (!is_array($thumbnailConfigSize)) {
            return '';
        }

        return $this->_helper->getBlogImageUrl($thumbnailPath, $thumbnailConfigSize);
    }

    /**
     * Return post layout type url, if it was specified for the post.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return string
     */
    public function getLayoutType($post)
    {
        /** @var \Aheadworks\Blog\Api\Data\PostExtensionInterface $extensionAttributes */
        $extensionAttributes = $post->getExtensionAttributes();
        if (is_null($extensionAttributes)) {
            return '';
        }

        return $extensionAttributes->getLayoutType();
    }

    /**
     * Return media content data, if it was specified for the post.
     *
     * @param \Aheadworks\Blog\Api\Data\PostInterface $post Post instance.
     *
     * @return string
     */
    public function getMediaContent($post)
    {
        /** @var \Aheadworks\Blog\Api\Data\PostExtensionInterface $extensionAttributes */
        $extensionAttributes = $post->getExtensionAttributes();
        if (is_null($extensionAttributes)) {
            return '';
        }

        $mediaContent = $extensionAttributes->getMediaContent();
        if (is_null($mediaContent)) {
            return '';
        }

        return $this->_templateFilterProvider->getFilter()
            ->setStoreId($this->_storeManager->getStore()->getId())
            ->filter($mediaContent);
    }
}