<?php

/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AheadworksBlog\Block;

use Magento\Framework\View\Element\Template;
use Balance\AheadworksBlog\Block\PostList\Widget\Filtered\Post;
use Magento\Theme\Block\Html\Pager;

class Pagination extends Pager
{

    /**
     * @var Post
     */
    private $post;

    /**
     * Pagination constructor.
     * @param Template\Context $context
     * @param Post $post
     * @param array $data
     */
    public function __construct(Template\Context $context, Post $post, array $data = [])
    {
        $this->post = $post;
        parent::__construct($context, $data);
        $this->setPagination();
    }

    /**
     * @return bool
     */
    public function checkPagination()
    {
        return !$this->post->isMoreButton() && $this->post->getPostCollection();
    }

    /**
     * @return void
     */
    public function setPagination()
    {
         $this
             ->setAvailableLimit([$this->post->config->getNumPostsPerPage()])
             ->setPageVarName('page')
             ->setPath(trim($this->getRequest()->getPathInfo(), '/'))
             ->setFrameLength($this->post->config->getNumberOfPages())
             ->setCollection(
                 $this->post->getPostCollection()
             );
    }

    /**
     * @param array $params
     * @return string
     */
    public function getPagerUrl($params = [])
    {
        return $this->getUrl(
            null,
            [
                '_current' => true,
                '_escape' => true,
                '_use_rewrite' => true,
                '_fragment' => $this->getFragment(),
                '_query' => $params,
                '_direct' => $this->getPath()
            ]
        );
    }

}