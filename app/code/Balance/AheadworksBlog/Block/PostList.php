<?php
namespace Balance\AheadworksBlog\Block;

use \Balance\AheadworksBlog\Block\PostList\DataProvider\DataProviderInterface;

/**
 * Class Recent.
 * Post list block.
 *
 * @package Balance\AheadworksBlog\Block
 */
class PostList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Aheadworks\Blog\Model\Url $_url Url model.
     */
    protected $_url;
    /**
     * @var \Aheadworks\Blog\Block\LinkFactory $_linkFactory Link factory.
     */
    protected $_linkFactory;

    /**
     * Recent constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context     Context model.
     * @param \Aheadworks\Blog\Model\Url                       $url         Url model.
     * @param \Aheadworks\Blog\Block\LinkFactory               $linkFactory Link block factory.
     * @param array                                            $data        Data array.
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Aheadworks\Blog\Model\Url $url,
        \Aheadworks\Blog\Block\LinkFactory $linkFactory,
        array $data = []
    )
    {
        $this->_url = $url;
        $this->_linkFactory = $linkFactory;

        parent::__construct($context, $data);
    }

    /**
     * Returns post list.
     *
     * @param int|null $numberOfPosts Number of posts to return.
     *
     * @return \Aheadworks\Blog\Api\Data\PostInterface[]
     */
    public function getPosts($numberOfPosts = null)
    {
        try {
            $posts = $this->getDataProvider()->getPosts($numberOfPosts);
        } catch (\Exception $exception) {
            return [];
        }

        return $posts;
    }

    /**
     * Returns data provider instance.
     *
     * @return DataProviderInterface
     *
     * @throws \Exception
     */
    public function getDataProvider()
    {
        $dataProvider = $this->getChildBlock(
            $this->getData('posts_data_provider')
        );

        if ($dataProvider === false
            || !$dataProvider instanceof DataProviderInterface
        ) {
            throw new \Exception('Data provider is not defined.');
        }

        return $dataProvider;
    }

    /**
     * Returns block title.
     *
     * @return string
     */
    public function getBlockTitle()
    {
        return $this->getData('block_title');
    }

    /**
     * Returns number of post to display.
     *
     * @return int
     */
    public function getNumberOfPostsToDisplay()
    {
        return (int) $this->getData('number_of_posts');
    }

    /**
     * Returns post item block class.
     *
     * @return string
     */
    public function getPostBlockClass()
    {
        return $this->getData('post_block_class');
    }

    /**
     * Returns post item block template.
     *
     * @return string
     */
    public function getPostBlockTemplate()
    {
        return $this->getData('post_block_template');
    }

    /**
     * Determine whether 'View all' button should be displayed.
     *
     * @return bool
     */
    public function isViewAllButtonDisplayed()
    {
        return (bool) $this->getData('display_view_all_button');
    }

    /**
     * Returns 'View all' button label.
     *
     * @return string
     */
    public function getViewAllButtonLabel()
    {
        return $this->getData('view_all_button_label');
    }

    /**
     * Returns 'View all' button html.
     *
     * @return string
     */
    public function getViewAllButtonHtml()
    {
        /** @var \Aheadworks\Blog\Block\Link $link */
        $link = $this->_linkFactory->create();
        $link->setHref($this->_url->getBlogHomeUrl());
        $link->setTitle(
            __($this->getViewAllButtonLabel())
        );
        $link->setLabel(
            __($this->getViewAllButtonLabel())
        );

        return $link->toHtml();
    }

    /**
     * Returns rendered post item block content.
     *
     * @param \Aheadworks\Blog\Model\Post $post Post instance.
     *
     * @return string
     */
    public function getItemHtml(\Aheadworks\Blog\Model\Post $post)
    {
        $postBlockClass = $this->getPostBlockClass();
        $postBlockTemplate = $this->getPostBlockTemplate();
        if (empty($postBlockClass)
            || empty($postBlockTemplate)
        ) {
            return '';
        }

        /** @var \Balance\AheadworksBlog\Block\Post $block */
        $block = $this->getLayout()->createBlock(
            $postBlockClass,
            '',
            [
                'data' => [
                    'template' => $postBlockTemplate,
                    'post' => $post,
                    'mode' => \Aheadworks\Blog\Block\Post::MODE_LIST_ITEM
                ]
            ]
        );

        return $block->toHtml();
    }
}