<?php
namespace Balance\AheadworksBlog\Block\PostList\DataProvider;

/**
 * Class Related.
 * Blog related posts data provider.
 *
 * @package Balance\AheadworksBlog\Block\PostList\DataProvider
 */
class Related
    extends \Aheadworks\Blog\Block\Product\PostList
    implements DataProviderInterface
{
    /**
     * Returns post list.
     *
     * @param int|null $numberOfPosts Number of posts to return.
     *
     * @return \Aheadworks\Blog\Api\Data\PostInterface[]
     */
    public function getPosts($numberOfPosts = null)
    {
        return parent::getPosts();
    }
}