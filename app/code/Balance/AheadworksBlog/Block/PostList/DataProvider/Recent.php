<?php
namespace Balance\AheadworksBlog\Block\PostList\DataProvider;

/**
 * Class Recent.
 * Blog recent posts data provider.
 *
 * @package Balance\AheadworksBlog\Block\PostList\DataProvider
 */
class Recent
    extends \Aheadworks\Blog\Block\Sidebar\Recent
    implements DataProviderInterface
{
}