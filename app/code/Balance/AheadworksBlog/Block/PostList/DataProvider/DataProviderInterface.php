<?php
namespace Balance\AheadworksBlog\Block\PostList\DataProvider;

/**
 * Interface DataProviderInterface.
 *
 * @package Balance\AheadworksBlog\Block\PostList\DataProvider
 */
interface DataProviderInterface
{
    /**
     * Returns post list.
     *
     * @param int|null $numberOfPosts Number of posts to return.
     *
     * @return \Aheadworks\Blog\Api\Data\PostInterface[]
     */
    public function getPosts($numberOfPosts = null);
}