<?php
/**
 * Created by PhpStorm.
 * User: pivodirtjumper
 * Date: 20.07.17
 * Time: 21:09
 */

namespace Balance\AheadworksBlog\Block\PostList;

class Widget extends \Magento\Framework\View\Element\Template
{

    public function getJsonConfig()
    {
        $config = [
            'ajaxUrl' => $this->getUrl(
                'balance_awblog/block/renderer',
                [
                    '_current' => true,
                    '_secure' => $this->templateContext->getRequest()->isSecure()
                ]
            )
        ];

        return json_encode($config);
    }

}