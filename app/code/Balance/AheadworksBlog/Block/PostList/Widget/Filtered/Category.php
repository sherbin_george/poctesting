<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AheadworksBlog\Block\PostList\Widget\Filtered;

use Magento\Framework\View\Element\Template;
use \Balance\AheadworksBlog\Model\Config;
use \Aheadworks\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use \Aheadworks\Blog\Model\CategoryFactory;
use \Aheadworks\Blog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

class Category extends Post
{

    /**
     * category param name
     */
    CONST CATEGORY_URL = 'category';

    /** @var \Aheadworks\Blog\Model\ResourceModel\Category\CollectionFactory */
    protected $categoryCollectionFactory;

    /**
     * @var Config
     */
    public $config;

    /**
     * Category constructor.
     * @param Template\Context $context
     * @param Config $config
     * @param PostCollectionFactory $postCollectionFactory
     * @param CategoryFactory $categoryFactory
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Config $config,
        PostCollectionFactory $postCollectionFactory,
        CategoryFactory $categoryFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        array $data = []
    )
    {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->config = $config;

        parent::__construct($context, $config, $postCollectionFactory, $categoryFactory, $data);
    }

    /**
     * @return \Balance\AheadworksBlog\Model\ResourceModel\Category\Collection
     */
    public function getCategoryCollection()
    {
        $postLayout = $this->getRequest()->getParam('layout');

        /** @var \Balance\AheadworksBlog\Model\ResourceModel\Category\Collection $categoryCollection */
        $categoryCollection = $this->categoryCollectionFactory->create();
        $categoryCollection
            ->addFieldToFilter(\Aheadworks\Blog\Api\Data\CategoryInterface::STATUS, \Aheadworks\Blog\Model\Source\Category\Status::ENABLED)
            ->addStoreFilter(\Aheadworks\Blog\Api\Data\CategoryInterface::STORE_IDS, $this->_storeManager->getStore()->getId());

        if ($postLayout) {
            $categoryCollection->addProductIdFilter($this->getPostsFiltered()->getAllIds());
        }

        $categoryCollection->addOrder('sort_order', 'ASC');

        return $categoryCollection;
    }

    /**
     * @return \Balance\AheadworksBlog\Model\ResourceModel\Category\Collection
     */
    public function getAllCategories()
    {
        /** @var \Balance\AheadworksBlog\Model\ResourceModel\Category\Collection $categoryCollection */
        $postLayout = $this->getRequest()->getParam('category');

        $categoryCollection = $this->categoryCollectionFactory->create();
        $categoryCollection
            ->addFieldToFilter(\Aheadworks\Blog\Api\Data\CategoryInterface::STATUS, \Aheadworks\Blog\Model\Source\Category\Status::ENABLED)
            ->addStoreFilter($this->_storeManager->getStore()->getId());


        if ($postLayout) {
            $categoryCollection->addProductIdFilter($this->getPostsFiltered()->getAllIds());
        }

        $categoryCollection->addOrder('sort_order', 'ASC');

        return $categoryCollection;
    }

    /**
     * @param $categoryId
     * @return string
     */
    public function getCategoryUrl($categoryId)
    {
        return $this->getUrl(
            null,
            [
                '_current' => true,
                '_escape' => true,
                '_use_rewrite' => true,
                '_query' => [
                    self::CATEGORY_URL => $categoryId
                ],
                '_direct' => $this->config->getRouteToBlog()
            ]
        );
    }

}