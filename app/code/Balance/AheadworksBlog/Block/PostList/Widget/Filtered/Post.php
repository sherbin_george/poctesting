<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AheadworksBlog\Block\PostList\Widget\Filtered;

use Magento\Framework\View\Element\Template;
use Balance\AheadworksBlog\Model\Config;
use Aheadworks\Blog\Model\ResourceModel\Post\CollectionFactory;
use \Aheadworks\Blog\Model\CategoryFactory;
use Balance\AheadworksBlog\Model\Source\Post\SharingButtons\PaginationType;
use \Aheadworks\Blog\Api\Data\PostInterface;

class Post extends Template
{

    /** @var \Aheadworks\Blog\Model\ResourceModel\Post\CollectionFactory */
    protected $postCollectionFactory;

    /** @var \Magento\Catalog\Api\CategoryRepositoryInterface */
    protected $categoryFactory;

    /** @var \Balance\AheadworksBlog\Model\Config */
    public $config;

    /**
     * Post constructor.
     * @param Template\Context $context
     * @param Config $config
     * @param CollectionFactory $postCollectionFactory
     * @param CategoryFactory $categoryFactory
     * @param array $data
     */

    public function __construct(
        Template\Context $context,
        Config $config,
        CollectionFactory $postCollectionFactory,
        CategoryFactory $categoryFactory,
        array $data = []
    )
    {
        $this->postCollectionFactory = $postCollectionFactory;
        $this->categoryFactory = $categoryFactory;
        $this->config = $config;
        parent::__construct($context, $data);
    }

    /**
     * @return int
     */
    public function getCurrentCategoryId()
    {
        return $this->isMoreButton() ?
            (int) $this->getRequest()->getPost('category') :
            (int) $this->getRequest()->getParam('category');
    }

    /**
     * @return $this
     */
    public function getCurrentCategory()
    {
        return $this->categoryFactory->create()->load($this->getCurrentCategoryId());
    }

    /**
     * @return mixed
     */
    public function getCurrentLayout()
    {
        return $this->getRequest()->getParam('layout');
    }

    /**
     * @return int
     */
    public function getCurrentTag()
    {
        return (int) $this->getRequest()->getParam('tag');
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->isMoreButton() ?
            (int) $this->getRequest()->getPost('page', 1) :
            (int) $this->getRequest()->getParam('page', 1);
    }

    /**
     * @return bool
     */
    public function isMoreButton()
    {
        return $this->config->isPaginationType(PaginationType::MORE_BUTTON);
    }

    /**
     * @return \Aheadworks\Blog\Model\ResourceModel\Post\Collection
     */
    public function getPostsFiltered()
    {
        /** @var \Aheadworks\Blog\Model\ResourceModel\Post\Collection $postCollection */
        $postCollection = $this->postCollectionFactory->create();

        $postLayout = $this->getCurrentLayout();
        $category = $this->getCurrentCategoryId();
        $tag = $this->getCurrentTag();

        if ($postLayout) {
            $postCollection->addFieldToFilter('layout_type', $postLayout);
        }

        if ($category && is_int($category)) {
            $postCollection->addCategoryFilter($category);
        }

        if ($tag && is_int($tag)) {
            $postCollection->addTagFilter($tag);
        }

        $postCollection->addFieldToFilter(PostInterface::STATUS, \Aheadworks\Blog\Model\Source\Post\Status::PUBLICATION)
             ->addStoreFilter($this->_storeManager->getStore()->getId());

        return $postCollection;
    }

    /**
     * @return \Aheadworks\Blog\Model\ResourceModel\Post\Collection
     */
    public function getPostCollection()
    {
        return $this->getPostsFiltered()->setPageSize($this->config->getNumPostsPerPage())->setCurPage($this->getCurrentPage());
    }

    /**
     * Returns rendered post item block content.
     *
     * @param \Aheadworks\Blog\Model\Post $post Post instance.
     *
     * @return string
     */
    public function getItemHtml(\Aheadworks\Blog\Model\Post $post)
    {
        return $this->getLayout()->createBlock(
            \Balance\AheadworksBlog\Block\Post::class,
            '',
            [
                'data' => [
                    'template' => 'Balance_AheadworksBlog::post/list/item.phtml',
                    'post' => $post,
                    'mode' => \Aheadworks\Blog\Block\Post::MODE_LIST_ITEM
                ]
            ]
        )->toHtml();
    }

    /**
     * @return bool
     */
    public function canShowMore()
    {
        return $this->isMoreButton() &&
            ($this->getPostCollection()->getLastPageNumber() > $this->getPostCollection()->getCurPage());
    }

}