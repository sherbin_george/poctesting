/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

var config = {
    map: {
        '*': {
            blogPostList: 'Balance_AheadworksBlog/js/post/list'
        }
    }
};
