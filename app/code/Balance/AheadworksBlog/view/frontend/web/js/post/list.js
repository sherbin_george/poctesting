/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 * @namespace Balace_AheadworksBlog
 */
define([
    "jquery",
    "jquery/ui"
], function($) {
    "use strict";

    $.widget('mage.blogPostList', {
        options: {
            ajaxUrl         : '/',
            currentPage     : 1,
            layoutSelector  : '[data-role=layout-tab]',
            categorySelector: '[data-role=category-filter]',
            loadMoreButton  : '[data-button=more]',
            currentLayout   : undefined,
            currentCategory : undefined,
            activeClass     : 'active'
        },

        /**
         *
         * @param layout
         * @private
         */
        _setLayout: function(layout) {
            /*this.options.currentTag = undefined;*/
            this.options.currentCategory = undefined;
            this.options.currentLayout = layout;
        },

        /**
         *
         * @param category
         * @private
         */
        _setCategory: function(category) {
            this.options.currentCategory = category;
        },

        /**
         *
         * @private
         */
        _create: function() {
            this.options.currentPage = 1;

            $(document).on('click', this.options.layoutSelector, $.proxy(function(event) {
                event.preventDefault();
                this.options.currentPage = 1;
                this._setLayout(event.target.getAttribute('data-value'));
                this._removeActiveClass(this.options.layoutSelector);
                this._addActiveClass(event.target);
                this.loadContent(this.loadInitial);
            }, this));
            $(document).on('click', this.options.categorySelector, $.proxy(function(event) {
                event.preventDefault();
                this.options.currentPage = 1;
                this._setCategory(event.target.getAttribute('data-value'));
                this.loadContent(this.loadInitial)
            }, this));
            $(document).on('click', this.options.loadMoreButton, $.proxy(function() {
                event.preventDefault();
                this.options.currentPage++;
                this.loadContent(this.loadMore);
            }, this));
        },

        /**
         * Removes active class for specified elements.
         *
         * @param {string|object} elements Element(s) to be processed.
         *
         * @returns void
         */
        _removeActiveClass: function(elements) {
            if (typeof elements !== 'object') {
                elements = $(elements);
            }
            $(elements).removeClass(this.options.activeClass);
        },

        /**
         * Adds active class to an element.
         *
         * @param {string|object} elements Element(s) to be processed.
         *
         * @returns void
         */
        _addActiveClass: function(elements) {
            if (typeof elements !== 'object') {
                elements = $(elements);
            }
            $(elements).addClass(this.options.activeClass);
        },

        /**
         *
         * @param content
         */
        loadMore: function(content) {
            $('[data-role=post-list]').append(content.postList);
            content.canShowMore ? $('[data-button=more]').show() : $('[data-button=more]').hide();
        },

        /**
         *
         * @param content
         */
        loadInitial: function(content) {
            /**
             * replace content in it's current container
             */
            $('[data-role=post-list]').html(content.postList);
            $('[data-role=category-links]').html(content.categories);

            content.canShowMore ? $('[data-button=more]').show() : $('[data-button=more]').hide();
        },

        /**
         *
         * @returns {string}
         * @private
         */
        loadContent: function(successCallback) {
            $.ajax({
                url: this.options.ajaxUrl,
                data: {
                    'category' : this.options.currentCategory,
                    'layout'   : this.options.currentLayout,
                    'tag'      : this.options.currentTag,
                    'page'     : this.options.currentPage
                },
                type: 'POST',
                cache: false,
                dataType: 'json',
                context: this,
                showLoader: true,
                success: function(response) {
                    successCallback(response);
                }
            });
        }
    });

    return $.mage.blogPostList;
});