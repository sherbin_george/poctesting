<?php
namespace Balance\AheadworksBlog\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class UpgradeData.
 * Sets the data on module upgrade.
 *
 * @package Balance\AheadworksBlog\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Magento\Catalog\Setup\CategorySetupFactory $_categorySetupFactory Category setup factory.
     */
    protected $_categorySetupFactory;
    /**
     * @var \Magento\Catalog\Setup\CategorySetup $_categoryEavSetup Category EAV setup model.
     */
    private $_categoryEavSetup;
    /**
     * @var string $_setupVersion Current setup version.
     */
    private $_setupVersion;

    /**
     * @var \Aheadworks\Blog\Model\ResourceModel\PostRepository $_postRepository Post repository.
     */
    private $_postRepository;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder Search criteria.
     */
    private $_searchCriteriaBuilder;

    /**
     * UpgradeData constructor.
     *
     * @param \Magento\Catalog\Setup\CategorySetupFactory         $categorySetupFactory  Category setup factory.
     * @param \Magento\Framework\Api\SearchCriteriaBuilder        $searchCriteriaBuilder Search criteria.
     * @param \Aheadworks\Blog\Model\ResourceModel\PostRepository $postRepository        Post repository.
     * @param \Magento\Framework\App\State                        $appState              App state.
     */
    public function __construct(
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Aheadworks\Blog\Model\ResourceModel\PostRepository $postRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\App\State $appState
    ) {
        $this->_categorySetupFactory = $categorySetupFactory;
        $this->_postRepository = $postRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_appState= $appState;
    }

    /**
     * Upgrades the data.
     *
     * @param ModuleDataSetupInterface $setup   Setup model.
     * @param ModuleContextInterface   $context Context model.
     *
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // Share current setup version with all class functionality.
        $this->_setupVersion = $context->getVersion();

        $this->_categoryEavSetup = $this->_categorySetupFactory->create(
            ['setup' => $setup]
        );

        $this->_updateSkuAttribute();
        $this->_updateProductCondition();
    }

    /**
     * Updates 'sku' product attribute.
     * Set 'is_used_for_promo_rules' property to 'true'.
     *
     * @return void
     */
    private function _updateSkuAttribute()
    {
        if (version_compare($this->_setupVersion, '1.0.1') < 0) {
            $this->_categoryEavSetup->updateAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'sku',
                'is_used_for_promo_rules',
                1
            );
        }
    }

    /**
     * Updates 'product_condition' post attribute.
     * Set property if it empty.
     *
     * @return void
     */
    private function _updateProductCondition()
    {
        if (version_compare($this->_setupVersion, '1.0.2') < 0) {

            $this->_appState->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);

            /** @var \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteria Search criteria. */
            $searchCriteria = $this->_searchCriteriaBuilder
                ->addFilter(\Aheadworks\Blog\Model\Post::PRODUCT_CONDITION, '');

            $posts = $this->_postRepository->getList($searchCriteria->create())->getItems();

            /** @var \Balance\AwBlogCustomFields\Model\Aheadworks\Blog\Post $post */
            foreach ($posts as $post) {
                $post->setProductCondition(
                    serialize([
                        'type' => 'Aheadworks\Blog\Model\Rule\Condition\Combine',
                        'aggregator' => 'all',
                        'value' => 0,
                        'value_type' => null
                    ])
                );
                $post->setStoreIds([0]);

                $this->_postRepository->save($post);
            }
        }
    }
}