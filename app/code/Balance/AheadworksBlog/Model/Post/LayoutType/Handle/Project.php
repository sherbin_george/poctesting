<?php
namespace Balance\AheadworksBlog\Model\Post\LayoutType\Handle;

/**
 * Class Project.
 * Layout type project handle model.
 *
 * @package Balance\AheadworksBlog\Model\Post\LayoutType\Handle
 */
class Project
    extends \Balance\AwBlogCustomFields\Model\Post\LayoutType\Handle\AbstractHandle
    implements \Balance\AwBlogCustomFields\Model\Post\LayoutType\HandleInterface
{
    /**
     * @const string Handle ID.
     */
    const ID = 'project';
    /**
     * @const string Handle title.
     */
    const TITLE = 'Project';
    /**
     * @const string Layout update handle.
     */
    const LAYOUT_UPDATE_HANDLE = 'project';
}