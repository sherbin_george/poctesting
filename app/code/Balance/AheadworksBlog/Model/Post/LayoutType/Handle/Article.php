<?php
namespace Balance\AheadworksBlog\Model\Post\LayoutType\Handle;

/**
 * Class Article.
 * Layout type article handle model.
 *
 * @package Balance\AheadworksBlog\Model\Post\LayoutType\Handle
 */
class Article
    extends \Balance\AwBlogCustomFields\Model\Post\LayoutType\Handle\AbstractHandle
    implements \Balance\AwBlogCustomFields\Model\Post\LayoutType\HandleInterface
{
    /**
     * @const string Handle ID.
     */
    const ID = 'article';
    /**
     * @const string Handle title.
     */
    const TITLE = 'Article';
    /**
     * @const string Layout update handle.
     */
    const LAYOUT_UPDATE_HANDLE = 'article';
}