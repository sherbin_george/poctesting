<?php
namespace Balance\AheadworksBlog\Model\Post\LayoutType\Handle;

/**
 * Class Video.
 * Layout type video handle model.
 *
 * @package Balance\AheadworksBlog\Model\Post\LayoutType\Handle
 */
class Video
    extends \Balance\AwBlogCustomFields\Model\Post\LayoutType\Handle\AbstractHandle
    implements \Balance\AwBlogCustomFields\Model\Post\LayoutType\HandleInterface
{
    /**
     * @const string Handle ID.
     */
    const ID = 'video';
    /**
     * @const string Handle title.
     */
    const TITLE = 'Video';
    /**
     * @const string Layout update handle.
     */
    const LAYOUT_UPDATE_HANDLE = 'video';
}