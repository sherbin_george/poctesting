<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AheadworksBlog\Model\Source\Post\SharingButtons;

use \Magento\Framework\Option\ArrayInterface;

class PaginationType implements ArrayInterface
{
    /**
     * 'more_button' option
     */
    const MORE_BUTTON = 'more_button';

    /**
     * ''pagination' option
     */
    const PAGINATION = 'pagination';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::MORE_BUTTON,
                'label' => __('Show more button')
            ],
            [
                'value' => self::PAGINATION,
                'label' => __('Pagination')
            ]
        ];
    }
}
