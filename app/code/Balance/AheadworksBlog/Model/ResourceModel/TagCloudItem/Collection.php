<?php
/**
 * Created by PhpStorm.
 * User: pivodirtjumper
 * Date: 01.08.17
 * Time: 18:37
 */

namespace Balance\AheadworksBlog\Model\ResourceModel\TagCloudItem;

class Collection extends \Aheadworks\Blog\Model\ResourceModel\TagCloudItem\Collection
{

    protected function _construct()
    {
        parent::_construct();
        $this->_map['fields']['post_layout'] = 'post_table.layout_type';
    }

    public function addPostLayoutFilter($postLayout)
    {
        if (! empty($postLayout)) {
            $this->addFilter('post_layout', ['eq' => $postLayout], 'public');
        }
    }

}