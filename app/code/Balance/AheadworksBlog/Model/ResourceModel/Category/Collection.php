<?php
/**
 * Created by PhpStorm.
 * User: pivodirtjumper
 * Date: 23.07.17
 * Time: 19:05
 */

namespace Balance\AheadworksBlog\Model\ResourceModel\Category;

class Collection extends \Aheadworks\Blog\Model\ResourceModel\Category\Collection
{

    public function addProductIdFilter($productIds)
    {
        $select = $this->getSelect();

        $select->joinLeft(
            ['category_linkage_table' => $this->getTable('aw_blog_post_category')],
            'main_table.id = category_linkage_table.category_id',
            []
        );

        $select->where('category_linkage_table.post_id IN (?)', $productIds);
        $select->group('main_table.id');

        return $this;
    }

}