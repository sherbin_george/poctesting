<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AheadworksBlog\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Aheadworks\Blog\Model\Config as AheadworksConfig;

/**
 * Blog config
 */
class Config extends AheadworksConfig
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Configuration path to number of pages display
     */
    const XML_PATH_NUMBER_OF_PAGES = 'aw_blog/general/number_of_pages';

    /**
     * Configuration path to pagination type
     */
    const XML_PATH_PAGINATION_TYPE = 'aw_blog/general/pagination_type';

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    )
    {
        parent::__construct($scopeConfig, $storeManager);
        $this->scopeConfig = $scopeConfig;
    }
    /**
     * Get number of pages
     *
     * @return int
     */
    public function getNumberOfPages()
    {
        return (int)$this->scopeConfig->getValue(self::XML_PATH_NUMBER_OF_PAGES, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get pagination type
     *
     * @return string
     */
    private function getPaginationType()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_PAGINATION_TYPE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Check pagination type is
     * @param string $paginationType
     * @return bool
     */
    public function isPaginationType($paginationType)
    {
        return $this->getPaginationType() === $paginationType;
    }
}