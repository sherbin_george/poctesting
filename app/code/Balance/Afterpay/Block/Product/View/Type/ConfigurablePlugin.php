<?php
/**
 * User: Denis Popov
 * Date: 16.07.17
 * Time: 00:24
 */

namespace Balance\Afterpay\Block\Product\View\Type;

use Magento\ConfigurableProduct\Block\Product\View\Type\Configurable;

/**
 * Class ConfigurablePlugin
 *
 * @package Balance\ConfigurableProduct\Block\Product\View\Type
 */
class ConfigurablePlugin
{
    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $currency;
    
    /**
     * @var \Magento\Framework\Json\DecoderInterface
     */
    protected $jsonDecoder;
    
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;
    
    public function __construct(
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\Json\DecoderInterface $jsonDecoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder
    )
    {
        $this->currency = $currency;
        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
    }
    
    /**
     * Replace ',' on '.' for js
     *
     * @param float $price
     * @return string
     */
    protected function _registerJsPrice($price)
    {
        return str_replace(',', '.', $price);
    }
    
    /**
     * Complete products with additional price.
     *
     * @param Configurable $configurable
     * @param string       $config
     *
     * @return string
     */
    public function afterGetJsonConfig(Configurable $configurable, $config)
    {
        $config = $this->jsonDecoder->decode($config);
        
        if (!empty($config['optionPrices'])) {
            foreach ($config['optionPrices'] as &$price) {
                $price['installments'] = [
                    'amount' => $this->_registerJsPrice(
                        $this->currency->getCurrencySymbol() . number_format($price['finalPrice']['amount'] / 4, 2)
                    )
                ];
            }
        }
        if (!empty($config['prices'])) {
            $finalPrice = $config['prices']['finalPrice'];
            $afterpay = $this->currency->getCurrencySymbol() . number_format($finalPrice['amount'] / 4, 2);
            $config['prices']['installments']['amount'] = $afterpay;
        }
        
        return $this->jsonEncoder->encode($config);
    }
}
