/*
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'Magento_Checkout/js/action/select-payment-method': 'Balance_Afterpay/js/action/payment/select-payment-method',
            'Afterpay_Afterpay/js/view/payment/method-renderer/afterpaypayovertime': 'Balance_Afterpay/js/view/payment/method-renderer/afterpaypayovertime'
        }
    }
};
