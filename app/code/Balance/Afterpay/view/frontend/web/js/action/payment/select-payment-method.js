/*
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'jquery',
        'uiComponent',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/get-totals'
    ], function ($, uiComponent, ko, quote, getTotalsAction) {
        'use strict';

        /**
         * Create XmlHttpRequest to build cache options
         */
        function makeRequest (value) {
            var requestUrl = window.checkoutConfig.payment.extras.updateCheckoutSummaryUrl,
                params = 'method' + '/' + value;

            $.ajax({
                type: 'POST',
                url: requestUrl.replace('isAjax/true', params),
                data: {method: value},
                dataType: 'json',
                showLoader: true
            }).success(function (data) {
                getTotalsAction([], $.Deferred());
            });
        }

        return function (paymentMethod) {
            if (paymentMethod) {
                makeRequest(paymentMethod.method);
            }
            quote.paymentMethod(paymentMethod);
        };
    }
);
