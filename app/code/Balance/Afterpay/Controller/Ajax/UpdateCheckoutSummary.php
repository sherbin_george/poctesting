<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Afterpay\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Model\Quote;
use Afterpay\Afterpay\Model\Payovertime;

/**
 * Update checkout summary ajax controller
 *
 * @package Balance\Afterpay\Controller\Ajax
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class UpdateCheckoutSummary extends Action
{
    /**
     * @var Quote|null
     */
    protected $quote = null;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @param Context         $context
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->isPayovertimeRequest()) {
            $responseData = $this->setPaymentMethod($this->getRequest()->getParam('method'));
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($responseData);
            return $resultJson;
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_url->getBaseUrl());
        return $resultRedirect;
    }

    /**
     * Get active or custom quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    private function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }

    /**
     * Set payment method to current quote
     *
     * @param string $method Payment method code
     *
     * @return array
     */
    private function setPaymentMethod($method)
    {
        $response = [
            'method' => $method,
            'is_afterpay' => false
        ];

        $this->getQuote()->getPayment()->setMethod((string)$method)->save();

        if ($method === Payovertime::METHOD_CODE) {
            $response['is_afterpay'] = true;
        }

        return $response;
    }

    /**
     * Check if current request from Afterpay payment method
     *
     * @return bool
     */
    private function isPayovertimeRequest()
    {
        return ($this->getRequest()->getParam(Payovertime::METHOD_CODE) && $this->getRequest()->getParam('method'));
    }
}
