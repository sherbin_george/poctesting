<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Afterpay\Controller\Payment;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Response
 * @package Afterpay\Afterpay\Controller\Payment
 */
class Process extends \Afterpay\Afterpay\Controller\Payment\Process
{
    /**
     * {@inheritdoc}
     */
    public function _processAuthorizeCapture()
    {
        //need to load the correct quote by store
        $data = $this->_checkoutSession->getData();

        $quote = $this->_checkoutSession->getQuote();
        $store_id = $this->_afterpayConfig->getStoreObjectFromRequest()->getId();

        if ($store_id > 1) {
            $quote = $this->_quoteFactory->create()->loadByIdWithoutStore($this->_checkoutSession->getQuoteId());
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');

        if ($customerSession->isLoggedIn()) {
            $customerId = $customerSession->getCustomer()->getId();
            $customer = $customerRepository->getById($customerId);

            // customer login
            $quote->setCustomer($customer);

            $billingAddress = $quote->getBillingAddress();
            $shippingAddress = $quote->getShippingAddress();

            //check if shipping address is missing - e.g. Gift Cards
            if (empty($shippingAddress) || empty($shippingAddress->getStreetLine(1)) && empty($billingAddress) || empty($billingAddress->getStreetLine(1))) {
                die(json_encode(["success" => false, "message" => "Please select an Address"]));
            } else {
                if (empty($shippingAddress) || empty($shippingAddress->getStreetLine(1)) || empty($shippingAddress->getFirstname())) {
                    $shippingAddress = $quote->getBillingAddress();
                    $quote->setShippingAddress($object->getBillingAddress());
                } else {
                    if (empty($billingAddress) || empty($billingAddress->getStreetLine(1)) || empty($billingAddress->getFirstname())) {
                        $billingAddress = $quote->getShippingAddress();
                        $quote->setBillingAddress($object->getShippingAddress());
                    }
                }
            }
        } else {
            $post = $this->getRequest()->getPostValue();

            if (!empty($post['email'])) {
                $quote->setCustomerEmail($post['email'])
                    ->setCustomerIsGuest(true)
                    ->setCustomerGroupId(\Magento\Customer\Api\Data\GroupInterface::NOT_LOGGED_IN_ID);
            }
        }

        $payment = $quote->getPayment();

        $payment->setMethod(\Afterpay\Afterpay\Model\Payovertime::METHOD_CODE);

        $quote->reserveOrderId();

        try {
            $payment = $this->_getAfterPayOrderToken($this->_afterpayOrderTokenV1, $payment, $quote);
        } catch (\Exception $e) {
            die(json_encode(['error' => 1, 'message' => $e->getMessage()]));
        }

        $quote->setPayment($payment);
        $quote->save();

        //$this->_checkoutSession->setQuote($quote);

        $token = $payment->getAdditionalInformation(\Afterpay\Afterpay\Model\Payovertime::ADDITIONAL_INFORMATION_KEY_TOKEN);

        die(json_encode(["success" => true, "token" => $token]));
    }

    /**
     * @param $payment
     *
     * @return bool
     * @throws LocalizedException
     */
    private function _getAfterPayOrderToken($afterpayOrderToken, $payment, $targetObject)
    {
        if ($targetObject && $targetObject->getReservedOrderId()) {
            $result = $afterpayOrderToken->generate($targetObject,
                \Afterpay\Afterpay\Model\Payovertime::AFTERPAY_PAYMENT_TYPE_CODE_V1,
                ['merchantOrderId' => $targetObject->getReservedOrderId()]);
        } else {
            if ($targetObject) {
                $result = $afterpayOrderToken->generate($targetObject,
                    \Afterpay\Afterpay\Model\Payovertime::AFTERPAY_PAYMENT_TYPE_CODE_V1);
            }
        }

        $result = $this->_jsonHelper->jsonDecode($result->getBody(), true);
        $orderToken = array_key_exists('token', $result) ? $result['token'] : false;

        if ($orderToken) {
            $payment->setAdditionalInformation(\Afterpay\Afterpay\Model\Payovertime::ADDITIONAL_INFORMATION_KEY_TOKEN,
                $orderToken);
        } else {
            $this->_helper->debug('No Token response from API');
            throw new \Magento\Framework\Exception\LocalizedException(__('There is an issue processing your order.'));
        }
        return $payment;
    }
}
