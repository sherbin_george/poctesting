<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Afterpay\Controller\Payment;

use Afterpay\Afterpay\Helper\Data as AfterpayHelper;
use Afterpay\Afterpay\Model\Adapter\V1\AfterpayOrderDirectCapture;
use Afterpay\Afterpay\Model\Adapter\V1\AfterpayOrderTokenCheck;
use Afterpay\Afterpay\Model\Config\Payovertime;
use Afterpay\Afterpay\Model\Response as AfterpayResponse;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory as ResultForwardFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface as TransactionBuilder;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;

/**
 * Class Response
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @package Balance\Afterpay\Controller\Payment
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Response extends \Afterpay\Afterpay\Controller\Payment\Response
{
    /**
     * @var OrderSender
     */
    protected $orderSender;

    /**
     * Response constructor.
     *
     * @param Context                           $context
     * @param ResultForwardFactory              $resultForwardFactory
     * @param CheckoutSession                   $checkoutSession
     * @param AfterpayResponse                  $response
     * @param AfterpayHelper                    $helper
     * @param AfterpayOrderDirectCapture        $directCapture
     * @param AfterpayOrderTokenCheck           $tokenCheck
     * @param JsonHelper                        $jsonHelper
     * @param Payovertime                       $afterpayConfig
     * @param QuoteManagement                   $quoteManagement
     * @param TransactionBuilder                $transactionBuilder
     * @param OrderSender                       $orderSender
     */
    public function __construct(
        Context $context,
        ResultForwardFactory $resultForwardFactory,
        CheckoutSession $checkoutSession,
        AfterpayResponse $response,
        AfterpayHelper $helper,
        AfterpayOrderDirectCapture $directCapture,
        AfterpayOrderTokenCheck $tokenCheck,
        JsonHelper $jsonHelper,
        Payovertime $afterpayConfig,
        QuoteManagement $quoteManagement,
        TransactionBuilder $transactionBuilder,
        OrderSender $orderSender
    ) {
        parent::__construct(
            $context,
            $resultForwardFactory,
            $checkoutSession,
            $response,
            $helper,
            $directCapture,
            $tokenCheck,
            $jsonHelper,
            $afterpayConfig,
            $quoteManagement,
            $transactionBuilder
        );
        $this->orderSender = $orderSender;
    }

    /**
     * Actual action when accessing url
     */
    public function execute()
    {
        // debug mode
        $this->_helper->debug(
            __('Start %1::execute() with request %2',
                \Afterpay\Afterpay\Controller\Payment\Response::class,
                $this->_jsonHelper->jsonEncode($this->getRequest()->getParams())
            ));

        $query = $this->getRequest()->getParams();
        $order = $this->_checkoutSession->getLastRealOrder();

        // Check if not fraud detected not doing anything (let cron update the order if payment successful)
        if ($this->_afterpayConfig->getPaymentAction() == AbstractMethod::ACTION_AUTHORIZE_CAPTURE) {
            //Steven - Bypass the response and do capture
            $redirect = $this->_processAuthCapture($query);
        } else {
            if (!$this->response->validCallback($order, $query)) {
                $this->_helper->debug('Request redirect url is not valid.');
            } else {
                $this->_helper->debug('Order triggered.');
                $redirect = $this->_processOrder($query, $order);
            }
        }

        // debug mode
        $this->_helper->debug('Finished \Afterpay\Afterpay\Controller\Payment\Response::execute()');

        // Redirect to cart
        $this->_redirect($redirect);
    }

    /**
     * @param $query
     *
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function _processAuthCapture($query)
    {
        $redirect = self::DEFAULT_REDIRECT_PAGE;
        try {
            switch ($query['status']) {
                case AfterpayResponse::RESPONSE_STATUS_CANCELLED:
                    $this->messageManager->addErrorMessage(__('You have cancelled your Afterpay payment. Please select an alternative payment method.'));
                    break;
                case AfterpayResponse::RESPONSE_STATUS_FAILURE:
                    $this->messageManager->addErrorMessage(__('Afterpay payment failure. Please select an alternative payment method.'));
                    break;
                case AfterpayResponse::RESPONSE_STATUS_SUCCESS:
                    //Steven - Capture here
                    $quote = $this->_checkoutSession->getQuote();
                    $payment = $quote->getPayment();
                    $token = $payment->getAdditionalInformation(\Afterpay\Afterpay\Model\Payovertime::ADDITIONAL_INFORMATION_KEY_TOKEN);
                    $merchant_order_id = $quote->getReservedOrderId();

                    /**
                     * Start fix
                     * LHA0003-701
                     */
                    $response_check = $this->_tokenCheck->generate($token);
                    if ($response_check instanceof \Zend_Http_Response) {
                        $response_check = $response_check->getBody();
                    }
                    
                    try {
                        $response_check = $this->_jsonHelper->jsonDecode($response_check);
                    } catch (\Exception $e) {
                        $response_check = false;
                    }
                    /**
                     * End fix
                     */

                    /**
                     * Validation to check between session and post request
                     */
                    if (!$response_check) {
                        // Check the order token being use
                        throw new LocalizedException(__('There are issues when processing your payment. Invalid Token'));
                    } else {
                        if ($merchant_order_id != $response_check['merchantReference']) {
                            // Check order id
                            throw new LocalizedException(__('There are issues when processing your payment. Invalid Merchant Reference'));
                        } else {
                            if (round($quote->getGrandTotal(), 2) != round($response_check['totalAmount']['amount'],
                                    2)
                            ) {
                                // Check the order amount
                                throw new LocalizedException(__('There are issues when processing your payment. Invalid Amount'));
                            }
                        }
                    }

                    $response = $this->_directCapture->generate($token, $merchant_order_id);
                    $response = $this->_jsonHelper->jsonDecode($response->getBody());

                    if (empty($response['status'])) {
                        $response['status'] = AfterpayResponse::RESPONSE_STATUS_DECLINED;
                        $this->_helper->debug("Transaction Exception (Empty Response): " . json_encode($response));
                    }

                    switch ($response['status']) {
                        case AfterpayResponse::RESPONSE_STATUS_APPROVED:
                            $payment->setAdditionalInformation(\Afterpay\Afterpay\Model\Payovertime::ADDITIONAL_INFORMATION_KEY_ORDERID,
                                $response['id']);

                            $this->_checkoutSession
                                ->setLastQuoteId($quote->getId())
                                ->setLastSuccessQuoteId($quote->getId())
                                ->clearHelperData();

                            // Create Order From Quote
                            $order = $this->_quoteManagement->submit($quote);
                            $order->setEmailSent(0);
                            try {
                                $this->orderSender->send($order);
                            } catch (\Exception $e) {
                                $this->_helper->debug(__('Email sender: %1', $e->getMessage()));
                            }

                            if ($order) {
                                $this->_checkoutSession->start();
                                $this->_checkoutSession->setLastOrderId($order->getId())
                                    ->setLastRealOrderId($order->getIncrementId())
                                    ->setLastOrderStatus($order->getStatus());

                                $this->_createTransaction($order, $response);

                                $this->messageManager->addSuccessMessage(__('Afterpay Transaction Completed'));

                                $redirect = 'checkout/onepage/success';
                                // $this->_redirect('checkout/onepage/success');
                            }

                            break;
                        case AfterpayResponse::RESPONSE_STATUS_DECLINED:
                            $this->messageManager->addErrorMessage(__('Afterpay payment declined. Please select an alternative payment method.'));
                            break;
                        // case \Afterpay\Afterpay\Model\Response::RESPONSE_STATUS_PENDING:
                        //     $payment->setTransactionId($payment->getAfterpayOrderId())
                        //         ->setIsTransactionPending(true);
                        //     break;
                        default:
                            // $this->messageManager->addError(__('There is a problem with your Afterpay payment. Please select an alternative payment method.'));
                            $this->messageManager->addErrorMessage($response);
                            break;
                    }
                    break;
            }
        } catch (LocalizedException $e) {
            $this->_helper->debug("Transaction Exception: " . $e->getMessage());
            $this->messageManager->addErrorMessage(
                $e->getMessage()
            );
        } catch (\Exception $e) {
            $this->_helper->debug("Transaction Exception: " . $e->getMessage());
            $this->messageManager->addErrorMessage(
                $e->getMessage()
            );
        }

        return $redirect;
    }

    /**
     * Create transaction
     *
     * @param \Magento\Sales\Model\Order|null $order
     * @param array                           $paymentData
     *
     * @return mixed
     * @throws LocalizedException
     */
    private function _createTransaction($order = null, $paymentData = [])
    {
        try {
            //get payment object from order object
            $payment = $order->getPayment();
            $payment->setLastTransId($paymentData['id']);
            $payment->setTransactionId($paymentData['id']);
            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );

            $message = __('The authorized amount is %1.', $formatedPrice);
            //get the object of builder class
            $trans = $this->_transactionBuilder;
            $transaction = $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentData['id'])
                ->setFailSafe(true)
                //build method creates the transaction and returns the object
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $payment->setParentTransactionId(null);
            $payment->save();
            $order->save();

            return $transaction->save()->getTransactionId();
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * Process order
     *
     * @param array                      $query Request params
     * @param \Magento\Sales\Model\Order $order
     *
     * @return string
     */
    private function _processOrder($query, $order)
    {
        $redirect = self::DEFAULT_REDIRECT_PAGE;
        try {
            switch ($query['status']) {
                case AfterpayResponse::RESPONSE_STATUS_CANCELLED:
                    // adding comment
                    $order->addStatusHistoryComment(__('Customer cancelled the payment'));

                    // perform cancel order and save the order
                    $this->response->cancelOrder($order, __('Payment is cancelled by Gateway.'));
                    $this->response->returnProductsToCart($order);
                    $this->messageManager->addErrorMessage(__('You have cancelled your Afterpay payment. Please select an alternative payment method.'));
                    break;
                case AfterpayResponse::RESPONSE_STATUS_FAILURE:
                    // adding comment
                    $order->addStatusHistoryComment(__('Customer payment was declined'));

                    // perform cancel order and return product to cart
                    $this->response->cancelOrder($order, __('Payment has detected failure by Gateway.'));
                    $this->response->returnProductsToCart($order);
                    $this->messageManager->addErrorMessage(__('Your Afterpay payment was declined. Please select an alternative payment method.'));
                    break;
                case AfterpayResponse::RESPONSE_STATUS_SUCCESS:
                    if (!array_key_exists('orderId', $query)) {
                        throw new LocalizedException(__('There are issues when processing your payment'));
                    } else {
                        if ($this->response->processSuccessPayment($order, $query['orderId'])) {
                            $redirect = 'checkout/onepage/success';
                        } else {
                            throw new LocalizedException(__('Afterpay Payment cannot be processes. Please contact administrator.'));
                        }
                    }
                    break;
            }
        } catch (LocalizedException $e) {
            $this->_helper->debug("Transaction Exception: " . $e->getMessage());
            $this->messageManager->addErrorMessage(
                $e->getMessage()
            );
        } catch (\Exception $e) {
            $this->_helper->debug("Transaction Exception: " . $e->getMessage());
            $this->messageManager->addErrorMessage(
                $e->getMessage()
            );
        }

        return $redirect;
    }
}
