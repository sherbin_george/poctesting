<?php

namespace Balance\Afterpay\Model\Adapter\Afterpay;

/**
 * Class Call
 * @package Carpetcourt\Afterpay\Model\Adapter\Afterpay
 */
class Call extends \Afterpay\Afterpay\Model\Adapter\Afterpay\Call
{
    /**
     * Send using HTTP call
     *
     * @param $url
     * @param bool $body
     * @param string $method
     * @param array $override
     * @return \Zend_Http_Response
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Http_Client_Exception
     */
    public function send($url, $body = false, $method = \Magento\Framework\HTTP\ZendClient::GET, $override = array() )
    {
        // set the client http
        $client = $this->httpClientFactory->create();

        /**
         * LHA0003-701
         * Start rewrite
         */
        // set body and the url
        $client->setUri($url);

        if ($body) {
            $client->setRawData($this->jsonHelper->jsonEncode($body), 'application/json');
        }
        /**
         * End rewrite
         */

        // add auth for API requirements
        $client->setAuth(
            trim($this->afterpayConfig->getMerchantId($override)),
            trim($this->afterpayConfig->getMerchantKey($override))
        );


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productMetadata = $objectManager->get('Magento\Framework\App\ProductMetadataInterface');
        $version = $productMetadata->getVersion(); //will return the magento version
        $description = $productMetadata->getName() . ' ' . $productMetadata->getEdition(); //will return the magento description


        if( !empty($override['website_id']) ) {
            $url = $this->getWebsiteUrl($override['website_id']);
        }
        else {
            $url = $this->getWebsiteUrl();
        }

        // set configurations
        $client->setConfig(
            [
                'maxredirects' => 0,
                'useragent'    => 'AfterpayMagento2Plugin ' . $this->helper->getModuleVersion() . ' (' . $description . ' ' . $version . ') MerchantID: ' . trim($this->afterpayConfig->getMerchantId($override) . ' URL: ' . $url )
            ]
        );

        // debug mode
        $requestLog = array(
            'type' => 'Request',
            'method' => $method,
            'url' => $url,
            'body' => $body
        );
        $this->helper->debug($this->jsonHelper->jsonEncode($requestLog));

        // do the request with catch
        try {
            $response = $client->request($method);

            // debug mode
            $responseLog = array(
                'type' => 'Response',
                'method' => $method,
                'url' => $url,
                'httpStatusCode' => $response->getStatus(),
                'body' => $this->jsonHelper->jsonDecode($response->getBody())
            );
            $this->helper->debug($this->jsonHelper->jsonEncode($responseLog));

            $responseBody = $response->getBody();
            $debugData['response'] = $responseBody;
        } catch (\Exception $e) {
            $this->helper->debug($e->getMessage());

            throw new \Magento\Framework\Exception\LocalizedException(
                __('Gateway error: %1', $e->getMessage())
            );
        }

        // return response
        return $response;
    }

    /**
     * CAC0011-95
     * Duplicate private method (unfortunately)
     *
     * @param null $website_id
     * @return null
     */
    private function getWebsiteUrl($website_id = NULL) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $url = NULL;

        if( !empty($website_id) ) {

            $websites = $storeManager->getWebsites();

            foreach($websites as $website){
                foreach($website->getStores() as $store){
                    if( !empty($website_id) && $website_id == $website->getId()) {
                        $storeObj = $storeManager->getStore($store);
                        $url = $storeObj->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                    }
                }
            }
        }
        else {
            $url = $storeManager->getStore()->getBaseUrl();
        }

        return $url;
    }
}