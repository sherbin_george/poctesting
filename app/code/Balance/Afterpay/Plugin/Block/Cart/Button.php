<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Afterpay\Plugin\Block\Cart;

use Afterpay\Afterpay\Block\Cart\Button as Subject;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Button
 *
 * @package Balance\Afterpay\Plugin\Block\Cart
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Button
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * Button constructor.
     *
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(PriceCurrencyInterface $priceCurrency)
    {
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * Override method getInstallmentsTotalHtml()
     * - The original method need to be terminated.
     *
     * @param Subject  $subject
     * @param \Closure $proceed
     *
     * @return string
     */
    public function aroundGetInstallmentsTotalHtml(Subject $subject, \Closure $proceed)
    {
        $currencySymbol = $this->priceCurrency->getCurrencySymbol(null, $this->priceCurrency->getCurrency());
        return $currencySymbol . number_format($subject->getInstallmentsTotal(), 2);
    }
}
