<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Afterpay\Plugin\Block\Catalog;

use Afterpay\Afterpay\Block\Catalog\Installments as Subject;
use Afterpay\Afterpay\Model\Config\Payovertime as AfterpayConfig;
use Magento\Framework\Registry as Registry;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Installments
 *
 * @package Balance\Afterpay\Plugin\Block\Catalog
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class Installments
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var AfterpayConfig
     */
    protected $afterpayConfig;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * Installments constructor.
     *
     * @param Registry               $registry
     * @param AfterpayConfig         $afterpayConfig
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        Registry $registry,
        AfterpayConfig $afterpayConfig,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->registry = $registry;
        $this->afterpayConfig = $afterpayConfig;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * Override getInstallsmentAmount to display correct prices
     *
     * @param Subject  $subject
     * @param \Closure $proceed
     *
     * @return string
     */
    public function aroundGetInstallmentsAmount(Subject $subject, \Closure $proceed)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->registry->registry('product');

        // set if final price is exist
        if ($price = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue()) {
            $currencySymbol = $this->priceCurrency->getCurrencySymbol(null, $this->priceCurrency->getCurrency());
            return $currencySymbol . number_format(($price / 4), 2);
        }
    }

    /**
     * Override canShow to get correct final prices
     *
     * @param Subject  $subject
     * @param \Closure $proceed
     *
     * @return bool|mixed
     */
    public function aroundCanShow(Subject $subject, \Closure $proceed)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->registry->registry('product');
        $finalPrice = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();

        // check if price is above max or min limit
        if ($finalPrice > $this->afterpayConfig->getMaxOrderLimit() // greater than max order limit
            || $finalPrice < $this->afterpayConfig->getMinOrderLimit()
        ) { // lower than min order limit
            return false;
        }

        return $proceed();
    }
}
