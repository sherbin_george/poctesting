<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Afterpay\Plugin\Model;

use Magento\Framework\UrlInterface;
use Afterpay\Afterpay\Model\Payovertime;
use Afterpay\Afterpay\Model\ConfigProvider as Subject;

/**
 * Class ConfigProvider
 *
 * @package Balance\Afterpay\Plugin\Model
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class ConfigProvider
{
    /**
     * Paybyfund js layout data
     */
    const PAYMENT_UPDATE_CHECKOUT_SUMMARY_URI = 'afterpay/ajax/updateCheckoutSummary';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get config set on JS global variable window.checkoutConfig
     *
     * @param Subject  $subject
     * @param \Closure $proceed
     *
     * @return array
     */
    public function aroundGetConfig(Subject $subject, \Closure $proceed)
    {
        // Get Afterpay default config array
        $config = $proceed();

        /**
         * Merging config array
         */
        $config = array_merge_recursive($config, [
            'payment' => [
                'extras' => [
                    'updateCheckoutSummaryUrl' => $this->getUpdateCheckoutSummaryUrl()
                ]
            ],
        ]);

        return $config;
    }

    /**
     * Build update checkout summary url
     *
     * @return string
     */
    protected function getUpdateCheckoutSummaryUrl()
    {
        return $this->urlBuilder->getUrl(
            self::PAYMENT_UPDATE_CHECKOUT_SUMMARY_URI,
            ['isAjax' => 'true', Payovertime::METHOD_CODE => 'true']
        );
    }
}
