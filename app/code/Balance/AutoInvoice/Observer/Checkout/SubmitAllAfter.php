<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AutoInvoice\Observer\Checkout;

use Psr\Log\LoggerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Balance\AutoInvoice\Services\CreateInvoice;

/**
 * Class SubmitAllAfter
 *
 * @package Balance\AutoInvoice\Observer\Checkout
 */
class SubmitAllAfter implements ObserverInterface
{
    /**
     * @var CreateInvoice
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SubmitAllAfter constructor.
     *
     * @param CreateInvoice $service
     * @param LoggerInterface $logger
     */
    public function __construct(
        CreateInvoice $service,
        LoggerInterface $logger
    )
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            $this->service->create($observer->getOrder());
        } catch (LocalizedException $e) {
            $this->logger->critical($e);
        }
    }
}