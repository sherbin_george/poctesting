<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AutoInvoice\Setup;

use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Balance\AutoInvoice\Helper\Data;

/**
 * Class InstallData
 *
 * @package Balance\AutoInvoice\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    private $resourceConfig;

    /**
     * InstallData constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param Config $resourceConfig
     */
    public function __construct(StoreManagerInterface $storeManager, Config $resourceConfig)
    {
        $this->storeManager = $storeManager;
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        foreach ([
                     Data::IS_ENABLED_XML_CONFIG_PATH           => 1,
                     Data::SEND_EMAIL_XML_CONFIG_PATH           => 1,
                     DATA::ALLOWED_PAYMENT_METHODS_CONFIG_PATH  => 'checkmo'
                 ] as $configPath => $value) {
            $this->resourceConfig->saveConfig(
                $configPath,
                $value,
                ScopeInterface::SCOPE_STORES,
                $this->storeManager->getStore('linen_au_b2b')->getId()
            );
        }

        $setup->endSetup();
    }
}