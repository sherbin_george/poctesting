<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AutoInvoice\Services;

use Psr\Log\LoggerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Service\InvoiceService;
use Balance\AutoInvoice\Helper\Data;

/**
 * Class CreateInvoice
 *
 * @package Balance\AutoInvoice\Services
 */
class CreateInvoice
{
    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;

    /**
     * CreateInvoice constructor.
     *
     * @param InvoiceService $invoiceService
     * @param ObjectManagerInterface $objectManager
     * @param LoggerInterface $logger
     * @param Data $helper
     * @param InvoiceSender $invoiceSender
     */
    public function __construct(
        InvoiceService $invoiceService,
        ObjectManagerInterface $objectManager,
        LoggerInterface $logger,
        Data $helper,
        InvoiceSender $invoiceSender
    )
    {
        $this->invoiceService = $invoiceService;
        $this->objectManager = $objectManager;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->invoiceSender = $invoiceSender;
    }

    /**
     * @param OrderInterface $order
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function create(OrderInterface $order)
    {
        $storeId = $order->getStoreId();

        if (!$this->helper->isEnabled($storeId)
            || !$this->paymentMethodAllowed($order)
        ) {
            return;
        }

        if (!$order->canInvoice()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The order does not allow an invoice to be created.')
            );
        }

        $invoice = $this->invoiceService->prepareInvoice($order);

        if (!$invoice) {
            throw new LocalizedException(__('We can\'t save the invoice right now.'));
        }

        $invoice->register();

        $invoice->getOrder()->setCustomerNoteNotify(true);
        $invoice->getOrder()->setIsInProcess(true);

        $transactionSave = $this->objectManager->create(
            'Magento\Framework\DB\Transaction'
        )->addObject(
            $invoice
        )->addObject(
            $invoice->getOrder()
        );

        $transactionSave->save();

        if ($this->helper->sendEmail($storeId)) {
            try {
                $this->invoiceSender->send($invoice);
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }

    /**
     * @param OrderInterface $order
     *
     * @return bool
     */
    protected function paymentMethodAllowed(OrderInterface $order)
    {
        $isAllowed = false;
        $payment = $order->getPayment();

        if ($payment) {
            $allowedPaymentMethods = $this->helper->getAllowedPaymentMethods($order->getStoreId());
            $isAllowed = in_array($payment->getMethod(), $allowedPaymentMethods);
        }

        return $isAllowed;
    }
}