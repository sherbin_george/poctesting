<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\AutoInvoice\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 *
 * @package Balance\AutoInvoice\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @const string
     */
    const IS_ENABLED_XML_CONFIG_PATH = 'checkout/autoinvoice/is_enabled';

    /**
     * @const string
     */
    const SEND_EMAIL_XML_CONFIG_PATH = 'checkout/autoinvoice/send_email';

    /**
     * @const string
     */
    const ALLOWED_PAYMENT_METHODS_CONFIG_PATH = 'checkout/autoinvoice/payment_methods';

    /**
     * Check is Autoinvoice feature enabled.
     *
     * @param null $scopeCode
     *
     * @return bool
     */
    public function isEnabled($scopeCode = null)
    {
        return $this->scopeConfig->isSetFlag(
            static::IS_ENABLED_XML_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * Get send email config flag
     *
     * @param null $scopeCode
     *
     * @return bool
     */
    public function sendEmail($scopeCode = null)
    {
        return $this->scopeConfig->isSetFlag(
            static::SEND_EMAIL_XML_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param null $scopeCode
     *
     * @return array
     */
    public function getAllowedPaymentMethods($scopeCode = null)
    {
        $result = $this->scopeConfig->getValue(
            static::ALLOWED_PAYMENT_METHODS_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );

        return (!$result) ? [] : explode(',', $result);
    }
}