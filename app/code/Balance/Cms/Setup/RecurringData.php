<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Cms\Setup;

use Magento\Framework\Setup;
use Balance\Cms\Helper\Fixture as FixtureHelper;

/**
 * Class Recurring
 * - Recurring will be executed whenever "bin/magento setup:upgrade" run.
 * - This class will replace upgrade data from now
 *
 * @package Balance\Cms\Setup
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
class RecurringData implements Setup\InstallDataInterface
{
    /**
     * @var Setup\SampleData\Executor
     */
    protected $executor;

    /**
     * @var Installer
     */
    protected $installer;

    /**
     * @var \Balance\Cms\Helper\Fixture
     */
    protected $helper;

    /**
     * Recurring constructor.
     *
     * @param Setup\SampleData\Executor $executor
     * @param Installer                 $installer
     * @param FixtureHelper             $helper
     */
    public function __construct(Setup\SampleData\Executor $executor, Installer $installer, FixtureHelper $helper)
    {
        $this->executor = $executor;
        $this->installer = $installer;
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function install(Setup\ModuleDataSetupInterface $setup, Setup\ModuleContextInterface $moduleContext)
    {
        $setup->startSetup();

        if ($this->helper->isDebugModeActive()) {
            // @codingStandardsIgnoreStart
            $this->getFixtureInfo();
            echo "\n\n!__/ Before exec(\$this->installer) invoked \\__!";
            // @codingStandardsIgnoreEnd
        }

        $this->executor->exec($this->installer);

        if ($this->helper->isDebugModeActive()) {
            // @codingStandardsIgnoreStart
            echo "\n\n!__/ After exec(\$this->installer) invoked \\__!";
            // @codingStandardsIgnoreEnd
        }

        if ($this->helper->isDebugModeActive()) {
            throw new \Exception("!__/ Before endSetup() invoked \\__!");
        }

        $setup->endSetup();
    }

    /**
     * Retrieve fixture info
     *
     * @return void
     */
    protected function getFixtureInfo()
    {
        // @codingStandardsIgnoreStart
        echo sprintf("\nClass name:   %s", get_class($this->helper));
        echo sprintf("\nModule name:  %s", $this->helper->getModuleName());
        echo sprintf("\nDEBUG_MODE:   %s", $this->helper->isDebugModeActive());
        echo sprintf("\nFIXTURE_MODE: %s", $this->helper->isFixtureModeActive());
        // @codingStandardsIgnoreEnd
    }
}