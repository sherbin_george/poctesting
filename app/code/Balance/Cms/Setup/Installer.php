<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balance\Cms\Setup;

use Balance\SampleData\Setup\AbstractInstaller;
use Magento\Framework\Setup;
use Balance\Cms\Helper\Fixture as FixtureHelper;

/**
 * Class Installer
 *
 * @package Balance\Cms\Setup
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
class Installer extends AbstractInstaller implements Setup\SampleData\InstallerInterface
{
    /**
     * @var \Balance\Cms\Helper\Fixture
     */
    protected $helper;

    /**
     * CmsInstaller constructor.
     *
     * @param FixtureHelper $helper
     */
    public function __construct(FixtureHelper $helper)
    {
        parent::__construct($helper);
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function install()
    {
        $this->process(
            [
                self::ENTITIES => [
                    'blocks' => [
                        self::ENTITY_NAME => 'blocks',
                        self::ENTITY_CLASSNAME => \Balance\Cms\Model\SampleData\Block::class
                    ],
                    'pages' => [
                        self::ENTITY_NAME => 'pages',
                        self::ENTITY_CLASSNAME => \Balance\Cms\Model\SampleData\Page::class
                    ]
                ]
            ]
        );
    }
}