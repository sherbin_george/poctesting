<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Cms\Helper;

/**
 * Fixture helper
 *
 * @package Balance\Cms\Helper
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
class Fixture extends \Balance\Core\Helper\Fixture
{
    /**#@+
     * Constants defined for keys of data array
     */
    const MODULE_NAME = 'Balance_Cms';
    const FIXTURES_MODE = false;
    const DEBUG_MODE = false;
}