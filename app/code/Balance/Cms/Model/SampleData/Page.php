<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Cms\Model\SampleData;

use Balance\Cms\Helper\Fixture as FixtureHelper;
use Balance\SampleData\Api\Data\SampleDataInterface;
use Balance\SampleData\Model\AbstractSampleData;
use Balance\SampleData\Model\Assets\AssetsManagement;
use Balance\SampleData\Model\Fixture\FixtureManagement;
use Magento\Framework\Setup\SampleData\Context as SampleDataContext;

/**
 * Class Page
 *
 * @package Balance\Cms\Model\SampleData
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
class Page extends AbstractSampleData
{
    /**
     * @var \Balance\Cms\Helper\Fixture
     */
    protected $helper;

    /**
     * Page constructor.
     *
     * @param SampleDataContext $sampleDataContext
     * @param AssetsManagement  $assetsManagement
     * @param FixtureManagement $fixtureManagement
     * @param FixtureHelper     $fixtureHelper
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        AssetsManagement $assetsManagement,
        FixtureManagement $fixtureManagement,
        FixtureHelper $fixtureHelper
    ) {
        parent::__construct($sampleDataContext, $assetsManagement, $fixtureManagement, $fixtureHelper);
        $this->helper = $fixtureHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function install($fixtures)
    {
        $this->process(
            $fixtures,
            [
                SampleDataInterface::PROCESS_TYPE => SampleDataInterface::PROCESS_CONTENT,
                SampleDataInterface::ENTITY => 'pages',
                SampleDataInterface::ENTITY_CLASS => \Magento\Cms\Model\Page::class
            ]
        );
    }
}
