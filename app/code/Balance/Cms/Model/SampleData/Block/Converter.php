<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\Cms\Model\SampleData\Block;

use Balance\SampleData\Model\Converter\AbstractConverter;

/**
 * Class Converter
 *
 * @package Balance\Cms\Model\SampleData\Block
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
class Converter extends AbstractConverter
{
    const CONVERT_KEY = 'content';
    const ENTITY = 'blocks';
}