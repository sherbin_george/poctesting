<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ForceLogin
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ForceLogin\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Enable module
     * @return bool
     */
    public function isEnable()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/general/enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for product page
     * @return bool
     */
    public function isEnableProductPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/product_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for category page
     * @return bool
     */
    public function isEnableCategoryPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/category_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for cart page
     * @return bool
     */
    public function isEnableCartPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/cart_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for checkout page
     * @return bool
     */
    public function isEnableCheckoutPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/checkout_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for contact page
     * @return bool
     */
    public function isEnableContactPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/contact_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for search term page
     * @return bool
     */
    public function isEnableSearchTermPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/search_term_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for search result page
     * @return bool
     */
    public function isEnableSearchResultPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/search_resulls_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable force login for advanced search page
     * @return bool
     */
    public function isEnableAdvancedSearchPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/advanced_search_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableOtherPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/other_page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Enable customer register
     * @return bool
     */
    public function isEnableRegister()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/general/disable_register',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get alert message after redirect login page
     * @return string
     */
    public function getAlertMessage()
    {
        $alertMessage = $this->scopeConfig->getValue(
            'forcelogin/page/message',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $alertMessage;
    }

    /**
     * Get redirect url after login
     * @return string
     */
    public function getRedirectUrl()
    {
        $pageRedirect = $this->scopeConfig->getValue(
            'forcelogin/redirect_url/page',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $pageRedirect;
    }

    /**
     * Get customer url after login
     * @return string
     */
    public function getCustomUrl()
    {
        $pageRedirect = $this->scopeConfig->getValue(
            'forcelogin/redirect_url/custom_url',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $pageRedirect;
    }

    /**
     * Enable force login for cms page
     * @return bool
     */
    public function isEnableCmsPage()
    {
        return $this->scopeConfig->isSetFlag(
            'forcelogin/page/enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get cms Page id
     * @return string
     */
    public function getCmsPageId()
    {
        $cmsPageId = $this->scopeConfig->getValue(
            'forcelogin/page/page_id',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $cmsPageId;
    }

    /**
     * Get Redirect config default
     * @return bool
     */
    public function getRedirectDashBoard()
    {
        $redirectToDashBoard = $this->scopeConfig->isSetFlag(
            'customer/startup/redirect_dashboard',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $redirectToDashBoard;
    }
}
