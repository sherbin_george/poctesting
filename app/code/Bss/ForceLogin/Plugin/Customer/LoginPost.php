<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ForceLogin
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ForceLogin\Plugin\Customer;

use Bss\ForceLogin\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Catalog\Model\Session as CatalogSession;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\State\UserLockedException ;
use Magento\Framework\Exception\LocalizedException;

class LoginPost
{
    /**
     * @var Context
     */
    protected $context;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var CustomerUrl
     */
    protected $customerHelperData;
    /**
     * @var PhpCookieManager
     */
    protected $cookieMetadataManager;
    /**
     * @var CookieMetadataFactory
     */
    protected $cookieMetadataFactory;
    /**
     * @var ScopeConfig
     */
    protected $scopeConfig;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    /**
     * @var Data
     */
    protected $helperData;
    /**
     * @var CatalogSession
     */
    protected $catalogSession;
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * LoginPost constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerUrl $customerHelperData
     * @param Validator $formKeyValidator
     * @param AccountRedirect $accountRedirect
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param PhpCookieManager $cookieMetadataManager
     * @param Data $helperData
     * @param CatalogSession $catalogSession
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        CustomerUrl $customerHelperData,
        Validator $formKeyValidator,
        AccountRedirect $accountRedirect,
        CookieMetadataFactory $cookieMetadataFactory,
        PhpCookieManager $cookieMetadataManager,
        Data $helperData,
        CatalogSession $catalogSession
    ) {
        $this->customerAccountManagement = $customerAccountManagement;
        $this->session = $customerSession;
        $this->customerUrl = $customerHelperData;
        $this->formKeyValidator = $formKeyValidator;
        $this->accountRedirect = $accountRedirect;
        $this->url = $context->getUrl();
        $this->messageManager = $context->getMessageManager();
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieMetadataManager = $cookieMetadataManager;
        $this->helperData = $helperData;
        $this->catalogSession = $catalogSession;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
    }

    /**
     * Get scope config
     *
     * @return string scopeConfig||\Store\Model\ScopeInterface
     * @deprecated
     */
    private function getScopeConfig()
    {
        if (!($this->scopeConfig instanceof \Magento\Framework\App\Config\ScopeConfigInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\App\Config\ScopeConfigInterface::class
            );
        } else {
            return $this->scopeConfig;
        }
    }

    /**
     * Retrieve cookie manager
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

    /**
     * Bss Login Redirect
     * @param \Magento\Customer\Controller\Account\LoginPost $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function aroundExecute(\Magento\Customer\Controller\Account\LoginPost $subject, \Closure $proceed)
    {
        $enable = $this->helperData->isEnable();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($enable) {
            if ($this->session->isLoggedIn() || !$this->formKeyValidator->validate($subject->getRequest())) {
                $resultRedirect->setPath('*/*/');
                return $resultRedirect;
            }

            if ($subject->getRequest()->isPost()) {
                $login = $subject->getRequest()->getPost('login');
                if (!empty($login['username']) && !empty($login['password'])) {
                    try {
                        $customer = $this->customerAccountManagement
                            ->authenticate($login['username'], $login['password']);
                        $this->session->setCustomerDataAsLoggedIn($customer);
                        $this->session->regenerateId();
                        $this->bssCookieManager();
                        $configRedirectUrl = $this->getLoginRedirectUrl();
                        return $resultRedirect->setPath($configRedirectUrl);
                    } catch (EmailNotConfirmedException $e) {
                        $value = $this->customerUrl->getEmailConfirmationUrl($login['username']);
                        $message = __(
                            'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                            $value
                        );
                        $this->messageManager->addError($message);
                        $this->session->setUsername($login['username']);
                    } catch (UserLockedException $e) {
                        $message = __(
                            'The account is locked. Please wait and try again or contact %1.',
                            $this->getScopeConfig()->getValue('contact/email/recipient_email')
                        );
                        $this->messageManager->addError($message);
                        $this->session->setUsername($login['username']);
                    } catch (AuthenticationException $e) {
                        $message = __('Invalid login or password.');
                        $this->messageManager->addError($message);
                        $this->session->setUsername($login['username']);
                    } catch (LocalizedException $e) {
                        $message = $e->getMessage();
                        $this->messageManager->addError($message);
                        $this->session->setUsername($login['username']);
                    } catch (\Exception $e) {
                        // PA DSS violation: throwing or logging an exception here can disclose customer password
                        $this->messageManager->addError(
                            __('An unspecified error occurred. Please contact us for assistance.')
                        );
                    }
                } else {
                    $this->messageManager->addError(__('A login and a password are required.'));
                }
            }
            $this->catalogSession->unsBssCurrentUrl();
            $this->catalogSession->unsBssPreviousUrl();
            return $this->accountRedirect->getRedirect();
        } else {
            return $proceed();
        }
    }

    /**
     * Bss Get Redirect
     * @return string
     */
    public function getLoginRedirectUrl()
    {
        $redirectToDashBoard = $this->helperData->getRedirectDashBoard();
        $currentUrl = $this->catalogSession->getBssCurrentUrl();
        $previousUrl = $this->catalogSession->getBssPreviousUrl();
        $configRedirectUrl = $this->helperData->getRedirectUrl();
        if ($configRedirectUrl == "home") {
            $configRedirectUrl = "";
        } elseif ($configRedirectUrl == "previous") {
            if ($currentUrl) {
                $configRedirectUrl = $currentUrl;
            } else {
                $configRedirectUrl = $previousUrl;
            }
        } elseif ($configRedirectUrl == "customurl") {
            $configRedirectUrl = $this->helperData->getCustomUrl();
        } elseif ($configRedirectUrl == "customer/account/index") {
            if ($redirectToDashBoard) {
                $configRedirectUrl = $configRedirectUrl;
            } elseif ($currentUrl) {
                $configRedirectUrl = $currentUrl;
            } else {
                $configRedirectUrl = $previousUrl;
            }
        }
        return $configRedirectUrl;
    }

    /**
     *  Get DefaultCooie
     * @return void
     */
    public function bssCookieManager()
    {
        if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
            $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
            $metadata->setPath('/');
            $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
        }
    }
}
