<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ConfigurableMatrixView
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ConfigurableMatrixView\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $productFactory;

    protected $scopeConfig;

    protected $_storeManager;

    protected $dataObjectHelper;

    protected $customer;

    protected $productMetadata;

    /**
     * Data constructor.
     * @param \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param \Magento\Customer\Model\Session $customer
     */
    public function __construct(
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customer,
        \Magento\Framework\Registry $registry
        ) {
        $this->productFactory = $productFactory;
        $this->productMetadata = $productMetadata;
        $this->scopeConfig = $scopeConfig;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->_storeManager = $storeManager;
        $this->customer = $customer;
        $this->registry = $registry;
    }

    /**
     * @param $path
     * @return bool
     */
    public function getConfigFlag($path)
    {
        return $this->scopeConfig->isSetFlag($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $path
     * @return null|string|bool|int
     */
    public function getConfigValue($path)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Check is allowed Customer and module Enable
     * @return bool
     */
    public function isEnabled()
    {
        $active =  $this->getConfigFlag('configurablematrixview/general/active');
        $customer_group =  $this->getConfigValue('configurablematrixview/general/customer_group');
        $customer_groups = explode(',', $customer_group);
        if ($active && $customer_group != ''
            && (in_array(32000, $customer_groups)
            || in_array($this->customer->getCustomerGroupId(), $customer_groups))) {
                return true;
        }
        return false;
    }

    /**
     * Check sort attribute by default or A-Z
     * @return bool
     */
    public function getSortOption()
    {
        return $this->getConfigFlag('configurablematrixview/general/sort_option');
    }

    /**
     * Check show unit price of product child in table matrix view
     * @return bool
     */
    public function canShowUnitPrice()
    {
        return $this->getConfigFlag('configurablematrixview/general/unit_price');
    }

    /**
     * Check show tier price of product child in table matrix view
     * @return bool
     */
    public function canShowTierPrice()
    {
        return $this->getConfigFlag('configurablematrixview/general/tier_price');
    }

    /**
     * Check show price range
     * @return bool
     */
    public function canShowPriceRange()
    {
        return $this->getConfigFlag('configurablematrixview/general/price_range');
    }

    /**
     * Check show total price
     * @return bool
     */
    public function canShowTotal()
    {
        return $this->getConfigFlag('configurablematrixview/general/show_total');
    }

    /**
     * Check show stock of product child in table matrix view
     * @return bool
     */
    public function canShowStock()
    {
        return $this->getConfigFlag('configurablematrixview/general/show_stock');
    }

    /**
     * Check show button increase of box qty 
     * @return bool
     */
    public function canShowButtonQty()
    {
        return $this->getConfigFlag('configurablematrixview/general/qty_increase');
    }

    /**
     * @return bool
     */
    public function calculateTierPrice()
    {
        return $this->getConfigFlag('configurablematrixview/general/tier_price_calculate');
    }

    /**
     * @param $productid
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct($productid)
    {
        $_product = $this->productFactory->create()->load($productid);
        return $_product;
    }

    /**
     * @param $number_childproduct
     * @param $number_tiers
     * @param $tierPrices
     * @return int
     */
    public function checkSameTierPrice($number_childproduct, $number_tiers, $tierPrices){
        if ($number_childproduct == $number_tiers) {
            $this->sortArrayTierPrice($tierPrices[0], 'qty');
            foreach ($tierPrices as $tierPrice) {
                $this->sortArrayTierPrice($tierPrice, 'qty');
                if ($tierPrice != $tierPrices[0]) {
                    return false;
                    break;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $tierPrice
     * @param $col
     * @param int $dir
     */
    public function sortArrayTierPrice(&$tierPrice, $col, $dir = SORT_ASC) {
        $sort_tier = [];
        foreach ($tierPrice as $key=> $row) {
            $function[$key] = $row[$col];
        }
        array_multisort($function, $dir, $tierPrice);
    }

    /**
     * Check version magento > 2.1.6
     * @return bool
     */
    public function getMagentoVersion()
    {
        $version = $this->productMetadata->getVersion();
        if (version_compare($version, '2.1.6') >= 0) {
            return true;
        }
        return false;
    }

    /**
     * Check version magento > 2.2.0
     * @return bool
     */
    public function getMagentoVersion22()
    {
        $version = $this->productMetadata->getVersion();
        if (version_compare($version, '2.2.0') >= 0) {
            return true;
        }
        return false;
    }
    
    public function getAddtocartButtonTemplate($name)
    {
        $product = $this->registry->registry('product');
        if ($product && $product->getConfigurableMatrixView() && $this->isEnabled()) {
            return $name;
        }
        return 'Magento_Catalog::product/view/addtocart.phtml';
    }

    public function getFormTemplate($name)
    {
        $product = $this->registry->registry('product');
        if ($product && $product->getConfigurableMatrixView() && $this->isEnabled()) {
            return $name;
        }
        return 'Magento_Catalog::product/view/form.phtml';
    }
}
