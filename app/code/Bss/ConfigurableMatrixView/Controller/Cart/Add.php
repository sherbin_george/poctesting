<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ConfigurableMatrixView
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ConfigurableMatrixView\Controller\Cart;

class Add extends \Magento\Checkout\Controller\Cart\Add
{
    /**
     * Add product to shopping cart action
     *
     */
    public function execute()
    {
    
        // if (!$this->_formKeyValidator->validate($this->getRequest())) {
        //     return $this->resultRedirectFactory->create()->setPath('*/*/');
        // }
        
        $params = $this->getRequest()->getParams();
        $addedProducts = $product_fail = $product_success = [];

        $productId = (int)$this->getRequest()->getPost('product');
        $productIds = $this->getRequest()->getPost('super_attribute_'.$productId);

        foreach($productIds as $k => $super_attribute) {
            try {
                $qty = $this->getRequest()->getPost('qty_'.$productId.'_'.$k, 0);
                $product = $this->getProductMTV($productId);

                if ($qty <= 0 || !$product) {
                    continue;
                }

                $paramsr = [];
                $paramsr['product'] = $productId;
                $paramsr['qty']= $qty;
                $paramsr["super_attribute"] = [];
                $paramsr["super_attribute"] = $super_attribute;
                if ($this->getRequest()->getPost('super_attribute')) {
                    $paramsr["super_attribute"] = $paramsr["super_attribute"] + $this->getRequest()->getPost('super_attribute');
                }
                $paramsr['options'] = isset($params['options'])? $params['options'] : [];

                $childProductId = $this->getRequest()->getPost('child_product_'.$productId.'_'.$k);
                $childProduct = $this->getProductMTV($childProductId);
                
                $this->cart->addProduct($product, $paramsr);
                
                if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                    if (!$this->cart->getQuote()->getHasError()) {
                        $addedProducts[] = $childProduct;
                    }
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $product_fail[$childProduct->getId()] = $e->getMessage();
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    $product_fail[$childProduct->getId()] = end($messages);
                }
                $cartItem = $this->cart->getQuote()->getItemByProduct($product);
                if ($cartItem) {
                    $this->cart->getQuote()->deleteItem($cartItem);
                }
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
                \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->critical($e);
            }
        }
        
        $this->saveAdd($addedProducts);

        $product_poup['errors'] = $product_fail;

        $this->getResponse()->setBody(json_encode($product_poup));
        return;
    }

    /**
     * save product to cart
     * @param $addedProducts
     */
    protected function saveAdd($addedProducts)
    {
        $related = $this->getRequest()->getParam('related_product');

        if ($addedProducts) {
            try {

                if (!empty($related)) {
                    $this->cart->addProductsByIds(explode(',', $related));
                }

                $this->cart->save()->getQuote()->collectTotals();
                if (!$this->cart->getQuote()->getHasError()) {
                    $products = [];
                    foreach ($addedProducts as $product) {
                        $products[] = '"' . $product->getName() . '"';
                    }

                    $this->messageManager->addSuccess(
                        __('%1 product(s) have been added to shopping cart: %2.', count($addedProducts), join(', ', $products))
                    );
                }
                
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $this->messageManager->addNotice(
                        \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                    );
                } else {
                    $errormessage = array_unique(explode("\n", $e->getMessage()));
                    $errormessageCart = end($errormessage);
                    $this->messageManager->addError(
                        \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Escaper')->escapeHtml($errormessageCart)
                    );
                }
                return;
            }
        } else {
             $this->messageManager->addError(__('We can\'t add this item to your shopping cart right now.'));
        }
    }

    /**
     * Get product child of product configurable
     * @param $super_attribute
     * @param $product
     * @return \Magento\Catalog\Model\Product|false
     */
    protected function getProductMTV($productId)
    {
        $storeId = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getId();
        $product = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Catalog\Model\Product')->setStoreId($storeId)->load($productId);
        
        return $product;
    }
}