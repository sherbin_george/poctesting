<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ConfigurableMatrixView
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ConfigurableMatrixView\Block\Product\View;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Product as CatalogProduct;
use Magento\ConfigurableProduct\Helper\Data;
use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Store\Model\ScopeInterface;
use Magento\Swatches\Helper\Data as SwatchData;
use Magento\Swatches\Helper\Media;
use Magento\Swatches\Model\Swatch;

class Configurable extends \Magento\Swatches\Block\Product\Renderer\Configurable
{
    protected $productTypeInstance;

    protected $eavConfig;

    protected $taxConfig;

    protected $taxCalculation;

    protected $localeFormat;

    protected $stockRegistryInterface;

    protected $helperMaTrixV;

    protected $sametierPrice = false;

    protected $from_to_price = [];

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        // $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->productTypeInstance = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable');
        $this->eavConfig = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Eav\Model\Config');
        $this->taxConfig = \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Tax\Model\Config');
        $this->taxCalculation = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Tax\Model\Calculation');
        $this->localeFormat = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Framework\Locale\Format');
        $this->stockRegistryInterface = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
        $this->helperMaTrixV = \Magento\Framework\App\ObjectManager::getInstance()->create('Bss\ConfigurableMatrixView\Helper\Data');
    }

    /**
     * @return string
     */
    protected function getRendererTemplate()
    {
        if ($this->isEnabledMatrixV()
            && $this->getProduct()->getConfigurableMatrixView()
            && $this->getCurrentUrl() != 'checkout_cart_configure'
        ) {
            return 'Bss_ConfigurableMatrixView::product/view/configurable.phtml';
        }

        return $this->isProductHasSwatchAttributeMatrix() ?
            self::SWATCH_RENDERER_TEMPLATE : self::CONFIGURABLE_RENDERER_TEMPLATE;
    }

    /**
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->getRequest()->getModuleName().'_'.$this->getRequest()->getControllerName().'_'.$this->getRequest()->getActionName();
    }



    /**
     * @return string
     */
    public function isProductHasSwatchAttributeMatrix()
    {
        if ($this->helperMaTrixV->getMagentoVersion()) {
            return $this->isProductHasSwatchAttribute();
        } else {
            return $this->isProductHasSwatchAttribute;
        }
    }

    /**
     * @return mixed
     */
    public function getAllowProducts()
    {
        if (!$this->hasAllowProducts()) {
            $products = [];
            $skipSaleableCheck = $this->catalogProduct->getSkipSaleableCheck();
            $allProducts = $this->getProduct()->getTypeInstance()->getUsedProducts($this->getProduct(), null);
            foreach ($allProducts as $product) {
                if ($product->isSaleable() || $skipSaleableCheck) {
                        $products[] = $product;
                }
            }

            $this->setAllowProducts($products);
        }

        return $this->getData('allow_products');
    }

    /**
     * @return array
     */
    public function getAttributeMatrix()
    {
        $product = $this->getProduct();
        $productAttributeOptions = $this->productTypeInstance->getConfigurableAttributesAsArray($product);
        $attribute_code = [];

        foreach ($productAttributeOptions as $option) {
            if (in_array($option['attribute_code'], $attribute_code)) {
                continue;
            }

            $attribute = $this->eavConfig->getAttribute('catalog_product', $option['attribute_code']);

            if (!$attribute->getIsMatrixView()) {
                continue;
            }

            $attribute_code[$option['attribute_id']] = $option['attribute_code'];
        }

        return array_slice($attribute_code, -2, 2, true);
    }

    /**
     * @return array
     */
    public function getAttributeMatrixArray()
    {
        $product = $this->getProduct();
        $productAttributeOptions = $this->productTypeInstance->getConfigurableAttributesAsArray($product);
        $attributes_matrix = [];
        $attribute_matrix = $this->getAttributeMatrix();
        foreach ($attribute_matrix as $k => $v) {
            $attributes_matrix[] = $productAttributeOptions[$k];
        }

        return $attributes_matrix;
    }

    /**
     * @return array
     */
    public function getChildProductAttribute()
    {
        $currentProduct = $this->getProduct();
        $optionPrice = $this->getOptionPrices();
        $options = $this->helper->getOptions($currentProduct, $this->getAllowProducts());
        $attributesData = $this->configurableAttributeData->getAttributesData($currentProduct, $options);
        $attribute_allow = $this->getAttributeMatrix();
        $assc_product_data = [];

        foreach ($this->getAllowProducts() as $product) {
            $data = $options['index'][$product->getId()];
            $data['product_id'] = $product->getId();
            $assc_product_data[] = $data;
        }

        return $assc_product_data;
    }

    /**
     * @return string
     */
    public function getConfigurableMatrixViewData()
    {
        $currentProduct = $this->getProduct();
        $optionPrice = $this->getOptionPrices();
        $options = $this->helper->getOptions($currentProduct, $this->getAllowProducts());
        $attributesData = $this->configurableAttributeData->getAttributesData($currentProduct, $options);
        $attribute_allow = $this->getAttributeMatrix();
        $assc_product_data = $data = $product_allow = $qty = $is_in_stock = $final_price = $tier_price = $_tierPrices = $minmaxids= [];
        $sort = false;
        $min_price = $max_price = null;
        $j = 0;
        foreach ($this->getAllowProducts() as $product) {
            
            $tierPrices = [];
            $priceInfo = $product->getPriceInfo();
            $tierPriceModel =  $priceInfo->getPrice('tier_price');

            $old_price = $priceInfo->getPrice('regular_price')->getAmount()->getValue();
            $basePrice = $priceInfo->getPrice('final_price')->getAmount()->getBaseAmount();
            $finalPrice = $priceInfo->getPrice('final_price')->getAmount()->getValue();

            $tierPricesList = $tierPriceModel->getTierPriceList();
            foreach ($tierPricesList as $tierPrice) {
                $tierPrices[] = [
                    'qty' => $this->localeFormat->getNumber($tierPrice['price_qty']),
                    'price' => $this->localeFormat->getNumber($tierPrice['price']->getValue()),
                    'percentage' => $this->localeFormat->getNumber(
                        $tierPriceModel->getSavePercent($tierPrice['price'])
                    ),
                ];
            }

            $productPrice = $product->getPrice();
            $minmaxids[$product->getId()] = $product->getPrice();
            $min_price = $min_price ? min($min_price, $productPrice) : $productPrice;
            $max_price = $max_price ? max($max_price, $productPrice) : $productPrice;

            if ($product->isSaleable()) {
               $product_allow[] = $product->getId();
               $j ++;
            }
            if ($this->canShowStock()) {
                $stockitem = $this->stockRegistryInterface->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
                $qty[$product->getId()] = (int)$stockitem->getData('qty');
                $is_in_stock[$product->getId()] = $stockitem->getData('is_in_stock');
            }
            if ($this->canShowUnitPrice()) {
                $final_price[$product->getId()] = $this->getPriceHtml($product, 'final_price');
            }
            if ($this->canShowTierPrice()) {
               $tier_price[$product->getId()] = $this->getPriceHtml($product, 'tier_price');
            }
            if (!empty($tierPrices)) {
               $_tierPrices[$product->getId()] = $tierPrices;
            }
        }

        $this->sametierPrice = $this->helperMaTrixV->checkSameTierPrice($j,count($_tierPrices), array_values($_tierPrices));

        if ($this->canShowUnitPrice() && $this->sametierPrice) {
            $tier_price[$currentProduct->getId()] =  array_values($tier_price)[0];
            $tier_price  = [$currentProduct->getId() => $tier_price[$currentProduct->getId()]];
        }
        
        if ($min_price && $max_price &&  $min_price < $max_price) {
            array_unique($minmaxids);
           $this->from_to_price = [array_search($min_price, $minmaxids),array_search($max_price, $minmaxids)];
        }
        $data['product_allow'] = $product_allow;
        $data['qty'] = $qty;
        $data['is_in_stock'] = $is_in_stock;
        $data['final_price'] = $final_price;
        $data['tier_price'] = $tier_price;
        $data['tierPrices'] = $_tierPrices;
        return $this->jsonEncoder->encode($data);
    }

    /**
     * @return string
     */
    public function getJsonConfigNoMatrix()
    {
        $store = $this->getCurrentStore();
        $currentProduct = $this->getProduct();

        $regularPrice = $currentProduct->getPriceInfo()->getPrice('regular_price');
        $finalPrice = $currentProduct->getPriceInfo()->getPrice('final_price');

        $options = $this->helper->getOptions($currentProduct, $this->getAllowProducts());
        $attributesData = $this->configurableAttributeData->getAttributesData($currentProduct, $options);
        $attribute_matrix = $this->getAttributeMatrix();

        foreach ($attribute_matrix as $key => $value) {
            unset($attributesData['attributes'][$key]);
        }

        if ( !isset($attributesData['attributes']) || empty($attributesData['attributes'])) {
            return false;
        }

        $config = [
            'attributes' => $attributesData['attributes'],
            'template' => str_replace('%s', '<%- data.price %>', $store->getCurrentCurrency()->getOutputFormat()),
            'currencyFormat' => $store->getCurrentCurrency()->getOutputFormat(),
            'optionPrices' => $this->getOptionPrices(),
            'priceFormat' => $this->localeFormat->getPriceFormat(),
            'prices' => [
                'oldPrice' => [
                    'amount' => $this->localeFormat->getNumber($regularPrice->getAmount()->getValue()),
                ],
                'basePrice' => [
                    'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getBaseAmount()),
                ],
                'finalPrice' => [
                    'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getValue()),
                ],
            ],
            'productId' => $currentProduct->getId(),
            'chooseText' => __('Choose an Option...'),
            'index' => isset($options['index']) ? $options['index'] : [],
        ];
        
        if ($this->helperMaTrixV->getMagentoVersion22()) {
            $config['images'] = $this->getOptionImages();
        } else {
            $config['images'] = isset($options['images']) ? $options['images'] : [];
        }

        if ($currentProduct->hasPreconfiguredValues() && !empty($attributesData['defaultValues'])) {
            $config['defaultValues'] = $attributesData['defaultValues'];
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return $this->jsonEncoder->encode($config);
    }

    /**
     * @return string
     */
    public function getJsonConfigMHide()
    {
        $store = $this->getCurrentStore();
        $currentProduct = $this->getProduct();

        $regularPrice = $currentProduct->getPriceInfo()->getPrice('regular_price');
        $finalPrice = $currentProduct->getPriceInfo()->getPrice('final_price');

        $options = $this->helper->getOptions($currentProduct, $this->getAllowProducts());
        $attributesData = $this->configurableAttributeData->getAttributesData($currentProduct, $options);
        
        $attribute_matrix = $this->getAttributeMatrix();
        foreach ($attributesData['attributes'] as $key => $value) {
            if (!isset($attribute_matrix[$key])) {
               unset($attributesData['attributes'][$key]);
            }
        }
        
        $config = [
            'attributes' => $attributesData['attributes'],
            'template' => str_replace('%s', '<%- data.price %>', $store->getCurrentCurrency()->getOutputFormat()),
            'currencyFormat' => $store->getCurrentCurrency()->getOutputFormat(),
            'optionPrices' => $this->getOptionPrices(),
            'priceFormat' => $this->localeFormat->getPriceFormat(),
            'prices' => [
                'oldPrice' => [
                    'amount' => $this->localeFormat->getNumber($regularPrice->getAmount()->getValue()),
                ],
                'basePrice' => [
                    'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getBaseAmount()),
                ],
                'finalPrice' => [
                    'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getValue()),
                ],
            ],
            'productId' => $currentProduct->getId(),
            'chooseText' => __('Choose an Option...'),
            'index' => isset($options['index']) ? $options['index'] : [],
        ];

        if ($this->helperMaTrixV->getMagentoVersion22()) {
            $config['images'] = $this->getOptionImages();
        } else {
            $config['images'] = isset($options['images']) ? $options['images'] : [];
        }

        if ($currentProduct->hasPreconfiguredValues() && !empty($attributesData['defaultValues'])) {
            $config['defaultValues'] = $attributesData['defaultValues'];
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return $this->jsonEncoder->encode($config);
    }
    /**
     * @return string
     */
    public function getJsonConfigM()
    {
        $store = $this->getCurrentStore();
        $currentProduct = $this->getProduct();

        $regularPrice = $currentProduct->getPriceInfo()->getPrice('regular_price');
        $finalPrice = $currentProduct->getPriceInfo()->getPrice('final_price');

        $options = $this->helper->getOptions($currentProduct, $this->getAllowProducts());
        $attributesData = $this->configurableAttributeData->getAttributesData($currentProduct, $options);

        $config = [
            'attributes' => $attributesData['attributes'],
            'optionPrices' => $this->getOptionPrices(),
            'productId' => $currentProduct->getId(),
        ];

        if ($currentProduct->hasPreconfiguredValues() && !empty($attributesData['defaultValues'])) {
            $config['defaultValues'] = $attributesData['defaultValues'];
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return $this->jsonEncoder->encode($config);
    }

    /**
     * @return string
     */
    public function getJsonSwatchNoMatrix()
    {
        $attribute_matrix = $this->getAttributeMatrix();
        $attributesData = $this->getSwatchAttributesData();
        $allOptionIds = $this->getConfigurableOptionsIds($attributesData);
        $swatchesData = $this->swatchHelper->getSwatchesByOptionsId($allOptionIds);

        $config = [];
        foreach ($attributesData as $attributeId => $attributeDataArray) {
            if (isset($attributeDataArray['options'])) {
                if (isset($attribute_matrix[$attributeId])) {
                    continue;
                }
                $config[$attributeId] = $this->addSwatchDataForAttribute(
                    $attributeDataArray['options'],
                    $swatchesData,
                    $attributeDataArray
                );
            }
        }

        return $this->jsonEncoder->encode($config);
    }

    /**
     * @return int
     */
    public function getRateTax()
    {
        $currentProduct = $this->getProduct();
        $store = $this->_storeManager->getStore();
        $request = $this->taxCalculation->getRateRequest(null, null, null, $store);
        $taxClassId = $currentProduct->getTaxClassId();
        $percent = $this->taxCalculation->getRate($request->setProductClassId($taxClassId));
        return $percent;
    }

    /**
     * @param $productid
     * @return \Magento\Catalog\Model\Product
     */
    public function getLoadProduct($productid)
    {
        return $this->helperMaTrixV->getProduct($productid);
    }

    /**
     * @param Product $product
     * @param null $priceType
     * @param array $arguments
     * @return string
     */
    public function getPriceHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null,
        array $arguments = []
    ) {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = 'item_view';
        }

        $priceRender = $this->getLayout()->getBlock('product.price.render.default');

        $price = '';
        if ($priceRender && $priceType) {
            $price = $priceRender->render(
                $priceType,
                $product,
                $arguments
            );
        }
        return $price;
    }

    /**
     * @return string
     */
    public function getPriceFormat()
    {
        return $this->localeFormat->getPriceFormat();
    }

    /**
     * @param $_product
     * @return array
     */
    public function getSwatchAttributesAsArrayMTV($_product){
        return $this->swatchHelper->getSwatchAttributesAsArray($_product);
    }

    /**
     * @return int
     */
    public function getPriceDisplayType(){
        return $this->taxConfig->getPriceDisplayType();
    }

    /**
     * Check is allowed Customer and module Enable
     * @return bool
     */
    public function isEnabledMatrixV()
    {
        return $this->helperMaTrixV->isEnabled();
    }

    /**
     * Check sort attribute by default or A-Z
     * @return bool
     */
    public function getSortOption()
    {
        return $this->helperMaTrixV->getSortOption();
    }

    /**
     * Check show unit price of product child in table matrix view
     * @return bool
     */
    public function canShowUnitPrice()
    {
        return $this->helperMaTrixV->canShowUnitPrice();
    }

    /**
     * Check show tier price of product child in table matrix view
     * @return bool
     */
    public function canShowTierPrice()
    {
        return $this->helperMaTrixV->canShowTierPrice();
    }

    /**
     * Check show total price
     * @return bool
     */
    public function canShowTotal()
    {
        return $this->helperMaTrixV->canShowTotal();
    }

    /**
     * Check show stock of product child in table matrix view
     * @return bool
     */
    public function canShowStock()
    {
        return $this->helperMaTrixV->canShowStock();
    }

    /**
     * Check show button increase of box qty 
     * @return bool
     */
    public function canShowButtonQty()
    {
        return $this->helperMaTrixV->canShowButtonQty();
    }

    public function calculateTierPrice()
    {
        return $this->helperMaTrixV->calculateTierPrice();
    }

    /**
     * Check show price range
     * @return bool
     */
    public function canShowPriceRange()
    {
        return $this->helperMaTrixV->canShowPriceRange();
    }

    /**
     * @return array
     */
    public function getPriceRange()
    {
        return $this->from_to_price;
    }

    /**
     * @return bool
     */
    public function getSameTierPrice()
    {
        return $this->sametierPrice;
    }

}
