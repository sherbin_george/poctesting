<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ConfigurableMatrixView
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ConfigurableMatrixView\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class TierAdvCalcAfterAddToCart implements ObserverInterface
{
    protected $_request;

    protected $_productRepository;

    protected $cart;

    protected $helper;

    /**
     * TierAdvCalcAfterAddToCart constructor.
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Bss\ConfigurableMatrixView\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Cart $cart,
        \Bss\ConfigurableMatrixView\Helper\Data $helper
        ) {
        $this->_request = $request;
        $this->_productRepository = $productRepository;
        $this->cart = $cart;
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->helper->isEnabled() && $this->helper->calculateTierPrice()) {
            $total_qty = $same_tierprices = [];
            $quote = $this->cart->getQuote();
            foreach ($quote->getAllVisibleItems() as $item) {
                $product = $this->_productRepository->getById($item->getProductId());
                if ($item->getProductType() == 'configurable' && $product->getConfigurableMatrixView()) {
                    if (isset($total_qty[$product->getId()])) {
                        $total_qty[$product->getId()] += (int)$item->getQty();
                    } else {
                        if (isset($this->_request->getParam('same_tier_price')[$product->getId()])) {
                           $item->setIsSameTierPrice($this->_request->getParam('same_tier_price')[$product->getId()]);
                        }
                        // $same_tierprices[$product->getId()] = $this->helper->getInfoMMT($product)[0];
                        $total_qty[$product->getId()]  = $item->getQty();
                    }
                }
            }
            
            foreach ($quote->getAllVisibleItems() as $item) {
                $_productparent = $this->_productRepository->getById($item->getProductId());
                if ($item->getProductType() == 'configurable' && $_productparent->getConfigurableMatrixView() && $item->getIsSameTierPrice()) {
                    $qty = $total_qty[$item->getProductId()];
                    $_productchild_id = $item->getOptionByCode('simple_product')->getProduct()->getId();
                    $_productchild  = $this->_productRepository->getById($_productchild_id);
                    $totalCustomOptionPrice= 0;
                    if ($product->getOptions()) {
                        $totalCustomOptionPrice = $this->_getTotalCustomOptionPrice($_productparent, $item);
                    }
                    $tierPrice = $_productchild->getTierPrice($qty);
                    if (isset($tierPrice) && $tierPrice > 0 && $tierPrice < $_productchild->getFinalPrice()) {
                        $item->setIsSameTierPrice(1);
                        $item->setCustomPrice(round($tierPrice + $totalCustomOptionPrice, 2));
                        $item->setOriginalCustomPrice(round($tierPrice + $totalCustomOptionPrice, 2));
                        $item->getProduct()->setIsSuperMode(true);
                    }
                }
            }
        }
    }

    /**
     * @param $product
     * @param $item
     * @return int
     */
    protected function _getTotalCustomOptionPrice($product, $item)
    {
        $totalCustomOptionPrice = 0;
        $options = $product->getOptions();
        foreach ($item->getBuyRequest()->getOptions() as $code => $option) {
            $customOptionItem[$code] = $option;
        }
        foreach ($options as $option) {
            if (!isset($customOptionItem[$option->getId()])) {
                continue;
            }
            if ($option->getType() === 'drop_down' || $option->getType() === 'radio') {
                $values = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Catalog\Model\Product\Option\Value')->getValuesCollection($option);
                foreach ($values as $value) {
                    if ($value->getId() == $customOptionItem[$option->getId()]) {
                        $totalCustomOptionPrice += $this->getOptionPrice($value);
                    }
                }
            } elseif ($option->getType() === 'checkbox' || $option->getType() === 'multiple') {
                $values = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Catalog\Model\Product\Option\Value')->getValuesCollection($option);
                foreach ($values as $value) {
                    if (in_array($value->getId(), $customOptionItem[$option->getId()])) {
                        $totalCustomOptionPrice += $this->getOptionPrice($value);
                    }
                }
            } else {
                $totalCustomOptionPrice += $this->getOptionPrice($option);
            }
        }
        return $totalCustomOptionPrice;
    }

    protected function getOptionPrice($option)
    {
        $price = 0;
        if ($option->getPriceType() == "fixed") {
            $price = $option->getPrice();
        }
        return $price;
    }
}