<?php

/**
 * Product:       Xtento_StockImport (2.2.6)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:45:21+00:00
 * Last Modified: 2016-05-07T12:22:36+00:00
 * File:          app/code/Xtento/StockImport/Helper/Entity.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\StockImport\Helper;

use Magento\Framework\Exception\LocalizedException;

class Entity extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Xtento\StockImport\Model\Import
     */
    protected $importModel;

    /**
     * @var \Xtento\XtCore\Helper\Utils
     */
    protected $utilsHelper;

    /**
     * Entity constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Xtento\StockImport\Model\Import $importModel
     * @param \Xtento\XtCore\Helper\Utils $utilsHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Xtento\StockImport\Model\Import $importModel,
        \Xtento\XtCore\Helper\Utils $utilsHelper
    ) {
        parent::__construct($context);
        $this->utilsHelper = $utilsHelper;
        $this->importModel = $importModel;
    }

    public function getEntityName($entity)
    {
        $entities = $this->importModel->getEntities();
        if (isset($entities[$entity])) {
            return rtrim($entities[$entity], 's');
        } else {
            return __("Undefined Entity");
        }
    }

    public function getPluralEntityName($entity)
    {
        return $entity;
    }

    public function getProcessorName($processor)
    {
        $processors = $this->importModel->getProcessors();
        if (!array_key_exists($processor, $processors)) {
            throw new LocalizedException(__('Processor "%1" does not exist. Cannot load profile.', $processor));
        }
        $processorName = $processors[$processor];
        return $processorName;
    }

    public function getMultiWarehouseSupport()
    {
        if ($this->utilsHelper->isExtensionInstalled('Innoexts_Warehouse')) {
            return true;
        }
        if ($this->utilsHelper->isExtensionInstalled('MDN_AdvancedStock')) {
            return true;
        }
        if ($this->utilsHelper->isExtensionInstalled('Aitoc_Aitquantitymanager')) {
            return true;
        }
        return false;
    }
}
