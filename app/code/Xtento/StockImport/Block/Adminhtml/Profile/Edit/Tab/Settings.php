<?php

/**
 * Product:       Xtento_StockImport (2.2.6)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:45:21+00:00
 * Last Modified: 2016-10-19T14:29:41+00:00
 * File:          app/code/Xtento/StockImport/Block/Adminhtml/Profile/Edit/Tab/Settings.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\StockImport\Block\Adminhtml\Profile\Edit\Tab;

class Settings extends \Xtento\StockImport\Block\Adminhtml\Widget\Tab implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $yesNo;

    /**
     * @var \Xtento\StockImport\Model\System\Config\Source\Product\Identifier
     */
    protected $productIdentifierSource;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $storeSource;

    /**
     * Settings constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Config\Model\Config\Source\Yesno $yesNo
     * @param \Magento\Store\Model\System\Store $storeSource
     * @param \Xtento\StockImport\Model\System\Config\Source\Product\Identifier $productIdentifierSource
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesNo,
        \Magento\Store\Model\System\Store $storeSource,
        \Xtento\StockImport\Model\System\Config\Source\Product\Identifier $productIdentifierSource,
        array $data = []
    ) {
        $this->yesNo = $yesNo;
        $this->productIdentifierSource = $productIdentifierSource;
        $this->storeSource = $storeSource;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function getFormMessages()
    {
        $formMessages = [];
        $formMessages[] = [
            'type' => 'notice',
            'message' => __(
                'The settings specified below will be applied to all manual and automatic imports.'
            )
        ];
        return $formMessages;
    }

    /**
     * Prepare form
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('stockimport_profile');
        if (!$model->getId()) {
            return $this;
        }

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $fieldset = $form->addFieldset('settings', ['legend' => __('Import Settings'), 'class' => 'fieldset-wide',]);

        $fieldset->addField(
            'reindex_mode',
            'select',
            [
                'label' => __('Reindex mode'),
                'name' => 'reindex_mode',
                'values' => [
                    ['value' => 'no_reindex', 'label' => __('No reindexing at all. (Recommended)')],
                    ['value' => 'flag_index', 'label' => __('No reindex. Flag as \'reindex required\'.')],
                    ['value' => 'full', 'label' => __('Full reindex (after import)')]
                ],
                'note' => __('No reindex is required in most cases.')
            ]
        );

        $fieldset->addField(
            'mark_out_of_stock',
            'select',
            [
                'label' => __(
                    'Set "In Stock" / "Out of Stock"'
                ),
                'name' => 'mark_out_of_stock',
                'values' => $this->yesNo->toOptionArray(),
                'note' => __('If stock qty is equal or below "Qty for Item\'s Status to become Out of Stock", mark as out of stock (and mark as "In Stock" again if qty is above "Qty for Item\'s Status ..."). This value will be overridden if you map the "Stock Status" field.')
            ]
        );

        $fieldset->addField(
            'import_relative_stock_level',
            'select',
            [
                'label' => __('Import relative stock level'),
                'name' => 'import_relative_stock_level',
                'values' => $this->yesNo->toOptionArray(),
                'note' => __(
                    'If set to "No", the stock level specified in the import file will be imported, whatever it is. If set to "Yes", if you import +5, 5 will be added to the current stock level, and if you import -5, 5 will be subtracted from the current stock level.'
                )
            ]
        );

        $fieldset->addField(
            'product_identifier',
            'select',
            [
                'label' => __('Product Identifier'),
                'name' => 'product_identifier',
                'values' => $this->productIdentifierSource->toOptionArray(),
                'note' => __(
                    'This is what is called the Product Identifier in the import settings and is what\'s used to identify the product in the import file. Almost always you will want to use the SKU.'
                )
            ]
        );
        $attributeCodeJs = "<script>
require([\"jquery\", \"prototype\"], function(jQuery) {
Event.observe(window, 'load', function() { function checkAttributeField(field) {if(field.value=='attribute') {\$('product_identifier_attribute_code').parentNode.parentNode.show()} else {\$('product_identifier_attribute_code').parentNode.parentNode.hide()}} checkAttributeField($('product_identifier')); $('product_identifier').observe('change', function(){ checkAttributeField(this); }); });
});
</script>";
        if ($model->getData('product_identifier') !== 'attribute') {
            // Not filled
            $attributeCodeJs .= "<script>
require([\"jquery\", \"prototype\"], function(jQuery) {
$('product_identifier_attribute_code').parentNode.parentNode.hide()
});
</script>";
        }
        $fieldset->addField(
            'product_identifier_attribute_code',
            'text',
            [
                'label' => __('Product Identifier: Attribute Code'),
                'name' => 'product_identifier_attribute_code',
                'note' => __(
                        'IMPORTANT: This is not the attribute name. It is the attribute code you assigned to the attribute.'
                    ) . $attributeCodeJs,
            ]
        );

        $fieldset = $form->addFieldset('misc_settings', ['legend' => __('Miscellaneous Settings'), 'class' => 'fieldset-wide']);

        $fieldset->addField('update_low_stock_date', 'select', [
            'label' => __('Update "low stock date" after importing'),
            'name' => 'update_low_stock_date',
            'values' => $this->yesNo->toOptionArray(),
            'note' => __('May make the import slower. Only enable if required.')
        ]);

        if (\Xtento\StockImport\Model\Import\Entity\Stock::$importPrices || \Xtento\StockImport\Model\Import\Entity\Stock::$importSpecialPrices) {
            $fieldset = $form->addFieldset('store', ['legend' => __('Store View (Price Update)'), 'class' => 'fieldset-wide']);

            $fieldset->addField('price_update_store_id', 'multiselect', [
                'label' => __('Store View'),
                'name' => 'price_update_store_id',
                'values' => array_merge_recursive([['value' => '', 'label' => __('--- Global (all) ---')]], $this->storeSource->getStoreValuesForForm()),
                'note' => __('The price will be set for the following website/store id.'),
            ]);
        }

        $this->setTemplate('Xtento_StockImport::profile/settings.phtml');

        $configuration = $model->getConfiguration();
        if (empty($configuration)) {
            // Set default values
            $configuration['mark_out_of_stock'] = true;
        }

        $form->setValues($configuration);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Import Settings');
    }

    /**
     * Prepare title for tab
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Import Settings');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}