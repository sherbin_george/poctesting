<?php

/**
 * Product:       Xtento_StockImport (2.2.6)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:45:21+00:00
 * Last Modified: 2017-06-14T13:58:14+00:00
 * File:          app/code/Xtento/StockImport/Model/Import/Entity/Stock.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\StockImport\Model\Import\Entity;

use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Xtento\StockImport\Helper\Entity;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\ResourceModel\Entity\AttributeFactory;
use \Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory as AttributeCollectionFactory;
use Xtento\StockImport\Helper\Module;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\PageCache\Cache;
use Magento\CatalogInventory\Model\ResourceModel\Stock as ResourceStock;

class Stock extends AbstractEntity
{
    static $importStock = true;

    /*
     * Enable price import functionality? Beta currently.
     */
    static $importPrices = true;
    static $importSpecialPrices = false;
    static $importCost = false;
    static $importStockId = false;
    static $importProductStatus = false;
    static $maxImportFilterCount = 3;

    /*
     * Attribute to identify stock items by
     */
    protected $attributeToLoadBy = 'sku';
    /*
     * Product identifiers (could be the SKU, an attribute, ... - attribute loaded by defined in function getProductIdsForProductIdentifiers)
     */
    protected $productIdentifiers = [];
    /*
     * Associative array holding productIdentifer => product_id
     */
    protected $productMap = [];
    protected $productTypeMap = [];
    /*
     * Products not found in Magento
     */
    protected $productsNotFound = [];
    /*
     * Existing stock_items taken directly from the cataloginventory_stock_item table.
     */
    protected $stockItems = [];
    /*
     * Existing stock_status items taken directly from the cataloginventory_stock_status table.
     */
    protected $stockStatusItems = [];
    /*
     * Which stock_items have been modified? Important for re-index
     */
    protected $modifiedStockItems = [];
    /*
     * Current prices for products
     */
    protected $prices = [];
    protected $specialPrices = [];
    protected $costValues = [];
    /*
     * Updated prices
     */
    protected $updatedPrices = [];
    protected $updatedSpecialPrices = [];
    protected $updatedCostValues = [];
    /*
     * Current product status
     */
    protected $productStatus = [];
    protected $updatedProductStatuses = [];

    /**
     * Entity ID field for catalog_product_entity_decimal update; changed in EE 2.1
     */
    protected $productEntityDecimalFieldName = 'entity_id';

    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var Entity
     */
    protected $entityHelper;

    /**
     * @var Config
     */
    protected $eavConfig;

    /**
     * @var AttributeFactory
     */
    protected $entityAttributeFactory;

    /**
     * @var AttributeCollectionFactory
     */
    protected $entityAttributeCollectionFactory;

    /**
     * @var Module
     */
    protected $moduleHelper;

    /**
     * @var Action
     */
    protected $productAction;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Configurable
     */
    protected $configurableProduct;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var IndexerRegistry
     */
    protected $indexerRegistry;

    /**
     * @var TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * @var Cache
     */
    protected $pageCache;

    /**
     * @var ResourceStock
     */
    protected $resourceStock;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var \Xtento\XtCore\Helper\Utils
     */
    protected $utilsHelper;

    /**
     * Stock constructor.
     *
     * @param ResourceConnection $resourceConnection
     * @param Registry $frameworkRegistry
     * @param ObjectManagerInterface $objectManager
     * @param ManagerInterface $eventManagerInterface
     * @param ProductFactory $productFactory
     * @param Entity $entityHelper
     * @param Config $eavConfig
     * @param AttributeFactory $entityAttributeFactory
     * @param AttributeCollectionFactory $attributeCollectionFactory
     * @param Module $moduleHelper
     * @param Action $productAction
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param Configurable $configurableProduct
     * @param StockRegistryInterface $stockRegistry
     * @param IndexerRegistry $indexerRegistry
     * @param TypeListInterface $typeList
     * @param Cache $pageCache
     * @param ResourceStock $resourceStock
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param \Xtento\XtCore\Helper\Utils $utilsHelper
     * @param array $data
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        Registry $frameworkRegistry,
        ObjectManagerInterface $objectManager,
        ManagerInterface $eventManagerInterface,
        ProductFactory $productFactory,
        Entity $entityHelper,
        Config $eavConfig,
        AttributeFactory $entityAttributeFactory,
        AttributeCollectionFactory $attributeCollectionFactory,
        Module $moduleHelper,
        Action $productAction,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Configurable $configurableProduct,
        StockRegistryInterface $stockRegistry,
        IndexerRegistry $indexerRegistry,
        TypeListInterface $typeList,
        Cache $pageCache,
        ResourceStock $resourceStock,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Xtento\XtCore\Helper\Utils $utilsHelper,
        array $data = []
    ) {
        $this->eventManager = $eventManagerInterface;
        $this->objectManager = $objectManager;
        $this->productFactory = $productFactory;
        $this->entityHelper = $entityHelper;
        $this->eavConfig = $eavConfig;
        $this->entityAttributeFactory = $entityAttributeFactory;
        $this->entityAttributeCollectionFactory = $attributeCollectionFactory;
        $this->moduleHelper = $moduleHelper;
        $this->productAction = $productAction;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->configurableProduct = $configurableProduct;
        $this->stockRegistry = $stockRegistry;
        $this->indexerRegistry = $indexerRegistry;
        $this->cacheTypeList = $typeList;
        $this->pageCache = $pageCache;
        $this->resourceStock = $resourceStock;
        $this->productMetadata = $productMetadata;
        $this->utilsHelper = $utilsHelper;

        parent::__construct($resourceConnection, $frameworkRegistry, $data);
    }

    /**
     * Prepare import by getting a mapping of the attribute used to identify the product and its product id
     *
     * @param $updatesInFilesToProcess
     *
     * @return bool
     */
    public function prepareImport($updatesInFilesToProcess)
    {
        // Check is Magento EE >=2.1, if so use different catalog_product_entity_decimal price field name (row_id in EE 2.1)
        if ($this->utilsHelper->isMagentoEnterprise() && $this->utilsHelper->mageVersionCompare($this->productMetadata->getVersion(), '2.1.0', '>=')) {
            $this->productEntityDecimalFieldName = 'row_id';
        }

        // Prepare import
        $this->eventManager->dispatch('xtento_stockimport_stockupdate_before', [
            'profile' => $this->getProfile(),
            'log' => $this->getLogEntry(),
            'updates' => &$updatesInFilesToProcess
        ]);

        if (!$this->getTestMode()) {
            // Reset stock, uncomment to enable
            /*$this->writeAdapter->update(
                $this->getTableName('cataloginventory_stock_item'),
                array('qty' => 0, 'is_in_stock' => 0)
            );
            $this->writeAdapter->update(
                $this->getTableName('cataloginventory_stock_status'),
                array('qty' => 0, 'stock_status' => 0)
            );*/

            // Reset stock for specific product IDs, uncomment to enable
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
            /*$productCollection = $this->productFactory->create()->getCollection();
            $productCollection->addAttributeToFilter('supplier', 'techdata');
            $productIds = $productCollection->getAllIds();
            if (is_array($productIds)) {
                $this->writeAdapter->update(
                    $this->getTableName('cataloginventory_stock_item'),
                    array('qty' => 0, 'is_in_stock' => 0),
                    "product_id IN (" . join(",", $productIds) . ")"
                );
                $this->writeAdapter->update(
                    $this->getTableName('cataloginventory_stock_status'),
                    array('qty' => 0, 'stock_status' => 0),
                    "product_id IN (" . join(",", $productIds) . ")"
                );
            }*/
        }

        // When importing duplicate SKUs from multiple files, the import may fail, as it thinks it needs to insert the stock item/status entry again. Refresh stock item/status tables after every file processed.
        // Prepare product identifiers
        $this->getProductIdentifiers($updatesInFilesToProcess);
        if (empty($this->productIdentifiers)) {
            $this->getLogEntry()->addDebugMessage(__('No products could be found in the import file.'));
            return false;
        }

        // Get product IDs for product identifiers
        $this->getProductIdsForProductIdentifiers();
        if (empty($this->productMap)) {
            $this->getLogEntry()->addDebugMessage(__('The products supplied in the import file could not be found in the Magento catalog.'));
            return false;
        }

        $this->applyFiltersToFoundProducts();
        if (empty($this->productMap)) {
            $this->getLogEntry()->addDebugMessage(__('The products supplied in the import file could not be found in the Magento catalog OR all were filtered by profile filters.'));
            return false;
        }

        // Find out which products couldn't be found in Magento
        foreach ($this->productIdentifiers as $productIdentifier) {
            if (!isset($this->productMap[$productIdentifier])) {
                array_push($this->productsNotFound, $productIdentifier);
            }
        }

        if (!$this->getTestMode()) {
            // Set all product IDs not in files to out of stock, uncomment to enable
            /*$this->writeAdapter->update(
                $this->getTableName('cataloginventory_stock_item'),
                array('qty' => 0, 'is_in_stock' => 0),
                "product_id NOT IN (" . join(",", $this->productMap) . ")"
            );
            $this->writeAdapter->update(
                $this->getTableName('cataloginventory_stock_status'),
                array('qty' => 0, 'stock_status' => 0),
                "product_id NOT IN (" . join(",", $this->productMap) . ")"
            );*/
        }

        // Which fields are in the file and should be handled for the import?
        $fieldsFound = [];
        foreach ($updatesInFilesToProcess as $updatesInFile) {
            if (isset($updatesInFile['FIELDS'])) {
                foreach ($updatesInFile['FIELDS'] as $field) {
                    $fieldsFound[] = $field;
                }
            }
        }
        if (!in_array('qty', $fieldsFound)
            && !in_array('is_in_stock', $fieldsFound)
            && !in_array('backorders', $fieldsFound)
            && !in_array('manage_stock', $fieldsFound)
            && !in_array('notify_stock_qty', $fieldsFound)
        ) {
            self::$importStock = false;
        } else {
            self::$importStock = true;
        }
        if (!in_array('price', $fieldsFound)) {
            self::$importPrices = false;
        } else {
            self::$importPrices = true;
        }
        if (!in_array('special_price', $fieldsFound)) {
            self::$importSpecialPrices = false;
        } else {
            self::$importSpecialPrices = true;
        }
        if (!in_array('cost', $fieldsFound)) {
            self::$importCost = false;
        } else {
            self::$importCost = true;
        }
        if (!in_array('status', $fieldsFound)) {
            self::$importProductStatus = false;
        } else {
            self::$importProductStatus = true;
        }
        if ($this->entityHelper->getMultiWarehouseSupport()) {
            self::$importStockId = true;
        }

        // Proceed with gathering information required for the import
        if (self::$importStock) {
            // Get current stock info - so what exists in the stock tables and what doesn't
            $this->getCurrentStockInfo();
        }

        if (self::$importPrices || self::$importSpecialPrices || self::$importCost) {
            // If price import is enabled.. get price info
            $this->getCurrentPriceInfo();
        }
        if (self::$importPrices || self::$importSpecialPrices) {
            // If price import is enabled.. set indexer to manual mode.
            // ... N/A in M2
        }
        if (self::$importProductStatus) {
            $this->getCurrentProductInfo();
        }

        // Start transaction for all the updates.. performance is the key here!
        #$this->writeAdapter->query('LOCK TABLES '.$this->getTableName('cataloginventory_stock_status').' WRITE');
        #$this->writeAdapter->query('LOCK TABLES '.$this->getTableName('cataloginventory_stock_item').' WRITE');

        // Only start a transaction if no product data is updated:
        if (!self::$importPrices && !self::$importSpecialPrices && !self::$importCost && !self::$importProductStatus) {
            $this->writeAdapter->beginTransaction();
        }

        $this->getLogEntry()->addDebugMessage(__('Transaction started. Starting import.'));
        return true;
    }

    /*
     * Get all the product identifiers we're supposed to identify stock items by. Could be the SKU or an attribute.
     */
    protected function getProductIdentifiers($updatesInFilesToProcess)
    {
        $this->productIdentifiers = [];
        foreach ($updatesInFilesToProcess as $updateFile) {
            foreach ($updateFile['ITEMS'] as $stockId => $updatesInFile) {
                foreach ($updatesInFile as $productIdentifier => $updateData) {
                    $productIdentifier = trim($productIdentifier);
                    array_push($this->productIdentifiers, strtolower($productIdentifier));
                }
            }
        }
        return $this->productIdentifiers;
    }

    /*
     * Get product ids for stock items based on the product identifiers supplied
     */
    protected function getProductIdsForProductIdentifiers()
    {
        // Which attribute is supposed be the identifier in the import file for the mapping to the actual products in Magento?
        if ($this->getConfig('product_identifier') == 'sku') {
            $this->attributeToLoadBy = 'sku';
        } else if ($this->getConfig('product_identifier') == 'attribute') {
            $this->attributeToLoadBy = $this->getConfig('product_identifier_attribute_code');
        } else if ($this->getConfig('product_identifier') == 'entity_id') {
            $this->attributeToLoadBy = 'entity_id';
        } else {
            throw new LocalizedException(__('Stock import: Attribute to use for identifying products not defined.'));
        }

        if ($this->attributeToLoadBy == 'sku') {
            $select = $this->readAdapter->select()
                ->from($this->getTableName('catalog_product_entity'), ['entity_id', 'type_id', 'sku'])
                ->where("LOWER(sku) in (" . $this->readAdapter->quote($this->productIdentifiers) . ")");
            $products = $this->readAdapter->fetchAll($select);

            foreach ($products as $product) {
                $this->productMap[trim(strtolower($product['sku']))] = $product['entity_id'];
                $this->productTypeMap[$product['entity_id']] = $product['type_id'];
            }

            $productsNotFound = [];
            foreach ($this->productIdentifiers as $productIdentifier) {
                if (!isset($this->productMap[$productIdentifier])) {
                    array_push($productsNotFound, $productIdentifier);
                }
            }
            if (!empty($productsNotFound)) {
                $this->getLogEntry()->addDebugMessage(__('The following SKUs defined in the import file could not be found in the catalog: %1', join(", ", $productsNotFound)));
                #mail($this->moduleHelper->getDebugEmail(), 'Magento Stock Import Module @ ' . @$_SERVER['SERVER_NAME'], 'Stock Import products not found: ' . join(", ", $productsNotFound));
            }

            unset($products, $select);
        } else if ($this->getConfig('product_identifier') == 'attribute') {
            // Check if attribute exists
            $entityType = $this->eavConfig->getEntityType(\Magento\Catalog\Model\Product::ENTITY);
            $eavAttribute = $this->entityAttributeCollectionFactory->create()
                ->addFieldToFilter('entity_type_id', $entityType->getId())
                ->addFieldToFilter('attribute_code', $this->attributeToLoadBy)
                ->getFirstItem();
            if (!$eavAttribute || !$eavAttribute->getId()) {
                throw new LocalizedException(__('The supplied product attribute code used to identify products does not exist.'));
            }

            // Load product collection
            $productCollection = $this->productFactory->create()->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect($this->attributeToLoadBy)
                ->addAttributeToFilter($this->attributeToLoadBy, ['in' => str_replace("'", "", $this->productIdentifiers)]);

            foreach ($productCollection as $product) {
                $attrValue = $product->getData($this->attributeToLoadBy);
                $attrValue = trim(strtolower($attrValue));
                $this->productMap[$attrValue] = $product->getId();
                $this->productTypeMap[$product->getId()] = $product->getTypeId();
            }
            unset($productCollection);
        } else if ($this->getConfig('product_identifier') == 'entity_id') {
            // We're supposed to use the entity_id to identify products.. that's great. Just use the IDs to load from tables etc. then!
            $select = $this->readAdapter->select()
                ->from($this->getTableName('catalog_product_entity'), ['entity_id', 'type_id', 'sku'])
                ->where("entity_id in (" . $this->readAdapter->quote($this->productIdentifiers) . ")");
            $products = $this->readAdapter->fetchAll($select);

            foreach ($products as $product) {
                if ($product['type_id'] == 'configurable' || $product['type_id'] == 'downloadable') {
                    continue;
                }
                $this->productMap[$product['entity_id']] = $product['entity_id'];
                $this->productTypeMap[$product['entity_id']] = $product['type_id'];
            }

            $productsNotFound = [];
            foreach ($this->productIdentifiers as $productIdentifier) {
                if (!isset($this->productMap[$productIdentifier])) {
                    array_push($productsNotFound, $productIdentifier);
                }
            }
            if (!empty($productsNotFound)) {
                $this->getLogEntry()->addDebugMessage(__('The following product IDs defined in the import file could not be found in the catalog: %1', join(", ", $productsNotFound)));
                #mail($this->moduleHelper->getDebugEmail(), 'Magento Stock Import Module @ ' . @$_SERVER['SERVER_NAME'], 'Stock Import products not found: ' . join(", ", $productsNotFound));
            }

            unset($products, $select);
        }
    }

    protected function applyFiltersToFoundProducts()
    {
        $profileConfig = $this->getProfile()->getConfiguration();
        for ($i = 1; $i <= self::$maxImportFilterCount; $i++) {
            if (!isset($profileConfig['import_filter_' . $i])) {
                continue;
            }
            $filter = $profileConfig['import_filter_' . $i];
            if (!array_key_exists('filter', $filter) ||
                !array_key_exists('attribute', $filter) ||
                !array_key_exists('condition', $filter) ||
                !array_key_exists('value', $filter)
            ) {
                $this->getLogEntry()->addDebugMessage(__('Warning: Filter %1 has not been configured properly. Filter skipped.', $i));
                continue;
            }
            if ($filter['filter'] == '' &&
                $filter['attribute'] == '' &&
                $filter['condition'] == '' &&
                $filter['value'] == ''
            ) {
                // Filter has not been set up - skip it.
                continue;
            }
            if ($filter['filter'] == '' ||
                $filter['attribute'] == '' ||
                $filter['condition'] == '' ||
                $filter['value'] == ''
            ) {
                $this->getLogEntry()->addDebugMessage(__('Warning: Filter %1 has not been configured properly. One more multiple filter fields are empty. Filter skipped.', $i));
                continue;
            }
            // Load products affected by filter, and "combine" products found + filter products so filter is applied.
            $productCollection = $this->productFactory->create()->getCollection()
                ->addAttributeToSelect('entity_id');
            // Determine product attribute to filter by, handle dropdown attributes
            $eavAttribute = $this->eavConfig->getAttribute('catalog_product', $filter['attribute']);
            if (!$eavAttribute || !$eavAttribute->getId()) {
                $this->getLogEntry()->addDebugMessage(__('Warning: Filter %1 uses a product attribute to filter which does not exist anymore. Filter skipped.', $i));
                continue;
            }
            if ($eavAttribute->getFrontendInput() == 'select') {
                $dropdownId = null;
                $attributeOptions = $eavAttribute->getSource()->getAllOptions();
                foreach ($attributeOptions as $option) {
                    if (strcasecmp($option['label'], $filter['value']) == 0 || $option['value'] == $filter['value']) {
                        $dropdownId = $option['value'];
                    }
                }
                if ($dropdownId === null) {
                    $this->getLogEntry()->addDebugMessage(__('Warning: Filter %1 tries to filter by a dropdown attribute value which does not exist. Please check the attribute "%2" and make sure the exact dropdown option ("%3") exists as an attribute option (Store View = Admin).', $i, $filter['attribute'], $filter['value']));
                    continue;
                } else {
                    $filter['value'] = $dropdownId;
                }
            } else {
                if ($filter['condition'] == 'like' || $filter['condition'] == 'nlike') {
                    $filter['value'] = '%' . $filter['value'] . '%';
                }
            }
            if ($filter['condition'] == 'neq') {
                $productCollection->addAttributeToFilter(
                    $filter['attribute'],
                    [
                        [$filter['condition'] => $filter['value']],
                        ['null' => true]
                    ],
                    'left'
                ); // Left join is required, so attribute values when joining attributes which don't have values which then are NULL can be checked
            } else {
                $productCollection->addAttributeToFilter(
                    $filter['attribute'],
                    [$filter['condition'] => $filter['value']]
                );
            }
            #echo (string)$productCollection->getSelect(); die();
            $foundProductIds = $productCollection->getAllIds();
            $removedProducts = 0;
            if ($filter['filter'] == 'include_only') {
                foreach ($this->productMap as $productIdentifier => $productId) {
                    if (!in_array($productId, $foundProductIds)) {
                        unset($this->productMap[$productIdentifier]);
                        $removedProducts++;
                    }
                }
            }
            if ($filter['filter'] == 'exclude') {
                foreach ($this->productMap as $productIdentifier => $productId) {
                    if (in_array($productId, $foundProductIds)) {
                        unset($this->productMap[$productIdentifier]);
                        $removedProducts++;
                    }
                }
            }
            $this->getLogEntry()->addDebugMessage(__('Filter %1 has removed/filtered %2 product(s) from the import files.', $i, $removedProducts));
        }
        #die();
    }

    /*
     * Get information about current stock settings, only for products we want to update though
     */
    protected function getCurrentStockInfo()
    {
        // Get stock_item information
        $select = $this->readAdapter->select()
            ->from($this->getTableName('cataloginventory_stock_item'),
                [
                    'product_id',
                    'qty',
                    'is_in_stock',
                    'stock_id',
                    'manage_stock',
                    'notify_stock_qty',
                    'use_config_manage_stock',
                    'min_qty',
                    'use_config_min_qty',
                    'backorders',
                    'use_config_backorders'
                ]
            )
            ->where("product_id in (" . join(",", array_values($this->productMap)) . ")");
        $stockItems = $this->readAdapter->fetchAll($select);

        foreach ($stockItems as $stockItem) {
            // Prepare qty field
            $stockItem['qty'] = sprintf('%.4f', $stockItem['qty']);
            $this->stockItems[$stockItem['stock_id']][$stockItem['product_id']] = $stockItem;
        }

        // Get stock_status information
        $select = $this->readAdapter->select()
            ->from(
                $this->getTableName('cataloginventory_stock_status'),
                ['product_id', 'qty', 'stock_status', 'stock_id']
            )
            ->where("product_id in (" . join(",", array_values($this->productMap)) . ")");
        $stockStatusItems = $this->readAdapter->fetchAll($select);

        foreach ($stockStatusItems as $stockStatusItem) {
            // Prepare qty field
            $stockStatusItem['qty'] = sprintf('%.4f', $stockStatusItem['qty']);
            $this->stockStatusItems[$stockStatusItem['stock_id']][$stockStatusItem['product_id']] = $stockStatusItem;
        }
    }

    /*
     * Get information about the current price levels for the products in the import file
     */
    protected function getCurrentPriceInfo()
    {
        if (self::$importPrices) {
            $priceAttributeId = $this->entityAttributeFactory->create()->getIdByCode('catalog_product', 'price');
            if ($priceAttributeId) {
                $select = $this->readAdapter->select()
                    ->from($this->getTableName('catalog_product_entity_decimal'))
                    ->where('attribute_id = ?', $priceAttributeId)
                    ->where($this->productEntityDecimalFieldName . " in (" . join(",", array_values($this->productMap)) . ")");
                $configPriceUpdateStoreId = $this->getConfig('price_update_store_id');
                if (!empty($configPriceUpdateStoreId)) {
                    $storeIds = join(",", $configPriceUpdateStoreId);
                    if (!empty($storeIds)) {
                        $select->where("store_id in (" . $storeIds . ")");
                    }
                }
                $currentPrices = $this->readAdapter->fetchAll($select);

                foreach ($currentPrices as $currentPrice) {
                    $this->prices[$currentPrice[$this->productEntityDecimalFieldName]] = ['price' => sprintf('%.4f', $currentPrice['value'])];
                }
            } else {
                throw new LocalizedException(__('Error while trying to get current price info. The price attribute could not be found.'));
            }
        }
        if (self::$importSpecialPrices) {
            $priceAttributeId = $this->entityAttributeFactory->create()->getIdByCode('catalog_product', 'special_price');
            if ($priceAttributeId) {
                $select = $this->readAdapter->select()
                    ->from($this->getTableName('catalog_product_entity_decimal'))
                    ->where('attribute_id = ?', $priceAttributeId)
                    ->where($this->productEntityDecimalFieldName . " in (" . join(",", array_values($this->productMap)) . ")");
                $configPriceUpdateStoreId = $this->getConfig('price_update_store_id');
                if (!empty($configPriceUpdateStoreId)) {
                    $storeIds = join(",", $configPriceUpdateStoreId);
                    if (!empty($storeIds)) {
                        $select->where("store_id in (" . $storeIds . ")");
                    }
                }
                $currentPrices = $this->readAdapter->fetchAll($select);

                foreach ($currentPrices as $currentPrice) {
                    if (!empty($currentPrice['value'])) {
                        $this->specialPrices[$currentPrice[$this->productEntityDecimalFieldName]] = ['special_price' => sprintf('%.4f', $currentPrice['value'])];
                    } else {
                        $this->specialPrices[$currentPrice[$this->productEntityDecimalFieldName]] = ['special_price' => ''];
                    }
                }
            } else {
                throw new LocalizedException(__('Error while trying to get current special price info. The special price attribute could not be found.'));
            }
        }
        if (self::$importCost) {
            $costAttributeId = $this->entityAttributeFactory->create()->getIdByCode('catalog_product', 'cost');
            if ($costAttributeId) {
                $select = $this->readAdapter->select()
                    ->from($this->getTableName('catalog_product_entity_decimal'))
                    ->where('attribute_id = ?', $costAttributeId)
                    ->where($this->productEntityDecimalFieldName . " in (" . join(",", array_values($this->productMap)) . ")");
                $currentPrices = $this->readAdapter->fetchAll($select);

                foreach ($currentPrices as $currentPrice) {
                    if (!empty($currentPrice['value'])) {
                        $this->costValues[$currentPrice[$this->productEntityDecimalFieldName]] = ['cost' => sprintf('%.4f', $currentPrice['value'])];
                    } else {
                        $this->costValues[$currentPrice[$this->productEntityDecimalFieldName]] = ['cost' => ''];
                    }
                }
            } else {
                self::$importCost = false;
                #throw new LocalizedException(__('Error while trying to get current "cost" info. The cost attribute could not be found.'));
            }
        }
        #var_dump($this->prices);
        #die();
    }

    /*
     * Get information about the current price levels for the products in the import file
     */
    protected function getCurrentProductInfo()
    {
        if (self::$importProductStatus) {
            $attributeId = $this->entityAttributeFactory->create()->getIdByCode('catalog_product', 'status');
            if ($attributeId) {
                $select = $this->readAdapter->select()
                    ->from($this->getTableName('catalog_product_entity_int'))
                    ->where('attribute_id = ?', $attributeId)
                    ->where($this->productEntityDecimalFieldName . " in (" . join(",", array_values($this->productMap)) . ")");
                $products = $this->readAdapter->fetchAll($select);

                foreach ($products as $product) {
                    $this->productStatus[$product[$this->productEntityDecimalFieldName]] = ['status' => $product['value']];
                }
            }
        }
    }

    /*
     * Update stock level for product
     */
    public function processItem($productIdentifier, $updateData)
    {
        // Result (and debug information) returned to observer
        $importResult = ['error' => 'Nothing happened yet.'];

        if (isset($updateData['product_identifier'])) {
            unset($updateData['product_identifier']);
        }
        $productIdentifier = strtolower($productIdentifier);

        if (!isset($updateData['stock_id']) || empty($updateData['stock_id'])) {
            $updateData['stock_id'] = 1;
        } else {
            $updateData['stock_id'] = intval($updateData['stock_id']);
        }

        // Update stock_item, stock_status and eventually the price
        if (isset($this->productMap[$productIdentifier])) {
            $productId = $this->productMap[$productIdentifier];
            // Current import result.. nothing has changed yet
            $importResult = ['changed' => false, 'debug' => __("Product '%1' was found in Magento, but no fields have changed compared to the current settings. Identified product ID is %2.", $productIdentifier, $productId)];

            // Adjust stock level by pending/processing orders
            /*if (isset($updateData['qty'])) {
                $orderItemCollection = $this->orderItemFactory->create()->getCollection();
                $orderItemCollection
                    ->getSelect()
                    ->joinInner(
                        array('order' => $this->modelResource->getTableName('sales/order')),
                        'order.entity_id = main_table.order_id'
                    )
                    ->where('main_table.product_id=?', $productId)
                    ->where('order.status="pending" or order.status="processing"');
                if ($orderItemCollection->count() > 0) {
                    $blockedQty = 0;
                    foreach ($orderItemCollection as $orderItem) {
                        $blockedQty += (int)$orderItem->getQtyOrdered();
                    }
                    $updateData['qty'] = $updateData['qty'] - $blockedQty;
                }
                #var_dump($orderItemCollection->count(), $productId, $updateData['qty'], $blockedQty); die();
            }*/
            // End stock level adjustment by pending/processing orders

            // Fetch updated fields
            $updatedFields = $this->getUpdatedFields($updateData, $productId); // See if anything has changed..

            if (self::$importStock) {
                // Check is supported product type
                $productType = $this->productTypeMap[$productId];
                if ($productType !== Configurable::TYPE_CODE  && $productType !== \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE) {
                    // stock_item routine:
                    if (isset($this->stockItems[$updateData['stock_id']][$productId])) {
                        // This is a known stock_item entry
                        if (!empty($updatedFields)) {
                            // Something has changed, update stock item
                            $importResult = $this->updateStockItem($productId, $productIdentifier, $updatedFields, $updateData);
                            // Push product ID to modified stock items.
                            array_push($this->modifiedStockItems, $productId);
                        } else {
                            // Nothing has changed
                            if ($this->getTestMode()) {
                                #return $importResult;
                            }
                        }
                    } else {
                        // New stock item, insert stock item
                        $importResult = $this->insertStockItem($productId, $productIdentifier, $updateData);
                        // Push product ID to modified stock items.
                        array_push($this->modifiedStockItems, $productId);
                    }

                    // stock_status routine, update only when not in test_mode
                    if (!$this->getTestMode()) {
                        if (isset($this->stockStatusItems[$updateData['stock_id']][$productId])) {
                            // This is a known stock_status entry
                            if (!empty($updatedFields)) {
                                // Something has changed, update stock_status
                                $this->updateStockStatus($productId, $updatedFields, $updateData);
                                // Push product ID to modified stock items.
                                if (!isset($this->modifiedStockItems[$productId])) {
                                    array_push($this->modifiedStockItems, $productId);
                                }
                            }
                        } else {
                            // New stock item, insert stock item
                            $this->insertStockStatus($productId, $updateData);
                            // Push product ID to modified stock items.
                            if (!isset($this->modifiedStockItems[$productId])) {
                                array_push($this->modifiedStockItems, $productId);
                            }
                        }
                    }
                }
            }

            // If using the Aheadworks Product Updates extension, uncomment the following line:
            // $this->productupdatesFactory->create()->setProductupdatesToSend($productId);

            $productFieldsUpdated = false;

            $priceUpdateStoreId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
            $configPriceUpdateStoreId = $this->getConfig('price_update_store_id');
            if ($configPriceUpdateStoreId !== '' && !empty($configPriceUpdateStoreId)) {
                $priceUpdateStoreId = $configPriceUpdateStoreId;
            }

            // price update routine
            if (self::$importPrices) {
                if (!empty($updateData) && isset($updateData['price'])) {
                    $newPrice = sprintf('%.4f', $updateData['price']);
                    if (isset($this->prices[$productId])) {
                        $currentPrice = $this->prices[$productId]['price'];
                        if ($currentPrice !== $newPrice) {
                            if (!$this->getTestMode()) {
                                foreach ($priceUpdateStoreId as $updateStoreId) {
                                    if ($updateStoreId == 0) {
                                        $updateStoreId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
                                    }
                                    $this->productAction->updateAttributes([$productId], ['price' => $newPrice], $updateStoreId);
                                }
                            }
                            // Price has changed.
                            array_push($this->updatedPrices, $productId);
                            $productFieldsUpdated['price'] = $newPrice;
                        }
                    } else {
                        if (!$this->getTestMode()) {
                            foreach ($priceUpdateStoreId as $updateStoreId) {
                                if ($updateStoreId == 0) {
                                    $updateStoreId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
                                }
                                $this->productAction->updateAttributes([$productId], ['price' => $newPrice], $updateStoreId);
                            }
                        }
                        // Price has changed.
                        array_push($this->updatedPrices, $productId);
                        $productFieldsUpdated['price'] = $newPrice;
                    }
                }
            }

            if (self::$importSpecialPrices && !$this->getTestMode()) {
                if (!empty($updateData) && isset($updateData['special_price'])) {
                    if (isset($this->specialPrices[$productId])) {
                        $currentPrice = $this->specialPrices[$productId]['special_price'];
                    } else {
                        $currentPrice = null;
                    }
                    if ($updateData['special_price'] != '') {
                        $newPrice = sprintf('%.4f', $updateData['special_price']);
                    } else {
                        $newPrice = '';
                    }
                    $fromDate = strftime('%Y-%m-%d');
                    if ($newPrice === "0.0000") {
                        $newPrice = "";
                        $fromDate = "";
                    }
                    if ($currentPrice !== $newPrice || $newPrice == '') {
                        foreach ($priceUpdateStoreId as $updateStoreId) {
                            if ($updateStoreId == 0) {
                                $updateStoreId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
                            }
                            $this->productAction->updateAttributes([$productId], ['special_price' => $newPrice], $updateStoreId);
                            $this->productAction->updateAttributes([$productId], ['special_from_date' => $fromDate], $updateStoreId);
                        }
                        // Special price has changed.
                        array_push($this->updatedSpecialPrices, $productId);
                        $productFieldsUpdated['special_price'] = $newPrice;
                        $productFieldsUpdated['special_from_date'] = $fromDate;
                    }
                } else {
                    if (isset($this->specialPrices[$productId])) {
                        if (!empty($this->specialPrices[$productId]['special_price'])) {
                            foreach ($priceUpdateStoreId as $updateStoreId) {
                                if ($updateStoreId == 0) {
                                    $updateStoreId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
                                }
                                $this->productAction->updateAttributes([$productId], ['special_price' => ''], $updateStoreId);
                                $this->productAction->updateAttributes([$productId], ['special_from_date' => ''], $updateStoreId);
                            }
                            array_push($this->updatedSpecialPrices, $productId);
                            $productFieldsUpdated['special_price'] = '';
                            $productFieldsUpdated['special_from_date'] = '';
                        }
                    }
                }
            }

            if (self::$importCost && !$this->getTestMode()) {
                if (!empty($updateData) && isset($updateData['cost'])) {
                    if (isset($this->costValues[$productId])) {
                        $currentPrice = $this->costValues[$productId]['cost'];
                        if ($updateData['cost'] != '') {
                            $newPrice = sprintf('%.4f', $updateData['cost']);
                        } else {
                            $newPrice = '';
                        }
                        if ($currentPrice !== $newPrice || $newPrice == '') {
                            $this->productAction->updateAttributes([$productId], ['cost' => $newPrice], \Magento\Store\Model\Store::DEFAULT_STORE_ID);
                            // Cost has changed.
                            array_push($this->updatedCostValues, $productId);
                            $productFieldsUpdated['cost'] = $newPrice;
                        }
                    }
                } else {
                    if (isset($this->costValues[$productId])) {
                        if (!empty($this->costValues[$productId]['cost'])) {
                            $this->productAction->updateAttributes([$productId], ['cost' => ''], \Magento\Store\Model\Store::DEFAULT_STORE_ID);
                            $productFieldsUpdated['cost'] = '';
                        }
                    }
                }
            }

            if (self::$importProductStatus && !$this->getTestMode()) {
                if (!empty($updateData) && isset($updateData['status'])) {
                    if (isset($this->productStatus[$productId])) {
                        $currentStatus = $this->productStatus[$productId]['status'];
                        if ($updateData['status'] != '') {
                            $updateStatus = strtolower($updateData['status']);
                            $newStatus = null;
                            if ($updateStatus == 'yes' || $updateStatus == '1' || $updateStatus == 'enabled' || $updateStatus == 'ja' || $updateStatus == 'true') {
                                $newStatus = 1;
                            }
                            if ($updateStatus == 'no' || $updateStatus == '0' || $updateStatus == 'disabled' || $updateStatus == 'nein' || $updateStatus == 'false') {
                                $newStatus = 2;
                            }
                            if ($newStatus === null && $updateStatus !== '') {
                                throw new LocalizedException(__('An invalid value was imported for the product status column. It should contain values like "Enabled" or "Disabled".'));
                            }
                            if ($currentStatus != $newStatus) {
                                if ($newStatus === 1) {
                                    $this->productAction->updateAttributes([$productId], ['status' => ProductStatus::STATUS_ENABLED], \Magento\Store\Model\Store::DEFAULT_STORE_ID);
                                } else {
                                    $this->productAction->updateAttributes([$productId], ['status' => ProductStatus::STATUS_DISABLED], \Magento\Store\Model\Store::DEFAULT_STORE_ID);
                                }
                                $productFieldsUpdated['status'] = $newStatus;
                                array_push($this->updatedProductStatuses, $productId);
                            }
                        }
                    }
                }
            }

            if (is_array($productFieldsUpdated)) {
                $tempPriceFields = $productFieldsUpdated;
                array_walk($tempPriceFields, create_function('&$i,$k', '$i=" \"$k\"=\"$i\"";'));
                // Add debug messages to $importResult
                if (isset($importResult['error']) || (isset($importResult['changed']) && $importResult['changed'] === false)) {
                    if ($this->getTestMode()) {
                        $importResult = ['changed' => true, 'debug' => __("Product '%1' would have been updated. Identified product ID is %2. Updated fields: %3", $productIdentifier, $productId, implode($tempPriceFields, " "))];
                    } else {
                        $importResult = ['changed' => true, 'debug' => __("Product '%1' has been updated. Identified product ID is %2. Updated fields: %3", $productIdentifier, $productId, implode($tempPriceFields, " "))];
                    }
                } else if (isset($importResult['changed']) && $importResult['changed'] === true) {
                    $importResult['debug'] .= implode($tempPriceFields, "");
                }
            }
        } else {
            // Product not found.
            #$importResult = array('error' => __("Product '%1' could not be found in Magento. We tried to identify the product by using the attribute '%2'.", $productIdentifier, $this->attributeToLoadBy));
            $importResult = ['changed' => false];
        }

        return $importResult;
    }

    /*
     * See which fields changed and if necessary modify other fields based on fields - example qty <= 0 -> is_in_stock = false
     */
    protected function getUpdatedFields($updateData, $productId)
    {
        $updatedFields = [];

        /*if (empty($this->stockItems)) {
            return $updatedFields;
        }*/

        /*
         * First run: See which values changed, adjust values based on that and return fields to update.
         */
        if (!isset($this->stockItems[$updateData['stock_id']]) || !isset($this->stockItems[$updateData['stock_id']][$productId])) {
            // New stock item/status
            foreach ($updateData as $field => $newValue) {
                if ($field == 'price' || $field == 'special_price' || $field == 'cost' || $field == 'stock_id' || $field == 'status') { // Do not process these here.
                    continue;
                }
                if ($field == 'manage_stock' || $field == 'is_in_stock' || $field == 'notify_stock_qty' || $field == 'backorders') {
                    $newValue = (int)$newValue; // Should be an integer coming from the database.
                }
                $updatedFields[$field] = $newValue;
            }
        } else {
            // Already existing
            $stockItem = $this->stockItems[$updateData['stock_id']][$productId];
            foreach ($updateData as $field => $newValue) {
                foreach ($stockItem as $stockField => $stockValue) {
                    if ($stockField == 'price' || $stockField == 'special_price' || $stockField == 'cost' || $stockField == 'stock_id' || $stockField == 'status') { // Do not process these here.
                        continue;
                    }
                    if ($field == $stockField) {
                        // Type casting - everything coming from the database is a string (apparently, at least in my tests)
                        if ($stockField == 'manage_stock' || $stockField == 'is_in_stock' || $stockField == 'notify_stock_qty' || $stockField == 'backorders') {
                            $stockValue = (int)$stockValue; // Should be an integer coming from the database.
                        }
                        // Preparing field values
                        if ($stockField == 'qty') {
                            if ($this->getConfigFlag('import_relative_stock_level')) {
                                // Check for relative updating
                                $tempValue = (string)$newValue;
                                if ($tempValue[0] == '+') {
                                    $newValue = $stockValue + substr($newValue, 1);
                                }
                                if ($tempValue[0] == '-') {
                                    $newValue = $stockValue - substr($newValue, 1);
                                }
                            }
                        }
                        // Compare and see if the value changed at all
                        if ($newValue !== $stockValue) {
                            if (trim($newValue) !== '') {
                                $updatedFields[$field] = $newValue;
                            }
                        }
                        // Uncomment this to *increment* stock levels by the imported QTY instead of replacing the stock level with the imported QTY.
                        /*
                        if ($stockField == 'qty') {
                            if ($stockValue <= 0) $stockValue = 0;
                            $updatedFields[$field] = $stockValue + $newValue;
                        }
                        */
                        break 1;
                    }
                }
            }
        }

        /*
         * Second run: See if we have to adjust values based on other field values.
         */
        foreach ($updatedFields as $field => $value) {
            if ($field == 'qty' && !isset($updateData['is_in_stock'])) {
                // Update is_in_stock field, only if not set in import file and if config flag mark_out_of_stock is set to yes
                if (!isset($stockItem) || (int)$stockItem['use_config_min_qty'] === 1) {
                    $outOfStockValue = (int)$this->scopeConfig->getValue('cataloginventory/item_options/min_qty');
                } else {
                    $outOfStockValue = $stockItem['min_qty'];
                }
                // Get "backorders allowed"
                if (!isset($stockItem) || (int)$stockItem['use_config_backorders'] === 1) {
                    $allowBackorders = (int)$this->scopeConfig->getValue('cataloginventory/item_options/backorders');
                } else {
                    $allowBackorders = $stockItem['backorders'];
                }
                /* $value = Stock level */
                if (!$allowBackorders && $this->getConfigFlag('mark_out_of_stock') && $value <= $outOfStockValue) {
                    $updatedFields['is_in_stock'] = 0;
                } else if ($value > 0) {
                    $updatedFields['is_in_stock'] = (int)($value > $outOfStockValue);
                }
                if ($allowBackorders && $this->getConfigFlag('mark_out_of_stock') && $value <= $outOfStockValue) {
                    // Debug message: Not setting to out of stock as its a backorderable item
                }
                #var_dump((int)$stockItem['use_config_min_qty'], $outOfStockValue, $updatedFields['is_in_stock']); die();
            }
            if ($field == 'backorders' && isset($updateData['backorders']) && $updateData['backorders'] !== '') {
                $updatedFields['use_config_backorders'] = 0;
            }
        }

        return $updatedFields;
    }

    protected function insertStockItem($productId, $productIdentifier, $updateData)
    {
        // Some debugging information
        $tempUpdateData = $updateData;
        array_walk($tempUpdateData, create_function('&$i,$k', '$i=" \"$k\"=\"$i\"";'));
        if ($this->getTestMode()) {
            $importResult = ['changed' => true, 'debug' => __("Product '%1' (New stock item) would have been imported into Magento. Identified product ID is %2. New fields: %3", $productIdentifier, $productId, implode($tempUpdateData, ""))];
            return $importResult;
        }

        // Prepare the stock_item and insert it
        if (isset($updateData['qty'])) {
            if (!isset($updateData['is_in_stock'])) {
                // Update is_in_stock field, only if not set in import file and if config flag mark_out_of_stock is set to yes
                $outOfStockValue = (int)$this->scopeConfig->getValue('cataloginventory/item_options/min_qty');
                if ($this->getConfigFlag('mark_out_of_stock') && $updateData['qty'] <= $outOfStockValue) {
                    $updateData['is_in_stock'] = 0;
                } else if ($updateData['qty'] > 0) {
                    $updateData['is_in_stock'] = (int)($updateData['qty'] > $outOfStockValue);
                }
            }
        }
        #$updateData['stock_id'] = 1;
        $updateData['product_id'] = $productId;

        $updatedFields = $updateData;
        foreach ($updatedFields as $field => $value) {
            if ($field != 'is_in_stock' && $field != 'qty' && $field != 'stock_id' && $field != 'product_id') { // Do not process these here.
                unset($updatedFields[$field]);
            }
        }
        $this->writeAdapter->insert($this->getTableName('cataloginventory_stock_item'), $updatedFields);

        // Import result
        $importResult = ['changed' => true, 'debug' => __("Product '%1' (New stock item) has been imported into Magento. Identified product ID is %2. New fields: %3", $productIdentifier, $productId, implode($tempUpdateData, ""))];
        return $importResult;
    }

    protected function insertStockStatus($productId, $updatedFields)
    {
        // Entry in stock_status does not exist yet, insert it
        #$updateData['stock_id'] = 1;
        $updateData['product_id'] = $productId;
        if (isset($updatedFields['is_in_stock'])) {
            $updateData['stock_status'] = $updatedFields['is_in_stock'];
        }
        if (isset($updatedFields['qty'])) {
            if (!isset($updateData['stock_status'])) {
                // Update is_in_stock field, only if not set in import file and if config flag mark_out_of_stock is set to yes
                $outOfStockValue = (int)$this->scopeConfig->getValue('cataloginventory/item_options/min_qty');
                if ($this->getConfigFlag('mark_out_of_stock') && $updatedFields['qty'] <= $outOfStockValue) {
                    $updateData['stock_status'] = 0;
                } else if ($updatedFields['qty'] > 0) {
                    $updateData['stock_status'] = (int)($updatedFields['qty'] > $outOfStockValue);
                }
            }
            $updateData['qty'] = $updatedFields['qty'];
        }
        if (isset($updatedFields['stock_id'])) {
            $updateData['stock_id'] = $updatedFields['stock_id'];
        }

        foreach ($this->storeManager->getWebsites() as $website) {
            $updateData['website_id'] = $website->getId();
            $this->writeAdapter->insert($this->getTableName('cataloginventory_stock_status'), $updateData);
        }

        return $this;
    }

    protected function updateStockItem($productId, $productIdentifier, $updatedFields, $updateData)
    {
        // Some debugging information
        $tempUpdatedFields = $updatedFields;
        array_walk($tempUpdatedFields, create_function('&$i,$k', '$i=" \"$k\"=\"$i\"";'));
        if ($this->getTestMode()) {
            // Don't touch the stock item. Just return some fancy debug information.
            $importResult = ['changed' => true, 'debug' => __("Product '%1' would have been imported into Magento. Identified product ID is %2. Updated fields: %3", $productIdentifier, $productId, implode($tempUpdatedFields, ""))];
            return $importResult;
        }

        // Update stock_item
        $this->writeAdapter->update($this->getTableName('cataloginventory_stock_item'), $updatedFields, "product_id=$productId and stock_id=" . $updateData['stock_id']);

        // Import result
        $importResult = ['changed' => true, 'debug' => __("Product '%1' has been imported into Magento. Identified product ID is %2. Updated fields: %3", $productIdentifier, $productId, implode($tempUpdatedFields, ""))];
        return $importResult;
    }

    protected function updateStockStatus($productId, $updatedFields, $updateData)
    {
        // Entry in stock_status already exists, update it
        $statusUpdate = [];
        if (isset($updatedFields['qty'])) {
            $statusUpdate['qty'] = $updatedFields['qty'];
        }
        if (isset($updatedFields['is_in_stock'])) {
            $statusUpdate['stock_status'] = $updatedFields['is_in_stock'];
        }

        // Update it only if something has changed which is interesting for the stock_status
        if (!empty($statusUpdate)) {
            $this->writeAdapter->update($this->getTableName('cataloginventory_stock_status'), $statusUpdate, "product_id=$productId and stock_id=" . $updateData['stock_id']); // . " and website_id=1");
        }

        return $this;
    }

    /*
     * After the import ran, currently the only thing done is committing the transaction and reindexing
     */
    public function afterRun()
    {
        // Commit the transaction, only if no product data is updated
        if (!self::$importPrices && !self::$importSpecialPrices && !self::$importCost && !self::$importProductStatus) {
            $this->writeAdapter->commit();
        }
        #$this->writeAdapter->query('UNLOCK TABLES');

        // Reindex routine
        if ($this->getTestMode()) {
            $this->getLogEntry()->addDebugMessage(__('Test mode enabled. Not running any reindex action.'));
            return $this;
        }
        try {
            if (!empty($this->modifiedStockItems)) {
                // Update all configurable products to in stock/out of stock based on the qty of their child products
                /*$configurableProducts = $this->productFactory->create()->getCollection()
                    ->addAttributeToFilter('type_id', Configurable::TYPE_CODE);
                foreach ($configurableProducts as $configurableProduct) {
                    $isInStock = false;
                    $childProducts = $this->configurableProduct->getUsedProducts($configurableProduct);
                    foreach ($childProducts as $childProduct) {
                        $childStockItem = $this->stockRegistry->getStockItem($childProduct->getId());
                        if ($childStockItem->getIsInStock()) {
                            $isInStock = true;
                        }
                    }
                    $this->writeAdapter->update($this->getTableName('cataloginventory_stock_item'), ['is_in_stock' => (int)$isInStock], "product_id=" . $configurableProduct->getId());
                }*/
                // Get "configurable products" and if update all associated child products, M1 sample code
                /*if (!empty($this->productTypeMap)) {
                    foreach ($this->productTypeMap as $parentProductId => $productType) {
                        if ($productType == Configurable::TYPE_CODE) {
                            $parentProduct = $this->productFactory->create()->load($parentProductId);
                            $parentStockItem = $this->stockRegistry->getStockItem($parentProductId);
                            if ($parentStockItem->getId()) {
                                $childProducts = $this->configurableProduct->getUsedProducts($parentProduct);
                                foreach ($childProducts as $childProduct) {
                                    $childStockItem = $this->stockRegistry->getStockItem($childProduct->getId());
                                    if ($parentStockItem->getQty() !== $childStockItem->getQty()) {
                                        $childStockItem->setQty($parentStockItem->getQty())->save();
                                    }
                                }
                            }
                        }
                    }
                }*/
                // Reindex, if required
                $this->getLogEntry()->addDebugMessage(__('Starting reindex.'));
                if ($this->getConfig('reindex_mode') == 'full') {
                    // Full reindex
                    $this->getLogEntry()->addDebugMessage(__('Running full reindex.'));
                    $startTime = microtime(true);
                    $indexer = $this->indexerRegistry->get(\Magento\CatalogInventory\Model\Indexer\Stock\Processor::INDEXER_ID);
                    if (!$indexer->isWorking()) {
                        $indexer->reindexAll();
                    }
                    $this->getLogEntry()->addDebugMessage(__('Full reindex completed in %1 seconds.', round(microtime(true) - $startTime)));
                } else if ($this->getConfig('reindex_mode') == 'flag_index') {
                    $this->getLogEntry()->addDebugMessage(__('Flagging stock index as reindex required.'));
                    // Flag as reindex required
                    $indexer = $this->indexerRegistry->get(\Magento\CatalogInventory\Model\Indexer\Stock\Processor::INDEXER_ID);
                    if (!$indexer->isWorking()) {
                        $indexer->getState()->setStatus(\Magento\Framework\Indexer\StateInterface::STATUS_INVALID);
                    }
                } else if ($this->getConfig('reindex_mode') == 'no_reindex') {
                    $this->getLogEntry()->addDebugMessage(__('Reindexing disabled. Not touching index at all.'));
                }
            } else {
                $this->getLogEntry()->addDebugMessage(__('No stock items modified. No reindex actions required.'));
            }

            // Refresh Magento Enterprise Edition Full Page Cache
            if ($this->getConfig('enterprise_fpc_action') == 'invalidate') {
                $this->getLogEntry()->addDebugMessage(__('Invalidating Magento Enterprise Full Page Cache.'));
                $this->cacheTypeList->invalidate('full_page');
            } else if ($this->getConfig('enterprise_fpc_action') == 'clean') {
                $this->getLogEntry()->addDebugMessage(__('Cleaning Magento Enterprise Full Page Cache.'));
                $this->pageCache->clean();
            }

            if ($this->getConfigFlag('update_low_stock_date')) {
                // Refresh "Low stock date"
                $this->resourceStock->updateLowStockDate(true);
            }

            // Reindex for price updates
            if (self::$importPrices || self::$importSpecialPrices) {
                if (!empty($this->updatedPrices) || !empty($this->updatedSpecialPrices) || !empty($this->updatedProductStatuses)) {
                    if ($this->getConfig('reindex_mode') == 'full') {
                        $this->getLogEntry()->addDebugMessage(__('Price update: Running full reindex.'));
                        $startTime = microtime(true);
                    }
                    $indexers = [
                        \Magento\Catalog\Model\Indexer\Product\Price\Processor::INDEXER_ID,
                        \Magento\Catalog\Model\Indexer\Product\Flat\Processor::INDEXER_ID,
                        \Magento\Catalog\Model\Indexer\Category\Product::INDEXER_ID
                    ];
                    foreach ($indexers as $indexerId) {
                        try {
                            $indexer = $this->indexerRegistry->get($indexerId);
                        } catch (\InvalidArgumentException $e) {
                            continue; // Flat indexer doesn't exist if disabled
                        }
                        if (!$indexer->isWorking()) {
                            $indexer->reindexAll();
                        }
                    }
                    if ($this->getConfig('reindex_mode') == 'full') {
                        $this->getLogEntry()->addDebugMessage(
                            __(
                                'Price update: Full reindex completed in %1 seconds.',
                                round(microtime(true) - $startTime)
                            )
                        );
                    }
                }
            }
        } catch (\Exception $e) {
            $this->getLogEntry()->addDebugMessage(__('Error while reindexing. Exception: %1', $e->getMessage()));
        }

        $this->eventManager->dispatch('xtento_stockimport_stockupdate_after', [
            'profile' => $this->getProfile(),
            'log' => $this->getLogEntry(),
            'modified_stock_items' => $this->modifiedStockItems
        ]);

        // End of reindexing routine
        $this->getLogEntry()->addDebugMessage(__('Done: afterRun() (Reindexer functions, ...)'));
    }
}