<?php

/**
 * Product:       Xtento_StockImport (2.2.6)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:45:21+00:00
 * Last Modified: 2016-04-26T19:47:21+00:00
 * File:          app/code/Xtento/StockImport/Model/System/Config/Source/Log/Result.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\StockImport\Model\System\Config\Source\Log;

use Magento\Framework\Option\ArrayInterface;

/**
 * @codeCoverageIgnore
 */
class Result implements ArrayInterface
{
    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $values = [
            \Xtento\StockImport\Model\Log::RESULT_NORESULT => __('No Result'),
            \Xtento\StockImport\Model\Log::RESULT_SUCCESSFUL => __('Successful'),
            \Xtento\StockImport\Model\Log::RESULT_WARNING => __('Warning'),
            \Xtento\StockImport\Model\Log::RESULT_FAILED => __('Failed')
        ];
        return $values;
    }
}
