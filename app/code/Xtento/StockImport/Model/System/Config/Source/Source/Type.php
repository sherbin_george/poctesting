<?php

/**
 * Product:       Xtento_StockImport (2.2.6)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:45:21+00:00
 * Last Modified: 2016-04-26T19:47:20+00:00
 * File:          app/code/Xtento/StockImport/Model/System/Config/Source/Source/Type.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\StockImport\Model\System\Config\Source\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * @codeCoverageIgnore
 */
class Type implements ArrayInterface
{
    /**
     * @var \Xtento\StockImport\Model\Source
     */
    protected $sourceModel;

    /**
     * @param \Xtento\StockImport\Model\Source $sourceModel
     */
    public function __construct(\Xtento\StockImport\Model\Source $sourceModel)
    {
        $this->sourceModel = $sourceModel;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->sourceModel->getTypes();
    }

    public function getName($type)
    {
        foreach ($this->toOptionArray() as $optionType => $name) {
            if ($optionType == $type) {
                return $name;
            }
        }
        return '';
    }
}
