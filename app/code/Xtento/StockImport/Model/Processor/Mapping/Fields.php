<?php

/**
 * Product:       Xtento_StockImport (2.2.6)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:45:21+00:00
 * Last Modified: 2017-05-24T14:48:48+00:00
 * File:          app/code/Xtento/StockImport/Model/Processor/Mapping/Fields.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\StockImport\Model\Processor\Mapping;

use \Xtento\StockImport\Model\Import\Entity\Stock;

class Fields extends AbstractMapping
{
    protected $importFields = null;
    protected $mappingType = 'fields';

    /*
     * [
     * 'label'
     * 'disabled'
     * 'tooltip'
     * 'default_value_disabled'
     * 'default_values'
     * ]
     */
    public function getMappingFields()
    {
        if ($this->importFields !== null) {
            return $this->importFields;
        }

        $importFields = [
            'item_info' => [
                 'label' => __('-- Product Information -- '),
                 'disabled' => true
             ],
             'product_identifier' => [
                 'label' => __('Product Identifier *'),
                 'default_value_disabled' => true
             ],
             'qty' => ['label' => __('Qty In Stock')],
             'stock_settings' => [
                 'label' => __('-- Optional: Stock Settings -- '),
                 'disabled' => true
             ],
             'manage_stock' => [
                 'label' => __('Manage Stock'),
                 'default_values' => $this->getDefaultValues('yesno')
             ],
             'is_in_stock' => [
                 'label' => __('Stock Status'),
                 'default_values' => $this->getDefaultValues('stock_status')
             ],
             'notify_stock_qty' => [
                 'label' => __('Notify Stock Qty (notify_stock_qty)')
             ],
             'backorders' => [
                 'label' => __('Backorders'),
                 'default_values' => $this->getDefaultValues('backorders')
             ],
             /*
             'custom_fields' => array(
                 'label' => '-- Custom Import Fields -- ',
                 'disabled' => true
             ),
             //'custom1' => array('label' => 'Custom Data 1'),
             //'custom2' => array('label' => 'Custom Data 2'),
             */
        ];


        if (Stock::$importPrices || Stock::$importSpecialPrices || Stock::$importCost) {
            $importFields['price_settings'] = [
                'label' => __('-- Beta: Price Import -- '),
                'disabled' => true
            ];
        }

        if (Stock::$importPrices) {
            $importFields['price'] = ['label' => __('Product Price')];
        }

        if (Stock::$importSpecialPrices) {
            $importFields['special_price'] = ['label' => __('Product Special Price')];
        }

        if (Stock::$importCost) {
            $importFields['cost'] = ['label' => __('Product Cost')];
        }

        if ($this->entityHelper->getMultiWarehouseSupport()) {
            $importFields['stock_id_settings'] = [
                'label' => __('-- Warehouse (Stock ID) --'),
                'disabled' => true
            ];
            $importFields['stock_id'] = ['label' => __('Stock ID')];
        }

        if (Stock::$importProductStatus) {
            $importFields['product_settings'] = [
                'label' => __('-- Beta: Product Update --'),
                'disabled' => true
            ];
            $importFields['status'] = ['label' => __('Product Status (Enabled/Disabled)')];
        }


        // Custom event to add fields
        $this->eventManager->dispatch(
            'xtento_stockimport_mapping_get_fields',
            [
                'importFields' => &$importFields,
            ]
        );

        // Feature: merge fields from custom/fields.php so custom fields can be added

        $this->importFields = $importFields;

        return $this->importFields;
    }

    public function formatField($fieldName, $fieldValue)
    {
        if ($fieldName == 'qty') {
            if (!is_numeric($fieldValue)) {
                $fieldValue = trim(strtolower($fieldValue));
                if ($fieldValue === 'yes' || $fieldValue === 'ja' || $fieldValue === 'in stock' || $fieldValue === 'available' || $fieldValue === 'y' || $fieldValue === 'j') {
                    $fieldValue = 1000;
                }
                if ($fieldValue === 'low stock') {
                    $fieldValue = 5;
                }
                if ($fieldValue === 'no' || $fieldValue === 'nein' || $fieldValue === 'out of stock' || $fieldValue === 'no stock' || $fieldValue === 'oos' || $fieldValue === 'n') {
                    $fieldValue = 0;
                }
            }
            if ($fieldValue[0] == '+') {
                $fieldValue = sprintf("%+.4f", $fieldValue);
            } else if ($fieldValue[0] == '-') {
                $fieldValue = str_replace("+", "-", sprintf("%+.4f", $fieldValue)); // Hack as sprintf doesn't support -
            } else {
                $fieldValue = str_replace(",", ".", $fieldValue);
                $fieldValue = preg_replace("/[^0-9.]/", "", $fieldValue);
                $fieldValue = sprintf("%.4f", $fieldValue);
            }
        }
        if ($fieldName == 'is_in_stock' && $fieldValue !== '') {
            if (!is_numeric($fieldValue)) {
                $fieldValue = trim(strtolower($fieldValue));
            }
            // Detect "In Stock" value using this criteria: (is_in_stock in database only accepts 0 or 1)
            if ($fieldValue === 'true' || $fieldValue === 'yes' || $fieldValue === 'ja' || $fieldValue === 'in stock' || $fieldValue === 'available' || (is_numeric($fieldValue) && $fieldValue > 0)
            ) {
                $fieldValue = 1;
            } else {
                $fieldValue = 0;
            }
        }
        if ($fieldName == 'backorders' && $fieldValue !== '') {
            if (!is_numeric($fieldValue)) {
                $fieldValue = trim(strtolower($fieldValue));
            }
            // Detect "In Stock" value using this criteria: (is_in_stock in database only accepts 0 or 1)
            if ($fieldValue === 'true' || $fieldValue === 'yes' || $fieldValue === 'ja' || $fieldValue === '1') {
                $fieldValue = 1;
            } else if ($fieldValue != 2) { // "Notify customer"
                $fieldValue = 0;
            }
        }
        if ($fieldName == 'manage_stock' && $fieldValue !== '') {
            if (!is_numeric($fieldValue)) {
                $fieldValue = trim(strtolower($fieldValue));
            }
            // Detect "Manage Stock" value using this criteria: (manage_stock in database only accepts 0 or 1)
            if ($fieldValue == 'true' || $fieldValue == 'yes' || $fieldValue == 1) {
                $fieldValue = 1;
            } else {
                $fieldValue = 0;
            }
        }
        if ($fieldName == 'price' || $fieldName == 'special_price' || $fieldName == 'cost') {
            if (strstr($fieldValue, '.') && strstr($fieldValue, ',')) {
                // Parse a number in format 1.234,56
                $fieldValue = str_replace('.', '', $fieldValue);
                $fieldValue = str_replace(',', '.', $fieldValue);
            } else if (strstr($fieldValue, ',')) {
                // Parse a number in format 1234,56
                $fieldValue = str_replace(',', '.', $fieldValue);
            }
        }
        if ($fieldName == 'product_identifier') {
            $fieldValue = trim($fieldValue);
        }
        return $fieldValue;
    }
}
