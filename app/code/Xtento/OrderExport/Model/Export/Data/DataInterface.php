<?php

/**
 * Product:       Xtento_OrderExport (2.4.7)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:43:43+00:00
 * Last Modified: 2015-09-01T18:15:56+00:00
 * File:          app/code/Xtento/OrderExport/Model/Export/Data/DataInterface.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Model\Export\Data;

interface DataInterface {
    public function getExportData($entityType, $collectionItem);
    public function getConfiguration();
}