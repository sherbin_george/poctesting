<?php

/**
 * Product:       Xtento_ProductExport (2.3.8)
 * ID:            2YQzcu16EsNwKXylVZPcV5Vum24nrIgnXgid+rI5KmU=
 * Packaged:      2017-08-27T22:52:47+00:00
 * Last Modified: 2016-04-14T15:37:35+00:00
 * File:          app/code/Xtento/ProductExport/Block/Adminhtml/Log.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\ProductExport\Block\Adminhtml;

class Log extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('add');
    }
}
