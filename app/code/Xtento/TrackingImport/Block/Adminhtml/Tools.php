<?php

/**
 * Product:       Xtento_TrackingImport (2.3.6)
 * ID:            SNxx1kctIgfzyShtMUQqkSC5mPy1zBgRjdYkNLnd3dA=
 * Packaged:      2018-01-07T23:40:39+00:00
 * Last Modified: 2016-03-11T17:40:19+00:00
 * File:          app/code/Xtento/TrackingImport/Block/Adminhtml/Tools.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\TrackingImport\Block\Adminhtml;

class Tools extends \Magento\Backend\Block\Template
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Xtento_TrackingImport::tools.phtml');
    }
}
