<?php
namespace BalanceInternet\MagestoreMegamenu\Block;

/**
 * Class Megamenu
 * NOTE: Rewritten function to resolve issue with cache on multiple websites
 *
 * @package BalanceInternet\MagestoreMegamenu\Block
 */
class Megamenu extends \Magestore\Megamenu\Block\Megamenu
{
    /**
     * Megamenu construct function.
     *
     * @return void
     */
    protected function _construct()
    {
        if ($this->getConfig('megamenu/general/ids') && $this->getConfig('megamenu/general/cache')) {

            $this->_typeListInterface->cleanType('block_html');
            $this->_typeListInterface->cleanType('full_page');

            $this->_configResoure->saveConfig('megamenu/general/ids', '0', 'default', 0);
            $this->_typeListInterface->cleanType('config');
            $this->_typeListInterface->cleanType('block_html');
            $this->_typeListInterface->cleanType('full_page');
        }
    }
}