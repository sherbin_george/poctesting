<?php
namespace BalanceInternet\MagestoreMegamenu\Plugin\Model;

use Magestore\Megamenu\Model\Megamenu as OriginalMegamenu;

/**
 * Class Megamenu
 *
 * @package BalanceInternet\MagestoreMegamenu\Plugin\Model
 */
class Megamenu
{
    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\CollectionFactory
     */
    private $blockCollectionFactory;

    /**
     * Megamenu constructor.
     *
     * @param \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory
     */
    public function __construct(\Magento\Cms\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory)
    {
        $this->blockCollectionFactory = $blockCollectionFactory;
    }

    /**
     * Rewrite the function deleteItem() to check block existence before deleting it
     *
     * @param OriginalMegamenu $subject
     * @param \Closure $closure
     *
     * @return OriginalMegamenu
     */
    public function aroundDeleteItem(OriginalMegamenu $subject, \Closure $closure)
    {
        if ($subject->getMenuType() != OriginalMegamenu::ANCHOR_TEXT) {
            $blocks = $this->blockCollectionFactory->create()
                ->addFieldToFilter('identifier', array('like' => 'mega_item_' . $subject->getId() . '_store_%'));
            foreach ($blocks as $block) {
                $block->delete();
            }
        }

        return $subject;
    }
}
