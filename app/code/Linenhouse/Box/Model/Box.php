<?php

namespace Linenhouse\Box\Model;

use Balance\Box\Api\Data\BoxInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Box extends \Balance\Box\Model\Box
{
    /**
     *
     * Contains unique ID for the instance
     *
     * @var null|string
     */
    protected $_uniqId = null;

    /**
     * Counter for unique Id
     *
     * @var int
     */
    static protected $_uniqIdIterator = 0;

    /**
     * Get unique id
     *
     * @return string
     */
    public function getUniqId() {
        if (is_null($this->_uniqId)) {
            self::$_uniqIdIterator++;
            $this->_uniqId = $this->getIdentifier() . '-' . uniqid() . '-' . self::$_uniqIdIterator;
        }
        return $this->_uniqId;
    }
}
