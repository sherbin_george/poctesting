<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linenhouse\Cms\Helper;

/**
 * Fixture helper
 *
 * @package Linenhouse\Cms\Helper
 * @author  Balance Dev <dev@balanceinternet.com.au>
 */
class Fixture extends \Balance\Cms\Helper\Fixture
{
    /**#@+
     * Constants defined for keys of data array
     */
    const MODULE_NAME = 'Linenhouse_Cms';
    const FIXTURES_MODE = true;
    const DEBUG_MODE = false;
}
