<?php

namespace Linenhouse\Data\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Linenhouse\Data\Model\Cms\Page\HierarchyAssigner;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Class UpgradeData
 * @package Linenhouse\Data\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $_eavSetupFactory;


    protected $_hierarchyAssigner;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param HierarchyAssigner $hierarchyAssigner
     */
    public function __construct(EavSetupFactory $eavSetupFactory, HierarchyAssigner $hierarchyAssigner)
    {
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_hierarchyAssigner = $hierarchyAssigner;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->updateManufacturerAttribute($setup);
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->_hierarchyAssigner->assignHierarchyToPage(
                'contact-us',
                'info'
            );
        }

        /**
         * Because upgrade which creates contact-us page
         * runs after assignation
         */
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->_hierarchyAssigner->assignHierarchyToPage(
                'contact-us',
                'info'
            );
        }

        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $this->createSizeGuideAttribute($setup);
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->createPromoAttribute($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    private function updateManufacturerAttribute(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->getEavSetup($setup);

        $eavSetup->updateAttribute(
            Product::ENTITY,
            'manufacturer',
            'used_in_product_listing',
            1
        );
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    private function createSizeGuideAttribute(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->getEavSetup($setup);

        $eavSetup->removeAttribute(Product::ENTITY, 'size_guide');
        if (!$eavSetup->getAttribute(Product::ENTITY, 'size_guide')) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                'size_guide',
                [
                    'type'              => 'text',
                    'backend'           => '',
                    'frontend'          => '',
                    'label'             => 'Size Guide',
                    'input'             => 'textarea',
                    'class'             => '',
                    'source'            => '',
                    'global'            => ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible'           => true,
                    'required'          => false,
                    'default'           => '',
                    'searchable'        => false,
                    'filterable'        => false,
                    'visible_on_front'  => true,
                    'wysiwyg_enabled'   => true,
                    'unique'            => false,
                ]
            );

            $this->addAttributeToSet(
                $setup,
                'size_guide',
                'Default',
                'Product Details',
                30
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    private function createPromoAttribute(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->getEavSetup($setup);

        $eavSetup->removeAttribute(Product::ENTITY, 'promo');
        if (!$eavSetup->getAttribute(Product::ENTITY, 'promo')) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                'promo',
                [
                    'type'              => 'text',
                    'backend'           => '',
                    'frontend'          => '',
                    'label'             => 'Promo',
                    'input'             => 'textarea',
                    'class'             => '',
                    'source'            => '',
                    'global'            => ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible'           => true,
                    'required'          => false,
                    'default'           => '',
                    'searchable'        => false,
                    'filterable'        => false,
                    'visible_on_front'  => true,
                    'wysiwyg_enabled'   => false,
                    'unique'            => false,
                ]
            );

            $this->addAttributeToSet(
                $setup,
                'promo',
                'Default',
                'Product Details',
                31
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @return \Magento\Eav\Setup\EavSetup
     */
    private function getEavSetup(ModuleDataSetupInterface $setup)
    {
        return $this->_eavSetupFactory->create(['setup' => $setup]);
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param string $attribute
     * @param string $attributeSet
     * @param string $group
     * @param int $position
     * @return void
     */
    private function addAttributeToSet(
        ModuleDataSetupInterface $setup,
        $attribute,
        $attributeSet,
        $group,
        $position = 0
    )
    {
        $eavSetup = $this->getEavSetup($setup);

        $entityTypeId = $eavSetup->getEntityTypeId(Product::ENTITY);
        $attributeSetId = $eavSetup->getAttributeSetId($entityTypeId, $attributeSet);

        $attribute = $eavSetup->getAttribute($entityTypeId, $attribute);
        if ($attribute && isset($attribute['attribute_id'])) {
            $eavSetup->addAttributeToGroup(
                $entityTypeId,
                $attributeSetId,
                $group,
                $attribute['attribute_id'],
                $position
            );
        }
    }
}