<?php

namespace Linenhouse\Data\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 * @package Linenhouse\Data\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->_installAuRegions($setup);

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    protected function _installAuRegions(ModuleDataSetupInterface $setup)
    {
        $connection = $setup->getConnection();

        $states = [
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'VIC',
                'default_name'  => 'Victoria'
            ],
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'TAS',
                'default_name'  => 'Tasmania'
            ],
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'NT',
                'default_name'  => 'Northern Territory'
            ],
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'WA',
                'default_name'  => 'Western Australia'
            ],
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'SA',
                'default_name'  => 'South Australia'
            ],
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'ACT',
                'default_name'  => 'Australia Capital Territory'
            ],
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'QLD',
                'default_name'  => 'Queensland'
            ],
            [
                'region_id'     => null,
                'country_id'    => 'AU',
                'code'          => 'NSW',
                'default_name'  => 'New South Wales'
            ],
        ];

        foreach ($states as $state) {
            $select = $connection->select()->from(
                $setup->getTable('directory_country_region'),
                'COUNT(*)'
            )->where('country_id=?', $state['country_id']
            )->where('code=?', $state['code']);

            if ($connection->fetchOne($select) > 0) {
                continue;
            }

            $connection->insertForce(
                $setup->getTable('directory_country_region'),
                $state
            );
        }
    }
}