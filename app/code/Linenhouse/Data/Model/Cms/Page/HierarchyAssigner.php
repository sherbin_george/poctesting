<?php

namespace Linenhouse\Data\Model\Cms\Page;

use Magento\Cms\Model\ResourceModel\Page\CollectionFactory as PageCollectionFactory;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class HierarchyAssigner
 * @package Linenhouse\Data\Model\Cms\Page
 */
class HierarchyAssigner
{
    /**
     * @var PageCollectionFactory
     */
    protected $pageCollectionFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * HierarchyAssigner constructor.
     * @param PageCollectionFactory $pageCollectionFactory
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        PageCollectionFactory $pageCollectionFactory,
        ObjectManagerInterface $objectManager
    )
    {
        $this->pageCollectionFactory = $pageCollectionFactory;
        $this->objectManager = $objectManager;
    }

    /**
     * Assign hierarchy to page with relevant urlKeys
     *
     * @param string $pageUrlKey
     * @param string $hierarchyUrlKey
     * @return void
     */
    public function assignHierarchyToPage($pageUrlKey, $hierarchyUrlKey)
    {
        $page = $this->getPageByIdentifier($pageUrlKey);

        if ($page->getId()) {
            $node = $this->getNodeDefaultScope();
            $rootNode = $node->getNodesCollection()
                ->addFieldToSelect('*')
                ->addFilterToMap('identifier', 'main_table.identifier')
                ->addFieldToFilter('identifier', $hierarchyUrlKey)
                ->getFirstItem();

            if ($rootNode->getNodeId()) {
                $childNodes = $node->getNodesCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('parent_node_id', $rootNode->getNodeId());

                if ($childNodes->count()) {
                    $node->appendPageToNodes($page, [$rootNode->getNodeId() => $childNodes->count()]);

                    $i = 0;
                    foreach ($childNodes->toArray()['items'] as $childNode) {
                        $node->getResource()->updateSortOrder($childNode['node_id'], $i);
                    }
                }
            }
        }
    }

    /**
     * @param string $identifier
     * @return \Magento\Cms\Model\Page
     */
    private function getPageByIdentifier($identifier)
    {
        return $this->pageCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('identifier', $identifier)
            ->getFirstItem();
    }

    /**
     * @return \Magento\VersionsCms\Model\Hierarchy\Node
     */
    private function getNodeDefaultScope()
    {
        $node = $this->objectManager->create(
            \Magento\VersionsCms\Model\Hierarchy\Node::class,
            ['data' => ['scope' => 'default', 'scope_id' => 0]]
        );

        return $node;
    }
}