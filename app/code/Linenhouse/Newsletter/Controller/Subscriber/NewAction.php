<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category Linenhouse
 * @package Linenhouse_Newsletter
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */
namespace Linenhouse\Newsletter\Controller\Subscriber;

use Linenhouse\Newsletter\Exception\DuplicateSubscriptionException;
use Magento\Customer\Api\AccountManagementInterface as CustomerAccountManagement;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Newsletter\Model\Subscriber;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory;

/**
 * Class NewActionPlugin
 *
 * @category Linenhouse
 * @package Linenhouse_Newsletter
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */
class NewAction extends \Magento\Newsletter\Controller\Subscriber\NewAction
{
    /**
     * @var CustomerAccountManagement
     */
    protected $customerAccountManagement;
    
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    
    /**
     * @var \Magento\Newsletter\Model\Subscriber
     */
    protected $_subscriberFactoryObj;
    
    /**
     * Initialize dependencies.
     *
     * @param Context                                          $context
     * @param SubscriberFactory                                $subscriberFactory
     * @param Session                                          $customerSession
     * @param StoreManagerInterface                            $storeManager
     * @param CustomerUrl                                      $customerUrl
     * @param CustomerAccountManagement                        $customerAccountManagement
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        SubscriberFactory $subscriberFactory,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        CustomerUrl $customerUrl,
        CustomerAccountManagement $customerAccountManagement,
        JsonFactory $resultJsonFactory
    ) {
        $this->customerAccountManagement = $customerAccountManagement;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->messageManager = $context->getMessageManager();
        
        parent::__construct(
            $context,
            $subscriberFactory,
            $customerSession,
            $storeManager,
            $customerUrl,
            $customerAccountManagement
        );
    }
    
    /**
     * @return \Magento\Newsletter\Model\Subscriber
     */
    protected function _getSubscriberFactoryObject()
    {
        if ($this->_subscriberFactoryObj === null) {
            $this->_subscriberFactoryObj = $this->_subscriberFactory->create();
        }
        
        return $this->_subscriberFactoryObj;
    }
    
    /**
     * Validates that the email address isn't being used by a different account.
     *
     * @param string $email
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function validateEmailAvailable($email)
    {
        parent::validateEmailAvailable($email);
        
        $subscribedEmail = $this->_getSubscriberFactoryObject()->getResource()->loadByEmail($email);
    
        if (!empty($subscribedEmail)) {
            if ($this->_customerSession->getCustomerDataObject()->getEmail() == $email) {
                throw new DuplicateSubscriptionException(
                    __('Thanks, but you\'ve already signed up to our email newsletter.')
                );
            } else {
                throw new DuplicateSubscriptionException(
                    __('This email address is already subscribed.')
                );
            }
        }
    }
    
    /**
     * Retrieve available Order fields list
     *
     * @return string
     */
    public function execute()
    {
        $response = [];
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $email = (string) $this->getRequest()->getPost('email');
            
            try {
                $this->validateEmailFormat($email);
                $this->validateGuestSubscription();
                $this->validateEmailAvailable($email);
                
                $status = $this->_getSubscriberFactoryObject()->subscribe($email);
                if ($status == Subscriber::STATUS_NOT_ACTIVE) {
                    $response = [
                        'success' => true,
                        'message' => __('The confirmation request has been sent.'),
                    ];
                } else {
                    $response = [
                        'success' => true,
                        'message' => __('Thank you for your subscription.'),
                    ];
                }
            } catch (DuplicateSubscriptionException $e) {
                $response = [
                    'success' => false,
                    'message' => __($e->getMessage()),
                ];
            } catch (LocalizedException $e) {
                $response = [
                    'success' => false,
                    'message' => __($e->getMessage()),
                ];
            } catch (\Exception $e) {
                $response = [
                    'success' => false,
                    'message' => __('Something went wrong with the subscription.'),
                ];
            }
        }
        
        return $this->resultJsonFactory->create()->setData($response);
    }
}
