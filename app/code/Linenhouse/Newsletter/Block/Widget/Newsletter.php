<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category Linenhouse
 * @package Linenhouse_Newsletter
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */
namespace Linenhouse\Newsletter\Block\Widget;

use Magento\Widget\Block\BlockInterface;

/**
 * Newsletter subscribe Block Widget
 *
 * @category Linenhouse
 * @package Linenhouse_Newsletter
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */
class Newsletter extends \Magento\Newsletter\Block\Subscribe implements BlockInterface
{
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $_jsonEncoder;
    
    /**
     * Ajax constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context     Context model.
     * @param \Magento\Framework\Json\EncoderInterface         $jsonEncoder Json encoder.
     * @param array                                            $data        Data array.
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        $this->_jsonEncoder = $jsonEncoder;
        parent::__construct($context, $data);
    }
    
    /**
     * Returns encoded default options data.
     *
     * @return string
     */
    public function getDefaultOptions()
    {
        return $this->_jsonEncoder->encode(
            [
                'defaultErrorMessage' => __('Something went wrong with the subscription.'),
                'messagesContainer' => '<div>',
                'messagesContainerId' => 'subscription-message',
                'messageHolder'=> '<div>',
            ]
        );
    }
}
