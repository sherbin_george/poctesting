/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category Linenhouse
 * @package Linenhouse_Newsletter
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */
define([
    'jquery'
], function ($) {
    'use strict';
    
    /**
     * Newsletter ajax subscribe widget.
     */
    $.widget('Linenhouse.newsletterAjaxSubscribe', {
        /**
         * Widget options list.
         * May be overridden when widget is instantiated.
         */
        options: {
            default: []
        },
        
        /**
         * Initializes widget instance.
         *
         * @return void
         */
        _create: function() {
            this.element.on('submit', this._processFormSubmit.bind(this));
        },
        
        /**
         * Submits the form, processes the result.
         *
         * @param {object} event Event instance.
         *
         * @return void
         */
        _processFormSubmit: function (event) {
            event.preventDefault();
            
            var self = this;
            if (!self._isFormValid()) {
                self._hideMessage();
                return;
            }
    
            try{
                $.ajax({
                    url: self._getFormAction(),
                    dataType: 'json',
                    type: self._getFromHttpMethod(),
                    cache: false,
                    data: self._getFormData(),
                    context: self,
                    /**
                     * A pre-request callback
                     */
                    beforeSend: function () {
                        self._hideMessage();
                        self._disableFormElements();
                    },
                    /**
                     * Called when request succeeds
                     *
                     * @param {Object} response
                     */
                    success: function (response){
                        if (response === null
                            || typeof response.success === 'undefined'
                            || typeof response.message === 'undefined'
                        ) {
                            self._displayMessage(self._getOption('defaultErrorMessage'), 'message-error error message');
                            return;
                        }

                        if (!response.success) {
                            self._displayMessage(response.message, 'message-error error message');
                            return;
                        }
    
                        self._displayMessage(response.message, 'message-success success message');
                        self._resetForm();
                    },
                    /**
                     * Called when request finishes
                     */
                    complete: function () {
                        self._enableFormElements();
                    }
                });
            } catch (e){
                self._displayMessage(self._getOption('defaultErrorMessage'), 'message-error error message');
            }
        },
        
        /**
         * Determines whether form is valid.
         *
         * @return {boolean}
         */
        _isFormValid: function() {
            var validator = this.element.data('validator');
            if (typeof validator === 'undefined') {
                return true;
            }
            
            return validator.form();
        },
        
        /**
         * Returns form action.
         *
         * @return {string}
         */
        _getFormAction: function() {
            return this.element.attr('action');
        },
        
        /**
         * Returns form http method.
         *
         * @return {string}
         */
        _getFromHttpMethod: function() {
            return this.element.attr('method');
        },
        
        /**
         * Returns serialized form data.
         *
         * @return {string}
         */
        _getFormData: function() {
            return this.element.serialize();
        },
        
        /**
         * Displays the message after the form.
         *
         * @param {array} messages Messages to be displayed.
         * @param {string} messageType Message type.
         *
         * @return void
         */
        _displayMessage: function(messages, messageType) {
            var messageContainer = $(this._getOption('messagesContainer'))
                .attr('id', this._getOption('messagesContainerId'))
                .addClass(messageType);
            
            if (typeof messages !== 'object') {
                messageContainer.append(
                    $(this._getOption('messageHolder')).text(messages)
                );
            } else {
                var messageHolder = this._getOption('messageHolder');
                $.each(messages, function() {
                    messageContainer.append(
                        $(messageHolder).text(this)
                    );
                });
            }
            
            this.element.parent().append(messageContainer);
        },
        
        /**
         * Hides the message.
         *
         * @return void
         */
        _hideMessage: function() {
            this.element.parent().find('#' + this._getOption('messagesContainerId')).remove();
        },
        
        /**
         * Disables form elements.
         *
         * @return void
         */
        _disableFormElements: function() {
            this.element.find('input, button').prop('disabled', true);
        },
        
        /**
         * Enables form elements.
         *
         * @return void
         */
        _enableFormElements: function() {
            this.element.find('input, button').prop('disabled', false);
        },
        
        /**
         * Resets the form(s).
         *
         * @return void
         */
        _resetForm: function() {
            $.each(this.element, function() {
                this.reset();
            });
        },
        
        /**
         * Returns specified option value.
         * NOTE: widget options have higher priority than default one.
         *
         * @param {string} key Option name.
         *
         * @return {string}
         */
        _getOption: function(key) {
            return typeof this.options[key] !== 'undefined'
                ? this.options[key]
                : typeof this.options.default[key] !== 'undefined'
                    ? this.options.default[key]
                    : '';
        }
    });
    
    return $.Linenhouse.newsletterAjaxSubscribe;
});
