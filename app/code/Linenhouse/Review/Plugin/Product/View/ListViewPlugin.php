<?php

namespace Linenhouse\Review\Plugin\Product\View;

use Magento\Catalog\Model\Product;
use Magento\Review\Block\Product\View\ListView;
use Magento\Review\Model\ReviewFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ListViewPlugin
 * @package Linenhouse\Review\Plugin\Product\View
 */
class ListViewPlugin
{
    /**
     * @var ReviewFactory
     */
    protected $reviewFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ListViewPlugin constructor.
     * @param ReviewFactory $reviewFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(ReviewFactory $reviewFactory, StoreManagerInterface $storeManager)
    {
        $this->reviewFactory = $reviewFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Assign rating summary for product in registry
     *
     * @param $subject
     * @param Product $product
     * @return mixed
     */
    public function afterGetProduct(ListView $subject, $product)
    {
        if (!$product->getRatingSummary()) {
            $this->reviewFactory->create()->getEntitySummary($product, $this->storeManager->getStore()->getId());
        }

        return $product;
    }
}