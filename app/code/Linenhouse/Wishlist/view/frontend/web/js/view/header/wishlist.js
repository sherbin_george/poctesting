define([
    'Magento_Wishlist/js/view/wishlist',
    'ko',
    'jquery'
], function (Component, ko, $) {
    return Component.extend({
        isCustomerLoggedIn: ko.observable(false),

        initialize: function () {
            this._super();
            this._getIsCustomerLoggedIn();
        },

        _getIsCustomerLoggedIn: function () {
            var self = this;

            $.ajax({
                url: this.wishlistCustomerIsLoggedInUrl,
                type: 'POST'
            }).fail(function (response) {
            }).done(function (response) {
                self.isCustomerLoggedIn(response.is_customer_logged_in);
            });
        },

        getWishListCounter: function () {
            var counter = this.wishlist().counter;

            return ((counter = parseInt(counter))) ? counter : '0';
        },

        getActionUrl: function () {
            return (this.isCustomerLoggedIn()) ? this.customerWishlistUrl : this.customerAccountLoginUrl;
        }
    });
});