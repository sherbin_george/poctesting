<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Wishlist\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class WishlistItemCollectionProductsObserver implements ObserverInterface
{
    /**
     * @var ProductFactory
     */
    protected $_productloader;

    /**
     * WishlistItemCollectionProductsObserver constructor.
     *
     * @param ProductFactory $_productloader
     */
    public function __construct(\Magento\Catalog\Model\ProductFactory $_productloader)
    {
        $this->_productloader = $_productloader;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $observer->getData('product_collection');
        foreach ($productCollection->getItems() as $item) {
            $product = $this->_productloader->create()->load($item->getId());
            $item->setData($product->getData());
        }
    }
}