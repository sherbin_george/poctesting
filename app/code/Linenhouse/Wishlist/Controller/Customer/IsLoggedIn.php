<?php

namespace Linenhouse\Wishlist\Controller\Customer;

use Magento\Customer\Model\Session;
use Magento\Framework\ {
    App\Action\Context, Controller\Result\JsonFactory as ResultJsonFactory
};

/**
 * Class IsLoggedIn
 * @package Linenhouse\Wishlist\Controller\Customer
 */
class IsLoggedIn extends \Magento\Framework\App\Action\Action
{
    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * IsLoggedIn constructor.
     * @param Context $context
     * @param ResultJsonFactory $resultJsonFactory
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        ResultJsonFactory $resultJsonFactory,
        Session $customerSession
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customerSession = $customerSession;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();

        return $result->setData([
            'is_customer_logged_in' => $this->customerSession->isLoggedIn()
        ]);
    }
}