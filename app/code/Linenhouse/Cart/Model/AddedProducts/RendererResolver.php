<?php

namespace Linenhouse\Cart\Model\AddedProducts;

use Magento\Framework\Registry;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Catalog\Model\Product\Type;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Bundle\Model\Product\Type as BundleType;

/**
 * Class RendererResolver
 * @package Linenhouse\Cart\Model\AddedProducts
 */
class RendererResolver
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var array
     */
    protected $rendererPool = [
        Grouped::TYPE_CODE      => \Linenhouse\Cart\Block\Grouped\Product::class,
        Type::TYPE_SIMPLE       => \Linenhouse\Cart\Block\Simple\Product::class,
        BundleType::TYPE_CODE   => \Linenhouse\Cart\Block\Bundle\Product::class
    ];

    /**
     * RendererResolver constructor.
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Resolve block class renderer name
     * depends on product type.
     * This block display added products to cart in popup
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function resolve($product)
    {
        /**
         * Because of Bss_ConfigurableMatrixView module
         * which able to add multiple simple products of
         * the configurable
         */
        if (in_array($product->getTypeId(), [Configurable::TYPE_CODE])) {
            return $this->_resolve();
        }

        $productRendererType = isset($this->rendererPool[$product->getTypeId()]) ?
            $product->getTypeId() : Type::TYPE_SIMPLE;

        return $this->rendererPool[$productRendererType];
    }

    /**
     * @return null|
     */
    protected function _resolve()
    {
        return ($this->registry->registry('matrixview_added_products')) ?
            \Linenhouse\ConfigurableMatrixView\Block\Cart\Product::class :
            \Linenhouse\Cart\Block\Configurable\Product::class;
    }
}