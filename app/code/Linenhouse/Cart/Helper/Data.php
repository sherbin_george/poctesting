<?php

namespace Linenhouse\Cart\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package Linenhouse\Cart\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Selling block product limit config path
     *
     * @const string
     */
    const SELLING_BLOCK_PRODUCTS_LIMIT_PATH = 'amasty_cart/selling/limit';

    /**
     * Retrieve selling block product limit
     *
     * @return int
     */
    public function getSellingProductsLimit()
    {
        return (int)$this->scopeConfig->getValue(
            self::SELLING_BLOCK_PRODUCTS_LIMIT_PATH,
            ScopeInterface::SCOPE_STORE
            );
    }
}