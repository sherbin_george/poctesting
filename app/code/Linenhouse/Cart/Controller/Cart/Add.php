<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Cart
 * @package Override Linenhouse_Cart
 */

namespace Linenhouse\Cart\Controller\Cart;

use Amasty\Cart\Model\Source\Option;
use Magento\Framework\App\ResponseInterface;

class Add extends \Amasty\Cart\Controller\Cart\Add
{
    /**
     * @return void|ResponseInterface
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            $message = __('We can\'t add this item to your shopping cart right now. Please reload the page.');
            return $this->addToCartResponse($message, 0);
        }

        $params = $this->getRequest()->getParams();
        $product = $this->_initProduct();

        /**
         * Check product availability
         */
        if (!$product) {
            $message = __('We can\'t add this item to your shopping cart right now.');
            return $this->addToCartResponse($message, 0);
        }

        /**
         * Start rewrite
         */
        /** @var \Bss\ConfigurableMatrixView\Helper\Data $matrixViewHelper */
        $matrixViewHelper = $this->_objectManager->get('Bss\ConfigurableMatrixView\Helper\Data');
        $registry = $this->_getRegistry();
        $matrixViewResponse = $registry->registry('matrixview_addtocart_response');
        $configurableRequireOption = isset($params['require_option']) ? true:false;

        if ($matrixViewHelper->isEnabled() &&
            $product->getConfigurableMatrixView() &&
            !$matrixViewResponse &&
            !$configurableRequireOption
//            isset($params['bss_super_attribute'])
        ) {
            $registry->register('matrixview_parent_product', $product);
            $this->_forward('add', 'cart', 'linenhousematrixview');

            return;
        } elseif ($matrixViewResponse &&
            isset($matrixViewResponse['errors']) &&
            !empty($matrixViewResponse['errors']) &&
            !$configurableRequireOption
        ) {

            return $this->addToCartResponse(__('ConfigurableMatrixView error'), 0, $matrixViewResponse['errors']);
        } elseif ($matrixViewResponse && !$configurableRequireOption) {
            $message = $this->_getParentProductAddedMessage($product);
            $message = $this->getProductAddedMessage($product, $message);

            return $this->addToCartResponse($message, 1);
        }
        /**
         * End rewrite
         */

        try {
            $showOptionsResponse = false;
            switch ($product->getTypeId()) {
                case 'configurable':
                    $attributesCount = $product->getTypeInstance()->getConfigurableAttributes($product)->count();
                    $superParamsCount = (array_key_exists('super_attribute', $params)) ?
                        count($params['super_attribute']) : 0;
                    if ($attributesCount != $superParamsCount) {
                        $showOptionsResponse = true;
                    }
                    break;
                case 'grouped':
                    if (!array_key_exists('super_group', $params)) {
                        $showOptionsResponse = true;
                    }
                    break;
                case 'bundle':
                    if (!array_key_exists('bundle_option', $params)) {
                        $showOptionsResponse = true;
                    }
                    break;
            }

            /* custom options block*/

            $needShowOptions = ($product->getTypeInstance()->hasRequiredOptions($product)
                    && $product->getTypeId() != 'bundle')
                || ($this->_helper->getModuleConfig('general/display_options') == Option::ALL_OPTIONS);
            if ($product->getOptions() && $needShowOptions && !array_key_exists('options', $params)) {
                $showOptionsResponse = true;
            }

            if ($showOptionsResponse) {
                $result = [
                    'configurable_require_option_error' => 1,
                    'redirect_product_url' => $product->getProductUrl()
                ];
                $this->messageManager->addErrorMessage(__('Please choose an options'));
                return $this->getResponse()->representJson(
                    $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                );
            }

            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $related = $this->getRequest()->getParam('related_product');

            $this->cart->addProduct($product, $params);
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }

            $this->cart->save();

            if ($product->getTypeId() == "configurable"
                && $this->_helper->getModuleConfig('display/disp_configurable_image')
            ) {
                $simpleProduct = $product->getTypeInstance()
                    ->getProductByAttributes($params['super_attribute'], $product);
                $this->_coreRegistry->register('amasty_cart_simple_product', $simpleProduct);
            } else {
                $this->_coreRegistry->unregister('amasty_cart_simple_product');
            }

            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                if (!$this->cart->getQuote()->getHasError()) {
                    $message = $this->_getParentProductAddedMessage($product);
                    $message = $this->getProductAddedMessage($product, $message);
                    return $this->addToCartResponse($message, 1);
                } else {
                    $message = '';
                    $errors = $this->cart->getQuote()->getErrors();
                    foreach ($errors as $error) {
                        $message .= $error->getText();
                    }
                    return $this->addToCartResponse($message, 0);
                }
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->addToCartResponse(
                $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage()),
                0
            );

        } catch (\Exception $e) {
            $message = __('We can\'t add this item to your shopping cart right now.');
            $message .= $e->getMessage();
            return $this->addToCartResponse($message, 0);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function showOptionsResponse(\Magento\Catalog\Model\Product $product)
    {
        $this->_productHelper->initProduct($product->getEntityId(), $this);
        $page = $this->resultPageFactory->create(false, ['isIsolated' => true]);
        $page->addHandle('catalog_product_view');

        $type = $product->getTypeId();
        $page->addHandle('catalog_product_view_type_' . $type);

        $block = $page->getLayout()->getBlock('product.info');
        if (!$block) {
            $block = $page->getLayout()->createBlock(
                'Magento\Catalog\Block\Product\View',
                'product.info',
                ['data' => []]
            );
        }

        $block->setProduct($product);
        $html = $block->toHtml();

        $html = str_replace(
            '"spConfig',
            '"priceHolderSelector": ".price-box[data-product-id=' . $product->getId() . ']", "spConfig',
            $html
        );

        $html = '<div class="popup-title">' . __('Choose options for your product:') . '</div>' .
            '<div class="product-info-main">' .
            '<div class="product-name">' . $product->getName() . '</div>' .
            $html .
            '</div>';

        /* replace uenc for correct redirect*/
        $currentUenc = $this->urlHelper->getEncodedUrl();
        $refererUrl = $product->getProductUrl();
        $newUenc = $this->urlHelper->getEncodedUrl($refererUrl);
        $html = str_replace($currentUenc, $newUenc, $html);

        $html = str_replace('"swatch-opt"', '"swatch-opt-' . $product->getId() . '"', $html);
        $html = str_replace('spConfig": {"attributes', 'spConfig": {"containerId":"#confirmBox", "attributes', $html);

        $result = [
            'title' => __('Set options'),
            'message' => $html,
            'b1_name' => __('Add to Bag'),
            'b2_name' => __('Cancel'),
            'b1_action' => 'self.submitFormInPopup();',
            'b2_action' => 'self.confirmHide();',
            'align' => 'self.confirmHide();',
            'is_add_to_cart' => '0'
        ];
        $result = $this->replaceJs($result);
        return $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }

    /**
     * {@inheritdoc}
     */
    private function replaceJs($result)
    {
        $arrScript = [];
        $result['script'] = '';
        preg_match_all("@<script type=\"text/javascript\">(.*?)</script>@s", $result['message'], $arrScript);
        $result['message'] = preg_replace(
            "@<script type=\"text/javascript\">(.*?)</script>@s",
            '',
            $result['message']
        );
        foreach ($arrScript[1] as $script) {
            $result['script'] .= $script;
        }
        $result['script'] = preg_replace("@var @s", '', $result['script']);
        return $result;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param $message
     * @return string
     */
    protected function getProductAddedMessage(\Magento\Catalog\Model\Product $product, $message)
    {
        /** @var \Magento\Framework\View\LayoutInterface $layout */
        $layout = $this->_objectManager->get('\Magento\Framework\View\LayoutInterface');

        if ($this->_helper->displayProduct()) {
            /**
             * Start rewrite
             */
            /** @var \Linenhouse\Cart\Model\AddedProducts\RendererResolver $addedProductsRendererResolver */
            $addedProductsRendererResolver = $this->_objectManager->get(
                \Linenhouse\Cart\Model\AddedProducts\RendererResolver::class
            );
            $productBlock = $addedProductsRendererResolver->resolve($product);

            $block = $layout->getBlock('amasty.cart.product');
            if (!$block) {
                $block = $layout->createBlock(
                    $productBlock,
                    'amasty.cart.product',
                    ['data' => []]
                );
            }

            $block->setProduct($product);
            $message = $block->toHtml();
            /**
             * End rewrite
             */
        }

        //display count cart item
        if ($this->_helper->displayCount()) {
            $summary = $this->cart->getSummaryQty();
            $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
            if ($summary == 1) {
                $message .=
                    "<p id='amcart-count'>" .
                    __('There is') .
                    ' <a href="' . $cartUrl . '" id="am-a-count">1' .
                    __(' item') .
                    '</a>'.
                    __(' in your cart.') .
                    "</p>";
            } else {
                $message .=
                    "<p id='amcart-count'>".
                    __('There are') .
                    ' <a href="'. $cartUrl .'" id="am-a-count">'.
                    $summary.  __(' items') .
                    '</a> '.
                    __(' in your cart.') .
                    "</p>";
            }
        }
        //display summ price
        if ($this->_helper->displaySumm()) {
            $message .=
                '<p>' .
                __('Cart Subtotal:') .
                ' <span class="am_price">'.
                $this->getSubtotalHtml() .
                '</span></p>';
        }

        //display related products
        $type = $this->_helper->getModuleConfig('selling/block_type');
        if ($type && $type !== '0') {
            $this->_productHelper->initProduct($product->getEntityId(), $this);

            if (!$layout->getBlock('product.price.render.default')) {
                $layout->createBlock(
                    'Magento\Framework\Pricing\Render',
                    'product.price.render.default',
                    [ 'data' => [
                        'price_render_handle' => 'catalog_product_prices',
                        'use_link_for_as_low_as' => true
                    ] ]
                );
            }

            $relBlock = $layout->createBlock(
                'Amasty\Cart\Block\Product\\' . ucfirst($type),
                'amasty.cart.product_' . $type,
                [ 'data' => [] ]
            );
            $relBlock->setProduct($product)->setTemplate("Amasty_Cart::product/list/items.phtml");
            $message .= $relBlock->toHtml();

            /* replace uenc for correct redirect*/
            $currentUenc = $this->urlHelper->getEncodedUrl();
            $refererUrl = $this->_request->getServer('HTTP_REFERER');
            $newUenc = $this->urlHelper->getEncodedUrl($refererUrl);
            $message = str_replace($currentUenc, $newUenc, $message);
        }

        return $message;
    }

    /**
     * @param string $message
     * @param int $status
     * @param array $matrixViewErrors
     * @return \Magento\Framework\App\ResponseInterface
     */
    protected function addToCartResponse($message, $status, $matrixViewErrors = [])
    {
        $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
        $result = [
            'title'     =>  __('Information'),
            'message'   =>  $message,
            'b1_name'   =>  __('View cart'),
            'b2_name'   =>  __('Continue'),
            'b1_action' =>  'document.location = "' . $cartUrl . '";',
            'b2_action' =>  'self.confirmHide();',
            'is_add_to_cart' =>  $status,
            'checkout'  => ''
        ];

        if (!empty($matrixViewErrors)) {
            $result['matrixViewErrors'] = $matrixViewErrors;
        }

        if ($this->_helper->getModuleConfig('display/disp_checkout_button')) {
            $goto = __('Go to Checkout');
            $result['checkout'] =
                '<a class="checkout action primary"
                    title="' . $goto . '"
                    data-role="proceed-to-checkout"
                    type="button"
                    href="' . $this->_helper->getUrl('checkout/cart') . '"
                    >
                        <span>' . $goto . '</span>
                </a>';
        }

        $isProductView = $this->getRequest()->getParam('product_page');
        if ($isProductView == 'true' && $this->_helper->getProductButton()) {
            $categoryId = $this->catalogSession->getLastVisitedCategoryId();
            if ($categoryId) {
                $category = $this->categoryFactory->create()->load($categoryId);
                if ($category) {
                    $result['b2_action'] =  'document.location = "'.
                        $category->getUrl()
                        .'";';
                }
            }

        }

        //add timer
        $time = $this->_helper->getTime();
        if (0 < $time) {
            $result['b2_name'] .= '(' . $time . ')';
        }
        $result = $this->replaceJs($result);

        return $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    protected function _getParentProductAddedMessage(\Magento\Catalog\Model\Product $product)
    {
        return '<p>' . __(
                'You added %1 to your shopping cart.',
                '<a href="' . $product->getProductUrl() .'" title=" . ' .
                $product->getName() . '">' .
                $product->getName() .
                '</a>'
            ) . '</p>';
    }

    /**
     * @return \Magento\Framework\Registry
     */
    protected function _getRegistry()
    {
        return $this->_objectManager->get('Magento\Framework\Registry');
    }
}
