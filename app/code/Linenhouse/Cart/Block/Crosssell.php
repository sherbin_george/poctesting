<?php

namespace Linenhouse\Cart\Block;

use Amasty\Cart\Block\Product\Crosssell as AmastyCrosssell;
use Magento\Catalog\Block\Product\Context;
use Amasty\Cart\Helper\Data as AmastyHelper;
use Linenhouse\Cart\Helper\Data as LinenhouseHelper;

/**
 * Class Crosssell
 * @package Linenhouse\Cart\Block
 */
class Crosssell extends AmastyCrosssell
{
    /**
     * @var LinenhouseHelper
     */
    protected $linenHelper;

    /**
     * Crosssell constructor.
     * @param Context $context
     * @param AmastyHelper $helper
     * @param LinenhouseHelper $linenHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        AmastyHelper $helper,
        LinenhouseHelper $linenHelper,
        array $data = []
    )
    {
        $this->linenHelper = $linenHelper;

        parent::__construct($context, $helper, $data);
    }

    /**
     * Prepare crosssell items data
     *
     * @return \Magento\Catalog\Block\Product\ProductList\Crosssell
     */
    protected function _prepareData()
    {
        $product = $this->_coreRegistry->registry('product');
        /* @var $product \Magento\Catalog\Model\Product */

        $this->_itemCollection = $product->getCrossSellProductCollection()->addAttributeToSelect(
            $this->_catalogConfig->getProductAttributes()
        )->setPositionOrder()->addStoreFilter();

        /**
         * Start rewrite
         */
        /*add limit to collection*/
        $this->_itemCollection->getSelect()->limit(
            $this->linenHelper->getSellingProductsLimit()
        );
        /**
         * End rewrite
         */
        $this->_itemCollection->load();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }
}