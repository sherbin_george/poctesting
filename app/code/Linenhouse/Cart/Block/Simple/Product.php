<?php

namespace Linenhouse\Cart\Block\Simple;

use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\Framework\Data\Form\FormKey;

/**
 * Class Product
 * @package Linenhouse\Cart\Block
 */
class Product extends \Amasty\Cart\Block\Product
{
    /**
     * @var string
     */
    protected $_priceRenderer;

    /**
     * Product constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Framework\Registry $registry
     * @param FormKey $formKey
     * @param array $data
     * @param \Amasty\Cart\Helper\Data $helper
     * @param string $priceRenderer
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Registry $registry,
        FormKey $formKey,
        array $data = [],
        \Amasty\Cart\Helper\Data $helper,
        $priceRenderer = \Linenhouse\Cart\Block\Pricing\Render::class
    )
    {
        $this->_priceRenderer = $priceRenderer;

        parent::__construct($context, $imageBuilder, $registry, $formKey, $data, $helper);
    }

    /**
     * @return \Linenhouse\Cart\Block\Pricing\Render|null
     */
    public function getPriceRenderer()
    {
        $price = $this->_getPriceRenderer();
        $product = $this->getProduct();

        if ($price) {
            return $price->setProduct($product);
        }

        return null;
    }

    /**
     * @return void
     */
    protected function _createDefaultPriceRenderer()
    {
        $layout = $this->getLayout();
        $priceRenderExists = $layout->getBlock('product.price.render.default');

        if (!$priceRenderExists) {
            $layout->createBlock(
                \Magento\Framework\Pricing\Render::class,
                'product.price.render.default',
                [
                    'data' => [
                        'price_render_handle' => 'catalog_product_prices',
                        'use_link_for_as_low_as' => true
                    ]
                ]
            );
        }
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface|null
     */
    protected function _getPriceRenderer()
    {
        if (!($this->getProduct() instanceof CatalogProduct)) {
            return null;
        }

        $this->_createDefaultPriceRenderer();

        $price = $this->getLayout()->createBlock(
            $this->_priceRenderer,
            '',
            [
                'data'  => [
                    'price_render'      => 'product.price.render.default',
                    'price_type_code'   => 'final_price',
                    'zone'              => 'item_view'
                ]
            ]
        );

        return $price;
    }
}