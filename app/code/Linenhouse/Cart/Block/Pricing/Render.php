<?php

namespace Linenhouse\Cart\Block\Pricing;

/**
 * Class Render
 * @package Linenhouse\Cart\Block\Pricing
 */
class Render extends \Magento\Catalog\Pricing\Render
{
    /**
     * @return null|\Magento\Catalog\Model\Product
     */
    protected function getProduct()
    {
        return $this->getData('product');
    }
}