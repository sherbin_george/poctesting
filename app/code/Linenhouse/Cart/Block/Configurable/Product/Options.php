<?php

namespace Linenhouse\Cart\Block\Configurable\Product;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

/**
 * Class Options
 * @package Linenhouse\Cart\Block\Configurable\Product
 */
class Options extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'Linenhouse_Cart::product/options.phtml';

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $_productResource;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * Options constructor.
     * @param Template\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResource
     * @param \Magento\Quote\Model\ResourceModel\Quote\Item\Option\CollectionFactory $optionsCollectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Quote\Model\ResourceModel\Quote\Item\Option\CollectionFactory $optionsCollectionFactory,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_productResource = $productResource;
        $this->_registry = $registry;

        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getOptionList()
    {
        $options = [];
        $super_attribute = $this->getRequest()->getParam('super_attribute');
        $product = $this->getProduct();

        if (!($product instanceof Product)
            || $product->getTypeId() != Configurable::TYPE_CODE) {
            return $options;
        }

        if ($super_attribute) {
            foreach ($super_attribute as $attributeId => $optionId) {
                $options[] = $this->_getOption($attributeId, $optionId);
            }
        }

        return $options;
    }

    /**
     * @param string|int $attributeId
     * @param string|int $optionId
     * @return array
     */
    protected function _getOption($attributeId, $optionId)
    {
        /** @var \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute */
        $attribute = $this->_productResource->getAttribute($attributeId);
        $optionValue = $attribute->getFrontend()->getOption($optionId);

        return [
            'label' => $attribute->getFrontend()->getLabel(),
            'value' => $optionValue
        ];
    }
}