<?php

namespace Linenhouse\Cart\Block\Configurable;

use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Data\Form\FormKey;

/**
 * Class Product
 * @package Linenhouse\Cart\Block
 */
class Product extends \Linenhouse\Cart\Block\Simple\Product
{
    /**
     * @var string
     */
    protected $_optionsRenderer;

    /**
     * @var string
     */
    protected $_priceRenderer;

    /**
     * Product constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Framework\Registry $registry
     * @param FormKey $formKey
     * @param array $data
     * @param \Amasty\Cart\Helper\Data $helper
     * @param string $optionsRenderer
     * @param string $priceRenderer
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Registry $registry,
        FormKey $formKey,
        array $data = [],
        \Amasty\Cart\Helper\Data $helper,
        $priceRenderer = \Linenhouse\Cart\Block\Pricing\Render::class,
        $optionsRenderer = \Linenhouse\Cart\Block\Configurable\Product\Options::class
    )
    {
        $this->_priceRenderer = $priceRenderer;
        $this->_optionsRenderer = $optionsRenderer;

        parent::__construct($context, $imageBuilder, $registry, $formKey, $data, $helper, $priceRenderer);
    }

    /**
     * @return \Linenhouse\Cart\Block\Configurable\Product\Options
     */
    public function getOptionsRenderer()
    {
        $renderer = $this->getLayout()->createBlock(
            $this->_optionsRenderer,
            '',
            []
        );
        $renderer->setProduct($this->getProduct());

        return $renderer;
    }

    /**
     * @return \Linenhouse\Cart\Block\Pricing\Render|null
     */
    public function getPriceRenderer()
    {
        $price = $this->_getPriceRenderer();

        $product = $this->getProduct();
        $child = $this->_getChildProduct($product);

        if ($child) {
            $product = $child;
        }

        if ($price) {
            return $price->setProduct($product);
        }

        return null;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Model\Product
     */
    protected function _getChildProduct($product)
    {
        $params = $this->getRequest()->getParams();
        $simple = null;

        if ($product->getTypeId() == Configurable::TYPE_CODE &&
            isset($params['super_attribute'])) {
            $simple = $product->getTypeInstance()
                ->getProductByAttributes($params['super_attribute'], $product);
        }

        return $simple;
    }
}