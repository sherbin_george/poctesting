<?php

namespace Linenhouse\Cart\Block\Grouped;

use Magento\Framework\Data\Form\FormKey;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Product
 * @package Linenhouse\Cart\Block\Grouped
 */
class Product extends \Linenhouse\Cart\Block\Simple\Product
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Product constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Framework\Registry $registry
     * @param FormKey $formKey
     * @param array $data
     * @param \Amasty\Cart\Helper\Data $helper
     * @param ProductRepository $productRepository
     * @param string $priceRenderer
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Registry $registry,
        FormKey $formKey,
        array $data = [],
        \Amasty\Cart\Helper\Data $helper,
        ProductRepository $productRepository,
        $priceRenderer = \Linenhouse\Cart\Block\Pricing\Render::class
    )
    {
        $this->productRepository = $productRepository;

        parent::__construct($context, $imageBuilder, $registry, $formKey, $data, $helper, $priceRenderer);
    }

    /**
     * @return string
     */
    public function _toHtml()
    {
        $params = $this->getRequest()->getParams();
        $result = '';

        if (isset($params['super_group'])) {
            $superGroup = array_filter($params['super_group']);
            foreach ($superGroup as $groupItemId => $qty) {
                try {
                    $groupProduct = $this->productRepository->getById($groupItemId);
                } catch (NoSuchEntityException $e) {
                    continue;
                }
                $groupProduct->setQty($qty);
                $this->setProduct($groupProduct);
                $result .= parent::_toHtml();
            }
        }

        return $result;
    }
}