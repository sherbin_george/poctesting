<?php

namespace Linenhouse\Cart\Block\Bundle\Product;

use Magento\Framework\View\Element\Template;
use Magento\Bundle\Helper\Catalog\Product\Configuration;
use Magento\Framework\Registry;
use Magento\Catalog\Helper\Product\Configuration as ConfigurationHelper;

/**
 * Class Options
 * @package Linenhouse\Cart\Block\Bundle\Product
 */
class Options extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'Linenhouse_Cart::product/options.phtml';

    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var ConfigurationHelper
     */
    protected $configurationHelper;

    /**
     * Options constructor.
     * @param Template\Context $context
     * @param Configuration $configuration
     * @param Registry $registry
     * @param ConfigurationHelper $configurationHelper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Configuration $configuration,
        Registry $registry,
        ConfigurationHelper $configurationHelper,
        array $data = []
    )
    {
        $this->configuration = $configuration;
        $this->registry = $registry;
        $this->configurationHelper = $configurationHelper;

        parent::__construct($context, $data);
    }

    /**
     * Retrieve selected bundle product options list
     *
     * @return array
     */
    public function getOptionList()
    {
        /** @var \Magento\Quote\Model\Quote\Item\AbstractItem $quoteItem */
        $quoteItem = $this->getQuoteItem();
        $options = null;

        if ($quoteItem) {
            $options = $this->configuration->getOptions($quoteItem);

            if (!$options) {
                return $options;
            }

            foreach ($options as $attr => $option) {
                if (isset($option['value']) && is_array($option['value'])) {
                    $formattedOption = $this->getFormatedOptionValue($option['value']);

                    $options[$attr]['value'] = (is_array($formattedOption) && isset($formattedOption['value'])) ?
                        $formattedOption['value'] : $formattedOption;
                }
            }
        }

        return $options;
    }

    /**
     * Accept option value and return its formatted view
     *
     * @param string|array $optionValue
     * Method works well with these $optionValue format:
     *      1. String
     *      2. Indexed array e.g. array(val1, val2, ...)
     *      3. Associative array, containing additional option info, including option value, e.g.
     *          array
     *          (
     *              [label] => ...,
     *              [value] => ...,
     *              [print_value] => ...,
     *              [option_id] => ...,
     *              [option_type] => ...,
     *              [custom_view] =>...,
     *          )
     *
     * @return array
     */
    public function getFormatedOptionValue($optionValue)
    {
        /* @var $helper \Magento\Catalog\Helper\Product\Configuration */
        $helper = $this->configurationHelper;
        $params = [
            'max_length' => 55,
            'cut_replacer' => ' <a href="#" class="dots tooltip toggle" onclick="return false">...</a>'
        ];
        return $helper->getFormattedOptionValue($optionValue, $params);
    }
}