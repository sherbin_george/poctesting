<?php

namespace Linenhouse\Cart\Block\Bundle\Product;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class PricingRender
 * @package Linenhouse\Cart\Block\Bundle\Product
 */
class PricingRender extends \Magento\Framework\View\Element\Template
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * PricingRender constructor.
     * @param Template\Context $context
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    )
    {
        $this->priceCurrency = $priceCurrency;

        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        /** @var \Magento\Quote\Model\Quote\Item\AbstractItem $quoteItem */
        $quoteItem = $this->getQuoteItem();

        return $this->priceCurrency->format(
            $quoteItem->getCalculationPrice(),
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->_storeManager->getStore()
        );
    }
}