<?php

namespace Linenhouse\Cart\Block\Bundle;

use Magento\Framework\Data\Form\FormKey;
use Linenhouse\Cart\Observer\LastAddedQuoteItemObserver;
use Magento\Quote\Model\Quote\Item\AbstractItem;

/**
 * Class Product
 * @package Linenhouse\Cart\Block\Bundle
 */
class Product extends \Linenhouse\Cart\Block\Simple\Product
{
    /**
     * @var \Magento\Bundle\Helper\Catalog\Product\Configuration
     */
    protected $configuration;

    /**
     * @var string
     */
    protected $_optionsRenderer;

    /**
     * Product constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Framework\Registry $registry
     * @param FormKey $formKey
     * @param array $data
     * @param \Amasty\Cart\Helper\Data $helper
     * @param \Magento\Bundle\Helper\Catalog\Product\Configuration $configuration
     * @param string $priceRenderer
     * @param string $optionsRenderer
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Registry $registry,
        FormKey $formKey,
        array $data = [],
        \Amasty\Cart\Helper\Data $helper,
        \Magento\Bundle\Helper\Catalog\Product\Configuration $configuration,
        $priceRenderer = \Linenhouse\Cart\Block\Bundle\Product\PricingRender::class,
        $optionsRenderer = \Linenhouse\Cart\Block\Bundle\Product\Options::class
    )
    {
        $this->configuration = $configuration;
        $this->_optionsRenderer = $optionsRenderer;

        parent::__construct($context, $imageBuilder, $registry, $formKey, $data, $helper, $priceRenderer);
    }

    /**
     * @return bool|\Magento\Framework\View\Element\BlockInterface
     */
    public function getOptionsRenderer()
    {
        return $this->_createRenderer($this->_optionsRenderer);
    }

    /**
     * @return bool|\Magento\Framework\View\Element\BlockInterface
     */
    public function getPriceRenderer()
    {
        return $this->_createRenderer($this->_priceRenderer);
    }

    /**
     * @param string $rendererInstance
     * @return bool|\Magento\Framework\View\Element\BlockInterface
     */
    protected function _createRenderer($rendererInstance)
    {
        if ($this->isRenderDisallowed()) {
            return false;
        }

        $renderer = $this->getLayout()->createBlock(
            $rendererInstance,
            '',
            []
        );

        $renderer->setQuoteItem($this->getQuoteItem());

        return $renderer;
    }

    /**
     * @return mixed|AbstractItem
     */
    public function getQuoteItem()
    {
        $quoteItem = $this->_registry->registry(LastAddedQuoteItemObserver::LAST_ADDED_QUOTE_ITEM_KEY);

        if (!($quoteItem instanceof AbstractItem)) {
            return false;
        }

        return $quoteItem;
    }

    /**
     * @return bool
     */
    protected function isRenderDisallowed()
    {
        return $this->getQuoteItem() === false;
    }
}