<?php

namespace Linenhouse\Cart\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Framework\Registry;

/**
 * Class LastAddedQuoteItemObserver
 * @package Linenhouse\Cart\Observer
 */
class LastAddedQuoteItemObserver implements ObserverInterface
{
    /**
     * Registry key for last added quote item
     */
    const LAST_ADDED_QUOTE_ITEM_KEY = 'last_added_quote_item';

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * LastAddedQuoteItemObserver constructor.
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Add last added to cart quote item to registry
     * For usage in amasty cart popup
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($observer->getQuoteItem() instanceof AbstractItem) {
            $this->registry->unregister(self::LAST_ADDED_QUOTE_ITEM_KEY);
            $this->registry->register(self::LAST_ADDED_QUOTE_ITEM_KEY, $observer->getQuoteItem());
        }
    }
}