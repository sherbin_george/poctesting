<?php

namespace Linenhouse\Cart\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

/**
 * Class LayoutLoadBefore
 * @package Linenhouse\Cart\Observer
 */
class EmptyCartSwitchLayout implements ObserverInterface
{
    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $cartHelper;

    /**
     * EmptyCartSwitchLayout constructor.
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     */
    public function __construct(\Magento\Checkout\Helper\Cart $cartHelper)
    {
        $this->cartHelper = $cartHelper;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Framework\View\Model\Layout\Merge $update */
        $update = $observer->getData('layout')->getUpdate();

        if (in_array('checkout_cart_index', $update->getHandles())
            && !$this->cartHelper->getItemsCount()) {
            $update->addHandle('empty_cart_index');
        }

        return $this;
    }
}
