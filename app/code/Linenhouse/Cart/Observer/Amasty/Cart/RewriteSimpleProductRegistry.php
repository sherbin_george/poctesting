<?php

namespace Linenhouse\Cart\Observer\Amasty\Cart;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\ConfigurableProduct\Block\Cart\Item\Renderer\Configurable as ConfigurableRenderer;
use Magento\Catalog\Model\Config\Source\Product\Thumbnail;

/**
 * Class RewriteSimpleProductRegistry
 * @package Linenhouse\Cart\Observer\Amasty\Cart
 */
class RewriteSimpleProductRegistry implements ObserverInterface
{
    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * RewriteSimpleProductRegistry constructor.
     * @param Registry $registry
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Registry $registry,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_registry = $registry;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getProduct();
        $params = $observer->getRequest()->getParams();

        $this->_registry->unregister('amasty_cart_simple_product');

            if ($product->getTypeId() == Configurable::TYPE_CODE &&
                isset($params['super_attribute']) &&
                $this->_getConfigurableImageSource() == Thumbnail::OPTION_USE_OWN_IMAGE
            ) {

            /** @var \Magento\Catalog\Model\Product $simple */
            $simple = $product->getTypeInstance()
                ->getProductByAttributes($params['super_attribute'], $product);

            if ($simple->getThumbnail() && $simple->getThumbnail() != 'no_selection') {
                $this->_registry->register('amasty_cart_simple_product', $simple);
            }
        }

        return $this;
    }

    /**
     * Possible values: 'parent', 'itself'
     *
     * @return null|string
     */
    protected function _getConfigurableImageSource()
    {
        return $this->_scopeConfig->getValue(
            ConfigurableRenderer::CONFIG_THUMBNAIL_SOURCE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }
}