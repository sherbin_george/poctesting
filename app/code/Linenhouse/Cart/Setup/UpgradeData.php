<?php

namespace Linenhouse\Cart\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Config\Model\ResourceModel\Config;

/**
 * Class UpgradeData
 * @package Linenhouse\Cart\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var Config
     */
    protected $_configResource;

    /**
     * UpgradeData constructor.
     * @param Config $configResource
     */
    public function __construct(Config $configResource)
    {
        $this->_configResource = $configResource;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->_updatePopupSettings();
        }

        $setup->endSetup();
    }

    /**
     * @return void
     */
    protected function _updatePopupSettings()
    {
        $this->_configResource->saveConfig(
            'amasty_cart/display/disp_product',
            1,
            'default',
            0
        );

        $this->_configResource->saveConfig(
            'amasty_cart/display/disp_count',
            0,
            'default',
            0
        );

        $this->_configResource->saveConfig(
            'amasty_cart/display/disp_sum',
            0,
            'default',
            0
        );
    }
}