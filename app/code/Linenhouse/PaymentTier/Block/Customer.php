<?php

namespace Linenhouse\PaymentTier\Block;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\GroupExtensionInterface;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class Customer.
 *
 * @package Linenhouse\PaymentTier\Block
 */
class Customer extends Template
{
    /**
     * @var CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * @var GroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * Customer constructor.
     *
     * @param CurrentCustomer $currentCustomer
     * @param GroupRepositoryInterface $groupRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        CurrentCustomer $currentCustomer,
        GroupRepositoryInterface $groupRepository,
        Template\Context $context,
        array $data = [])
    {
        $this->currentCustomer = $currentCustomer;
        $this->groupRepository = $groupRepository;

        parent::__construct($context, $data);
    }

    /**
     * Get the logged in customer.
     *
     * @return CustomerInterface|null
     */
    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getStaticBlock()
    {
        $staticBlock = 'customer_payment_tier';
        $customer = $this->currentCustomer->getCustomer();

        if ($customer && $customer->getGroupId()) {
            try {
                /** @var GroupInterface $group */
                $group = $this->groupRepository->getById($customer->getGroupId());
                $extensionAttributes = $group->getExtensionAttributes();
                if ($extensionAttributes instanceof GroupExtensionInterface
                    && $extensionAttributes->getCmsBlockIdentifier()
                ) {
                    $staticBlock = $extensionAttributes->getCmsBlockIdentifier();
                }
            } catch (LocalizedException $e) { }
        }

        return $this->getLayout()->createBlock(\Magento\Cms\Block\Block::class)
            ->setBlockId($staticBlock)
            ->toHtml();
    }
}
