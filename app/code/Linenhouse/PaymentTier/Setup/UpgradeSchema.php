<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linenhouse\PaymentTier\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('customer_group'),
                'static_block',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'unsigned' => true,
                    'nullable' => true,
                    'default' => '',
                    'comment' => 'Static Block Banner'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $connection = $setup->getConnection();
            $customerGroupTable = $setup->getTable('customer_group');
            if ($connection->tableColumnExists($customerGroupTable, 'static_block')) {
                $connection->dropColumn($customerGroupTable, 'static_block');
            }

            if (!$connection->isTableExists($setup->getTable('customer_group_cms_block'))) {
                $this->createCustomerGroupCmsBlockTable($setup);
            }
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     *
     * @throws \Zend_Db_Exception
     */
    private function createCustomerGroupCmsBlockTable(SchemaSetupInterface $setup)
    {
        /**
         * Create table 'group_cms_block'
         */
        $installer = $setup;
        $table = $installer->getConnection()->newTable(
            $installer->getTable('customer_group_cms_block')
        )->addColumn(
            'customer_group_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Customer Group ID'
        )->addColumn(
            'cms_block_identifier',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'CMS Block Identifier'
        )->addForeignKey(
            $installer->getFkName(
                'customer_group_cms_block',
                'customer_group_id',
                'customer_group',
                'customer_group_id'
            ),
            'customer_group_id',
            $installer->getTable('customer_group'),
            'customer_group_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Customer Group ID To CMS Block Identifier Linkage Table'
        );

        $installer->getConnection()->createTable($table);
    }
}
