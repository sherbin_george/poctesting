<?php

namespace Linenhouse\PaymentTier\Plugin\Block\Plugin;

use Magento\Customer\Api\Data\GroupExtensionInterface;
use Magento\Customer\Controller\RegistryConstants;

/**
 * Class Form.
 *
 * @package Linenhouse\PaymentTier\Plugin\Block\Plugin
 */
class Form
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $_backendSession;

    /**
     * @var GroupFactory
     */
    protected $_groupFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Customer\Api\Data\GroupInterfaceFactory
     */
    protected $groupDataFactory;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    protected $_taxHelper;

    /**
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    protected $_groupRepository;

    /**
     * Form constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Customer\Api\Data\GroupInterfaceFactory $groupDataFactory
     * @param \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
     * @param \Magento\Tax\Helper\Data $taxHelper
     * @param \Magento\Customer\Model\GroupFactory $groupFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Api\Data\GroupInterfaceFactory $groupDataFactory,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Customer\Model\GroupFactory $groupFactory
    ) {
        $this->_backendSession = $context->getBackendSession();
        $this->_coreRegistry = $registry;
        $this->_taxHelper = $taxHelper;
        $this->groupDataFactory = $groupDataFactory;
        $this->_groupRepository = $groupRepository;
        $this->_groupFactory = $groupFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function aftersetForm(\Magento\Customer\Block\Adminhtml\Group\Edit\Form $form)
    {
        $groupId = $this->_coreRegistry->registry(RegistryConstants::CURRENT_GROUP_ID);
        /** @var \Magento\Customer\Api\Data\GroupInterface $customerGroup */
        if ($groupId === null) {
            $customerGroup = $this->groupDataFactory->create();
        } else {
            $customerGroup = $this->_groupRepository->getById($groupId);
        }

        $fieldset = $form->getForm()->addFieldset(
            'lh_payment_tier_fieldset',
            ['legend' => __('Linenhouse Payment Tier')]
        );

        $fieldset->addField(
            'cms_block_identifier',
            'text',
            [
                'name' => 'cms_block_identifier',
                'label' => __('Static Block Identifier'),
                'title' => __('Static Block Identifier'),
                'class' => '',
                'required' => false
            ]
        );

        if (!$this->_backendSession->getCustomerGroupData()) {
            // TODO: need to figure out how the DATA can work with forms

            $extensionAttributes = $customerGroup->getExtensionAttributes();
            if ($extensionAttributes instanceof GroupExtensionInterface
                && $extensionAttributes->getCmsBlockIdentifier()
            ) {
                $form->getForm()->addValues(
                    [
                        'cms_block_identifier' => $extensionAttributes->getCmsBlockIdentifier()
                    ]
                );
            }

        }

        return $form;
    }
}
