<?php

namespace Linenhouse\PaymentTier\Plugin;

use Magento\Customer\Api\Data\GroupInterface;
use Magento\Customer\Api\Data\GroupExtensionFactory;
use Magento\Customer\Api\Data\GroupExtensionInterface;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;

/**
 * Class GroupRepositoryPlugin.
 *
 * @package Linenhouse\PaymentTier\Plugin
 */
class GroupRepositoryPlugin
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var GroupExtensionFactory
     */
    private $groupExtensionFactory;

    /**
     * GroupRepositoryPlugin constructor.
     *
     * @param ResourceConnection $resourceConnection
     * @param RequestInterface $request
     * @param GroupExtensionFactory $groupExtensionFactory
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        RequestInterface $request,
        GroupExtensionFactory $groupExtensionFactory
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->request = $request;
        $this->groupExtensionFactory = $groupExtensionFactory;
    }

    /**
     * @param GroupRepositoryInterface $subject
     * @param GroupInterface $group
     *
     * @return GroupInterface
     */
    public function afterGetById(GroupRepositoryInterface $subject, GroupInterface $group)
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()->from(
            'customer_group_cms_block',
            ['cms_block_identifier']
        );
        $select->where('customer_group_id = ?', $group->getId());
        $cmsBlock = $connection->fetchRow($select);

        if (is_array($cmsBlock) && isset($cmsBlock['cms_block_identifier'])) {
            $extensionAttributes = $group->getExtensionAttributes();
            if (!($extensionAttributes instanceof GroupExtensionInterface)) {
                $extensionAttributes = $this->groupExtensionFactory->create();
                $extensionAttributes->setCmsBlockIdentifier($cmsBlock['cms_block_identifier']);
                $group->setExtensionAttributes($extensionAttributes);
            }
        }

        return $group;
    }

    /**
     * @param GroupRepositoryInterface $subject
     * @param GroupInterface $group
     *
     * @return GroupInterface
     */
    public function afterSave(GroupRepositoryInterface $subject, GroupInterface $group)
    {
        $connection = $this->resourceConnection->getConnection();
        $customerGroupCmsBlockTable = $connection->getTableName('customer_group_cms_block');
        $connection->delete(
            $customerGroupCmsBlockTable,
            $connection->quoteInto('customer_group_id', $group->getId())
        );

        $blockIdentifier = $this->request->getParam('cms_block_identifier', '');
        if ($blockIdentifier && $group->getId()) {
            $connection->insert($customerGroupCmsBlockTable, [
                'customer_group_id'     => $group->getId(),
                'cms_block_identifier'  => $blockIdentifier
            ]);
        }

        return $group;
    }
}
