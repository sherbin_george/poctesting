<?php
/**
 * @author     Balance Internet
 * @package    Linenhouse_Rules
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Linenhouse\Rules\Observer\Salesrule;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Discount implements ObserverInterface
{

    protected $processedRuleItem = [];

    /**
     * @var \Amasty\Rules\Model\DiscountRegistry
     */
    private $discountRegistry;

    public function __construct(
        \Amasty\Rules\Model\DiscountRegistry $discountRegistry
    ) {
        $this->discountRegistry = $discountRegistry;
    }

    /**
     * @param Observer $observer
     * @return \Magento\SalesRule\Model\Rule\Action\Discount\Data|void
     */
    public function execute(Observer $observer)
    {
        /* @var \Magento\SalesRule\Model\Rule $rule */
        $rule = $observer->getRule();
        /* @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $result */
        $result = $observer->getResult();
        $item = $observer->getItem();

        if (isset($this->processedRuleItem[$rule->getId()][$item->getId()])) {
            return ;
        }
        $this->discountRegistry->setDiscount($result, $rule);
        $this->processedRuleItem[$rule->getId()][$item->getId()] = true;
    }
}
