<?php

namespace Linenhouse\ConfigurableMatrixView\Block\Product\View;

use Bss\ConfigurableMatrixView\Block\Product\View\Configurable as BssConfigurable;

/**
 * Class Configurable
 * @package Linenhouse\ConfigurableMatrixView\Block\Product\View
 */
class Configurable extends BssConfigurable
{
    /**
     * Get Allowed Products
     *
     * @return \Magento\Catalog\Model\Product[]
     */
    public function getAllowProducts()
    {
        if (!$this->hasAllowProducts()) {
            $skipSaleableCheck = $this->catalogProduct->getSkipSaleableCheck();

            if ($this->isEnabledMatrixV() && $this->getProduct()->getConfigurableMatrixView()) {
                $products = [];
                $allProducts = $this->getProduct()->getTypeInstance()->getUsedProducts($this->getProduct(), null);
                foreach ($allProducts as $product) {
                    if ($product->isSaleable() || $skipSaleableCheck) {
                        $products[] = $product;
                    }
                }
            } else {
                $products = $skipSaleableCheck ?
                    $this->getProduct()->getTypeInstance()->getUsedProducts($this->getProduct(), null) :
                    $this->getProduct()->getTypeInstance()->getSalableUsedProducts($this->getProduct(), null);
            }
            $this->setAllowProducts($products);
        }

        return $this->getData('allow_products');
    }

    /**
     * Add size_guide attribute existence flag to swatch config
     * Checks is size_guide attribute exists in parent configurable product
     *
     * @return string
     */
    public function getJsonConfig()
    {
        $config = parent::getJsonConfig();
        $sizeGuide = $this->getProduct()->getSizeGuide();

        if ($config && (bool)$sizeGuide) {
            try {
                /** @var \Magento\Framework\Json\DecoderInterface $decoder */
                $decoder = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get('Magento\Framework\Json\DecoderInterface');
                $config = $decoder->decode($config);
                $config['parentProductHaveSizeGuide'] = true;
                $config['sizeGuideBtnLabel'] = __('sizing guide');

                return $this->jsonEncoder->encode($config);
            } catch (\Exception $e) {
                return $config;
            }
        }

        return $config;
    }

    /**
     * Add body class for only configurable maxtrix product
     *
     * @return $this
     */
    protected function _prepareLayout() {
        $product = $this->getProduct();

        if ($this->isEnabledMatrixV()
            && $product instanceof \Magento\Catalog\Model\Product
            && $product->getConfigurableMatrixView()
        ) {
            $this->pageConfig->addBodyClass('configurable-matrix-page');
        }

        return parent::_prepareLayout();
    }
}