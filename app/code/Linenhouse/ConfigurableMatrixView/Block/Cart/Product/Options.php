<?php

namespace Linenhouse\ConfigurableMatrixView\Block\Cart\Product;

use Magento\Catalog\Model\Product;
use Linenhouse\Cart\Block\Configurable\Product\Options as CartOptions;

/**
 * Class Options
 * @package Linenhouse\ConfigurableMatrixView\Block\Product
 */
class Options extends CartOptions
{
    /**
     * @return array
     */
    public function getOptionList()
    {
        $options = [];
        $child = $this->getChildProduct();
        /** @var \Magento\Catalog\Model\Product $parent */
        $parent = $this->getProduct();
        $productIds = $this->getRequest()->getParam('super_attribute_' . $parent->getId());

        if (!($child instanceof Product)
            || !$parent
            || !$parent->getEntityId()
            || !is_array($productIds)
        ) {
            return $options;
        }

        foreach ($productIds as $key => $super_attribute) {
            /** @var \Magento\Catalog\Model\Product $childRetrieved */
            $childRetrieved = $parent->getTypeInstance()
                ->getProductByAttributes($super_attribute, $parent);

            if (!$childRetrieved instanceof Product) {
                continue;
            }

            if ($childRetrieved->getId() == $child->getId()) {
                foreach ($super_attribute as $attributeId => $optionId) {
                    $options[] = $this->_getOption($attributeId, $optionId);
                }
            }
        }

        return $options;
    }
}