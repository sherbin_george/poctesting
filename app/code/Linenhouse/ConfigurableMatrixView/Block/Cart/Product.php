<?php

namespace Linenhouse\ConfigurableMatrixView\Block\Cart;

use Magento\ConfigurableProduct\Block\Cart\Item\Renderer\Configurable as ConfigurableRenderer;
use Magento\Catalog\Model\Config\Source\Product\Thumbnail;
use Magento\Catalog\Model\Product as CatalogProduct;

/**
 * Class Product
 * @package Linenhouse\ConfigurableMatrixView\Block\Cart
 */
class Product extends \Linenhouse\Cart\Block\Configurable\Product
{
    /**
     * @return string
     */
    public function _toHtml()
    {
        $addedProducts = $this->_registry->registry('matrixview_added_products');
        $configurable = $this->_registry->registry('matrixview_parent_product');
        $result = '';

        if ($addedProducts && $configurable) {
            foreach ($addedProducts as $product) {
                $this->setProduct($configurable);
                $this->setChildProduct($product);
                $result .= parent::_toHtml();
            }
        }

        return $result;
    }

    /**
     * @return \Linenhouse\Cart\Block\Configurable\Product\Options
     */
    public function getOptionsRenderer()
    {
        $renderer = $this->getLayout()->createBlock(
            $this->_optionsRenderer,
            '',
            []
        );
        $renderer->setProduct($this->getProduct());
        $renderer->setChildProduct($this->getChildProduct());

        return $renderer;
    }

    /**
     * Retrieve product image
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @param array $attributes
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getImage($product, $imageId, $attributes = [])
    {
        $simple = $this->getChildProduct();
        $configurableImageSource = $this->_scopeConfig->getValue(
            ConfigurableRenderer::CONFIG_THUMBNAIL_SOURCE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($simple instanceof CatalogProduct &&
            $configurableImageSource == Thumbnail::OPTION_USE_OWN_IMAGE &&
            ($simple->getThumbnail() && $simple->getThumbnail() != 'no_selection')
        ) {
            $product = $simple;
        }

        return $this->imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    /**
     * @return \Linenhouse\Cart\Block\Pricing\Render|false
     */
    public function getPriceRenderer()
    {
        if (!($this->getProduct() instanceof CatalogProduct)) {
            return null;
        }

        $this->_createDefaultPriceRenderer();

        $price = $this->getLayout()->createBlock(
            $this->_priceRenderer,
            '',
            [
                'data'  => [
                    'price_render'      => 'product.price.render.default',
                    'price_type_code'   => 'final_price',
                    'zone'              => 'item_view'
                ]
            ]
        );

        $parent = $this->getProduct();
        $child = $this->getChildProduct();

        if ($child instanceof CatalogProduct) {
            $parent = $child;
        }

        return $price->setProduct($parent);
    }
}