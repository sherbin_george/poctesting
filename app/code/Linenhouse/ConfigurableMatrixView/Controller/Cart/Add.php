<?php

namespace Linenhouse\ConfigurableMatrixView\Controller\Cart;

/**
 * Class Add
 * @package Linenhouse\ConfigurableMatrixView\Controller\Cart
 */
class Add extends \Bss\ConfigurableMatrixView\Controller\Cart\Add
{
    /**
     * @return $this|\Magento\Framework\Controller\Result\Redirect|void
     */
    public function execute()
    {

        // if (!$this->_formKeyValidator->validate($this->getRequest())) {
        //     return $this->resultRedirectFactory->create()->setPath('*/*/');
        // }

        $params = $this->getRequest()->getParams();
        $addedProducts = $product_fail = $product_success = [];

        $productId = (int)$this->getRequest()->getPost('product');
        $productIds = $this->getRequest()->getPost('super_attribute_'.$productId);
        /**
         * Start rewrite
         */
        $totalQty = 0;
        /**
         * End rewrite
         */

        foreach($productIds as $k => $super_attribute) {
            try {
                $qty = $this->getRequest()->getPost('qty_'.$productId.'_'.$k, 0);
                $product = $this->getProductMTV($productId);
                /**
                 * Start rewrite
                 */
                $totalQty += $qty;
                /**
                 * End rewrite
                 */

                if ($qty <= 0 || !$product) {
                    continue;
                }

                $paramsr = [];
                $paramsr['product'] = $productId;
                $paramsr['qty']= $qty;
                $paramsr["super_attribute"] = [];
                $paramsr["super_attribute"] = $super_attribute;
                if ($this->getRequest()->getPost('super_attribute')) {
                    $paramsr["super_attribute"] = $paramsr["super_attribute"] + $this->getRequest()->getPost('super_attribute');
                }
                $paramsr['options'] = isset($params['options'])? $params['options'] : [];

                $childProductId = $this->getRequest()->getPost('child_product_'.$productId.'_'.$k);
                $childProduct = $this->getProductMTV($childProductId);

                $this->cart->addProduct($product, $paramsr);

                if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                    if (!$this->cart->getQuote()->getHasError()) {
                        $addedProducts[] = $childProduct;
                    }
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $product_fail[$childProduct->getId()] = $e->getMessage();
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    $product_fail[$childProduct->getId()] = end($messages);
                }
                $cartItem = $this->cart->getQuote()->getItemByProduct($product);
                if ($cartItem) {
                    $this->cart->getQuote()->deleteItem($cartItem);
                }
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
                \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->critical($e);
            }
        }

        /**
         * Start rewrite
         * If already was forwarded from Amasty_Cart add product return to it with error
         */
        if ($totalQty <= 0) {
            $this->messageManager->addError("Can't add product to cart !");

            if ($this->_getRegistry()->registry('matrixview_parent_product')) {
                $this->_setForward([
                    'errors'    => __('Can\'t add product to cart !')
                ]);
                return;
            } else {
                return $this->goBack();
            }
        }
        /**
         * End rewrite
         */

        $this->saveAdd($addedProducts);

        $product_poup['errors'] = $product_fail;

        /**
         * Start rewrite
         */
        $this->_setForward($product_poup);
        return;

//        $this->getResponse()->setBody(json_encode($product_poup));
//        return;
        /**
         * End rewrite
         */
    }

    /**
     * save product to cart
     * @param $addedProducts
     */
    protected function saveAdd($addedProducts)
    {
        $related = $this->getRequest()->getParam('related_product');

        if ($addedProducts) {
            try {

                if (!empty($related)) {
                    $this->cart->addProductsByIds(explode(',', $related));
                }

                $this->cart->save()->getQuote()->collectTotals();
                if (!$this->cart->getQuote()->getHasError()) {
                    $products = [];
                    foreach ($addedProducts as $product) {
                        $products[] = '"' . $product->getName() . '"';
                    }

                    $this->messageManager->addSuccess(
                        __('%1 product(s) have been added to shopping cart: %2.', count($addedProducts), join(', ', $products))
                    );

                    /**
                     * Start rewrite
                     */
                    $registry = $this->_getRegistry();
                    $registry->register('matrixview_added_products', $addedProducts);
                    /**
                     * End rewrite
                     */
                }

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $this->messageManager->addNotice(
                        \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                    );
                } else {
                    $errormessage = array_unique(explode("\n", $e->getMessage()));
                    $errormessageCart = end($errormessage);
                    $this->messageManager->addError(
                        \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Escaper')->escapeHtml($errormessageCart)
                    );
                }
                return;
            }
        } else {
            $this->messageManager->addError(__('We can\'t add this item to your shopping cart right now.'));
        }
    }

    protected function goBack($backUrl = null, $product = null)
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::_goBack($backUrl);
        }

        $result = [];

        if ($backUrl || $backUrl = $this->getBackUrl()) {
            $result['backUrl'] = $backUrl;
        } else {
            if ($product && !$product->getIsSalable()) {
                $result['product'] = [
                    'statusText' => __('Out of stock')
                ];
            }
        }

        /**
         * Start rewrite
         */
        $this->_setForward($result);


//        $this->getResponse()->representJson(
//            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
//        );
        /**
         * End rewrite
         */
    }

    /**
     * @param array $response
     * @return void
     */
    protected function _setForward($response)
    {
        $registry = $this->_getRegistry();
        $registry->unregister('matrixview_addtocart_response');
        $registry->register('matrixview_addtocart_response', $response);

        $this->_forward('add', 'cart', 'amasty_cart');
    }

    /**
     * @return \Magento\Framework\Registry
     */
    protected function _getRegistry()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Framework\Registry');
    }
}