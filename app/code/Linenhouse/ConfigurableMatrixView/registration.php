<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Linenhouse_ConfigurableMatrixView',
    __DIR__
);