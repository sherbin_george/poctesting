<?php
/**
 * @author     Balance Internet
 * @package    Linenhouse_Sales
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Linenhouse\Sales\Block\Shipping;

use Magento\Framework\View\Element\Template;

class Tracking extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Linenhouse\Sales\Helper\Data
     */
    private $helper;

    /**
     * Tracking constructor.
     * @param Template\Context $context
     * @param \Linenhouse\Sales\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Linenhouse\Sales\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context,$data);
        $this->helper = $helper;
    }

    /**
     * Get tracking web url
     *
     * @return int
     */
    public function getShippingCarrierUrl($_item)
    {
        // For now always this is auspost
        return $this->helper->getAuspostUrl();
    }

    /**
     * Get tracking code url
     *
     * @param $code
     * @return int|string
     */
    public function getShippingTrackingUrl($_item)
    {
        if ($_item) {
            // For now always this is auspost
            $trackingUrl = $this->helper->getAuspostTracking();
            $macros = [
                '{{TRACKING_CODE}}' => $_item->getNumber()
            ];
            if ($trackingUrl) {
                $trackingUrl = strtr($trackingUrl, $macros);
            }
            return $trackingUrl;
        }
    }
}
