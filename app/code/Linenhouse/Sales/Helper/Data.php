<?php
/**
 * @author     Balance Internet
 * @package    Linenhouse_Sales
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Linenhouse\Sales\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * Auspost website tracking URL
     *
     * @const string
     */
    const XML_PATH_SHIPPING_AUSPOST_URL = 'shipping/linenhouse_shipping/auspost_url';

    /**
     * Auspost tracking by tracking code
     *
     * @const string
     */
    const XML_PATH_SHIPPING_AUSPOST_TRACKING = 'shipping/linenhouse_shipping/auspost_tracking';

    /**
     * Retrieve selling block product limit
     *
     * @return int
     */
    public function getAuspostUrl()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHIPPING_AUSPOST_URL,
            ScopeInterface::SCOPE_STORE
            );
    }

    /**
     * Retrieve selling block product limit
     *
     * @return int
     */
    public function getAuspostTracking()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHIPPING_AUSPOST_TRACKING,
            ScopeInterface::SCOPE_STORE
        );
    }
}