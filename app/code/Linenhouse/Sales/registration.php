<?php
/**
 * @author     Balance Internet
 * @package    Linenhouse_Sales
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Linenhouse_Sales',
    __DIR__
);
