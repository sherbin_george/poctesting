<?php

namespace Linenhouse\Rewards\Observer\Behavior;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Mirasvit\Rewards\Helper\Behavior;
use Magento\Newsletter\Model\Subscriber;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class SiteNewsletterSignUp
 * @package Linenhouse\Rewards\Observer\Behavior
 */
class SiteNewsletterSignUp implements ObserverInterface
{
    /**
     * @var Behavior
     */
    protected $_rewardsBehavior;

    /**
     * @var Subscriber
     */
    protected $_subscriber;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * SiteNewsletterSignUp constructor.
     * @param Behavior $rewardsBehavior
     * @param Subscriber $subscriber
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Behavior $rewardsBehavior,
        Subscriber $subscriber,
        ManagerInterface $messageManager
    )
    {
        $this->_rewardsBehavior = $rewardsBehavior;
        $this->_subscriber = $subscriber;
        $this->_messageManager = $messageManager;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Customer\Api\Data\CustomerInterface $customer */
        $customer = $observer->getCustomer();

        if ($customer->getId()) {
            $subscriber = $this->_subscriber->loadByCustomerId($customer->getId());

            if (!$subscriber->isSubscribed()) {
                return $this;
            }

            $transaction = $this->_rewardsBehavior->processRule(
                'store_and_newsletter_sign_up',
                $customer->getId(),
                false,
                null
            );

            if ($transaction && $transaction->getComment()) {
                $this->_messageManager->addSuccess($transaction->getComment());
            }
        }

        return $this;
    }
}