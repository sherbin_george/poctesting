<?php

namespace Linenhouse\Rewards\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Config\Model\ResourceModel\Config;

/**
 * Class InstallData
 * @package Linenhouse\Rewards\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var Config
     */
    protected $_configResource;

    /**
     * UpgradeData constructor.
     * @param Config $configResource
     */
    public function __construct(Config $configResource)
    {
        $this->_configResource = $configResource;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->_setSiteAndNewsletterSignUpRewardRule();

        $setup->endSetup();
    }

    /**
     * @return void
     */
    protected function _setSiteAndNewsletterSignUpRewardRule()
    {
        $this->_configResource->saveConfig(
            'rewards/advanced_settings/custom_rules_list',
            'store_and_newsletter_sign_up, Store And Newsletter Sign Up',
            'default',
            0
        );
    }
}