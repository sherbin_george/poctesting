<?php

namespace Linenhouse\Rewards\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Config\Model\ResourceModel\Config;

/**
 * Class UpgradeData
 * @package Linenhouse\Rewards\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var Config
     */
    protected $configResource;

    /**
     * UpgradeData constructor.
     * @param Config $configResource
     */
    public function __construct(Config $configResource)
    {
        $this->configResource = $configResource;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->_renameSiteAndNewsletterSignUpRewardRule();
        }

        $setup->endSetup();
    }

    /**
     * @return void
     */
    protected function _renameSiteAndNewsletterSignUpRewardRule()
    {
        $this->configResource->saveConfig(
            'rewards/advanced_settings/custom_rules_list',
            'store_and_newsletter_sign_up, Customer signs up in store and newsletter',
            'default',
            0
        );
    }
}