<?php

namespace Linenhouse\Rewards\Ui\Earning\Form\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Mirasvit\Rewards\Ui\Earning\Form\Source\EarningCartStyle as Origin;

/**
 * Class EarningCartStyle
 * @package Linenhouse\Rewards\Ui\Earning\Form\Source
 */
class EarningCartStyle extends Origin implements OptionSourceInterface
{
    use ToEarningOptionArrayTrait;
}