<?php

namespace Linenhouse\Rewards\Ui\Earning\Form\Source;

use Magento\Framework\Phrase;

/**
 * Trait ToEarningOptionArrayTrait
 * @package Linenhouse\Rewards\Ui\Earning\Form\Source
 */
trait ToEarningOptionArrayTrait
{
    /**
     * @param array $options
     * @return array
     */
    public static function toEarningOptionArray($options)
    {
        $result = [];

        foreach ($options as $key => $value) {
            $label = '';
            if ($value instanceof Phrase) {
                $label = $value->getText();
            } else if (is_string($value)) {
                $label = $value;
            }

            $result[] = [
                'value' => $key,
                'label' => $label,
            ];
        }

        return $result;
    }
}