<?php

namespace Linenhouse\Rewards\Ui\Earning\Form\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Mirasvit\Rewards\Ui\Earning\Form\Source\EarningBehaviorStyle as Origin;

/**
 * Class EarningBehaviorStyle
 * @package Linenhouse\Rewards\Ui\Earning\Form\Source
 */
class EarningBehaviorStyle extends Origin implements OptionSourceInterface
{
    use ToEarningOptionArrayTrait;
}