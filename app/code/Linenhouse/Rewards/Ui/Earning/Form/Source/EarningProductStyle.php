<?php

namespace Linenhouse\Rewards\Ui\Earning\Form\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Mirasvit\Rewards\Ui\Earning\Form\Source\EarningProductStyle as Origin;

/**
 * Class EarningProductStyle
 * @package Linenhouse\Rewards\Ui\Earning\Form\Source
 */
class EarningProductStyle extends Origin implements OptionSourceInterface
{
    use ToEarningOptionArrayTrait;
}