<?php
/**
 * @author     Balance Internet
 * @package    Linenhouse_Catalog
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Linenhouse\Catalog\Cron;

/**
 * Product stock status refreshed based on the prodcut qty LHA0000-178
 */
class StockStatusRefresher
{
    /**
     * @var \Linenhouse\Catalog\Model\Product\StockStatusRefresher
     */
    protected $stockStatusRefresher;

    /**
     * StockStatusRefresher constructor.
     * @param \Linenhouse\Catalog\Model\Product\StockStatusRefresher\Proxy $stockStatusRefresher
     */
    public function __construct(
        \Linenhouse\Catalog\Model\Product\StockStatusRefresher\Proxy $stockStatusRefresher
    ) {
        $this->stockStatusRefresher = $stockStatusRefresher;
    }

    /**
     * Execute the model method
     */
    public function execute()
    {
        try {
            $this->stockStatusRefresher->refreshSimpleProducts();
            $this->stockStatusRefresher->refreshConfigurableProducts();
        } catch (\Exception $exception) {
            echo "ERRORS FOUND: ".$exception->getMessage();
        }
    }
}
