<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Helper\Gift;

use \Magento\Quote\Model\ResourceModel\Quote\Item\Option\CollectionFactory as QuoteRepository;

/**
 * Class Quote
 */
class Quote extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var QuoteFactory
     */
    private $_itemOptionCollectionFactory;


    /**
     * @param QuoteRepository $quoteFactory
     */
    public function __construct(QuoteRepository $quoteFactory)
    {
        $this->_itemOptionCollectionFactory = $quoteFactory;
    }

    /**
     * @param int $itemIds
     * @return float
     */
    public function getItemPrice($itemIds){
        $value = 0.;
        $optionCollection = $this->_itemOptionCollectionFactory->create()->addItemFilter($itemIds);
        if($optionCollection){
            foreach ($optionCollection as $item){
                if($item->getCode() == 'info_buyRequest'){
                    $valueOptions = unserialize($item->getValue());
                    $value = $valueOptions['giftcard_amount'];
                }
            }
        }
        return $value;
    }

    /**
     * @param int $itemIds
     * @return float
     */
    public function getItemCustomPrice($itemIds){
        $value = 0.;
        $optionCollection = $this->_itemOptionCollectionFactory->create()->addItemFilter($itemIds);
        if($optionCollection){
            foreach ($optionCollection as $item){
                if($item->getCode() == 'info_buyRequest'){
                    $valueOptions = unserialize($item->getValue());
                    $value = $valueOptions['custom_giftcard_amount'];
                }
            }
        }
        return $value;
    }
}
