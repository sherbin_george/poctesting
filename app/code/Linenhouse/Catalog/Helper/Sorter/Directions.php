<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Helper\Sorter;

/**
 * Class Directions
 */
class Directions
{
    /**
     * All possible sorterable directions
     *
     * @var array
     */
    protected $_directions = ['asc' => 'ASC', 'desc' => 'DESC'];

    /**
     * All possible credit card types
     *
     * @return array
     */
    public function getDirections()
    {
        return $this->_directions;
    }
}
