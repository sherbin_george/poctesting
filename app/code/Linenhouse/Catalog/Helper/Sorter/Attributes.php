<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Helper\Sorter;

use Magento\Catalog\Model\Config as CatalogConfig;

/**
 * Class Country
 */
class Attributes
{
    /**
     * @var CountryConfig
     */
    private $catalogConfig;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @param CollectionFactory $factory
     * @param CountryConfig $countryConfig
     */
    public function __construct(CatalogConfig $catalogConfig)
    {
        $this->catalogConfig = $catalogConfig;
    }

    /**
     * Returns countries array
     *
     * @return array
     */
    public function getAttributes()
    {
        if (!$this->attributes) {
            $this->attributes = $this->catalogConfig->getAttributeUsedForSortByArray();
        }

        return $this->attributes;
    }
}
