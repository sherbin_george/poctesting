<?php

namespace Linenhouse\Catalog\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;

/**
 * Class ProductInitAfterObserver
 * @package Linenhouse\Catalog\Observer
 */
class ProductInitAfterObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * ProductInitAfterObserver constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Registry $registry
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Registry $registry,
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->coreRegistry = $registry;
        $this->scopeConfig = $scopeConfig;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Set category in to registry on product init
     * The purpose to display category path in breadcrumbs if product PDP
     * accessed from anywhere, not only from category page
     *
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return $this
     */
    public function execute(Observer $observer)
    {
        if (!$this->scopeConfig->getValue(
            'catalog/frontend/breadcrumbs_extend',
            ScopeInterface::SCOPE_STORE
        )) {
            return $this;
        }

        $category = $this->coreRegistry->registry('current_category');
        if ($category) {
            return $this;
        }

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getProduct();
        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categories */
        $categories = $product->getCategoryCollection();

        $select = $categories->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns(['entity_id'])
            ->order('level DESC')
            ->limit(1);

        $categoryId = $categories->getConnection()->fetchOne($select);
        if ($categoryId) {
            $category = $this->categoryRepository->get($categoryId);

            if ($category) {
                $this->coreRegistry->register('current_category', $category);
            }
        }
    }
}