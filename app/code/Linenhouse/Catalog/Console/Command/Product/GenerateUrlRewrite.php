<?php

namespace Linenhouse\Catalog\Console\Command\Product;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

use Magento\Framework\App\State as AppState;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Store;
use Magento\Backend\App\Area\FrontNameResolver as BackendFrontNameResolver;

use Linenhouse\Catalog\Model\Product\UrlRewriteGenerator;

/**
 * Class GenerateUrlRewrite
 * @package Linenhouse\Catalog\Console\Command\Product
 */
class GenerateUrlRewrite extends Command
{
    /**
     * Console command sku argument name
     * @const string
     */
    const ARGUMENT_SKU = 'sku';

    /**
     * Console command store argument name
     * @const string
     */
    const ARGUMENT_STORE = 'store';
    /**
     * Console command all argument name
     * @const string
     */
    const ARGUMENT_ALL = 'all';

    /**
     * @var AppState
     */
    protected $appState;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var UrlRewriteGenerator
     */
    protected $generator;

    /**
     * @var array
     */
    protected $arguments = [
        self::ARGUMENT_SKU,
        self::ARGUMENT_STORE,
        self::ARGUMENT_ALL
    ];

    /**
     * GenerateUrlRewrite constructor.
     * @param AppState $appState
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param UrlRewriteGenerator $generator
     * @param null $name
     */
    public function __construct(
        AppState $appState,
        Registry $registry,
        StoreManagerInterface $storeManager,
        UrlRewriteGenerator $generator,
        $name = null
    )
    {
        parent::__construct($name);

        $this->appState = $appState;
        $this->registry = $registry;
        $this->storeManager = $storeManager;
        $this->generator = $generator;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('linen:product:generate-url-rewrite');

        foreach ($this->arguments as $name) {
            $this->addOption(
                $name,
                null,
                InputOption::VALUE_OPTIONAL
            );
        }

        $this->setDescription(
            "Re-generates url rewrites for products \n"
            . "Example: bin/magento esg:product:generate-url-rewrite productSku storeCode"
        );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->registry->register('isSecureArea', true);
        $this->appState->setAreaCode(BackendFrontNameResolver::AREA_CODE);
        $this->storeManager->setCurrentStore(Store::ADMIN_CODE);

        try {
            $this->generator->setOutput($output);
            $this->generator->generate(
                $input->getOption(self::ARGUMENT_SKU),
                $input->getOption(self::ARGUMENT_STORE),
                $input->getOption(self::ARGUMENT_ALL)
            );
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln($e->getTraceAsString());
        }
    }
}