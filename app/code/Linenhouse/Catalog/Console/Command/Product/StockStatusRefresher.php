<?php
/**
 * @author     Balance Internet
 * @package    Linenhouse_Catalog
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Linenhouse\Catalog\Console\Command\Product;

use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Framework\App\State as AppState;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Backend\App\Area\FrontNameResolver as BackendFrontNameResolver;
use Magento\Store\Model\Store;

/**
 * Class GenerateUrlRewrite
 * @package Linenhouse\Catalog\Console\Command\Product
 */
class StockStatusRefresher extends Command
{
    /**
     * @var AppState
     */
    protected $appState;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Linenhouse\Catalog\Model\Product\StockStatusRefresher\Proxy
     */
    protected $stockStatusRefresher;

    /**
     * StockStatusRefresher constructor.
     * @param AppState $appState
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param \Linenhouse\Catalog\Model\Product\StockStatusRefresher $stockStatusRefresher
     * @param null $name
     */
    public function __construct(
        AppState $appState,
        Registry $registry,
        StoreManagerInterface $storeManager,
        \Linenhouse\Catalog\Model\Product\StockStatusRefresher\Proxy $stockStatusRefresher,
        $name = null
    )
    {
        parent::__construct($name);

        $this->appState = $appState;
        $this->registry = $registry;
        $this->storeManager = $storeManager;
        $this->stockStatusRefresher = $stockStatusRefresher;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('linen:product:refresh-stock-status');

        $this->setDescription(
            "If a product is having qty > 0 and stock status = out-of-stock, make it in-stock"
            . "Example: bin/magento linen:product:refresh-stock-status"
        );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->registry->register('isSecureArea', true);
            $this->appState->setAreaCode(BackendFrontNameResolver::AREA_CODE);
            $this->storeManager->setCurrentStore(Store::ADMIN_CODE);
            $this->stockStatusRefresher->setOutput($output);
            $this->stockStatusRefresher->refreshSimpleProducts();
            $this->stockStatusRefresher->refreshConfigurableProducts();
        } catch (LocalizedException $exception) {
            $output->writeln($exception->getMessage());
            $output->writeln($exception->getTraceAsString());
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            $output->writeln($exception->getTraceAsString());
        }
    }
}