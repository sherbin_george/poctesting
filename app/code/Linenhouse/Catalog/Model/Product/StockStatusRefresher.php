<?php
/**
 * @author     Balance Internet
 * @package    Linenhouse_Catalog
 * @author     Balance Internet Team  <info@balanceinternet.com.au>
 * @copyright  Copyright (c) 2018, Balance Internet  (http://www.balanceinternet.com.au/)
 */

namespace Linenhouse\Catalog\Model\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class UrlRewriteGenerator
 * @package Linenhouse\Catalog\Model\Product
 */
class StockStatusRefresher
{
    const XML_CONFIG_STOCK_STATUS_REFRESH_CRON_STATUS = 'catalog/custom_settings/stock_refresh_cron_status';
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var OutputInterface
     */
    protected $output = null;

    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock\Item\CollectionFactory
     */
    protected $stockItemCollectionFactory;


    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * @var \Magento\Framework\App\Config\MutableScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Collection\SalableProcessor
     */
    protected $salableProcessor;

    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock\StatusFactory
     */
    private $stockStatusFactory;
    /**
     * @var \Magento\Store\Api\Data\WebsiteInterface
     */
    private $website;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    private $stockRegistry;
    /**
     * @var \Magento\CatalogInventory\Api\StockItemRepositoryInterface
     */
    private $stockItemRepository;
    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;
    /**
     * @var \Magento\CatalogInventory\Model\Indexer\Stock\Processor
     */
    private $stockIndexerProcessor;

    /**
     * StockStatusRefresher constructor.
     * @param CollectionFactory $collectionFactory
     * @param \Magento\CatalogInventory\Model\ResourceModel\Stock\Item\CollectionFactory $stockItemCollectionFactory
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Collection\SalableProcessor $salableProcessor
     * @param \Magento\CatalogInventory\Model\ResourceModel\Stock\StatusFactory $statusFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Store\Api\Data\WebsiteInterface $website
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockItemRepositoryInterface $stockItemRepository
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\CatalogInventory\Model\Indexer\Stock\Processor $stockIndexerProcessor
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        \Magento\CatalogInventory\Model\ResourceModel\Stock\Item\CollectionFactory $stockItemCollectionFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Collection\SalableProcessor $salableProcessor,
        \Magento\CatalogInventory\Model\ResourceModel\Stock\StatusFactory $statusFactory,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        \Magento\Store\Api\Data\WebsiteInterface $website,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockItemRepositoryInterface $stockItemRepository,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\CatalogInventory\Model\Indexer\Stock\Processor $stockIndexerProcessor
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->stockItemCollectionFactory = $stockItemCollectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->salableProcessor = $salableProcessor;
        $this->stockStatusFactory = $statusFactory;
        $this->website = $website;
        $this->stockRegistry = $stockRegistry;
        $this->stockItemRepository = $stockItemRepository;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->stockIndexerProcessor = $stockIndexerProcessor;
    }

    /**
     * @return bool
     */
    public function refreshSimpleProducts()
    {
        $status = $this->scopeConfig->getValue(self::XML_CONFIG_STOCK_STATUS_REFRESH_CRON_STATUS, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);

        if (!$status) {
            $this->showInfo('Cron is disabled from admin panel');
            return false;
        }

        // ** Simple product collection where qty > 0 but is_in_stock is 0
        $simpleUpdatedCount = 0;
        $simpleProducts = $this->getSimpleProducts();
        $this->connection = $simpleProducts->getConnection();

        $this->showInfo('Start processing the simple products for stock status');

        if ($itemCount = $simpleProducts->getSize()) {
            $this->showInfo('Processing simple products: '.$itemCount);
            // ** update the cataloginventory_stock_item.is_in_stock field to 1
            $simpleIdList = $simpleProducts->getAllIds();
            $simpleUpdatedCount = $this->refreshProductStockStatus($simpleProducts->getAllIds());
            $this->stockIndexerProcessor->reindexList($simpleIdList, true);
        }

        $this->showInfo('Updated simple products: '.$simpleUpdatedCount);

        return true;

    }

    /**
     * Refresh the configurable product list based on the simples.
     */
    public function refreshConfigurableProducts()
    {
        $status = $this->scopeConfig->getValue(self::XML_CONFIG_STOCK_STATUS_REFRESH_CRON_STATUS, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);

        if (!$status) {
            $this->showInfo('Cron is disabled from admin panel');
            return false;
        }

        // ** Get the configurable products ids having at least one child with qty > 0
        $this->showInfo('Start processing the configurable products for stock status');

        $configurableProducts = $this->getConfigurableIdsInStock();

        if ($configurableProducts) {
            //if ($configurableItemCount = $finalIdCollection->getSize()) {
            $this->showInfo('Processing configurable products: '.count($configurableProducts));

            // ** update the cataloginventory_stock_item.is_in_stock field to 1
            $this->updateConfigurableStockStatus($configurableProducts);
            $this->stockIndexerProcessor->reindexList($configurableProducts, true);
        }
        $this->showInfo('updated configurable products: '.count($configurableProducts));
    }
    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getSimpleProducts()
    {
        $collection = $this->collectionFactory->create();
        $collection->getSelect()->join(
            ['si' => $collection->getConnection()->getTableName('cataloginventory_stock_item')],
            'e.entity_id = si.product_id',
            ['e.sku', 'e.type_id']
        );
        $collection->addFieldToFilter('type_id', Product\Type::TYPE_SIMPLE);
        $collection->getSelect()->where('si.qty > ? ', 0);
        $collection->getSelect()->where('si.is_in_stock = ? ', 0);
        return $collection;
    }

    /**
     * @param $productIdsToUpdate
     * @return int
     */
    protected function refreshProductStockStatus($productIdsToUpdate)
    {
        if ($productIdsToUpdate && is_array($productIdsToUpdate)) {
            /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
            $connection = $this->connection;
            return $connection->update(
                $connection->getTableName('cataloginventory_stock_item'),
                ['is_in_stock' => 1],
                ['product_id in (?)' => $productIdsToUpdate]
            );
        }
    }


    /**
     * Get the configurable products ids having at least one child with qty > 0
     * @return array
     */
    public function getConfigurableIdsInStock()
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('type_id', Configurable::TYPE_CODE);
        $stockStatusResource = $this->stockStatusFactory->create();
        $stockStatusResource->addStockStatusToSelect($collection->getSelect(), $this->website);
        $collection->getSelect()->where('stock_status = ? ', 0);

        $configurableProductsToStockUpdate = [];
        foreach ($collection as $product) {
            if (!$product->isSalable($product)) {
                foreach ($product->getTypeInstance()->getUsedProducts($product) as $childProduct) {
                    if ($childProduct->isSaleable()) {
                        $configurableProductsToStockUpdate[$product->getId()] = $product->getId();
                        break;
                    }
                }
            }
        }
        return $configurableProductsToStockUpdate;
    }

    /**
     * update the configurable product ids using the logic \Magento\Catalog\Controller\Adminhtml\Product\Action\Attribute\Save::execute
     * @param $productIds
     */
    protected function updateConfigurableStockStatus($productIds)
    {
        $inventoryData = ['is_in_stock' => 1];
        if ($inventoryData) {
            // TODO why use ObjectManager?
            /** @var \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry */
            $stockRegistry = $this->stockRegistry;
            /** @var \Magento\CatalogInventory\Api\StockItemRepositoryInterface $stockItemRepository */
            $stockItemRepository = $this->stockItemRepository;

            foreach ($productIds as $productId) {
                $stockItemDo = $stockRegistry->getStockItem(
                    $productId
                );
                if (!$stockItemDo->getProductId()) {
                    $inventoryData[] = $productId;
                }

                $stockItemId = $stockItemDo->getId();
                $this->dataObjectHelper->populateWithArray(
                    $stockItemDo,
                    $inventoryData,
                    '\Magento\CatalogInventory\Api\Data\StockItemInterface'
                );
                $stockItemDo->setItemId($stockItemId);
                $stockItemRepository->save($stockItemDo);
            }
        }
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }


    /**
     * @param $message
     */
    protected function showInfo($message)
    {
        if ($this->isOutputValid()) {
            $this->output->writeln($message);
        }
    }

    /**
     * @return bool
     */
    protected function isOutputValid()
    {
        return $this->output instanceof OutputInterface;
    }
}