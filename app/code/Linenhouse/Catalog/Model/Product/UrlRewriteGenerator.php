<?php

namespace Linenhouse\Catalog\Model\Product;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Store\Model\StoreManagerInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Attribute\Source\Status;

/**
 * Class UrlRewriteGenerator
 * @package Linenhouse\Catalog\Model\Product
 */
class UrlRewriteGenerator
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;
    /**
     * @var visibility
     */
    protected $visibility;

    /**
     * Property carries about arguments with which generator started
     *
     * @var array
     */
    protected $args = [];

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * UrlRewriteGenerator constructor.
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        Visibility $visibility
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->visibility = $visibility;
    }

    /**
     * @param string $sku
     * @param string $store
     * @throws \Exception
     */
    public function generate($sku, $store, $all = false)
    {
        if ($store) {
            if (is_string($store)) {
                $store = $this->getStore($store);
            }

            $this->_generate(
                $this->getProductCollection($sku, $store, $all),
                $store
            );
        } else {
            foreach ($this->storeManager->getStores() as $store) {
                $this->_generate(
                    $this->getProductCollection($sku, $store, $all),
                    $store
                );
            }
        }
    }
    /**
     * @param string $code
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore($code)
    {
        return $this->storeManager->getStore($code);
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * @param Collection $products
     * @param StoreInterface $store
     * @throws \Exception
     * @return string
     */
    protected function _generate(Collection $products, $store)
    {
        foreach ($products as $product) {
            $product->setStoreId($store->getId());

            try {
                $this->urlPersist->deleteByData([
                    UrlRewrite::ENTITY_ID => $product->getId(),
                    UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                    UrlRewrite::REDIRECT_TYPE => 0,
                    UrlRewrite::STORE_ID => $product->getStoreId()
                ]);

                $this->urlPersist->replace($this->productUrlRewriteGenerator->generate($product));
            } catch (\Exception $e) {
                $this->showError($e->getMessage() . "...store {$store->getId()}: product {$product->getSku()}");
                continue;
            }

            $this->showStatus($product);
        }
    }
    /**
     * @param string $sku
     * @param StoreInterface $store
     * @return Collection
     */
    protected function getProductCollection($sku = null, $store, $all = false)
    {
        $visibility = $this->visibility->getVisibleInSiteIds();
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addStoreFilter($store->getId())
            ->addAttributeToFilter('visibility', ['in' => $visibility])
            ->addAttributeToFilter('url_key', ['notnull' => '']);

        if ($sku) {
            $skus = explode(',', $sku);
            $collection->addAttributeToFilter('sku', ['in' => $skus]);
        }
        elseif (!$all) {
            $ids = $this->getInvalidProductsOnly($store);
            if (!empty($ids)) {
                $collection->addFieldToFilter('entity_id', ['in' => $ids]);
            }
        }
        return $collection;
    }
    protected function getInvalidProductsOnly($store)
    {
        $invalidInCats = $this->getInvalidProductCategory($store);
        $invalidInDefault = $this->getInvalidProducts($store);

        return array_unique(array_merge($invalidInCats, $invalidInDefault));
    }
    /**
     * get All invalid product ids for categories
     * @param $store
     * @param $skus
     *
     * @return array
     */
    protected function getInvalidProductCategory($store)
    {
        $visibility = $this->visibility->getVisibleInSiteIds();
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $connection = $collection->getConnection();
        $collection->addStoreFilter($store->getId())
            ->addAttributeToFilter('visibility', ['in' => $visibility])
            ->addAttributeToFilter('status', Status::STATUS_ENABLED)
            ->addAttributeToFilter('url_key', ['notnull' => '']);
        $collection->getSelect()
            ->joinInner(
                ['cpi' => $collection->getTable('catalog_category_product_index')],
                'cpi.product_id = e.entity_id and cpi.is_parent=1'
                . $connection->quoteInto(' AND cpi.store_id = ?', $store->getId())
            )
            ->joinInner(
                ['ce' => $collection->getTable('catalog_category_entity')],
                'ce.entity_id = cpi.category_id and ce.level > 1'
            )
            ->joinLeft(
                ['rcp' => $collection->getTable('catalog_url_rewrite_product_category')],
                'rcp.product_id = cpi.product_id and rcp.category_id = cpi.category_id'
            )
            ->joinLeft(
                ['ur' => $collection->getTable('url_rewrite')],
                'ur.entity_id = rcp.product_id and rcp.url_rewrite_id = ur.url_rewrite_id and ur.is_autogenerated = 1'
                . $connection->quoteInto(' AND ur.store_id = ?', $store->getId())
                . $connection->quoteInto(' AND ur.entity_type = ?', ProductUrlRewriteGenerator::ENTITY_TYPE)
            )
            ->where('rcp.url_rewrite_id is null or ur.url_rewrite_id is null')
            ->reset('columns')
            ->columns('e.entity_id')
            ->group('e.entity_id')
        ;

        return $collection->getAllIds();
    }

    /**
     * get All Products which dont have valid URLs for like home age
     * @param $store
     *
     * @return mixed
     */
    protected  function getInvalidProducts($store)
    {
        $visibility = $this->visibility->getVisibleInSiteIds();
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $connection = $collection->getConnection();
        $collection->addStoreFilter($store->getId())
            ->addAttributeToFilter('visibility', ['in' => $visibility])
            ->addAttributeToFilter('status', Status::STATUS_ENABLED)
            ->addAttributeToFilter('url_key', ['notnull' => '']);
        $collection->getSelect()
            ->joinLeft(
                ['ur' => $collection->getTable('url_rewrite')],
                'ur.entity_id = e.entity_id and ur.is_autogenerated = 1'.
                ' and ur.request_path = IF(at_url_key.value_id > 0, at_url_key.value, at_url_key_default.value)'
                . $connection->quoteInto(' AND ur.store_id = ?', $store->getId())
                . $connection->quoteInto(' AND ur.entity_type = ?', ProductUrlRewriteGenerator::ENTITY_TYPE)
            )
            //->where('rcp.url_rewrite_id is null')
            ->where('ur.url_rewrite_id is null')
            ->reset('columns')
            ->columns('e.entity_id')
            ->group('e.entity_id')
        ;
        return $collection->getAllIds();
    }
    /**
     * @param \Magento\Catalog\Model\Product $product
     */
    protected function showStatus($product)
    {
        if ($this->isOutputValid()) {
            $this->output->writeln("Product {$product->getSku()} url rewrite regenerated"
                . " for store {$product->getStoreId()}.");
        }
    }

    /**
     * @param $message
     */
    protected function showError($message)
    {
        if ($this->isOutputValid()) {
            $this->output->writeln($message);
        }
    }

    /**
     * @return bool
     */
    protected function isOutputValid()
    {
        return $this->output instanceof OutputInterface;
    }
}