<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linenhouse\Catalog\Model;

use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Store\Model\ScopeInterface;

class Product extends ModelProduct
{
    /**
     * Check if product is available && stock <= 0
     *
     * @return bool
     */
    public function isBackOrder()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /**
         * @var \Magento\CatalogInventory\Api\StockStateInterface $stockState
         */
        $stockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
        /**
         * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
         */
        $scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

        $backOrdersFlag = (int) $scopeConfig->getValue(
            'cataloginventory/item_options/backorders',
            ScopeInterface::SCOPE_STORE
        );
        if ($backOrdersFlag) {
            return $this->isAvailable() && $stockState->getStockQty($this->getId(), $this->getStore()->getWebsiteId()) <= 0;
        }

        return false;
    }
}