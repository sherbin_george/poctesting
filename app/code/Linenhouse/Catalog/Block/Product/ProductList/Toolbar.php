<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Block\Product\ProductList;

/**
 * Product list toolbar
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Toolbar extends \Magento\Catalog\Block\Product\ProductList\Toolbar
{
    protected $_availableOrderCustom = null;
    const XML_SORTER_SCOPE = 'catalog/frontend/sorters';
    protected $_sorters = array();

    /**
     * Retrieve available Order fields list
     *
     * @return array
     */
    public function getAvailableOrdersCustom()
    {
        $this->loadAvailableOrdersCustom();
        return $this->_availableOrderCustom;
    }
    /**
     * Compare defined order field with current order field
     *
     * @param string $order
     * @return bool
     */
    public function isOrderCurrent($order)
    {
        if (!$this->useCustomSorter()) {
            return parent::isOrderCurrent($order);
        }
        return $this->getCurrentSorterCode($order) == $this->getCurrentOrder();
    }

    /**
     * Load Available Orders
     *
     * @return $this
     */
    private function loadAvailableOrdersCustom()
    {
        if ($this->_availableOrderCustom) {
            return $this;
        }
        $attributes = $this->_catalogConfig->getAttributeUsedForSortByArray();
        if ($this->useCustomSorter()) {
            $sorters = $this->getSortersScope();
            $options = array();
            foreach ($sorters as $sorter) {
                if (!empty($sorter['label']) && isset($attributes[$sorter['attribute_code']])) {
                    $options[$sorter['attribute_code'].'|'.$sorter['direction']] = __($sorter['label']);
                }
            }
        }
        $this->_availableOrderCustom = !empty($options) ? $options : $attributes;
        return $this;

    }

    /**
     * @return bool
     */
    public function useCustomSorter()
    {
        return !empty($this->getSortersScope());
    }

    /**
     * @return array|mixed
     */
    public function getSortersScope()
    {
        if (empty($this->_sorters)) {
            $sorters = $this->_scopeConfig->getValue(
                self::XML_SORTER_SCOPE,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $sorters = unserialize($sorters);
            $this->_sorters = $sorters ? $sorters : [];
        }

        return $this->_sorters;
    }

    /**
     * get attribute direction
     * @param $attr
     *
     * @return string | null
     */
    public function getDirection($attr)
    {
        $sorter = explode('|', $attr);
        return @$sorter[1];
    }
    /**
     * get attribute code
     * @param $attr
     *
     * @return string
     */
    public function getSorterCode($attr)
    {
        $sorter = explode('|', $attr);
        return @$sorter[0];
    }
    /**
     * get attribute code
     * @param $attr
     *
     * @return string
     */
    public function getCurrentSorterCode($attr)
    {
        if ($this->getCurrentDirection() == $this->getDirection($attr) || !$this->getDirection($attr)) {
            return $this->getSorterCode($attr);
        }
    }
}
