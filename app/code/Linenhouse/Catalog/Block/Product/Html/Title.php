<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Block\Product\Html;

use Magento\Framework\View\Element\Template;
use Magento\Theme\Block\Html\Title as MagentoTitle;
use Magento\Catalog\Block\Product\View;

class Title extends MagentoTitle
{
    /**
     * @var View
     */
    protected $product;

    /**
     * View constructor.
     * @param Template\Context $context
     * @param View $product
     * @param array $data
     */
    public function __construct(Template\Context $context, View $product, array $data = [])
    {
        $this->product = $product;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getPageHeading()
    {
        return $this->product->getProduct()->getName();
    }

}