<?php

namespace Linenhouse\Catalog\Block\Product;

use Magento\CatalogInventory\Helper\Stock as StockHelper;
use Magento\Checkout\Block\Cart\Crosssell as CartCrosssell;

/**
 * Class Crosssell
 * @package Linenhouse\Catalog\Block\Product
 */
class Crosssell extends CartCrosssell
{
    /**
     * @inheritdoc
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Catalog\Model\Product\LinkFactory $productLinkFactory,
        \Magento\Quote\Model\Quote\Item\RelatedProducts $itemRelationsList,
        StockHelper $stockHelper,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $checkoutSession,
            $productVisibility,
            $productLinkFactory,
            $itemRelationsList,
            $stockHelper,
            $data
        );

        $this->_isScopePrivate = false;
    }

    /**
     * @inheritdoc
     */
    public function getItems()
    {
        $items = $this->getData('items');
        if ($items === null && $this->getProduct()) {
            $collection = $this->_getCollection();
            $items = [];
            foreach ($collection as $item) {
                $items[] = $item;
            }

            $this->setData('items', $items);
        }

        return $items;
    }

    /**
     * @inheritdoc
     */
    protected function _getCollection()
    {
        $collection = $this->getProduct()
            ->getCrossSellProductCollection()
            ->setStoreId(
                $this->_storeManager->getStore()->getId()
            )->addStoreFilter()->setPageSize(
                $this->_maxItemCount
            )->setVisibility(
                $this->_productVisibility->getVisibleInCatalogIds()
            );
        $this->_addProductAttributesAndPrices($collection);

        return $collection;
    }
}