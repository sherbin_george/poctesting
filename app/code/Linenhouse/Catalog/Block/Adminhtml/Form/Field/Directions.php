<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Block\Adminhtml\Form\Field;

use Linenhouse\Catalog\Helper\Sorter\Directions as SorterHelper;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class CcTypes
 */
class Directions extends Select
{
    /**
     * @var CcType
     */
    private $_sorterHelper;

    /**
     * Constructor
     *
     * @param Context $context
     * @param CcType $ccTypeHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        SorterHelper $sorterHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_sorterHelper = $sorterHelper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->_sorterHelper->getDirections());
        }
        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
