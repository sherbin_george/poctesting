<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Block\Adminhtml\Form\Field;

use Magento\Framework\DataObject;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class SorterAttributes
 */
class SorterAttributes extends AbstractFieldArray
{
    /**
     * @var Countries
     */
    protected $attributeRenderer = null;

    /**
     * @var Directions
     */
    protected $directionRenderer = null;
    
    /**
     * Returns renderer for Attribute element
     * 
     * @return Countries
     */
    protected function getAttributesRenderer()
    {
        if (!$this->attributeRenderer) {
            $this->attributeRenderer = $this->getLayout()->createBlock(
                Attributes::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->attributeRenderer;
    }

    /**
     * Returns renderer for country element
     * 
     * @return CcTypes
     */
    protected function getDirectionsRenderer()
    {
        if (!$this->directionRenderer) {
            $this->directionRenderer = $this->getLayout()->createBlock(
                Directions::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->directionRenderer;
    }

    /**
     * Prepare to render
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'attribute_code',
            [
                'label'     => __('Attribute'),
                'renderer'  => $this->getAttributesRenderer(),
            ]
        );
        $this->addColumn(
            'direction',
            [
                'label' => __('Direction'),
                'renderer'  => $this->getDirectionsRenderer(),
            ]
        );
        $this->addColumn(
            'label',
            [ 'label' => __('Label') ]
        );
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Sorter');
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $attribute = $row->getAttributeCode();
        $options = [];
        if ($attribute) {
            $options['option_' . $this->getAttributesRenderer()->calcOptionHash($attribute)]
                = 'selected="selected"';

            $direction = $row->getDirection();
            $options['option_' . $this->getDirectionsRenderer()->calcOptionHash($direction)]
                = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}
