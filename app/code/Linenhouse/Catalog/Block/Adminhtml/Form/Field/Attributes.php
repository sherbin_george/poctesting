<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Linenhouse\Catalog\Block\Adminhtml\Form\Field;

use Linenhouse\Catalog\Helper\Sorter\Attributes as AttributeConfig;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class Countries
 */
class Attributes extends Select
{
    /**
     * @var Country
     */
    private $attributeHelper;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Country $countryHelper
     * @param array $data
     */
    public function __construct(Context $context, AttributeConfig $attributeConfig, array $data = [])
    {
        parent::__construct($context, $data);
        $this->attributeHelper = $attributeConfig;
    }
    
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->attributeHelper->getAttributes());
        }
        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     * 
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
