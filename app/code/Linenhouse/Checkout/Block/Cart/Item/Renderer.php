<?php

namespace Linenhouse\Checkout\Block\Cart\Item;

use Magento\Quote\Model\Quote\Item\AbstractItem as AbstractQuoteItem;

/**
 * Class Renderer
 * @package Linenhouse\Checkout\Block\Cart\Item
 */
class Renderer extends \Magento\Checkout\Block\Cart\Item\Renderer implements \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * Need to set correct template because of rewrite
     *
     * @inheritdoc
     */
    public function _beforeToHtml()
    {
        if (strpos($this->getTemplate(),'::') === false) {
            $this->setTemplate('Magento_Checkout::' . $this->getTemplate());
        }

        return parent::_beforeToHtml();
    }

    /**
     * @param AbstractQuoteItem $quoteItem
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getOriginalPriceHtml($quoteItem)
    {
        return $this->getLayout()->getBlock('checkout.item.price.original')->setItem($quoteItem)->toHtml();
    }

    /**
     * @param AbstractQuoteItem $quoteItem
     * @return bool
     */
    public function hasSpecialPrice($quoteItem)
    {
        /** @var \Linenhouse\Checkout\Helper\OriginPrice $helper */
        $helper = \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Linenhouse\Checkout\Helper\OriginPrice::class);

        return $helper->hasSpecialPrice($quoteItem);
    }
}