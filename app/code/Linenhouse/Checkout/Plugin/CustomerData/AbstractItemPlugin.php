<?php

namespace Linenhouse\Checkout\Plugin\CustomerData;

use Magento\Catalog\Pricing\Price\RegularPrice;

/**
 * Class AbstractItemPlugin
 * @package Linenhouse\Checkout\Plugin\CustomerData
 */
class AbstractItemPlugin
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * @var \Linenhouse\Checkout\Helper\OriginPrice
     */
    protected $helper;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * AbstractItemPlugin constructor.
     * @param \Magento\Checkout\Model\Session $session
     * @param \Linenhouse\Checkout\Helper\OriginPrice $helper
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     */
    public function __construct(
        \Magento\Checkout\Model\Session $session,
        \Linenhouse\Checkout\Helper\OriginPrice $helper,
        \Magento\Checkout\Helper\Data $checkoutHelper
    )
    {
        $this->session = $session;
        $this->helper = $helper;
        $this->checkoutHelper = $checkoutHelper;
    }

    /**
     * Assign Original Price to items in mini-cart
     *
     * @param \Magento\Checkout\CustomerData\AbstractItem $subject
     * @param $result
     * @return mixed
     */
    public function afterGetItemData(\Magento\Checkout\CustomerData\AbstractItem $subject, $result)
    {
        if (isset($result['item_id'])) {
            $item = $this->session->getQuote()->getItemById($result['item_id']);

            if ($this->helper->hasSpecialPrice($item)) {
                if($item->getProductType() == 'configurable') {
                    $price = $item->getProduct()->getPrice();
                } else {
                    $price = $this->helper->getPriceType($item, RegularPrice::PRICE_CODE);
                }

                $price = $this->checkoutHelper->formatPrice($price);
                $result['originalPrice'] = $price;
            }
        }

        return $result;
    }
}