<?php

namespace Linenhouse\Checkout\Helper;

use Magento\Quote\Model\Quote\Item\AbstractItem as AbstractQuoteItem;
use Magento\Catalog\Pricing\Price;

/**
 * Class OriginPrice
 * @package Linenhouse\Checkout\Helper
 */
class OriginPrice extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param AbstractQuoteItem $quoteItem
     * @return bool
     */
    public function hasSpecialPrice($quoteItem)
    {
        if (!($quoteItem instanceof AbstractQuoteItem)) {
            return false;
        }

        $displayRegularPrice = $this->getPriceType($quoteItem, Price\RegularPrice::PRICE_CODE);
        $displayFinalPrice = $this->getPriceType($quoteItem, Price\FinalPrice::PRICE_CODE);

        return $displayFinalPrice < $displayRegularPrice;
    }

    /**
     * @param AbstractQuoteItem $quoteItem
     * @param string $type
     * @return float
     */
    public function getPriceType($quoteItem, $type)
    {
        return $quoteItem->getProduct()
            ->getPriceInfo()
            ->getPrice($type)
            ->getAmount()
            ->getValue();
    }
}