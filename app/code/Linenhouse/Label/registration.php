<?php
/**
 *  * Developed by Balanceinternet
 *  *
 *  * Do not edit or add to this file if you wish to upgrade to newer
 *  * versions in the future.
 *  *
 *  * @author      Hai@balanceinternet.com.au
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Linenhouse_Label',
    __DIR__
);
