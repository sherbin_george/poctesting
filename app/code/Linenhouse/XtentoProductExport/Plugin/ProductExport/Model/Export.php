<?php

namespace Linenhouse\XtentoProductExport\Plugin\ProductExport\Model;


/**
 * Class Export
 * @package Linenhouse\XtentoProductExport\Plugin\ProductExport\Model
 */
class Export
{
    protected $_emulator;

    /**
     * Export constructor.
     *
     * @param \Magento\Store\Model\App\Emulation $appEmulation
     */
    public function __construct(\Magento\Store\Model\App\Emulation $appEmulation)
    {
        $this->_emulator = $appEmulation;
    }

    public function beforeManualExport(\Xtento\ProductExport\Model\Export $subject, $filters)
    {
        $this->_emulator->startEnvironmentEmulation($subject->getProfile()->getStoreId());
    }

    public function beforeCronExport(\Xtento\ProductExport\Model\Export $subject, $filters)
    {
        $this->_emulator->startEnvironmentEmulation($subject->getProfile()->getStoreId());
    }
}