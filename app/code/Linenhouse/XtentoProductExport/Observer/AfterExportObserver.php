<?php

namespace Linenhouse\XtentoProductExport\Observer;

use Magento\Framework\Event\ObserverInterface;

class AfterExportObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_emulator;

    public function __construct(
        \Magento\Store\Model\App\Emulation $appEmulation
    ) {
        $this->_emulator = $appEmulation;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_emulator->stopEnvironmentEmulation();
        return $this;
    }
}
