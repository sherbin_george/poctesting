<?php
namespace Linenhouse\Customer\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Setup\CustomerSetup;
/**
 * Class UpgradeData
 * @package Linenhouse\Customer\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var Config
     */
    protected $configResource;

    /**
     * @var \Magento\Customer\Setup\CustomerSetup
     */
    protected $customerSetupFactory;

    /**
     * @var \Magento\Customer\Setup\CustomerSetup
     */
    protected $customerSetup;

    /**
     * UpgradeData constructor.
     * @param Config $configResource
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     * @param \Magento\Customer\Setup\CustomerSetup $customerSetup
     */
    public function __construct(
        Config $configResource,
        CustomerSetupFactory $customerSetupFactory,
        CustomerSetup $customerSetup
    )
    {
        $this->configResource = $configResource;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->customerSetup = $customerSetup;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $this->_updateCustomerAttribute($setup);
        }

        if (version_compare($context->getVersion(), '2.0.2', '<')) {
            $this->_updateCustomerAttributeTotalSpendCustom($setup);
        }

        $setup->endSetup();
    }

    /**
     * @return void
     */
    protected function _updateCustomerAttribute($setup)
    {
        $attributeCode = 'timeframe_12_months';
        $attributeId = $this->customerSetup->getAttributeId(
            Customer::ENTITY,
            $attributeCode
        );

        if (!$attributeId) {
            $setup->startSetup();
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, $attributeCode, [
                'type' => 'varchar',
                'label' => 'Timeframe 12 Months',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => false,
                'position' => 333,
                'system' => false,
                'backend' => ''
            ]);

            $setup->endSetup();
        }
    }

    /**
     * @return void
     */
    protected function _updateCustomerAttributeTotalSpendCustom($setup)
    {
        $attributeCode = 'total_spend_custom';
        $attributeId = $this->customerSetup->getAttributeId(
            Customer::ENTITY,
            $attributeCode
        );

        if (!$attributeId) {
            $setup->startSetup();
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, $attributeCode, [
                'type' => 'varchar',
                'label' => 'Total Spend Custom',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => false,
                'position' => 334,
                'system' => false,
                'backend' => ''
            ]);

            $setup->endSetup();
        }
    }
}