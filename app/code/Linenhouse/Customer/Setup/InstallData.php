<?php

namespace Linenhouse\Customer\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Config\Model\ResourceModel\Config;

/**
 * Class InstallData
 * @package Linenhouse\Customer\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var Config
     */
    protected $_configResource;

    /**
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    protected $_customerSetupFactory;

    /**
     * UpgradeData constructor.
     * @param Config $configResource
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        Config $configResource,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    )
    {
        $this->_configResource = $configResource;
        $this->_customerSetupFactory = $customerSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'timeframe_12_months', [
            'type' => 'varchar',
            'label' => 'Timeframe 12 Months',
            'input' => 'text',
            'source' => '',
            'required' => false,
            'visible' => false,
            'position' => 333,
            'system' => false,
            'backend' => ''
        ]);

        $setup->endSetup();
    }
}