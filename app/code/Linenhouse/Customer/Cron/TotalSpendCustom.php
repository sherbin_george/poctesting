<?php
namespace Linenhouse\Customer\Cron;

/**
 * Class Linenhouse\Customer\Cron\TotalSpendCustom
 *
 */
class TotalSpendCustom extends TotalSpendAbstract
{
    /**
     * Execute method
     *
     * @return void
     */
    public function execute()
    {
        $this->collectTotalSpend(
            'total_spend_custom',
            'TOTAL_SPEND_CUSTOM',
            \Linenhouse\Customer\Model\Config\Source\TimeFrame::XML_PATH_CONNECTOR_MAPPING_CUSTOMER_TIMEFRAME
        );
    }

}