<?php
namespace Linenhouse\Customer\Cron;

use \Magento\Framework\App\ResourceConnection;
use \Magento\Eav\Model\EntityFactory;

/**
 * Class Linenhouse\Customer\Cron\TotalSpendAbstract
 */
class TotalSpendAbstract
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Dotdigitalgroup\Email\Helper\Data
     */
    private $helper;
    /**
     * @var \Dotdigitalgroup\Email\Model\ImporterFactory
     */
    private $importerFactory;
    /**
     * @var \Magento\Eav\Model\EntityFactory
     */
    private $entityFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    private $datetime;
    /**
     * @var array
     */
    private $attributes = [];
    private $read;
    private $write;
    /**
     * TotalSpend constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection    $resourceConnection
     * @param \Dotdigitalgroup\Email\Helper\Data           $helper
     * @param \Dotdigitalgroup\Email\Model\ImporterFactory $importerFactory
     * @param \Magento\Eav\Model\EntityFactory             $entityFactory
     * @param \Magento\Framework\Stdlib\DateTime           $datetime
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        \Dotdigitalgroup\Email\Helper\Data $helper,
        \Dotdigitalgroup\Email\Model\ImporterFactory $importerFactory,
        EntityFactory $entityFactory,
        \Magento\Framework\Stdlib\DateTime $datetime
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->helper = $helper;
        $this->importerFactory = $importerFactory;
        $this->entityFactory = $entityFactory;
        $this->datetime = $datetime;
        $this->read = $this->resourceConnection->getConnection('read');
        $this->write = $this->resourceConnection->getConnection('write');
    }

    /**
     * @param string $magentoType
     * @param string $ddgType
     * @param string $timeRage
     */
    public function collectTotalSpend(
        $magentoType = 'timeframe_12_months',
        $ddgType = 'TOTAL_SPEND_12_MONS',
        $timeRage = '-1 year'
    )
    {
        $read = $this->read;
        $write = $this->write;
        /**
         * Define required tables
         */
        $salesOrderTbl = $read->getTableName('sales_order');
        $storeTbl = $read->getTableName('store');
        $customerTbl = $read->getTableName('customer_entity');
        $totalSpend12MonthdAttr = $this->getCustomerAttribute($magentoType);
        $attributeId = $totalSpend12MonthdAttr->getId();
        $attributeIdTbl = $totalSpend12MonthdAttr->getBackend()->getTable();
        /**
         * Dotmailer queue importer table
         */
        $importerTbl = $read->getTableName('email_importer');

        $select = $read->select()
            ->from(['e' => $salesOrderTbl])
            ->join(
                ['s' => $storeTbl],
                's.store_id = e.store_id'
            )
            ->join(
                ['c' => $customerTbl],
                'c.entity_id = e.customer_id and c.website_id = s.website_id' // change this if we want to count on guest orders for customes who made guest orders before registered
            )
            ->joinLeft(
                ['ta' => $attributeIdTbl],
                'ta.entity_id = c.entity_id and ta.attribute_id = '.$attributeId
            )
            ->where($this->joinStatusForWebsites('s.website_id', 'e.status', $read))
            ->where($this->joinDateTimeForWebsites('s.website_id', 'e.created_at', $timeRage))
            ->group(['c.entity_id', 'c.email', 'c.website_id','ta.value'])
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns(['c.entity_id', 'c.email', 'total_spend' => 'sum(e.grand_total)','c.website_id', 'old_total'=> 'ta.value'])
            ->having('toal_spend != old_total') // get only customers that have changes in specific time frame
        ;
        $subselect = clone $select;
        $customerToRemove = $read->select()
            ->from(['e' => $customerTbl])
            ->join(
                ['v' => $attributeIdTbl],
                'v.entity_id = e.entity_id and v.attribute_id = '.$attributeId
            )
            ->joinLeft(
                ['subselect' => $subselect->reset(\Zend_Db_Select::HAVING)], // reset having to get all customers that are not in specific time frame
                'e.entity_id = subselect.entity_id'
            )
            ->where('v.value >?', 0)
            ->where('subselect.entity_id is null')
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns(['e.entity_id', 'e.email', 'total_spend' => new \Zend_Db_Expr(0),'e.website_id', 'old_total'=> 'v.value'])
            ;
        $write->beginTransaction();
        try {
            /**
             * remove old value
             */
            $this->processData($customerToRemove, $ddgType, $attributeId, $attributeIdTbl, $importerTbl);
            /**
             * update only new data
             */
            $this->processData($select, $ddgType, $attributeId, $attributeIdTbl, $importerTbl);
            $write->commit();
        }
        catch (\Exception $e) {
            echo $e->__toString();
            $this->helper->log($e);
            $write->rollback();
        }
    }

    /**
     * @param $select
     * @param $ddgType
     * @param $attributeId
     * @param $attributeIdTbl
     * @param $importerTbl
     */
    protected function processData(
        $select,
        $ddgType,
        $attributeId,
        $attributeIdTbl,
        $importerTbl
    )
    {
        $read = $this->read;
        $write = $this->write;
        $count = 0;
        $customerAttrData = [];
        $importerData = [];
        $result = $read->query($select);
        $importType = \Dotdigitalgroup\Email\Model\Importer::IMPORT_TYPE_CONTACT_UPDATE;
        $importName = \Dotdigitalgroup\Email\Model\Importer::MODE_CONTACT_EMAIL_UPDATE;
        $now = $this->datetime->formatDate(true);
        while ($row = $result->fetch()) {
            /**
             * dot not update the customers havent changed total
             */
            if ($row['total_spend'] == $row['old_total']) {
                continue;
            }
            $customerAttrData[] = [
                'attribute_id' => $attributeId,
                'entity_id' => $row['entity_id'],
                'value' => $row['total_spend']
            ];
            $data = [
                'emailBefore' => $row['email'],
                'email' => $row['email'],
                'isSubscribed' => true,
                'dataFields' => ['key' => $ddgType, 'value' => $row['total_spend']]
            ];
            $importerData[$row['entity_id']] = [
                'import_type' => $importType,
                'website_id' => $row['website_id'],
                'import_status' => 0,
                'import_id' => '',
                'import_data' => serialize($data),
                'import_mode' => $importName,
                'import_file' => '',
                'message' => '',
                'created_at' => $now
            ];
            if ($count % 1000) {
                $write->insertOnDuplicate($attributeIdTbl, $customerAttrData);
                $customerAttrData = [];
                $write->insertOnDuplicate($importerTbl, $importerData);
                $importerData = [];
            }
            $count++;
        }
        if (!empty($customerAttrData)) {
            $write->insertOnDuplicate($attributeIdTbl, $customerAttrData);
        }
        if (!empty($importerData)) {
            $write->insertOnDuplicate($importerTbl, $importerData);
        }
    }
    /**
     * @param $code
     *
     * @return mixed
     */
    protected function getCustomerAttribute($code)
    {
        if (!isset($this->attributes[$code])) {
            $this->attributes[$code] = $this->entityFactory->create()->setType('customer')->getAttribute($code);
        }
        return $this->attributes[$code];
    }

    /**
     * @param $websiteField
     * @param $statusField
     * @param $read
     *
     * @return string
     */
    protected function joinStatusForWebsites($websiteField, $statusField, $read)
    {
        $result = "";
        $websites  = $this->helper->getWebsites(true);
        $i = 1;
        $end = "";
        foreach ($websites as $website) {
            $id = $website->getId();
            $statuses = $this->helper->getWebsiteConfig(
                \Dotdigitalgroup\Email\Helper\Config::XML_PATH_CONNECTOR_SYNC_DATA_FIELDS_STATUS,
                $id
            );
            $statuses = $read->quote(explode(',', $statuses));
            if ($i == count($websites)) {
                $result .= "$statusField IN($statuses)";
            }
            else {
                $result .= "IF($websiteField=$id, $statusField IN($statuses),";
                $end .= ")";
            }

            $i++;
        }
        return $result.$end;
    }

    /**
     * @param $websiteField
     * @param $dateField
     * @param $dateValue
     *
     * @return string
     */
    protected function joinDateTimeForWebsites($websiteField, $dateField, $dateValue)
    {
        if (strtotime($dateValue)) {
            $fromDate = $this->datetime->formatDate($dateValue);
            return "$dateField >= '$fromDate'";
        }

        $result = "";
        $websites  = $this->helper->getWebsites(true);
        $i = 1;
        $end = "";

        foreach ($websites as $website) {
            $id = $website->getId();
            $date = $this->helper->getWebsiteConfig($dateValue, $id);
            if ($date == \Linenhouse\Customer\Model\Config\Source\TimeFrame::CUSTOM) {
                $date = $this->helper->getWebsiteConfig(
                    \Linenhouse\Customer\Model\Config\Source\TimeFrame::XML_PATH_CUSTOM,
                    $id
                );
            }
            $date  = $this->datetime->formatDate(strtotime($date) ? $date : true);
            if ($i == count($websites)) {
                $result .= "$dateField >= '$date'";
            }
            else {
                $result .= "IF($websiteField=$id, $dateField >= '$date',";
                $end .= ")";
            }

            $i++;
        }
        return $result.$end;
    }

}