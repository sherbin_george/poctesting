<?php
/**
 *  Developed by Balanceinternet
 *  Do not edit or add to this file if you wish to upgrade This to newer
 *  versions in the future.
 *  @author      Hai@balanceinternet.com.au
 *
 */

namespace Linenhouse\Customer\Cron;

/**
 * Class Linenhouse\Customer\Cron
 */
class TotalSpend extends TotalSpendAbstract
{
    /**
     * Execute method
     *
     * @return void
     */
    public function execute()
    {
        $this->collectTotalSpend();
    }

}