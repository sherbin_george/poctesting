<?php

namespace Linenhouse\Customer\Model\Config\Source;

class TimeFrame implements \Magento\Framework\Option\ArrayInterface
{
    CONST CUSTOM = 'custom';
    CONST XML_PATH_CUSTOM = 'connector_data_mapping/customer_totalspend_custom/timeframe_from';
    CONST XML_PATH_CONNECTOR_MAPPING_CUSTOMER_TIMEFRAME = 'connector_data_mapping/customer_totalspend_custom/timeframe';
    /**
     * Retrieve list of options.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $fields = [
            ['value' => '-1 month', 'label' => 'Last 1 month'],
            ['value' => '-2 month', 'label' => 'Last 2 months'],
            ['value' => '-3 month', 'label' => 'Last 3 months'],
            ['value' => '-4 month', 'label' => 'Last 4 months'],
            ['value' => '-5 month', 'label' => 'Last 5 months'],
            ['value' => '-6 month', 'label' => 'Last 6 months'],
            ['value' => '-7 month', 'label' => 'Last 7 months'],
            ['value' => '-8 month', 'label' => 'Last 8 months'],
            ['value' => '-9 month', 'label' => 'Last 9 months'],
            ['value' => '-10 month', 'label' => 'Last 10 months'],
            ['value' => '-11 month', 'label' => 'Last 11 months'],
            ['value' => self::CUSTOM, 'label' => 'Custom'],
        ];
        return $fields;
    }
}
