define([
    'jquery',
    'uiComponent',
    'underscore',
    'Magento_Customer/js/customer-data',
    'ko',
    'jquery/jquery-storageapi'
], function ($, Component, _, customerData, ko) {
    'use strict';

    return Component.extend({
        defaults: {
            dummyCookieMessages: [],
            cookieMessages: ko.observable([]),
            messages: [],
            messagesOld: ko.observable([])
        },

        /** @inheritdoc */
        initialize: function () {
            var self = this;

            this._super();

            /**
             * Clear cookie
             */
            $(window).on('beforeunload', function () {
                $.cookieStorage.set('mage-messages', '');
            });

            this.dummyCookieMessages = $.cookieStorage.get('mage-messages');

            this.messages = customerData.get('messages').extend({
                disposableCustomerData: 'messages'
            });

            if (!_.isEmpty(this.messages().messages)) {
                /**
                 * The message should be shown at least once before it's removed
                 */
                this.messagesOld(this.messages().messages);
                customerData.set('messages', {});
            }

            if (!_.isEmpty(this.dummyCookieMessages)) {
                this.cookieMessages(this.dummyCookieMessages);
                $.cookieStorage.set('mage-messages', '');
            }

            /**
             * Clean old messages
             */
            this.messages.subscribe(function (newMessages) {
                /**
                 * Clear cookie messages if ajax returned cookie with messages
                 */
                $.cookieStorage.set('mage-messages', '');

                if ((!_.isEmpty(newMessages['messages']))) {
                    self.messagesOld([]);
                    self.cookieMessages([]);
                }
            });

            /**
             * If load of the page or ajax request doesn't triggered update message section
             * We should check is there was messages to show
             */
            if (_.isEmpty(this.messages().messages) && _.isEmpty(this.messagesOld())) {
                customerData.invalidate(['messages']);
                customerData.reload(['messages'], true);
            }
        }
    });
});