<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category Linenhouse
 * @package Linenhouse_CustomerSegment
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */

namespace Linenhouse\CustomerSegment\Model\Segment\Condition\RewardPoints;

use Magento\Framework\App\ResourceConnection;
use Magento\Rule\Model\Condition\AbstractCondition;
use Magento\Rule\Model\Condition\Context;
use Mirasvit\CustomerSegment\Api\Service\OperatorConversionInterface;
use Mirasvit\Rewards\Helper\Balance;
use Magento\Framework\Model\AbstractModel;

/**
 * Class adds ability to validate current active shopping cart amount values.
 *
 * @category Linenhouse
 * @package Linenhouse_CustomerSegment
 * @author Balance Internet Pty. , Ltd <dev@balanceinternet.com.au>
 * @copyright 2017 Balance Internet Pty., Ltd. All rights reserved.
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link http://balanceinternet.com.au/
 */
class Amount extends AbstractCondition
{
    /**
     * @inheritdoc
     */
    protected $_inputType = 'numeric';

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var OperatorConversionInterface
     */
    private $operatorConverter;

    /**
     * @var Balance
     */
    private $balance;

    /**
     * Amount constructor.
     *
     * @param OperatorConversionInterface $operatorConverter
     * @param ResourceConnection $resourceConnection
     * @param Context $context
     * @param Balance $balance
     * @param array $data
     */
    public function __construct(
        OperatorConversionInterface $operatorConverter,
        ResourceConnection $resourceConnection,
        Context $context,
        Balance $balance,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->resourceConnection = $resourceConnection;
        $this->operatorConverter = $operatorConverter;
        $this->balance = $balance;
    }

    /**
     * Customize default operator input by type mapper for numeric type.
     *
     * {@inheritdoc}
     */
    public function getDefaultOperatorInputByType()
    {
        if (null === $this->_defaultOperatorInputByType) {
            parent::getDefaultOperatorInputByType();
            $this->_defaultOperatorInputByType['numeric'] = array('==', '!=', '>=', '>', '<=', '<');
        }

        return $this->_defaultOperatorInputByType;
    }

    /**
     * {@inheritDoc}
     */
    public function asHtml()
    {
        return $this->getTypeElementHtml()
            . __('Reward points %1 amount %2',
                $this->getOperatorElementHtml(),
                $this->getValueElementHtml()
            )
            . $this->getRemoveLinkHtml();
    }

    /**
     * Validate rule conditions to segment customers.
     *
     * @param AbstractModel $object
     *
     * @return bool
     */

    public function validate(AbstractModel $object)
    {
        return $this->validateAttribute($this->balance->getBalancePoints($object->getCustomerId()));
    }
}
