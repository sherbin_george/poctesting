<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linenhouse\AccountMenu\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Customer\Model\Session;
use \Magento\Customer\Model\Group;

class AccountMenu extends Template
{
    /**
     * @var Group
     */
    private $customerGroupCollection;

    /**
     * @var Session
     */
    private $session;

    /**
     * AccountMenu constructor.
     * @param Template\Context $context
     * @param Session $customerSession
     * @param Group $customerGroupCollection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Session $customerSession,
        Group $customerGroupCollection,
        array $data = []
    ) {
        $this->customerGroupCollection = $customerGroupCollection;
        $this->session = $customerSession;

        parent::__construct($context, $data);
    }
    
    /**
     * Get customer group name.
     *
     * @return string
     */
    public function getCustomerGroupName()
    {
        $currentGroupId = $this->session->getCustomer()->getGroupId();
        $collection = $this->customerGroupCollection->load($currentGroupId);
        
        return (string) $collection->getCustomerGroupCode();
    }
}
