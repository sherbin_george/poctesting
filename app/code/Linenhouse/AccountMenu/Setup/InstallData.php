<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linenhouse\AccountMenu\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Model\BlockFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @param BlockFactory $modelBlockFactory
     */
    public function __construct(BlockFactory $modelBlockFactory)
    {
        $this->blockFactory = $modelBlockFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $cmsBlock = [
            'title' => 'Account menu silver',
            'identifier' => 'account_menu_silver',
            'content' => '',
            'is_active' => 1,
            'stores' => 0,
        ];

        $this->blockFactory->create()->setData($cmsBlock)->save();

        $setup->endSetup();
    }
}
