<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'LinenHouse_FreeShippingCounter',
    __DIR__
);