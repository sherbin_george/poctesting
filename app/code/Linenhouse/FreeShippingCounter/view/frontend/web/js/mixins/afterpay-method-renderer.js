/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/payment/additional-validators'
], function (customerData, additionalValidators) {
    return function (target) {
        var continueAfterpayPayment = target.prototype.continueAfterpayPayment;

        target.prototype.continueAfterpayPayment = function () {
            continueAfterpayPayment.apply(this, arguments);

            var afterpay = window.checkoutConfig.payment.afterpay;
            if (afterpay.paymentAction !== 'order'  && additionalValidators.validate()) {
                customerData.invalidate(['shipping-counter']);
            }
        };

        return target;
    }
});