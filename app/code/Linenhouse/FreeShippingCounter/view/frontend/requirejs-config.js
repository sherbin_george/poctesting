/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    config: {
        mixins: {
            'Balance_Afterpay/js/view/payment/method-renderer/afterpaypayovertime': {
                'LinenHouse_FreeShippingCounter/js/mixins/afterpay-method-renderer' : true
            }
        }
    }
};