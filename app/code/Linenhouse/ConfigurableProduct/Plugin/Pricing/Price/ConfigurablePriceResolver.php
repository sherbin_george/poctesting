<?php
/**
 * Copyright © 2018 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linenhouse\ConfigurableProduct\Plugin\Pricing\Price;

class ConfigurablePriceResolver
{
    protected $_storeManager;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->_storeManager = $storeManager;
    }

    /**
     * Fix product is zero after when out stock
     *
     * @param \Magento\ConfigurableProduct\Pricing\Price\ConfigurablePriceResolver $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Pricing\SaleableInterface $product
     * @return float|null
     */
    public function aroundResolvePrice($subject, \Closure $proceed, \Magento\Framework\Pricing\SaleableInterface $product)
    {
        $result = $proceed($product);
        if (!empty($result)) {
            return $result;
        } else {
            $price = null;
            $childObj = $this->getChildProductObj($product['entity_id']);
            foreach ($childObj as $child) {
                $productPrice = $child->getData('special_price') ?: $child->getFinalPrice();
                $price = $price ? min($price, $productPrice) : $productPrice;
            }
            return $price;
        }

    }

    /**
     * Get product data
     *
     * @param int $id
     * @return object|null
     */
    public function getProductInfo($id)
    {
        if (is_numeric($id)) {
            return $this->productRepository->getById($id);
        } else {
            return;
        }
    }

    /**
     * Get list child product in product configurable
     *
     * @param int $id
     * @return object|null
     */
    public function getChildProductObj($id)
    {
        $product = $this->getProductInfo($id);
        if (!isset($product)) {
            return;
        }

        if ($product->getTypeId() != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            return [];
        }

        $storeId = $this->_storeManager->getStore()->getId();
        $productTypeInstance = $product->getTypeInstance();
        $productTypeInstance->setStoreFilter($storeId, $product);
        $childrenList = [];

        foreach ($productTypeInstance->getUsedProducts($product) as $child) {
            $attributes = [];
            $isSaleable = $child->isSaleable();

            //get only in stock product info
            if ($isSaleable) {
                foreach ($child->getAttributes() as $attribute) {
                    $attrCode = $attribute->getAttributeCode();
                    $value = $child->getDataUsingMethod($attrCode) ?: $child->getData($attrCode);
                    if (null !== $value && $attrCode != 'entity_id') {
                        $attributes[$attrCode] = $value;
                    }
                }

                $attributes['store_id'] = $child->getStoreId();
                $attributes['id'] = $child->getId();
                /** @var \Magento\Catalog\Api\Data\ProductInterface $productDataObject */
                $productDataObject = $this->productFactory->create();
                $this->dataObjectHelper->populateWithArray(
                    $productDataObject,
                    $attributes,
                    '\Magento\Catalog\Api\Data\ProductInterface'
                );
                $childrenList[] = $productDataObject;
            }
        }

        $childConfigData = array();
        foreach ($childrenList as $child) {
            $childConfigData[] = $child;
        }

        return $childConfigData;
    }
}