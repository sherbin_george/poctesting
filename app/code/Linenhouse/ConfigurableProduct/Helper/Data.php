<?php
/**
 * Copyright © 2018 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linenhouse\ConfigurableProduct\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_storeManagerInterface;
    protected $objectManager;
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->objectManager = $objectManager;
    }

    public function getMaxMinPrice($prd)
    {
        $minpr = array();
        $storeId = $this->_storeManagerInterface->getStore()->getStoreId();
        if ($prd->getTypeId() == 'configurable') {
            $productTypeInstance = $prd->getTypeInstance();
            $productTypeInstance->setStoreFilter($storeId, $prd);
            $priceFrom = null;
            $priceTo = null;
            foreach ($prd->getTypeInstance()->getUsedProducts($prd) as $childProduct) {

                $final_price = $childProduct->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
                $regular_price = $childProduct->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();
                $realPrd = $this->objectManager->create('Magento\Catalog\Model\Product')->load($childProduct->getEntityId());
                $special = $realPrd->getPriceInfo()->getPrice('special_price')->getAmount()->getValue();
                if (!empty($special)) {
                    $minpr[] = min(array($final_price, $regular_price, $special));
                    $priceFrom = $priceFrom ? min($priceFrom, $final_price, $regular_price, $special) : $special;
                    if ($priceFrom == $special) {
                        $priceTo = $regular_price;
                    }
                } else {
                    $minpr[] = min(array($final_price, $regular_price));
                    $priceFrom = min(array($final_price, $regular_price));

                }

            }
        }
        if (empty($minpr)) return 0;
        $min = min($minpr);
        $max = max($minpr);
        if ($min == $max) return 0;
        return array('min' => $min, 'max' => $max, 'priceFrom' => $priceFrom, 'priceTo' => $priceTo);
    }

}