<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Cart
 */
namespace Amasty\Cart\Model\Source;

class Configurable implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $options[] = array(
                'value' => '0',
                'label' =>__('Parent Configurable Product Image')
        );
        $options[] = array(
                'value' => '1',
                'label' =>__('Child Simple Product Image')
        );

        return $options;
    }
}
