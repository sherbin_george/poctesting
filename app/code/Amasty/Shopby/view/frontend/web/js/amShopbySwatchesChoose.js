define([
    "jquery"
], function ($) {
    'use strict';

    $.widget('mage.amShopbySwatchesChoose', {
        options: {
            listSwatches: {}
        },
        _create: function () {
            $.each(this.options.listSwatches, $.proxy(function (attributeCode, optionId) {
                this.element.find('.swatch-attribute-options [option-id="' + optionId + '"]').trigger('click');
            }, this));
        }
    });
});
