<?php
/**
 * Navin Bhudiya
 * Copyright (C) 2016 Navin Bhudiya <navindbhudiya@gmail.com>
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category  Navin
 * @package   Navin_Importexportcategory
 * @copyright Copyright (c) 2016 Mage Delight (http://www.navinbhudiya.com/)
 * @license   http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author    Navin Bhudiya <navindbhudiya@gmail.com>
 */

namespace Navin\Importexportcategory\Console\Command;

use Magento\Framework\App\State;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class NavinCategoryImport.
 *
 * @category  Navin
 * @package   Navin_Importexportcategory
 * @copyright Copyright (c) 2016 Mage Delight (http://www.navinbhudiya.com/)
 * @license   http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author    Navin Bhudiya <navindbhudiya@gmail.com>
 */
class NavinCategoryImportCommand extends Command
{
    const IMPORT_CATEGORIES_FILENAME = 'categories.csv';
    const FILENAME_PARAM = 'filename';

    protected $_filesystem;
    protected $_moduleReader;
    protected $_fileCsv;
    protected $_storeManager;
    protected $_categoryFactory;
    protected $registry;
    protected $_logger;
    protected $_fileio;
    private $state;
    private $_dir;
    protected $_filename;
    /**
     * @var OutputInterface
     */
    private $_output;

    /**
     * CSVImport constructor.
     *
     * @param Registry                 $registry
     * @param Filesystem               $fileSystem
     * @param Reader        $moduleReader
     * @param Csv                 $fileCsv
     * @param StoreManagerInterface  $storeManagerInterface
     * @param \Magento\Catalog\Model\CategoryFactory      $categoryFactory
     * @param LoggerInterface                    $logger
     * @param File       $fileio
     * @param State                $state
     * @param DirectoryList $dir
     * @param null                                        $name
     */
    public function __construct(
        Registry $registry,
        Filesystem $fileSystem,
        Reader $moduleReader,
        Csv $fileCsv,
        StoreManagerInterface $storeManagerInterface,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        LoggerInterface $logger,
        File $fileio,
        State $state,
        DirectoryList $dir,
        $name = null
    )
    {
        $this->state = $state;
        $this->_filesystem = $fileSystem;
        $this->_moduleReader = $moduleReader;
        $this->_fileCsv = $fileCsv;
        $this->_storeManager = $storeManagerInterface;
        $this->_categoryFactory = $categoryFactory;
        $this->registry = $registry;
        $this->_logger = $logger;
        $this->_fileio = $fileio;
        $this->_dir = $dir;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $options = [
            new InputOption(
                self::FILENAME_PARAM,
                null,
                InputOption::VALUE_OPTIONAL,
                'Imported file name'
            )
        ];

        $this->setName('navin:category-import');
        $this->setDescription("Import categories");
        $this->setDefinition($options);
    }

    protected function setFileName($filename)
    {
        $this->_filename = $filename;
    }

    protected function getFileName()
    {
        if (empty($this->_filename)) {
            return self::IMPORT_CATEGORIES_FILENAME;
        }

        return $this->_filename;
    }

    protected function _uploadFileAndGetName()
    {
        $path = $this->_dir->getPath('var') . '/categoryimport/' . $this->getFileName();

        if (file_exists($path)) {
            return $path;
        }

        return false;
    }

    protected function _getKeyValue($row, $headerArray)
    {
        $temp = [];
        foreach ($headerArray as $key => $value) {
            if ($value == 'image') {
                $temp[$value] = $this->_getImagePath($row[$key]);
            } elseif ($value == 'products' && $row[$key] != '') {
                $temp['posted_products'] = array_flip(explode('|', $row[$key]));
            } else {
                $temp[$value] = $row[$key];
            }
        }
        return $temp;
    }

    protected function _getImagePath($categoryimage)
    {
        $weburl = strpos($categoryimage, 'http://');
        if ($weburl !== false) {
            $imagepath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath('catalog/category');
            $this->_fileio->mkdir($imagepath, '0777', true);
            $file = file_get_contents($categoryimage);
            if ($file != '') {
                $allowed = ['gif', 'png', 'jpg', 'jpeg'];
                $ext = strtolower(pathinfo($categoryimage, PATHINFO_EXTENSION));
                if (in_array($ext, $allowed)) {
                    $imagename = pathinfo($categoryimage, PATHINFO_BASENAME);
                    if (!is_dir($imagepath)) {
                        $this->_fileio->mkdir($imagepath, '0777', true);
                        $this->_fileio->chmod($imagepath, '0777', true);
                    }
                    $imagepath = $imagepath . '/' . $imagename;
                    $result = file_put_contents($imagepath, $file);
                    if ($result) {
                        return $imagename;
                    }
                }
            }
        } else {
            return $categoryimage;
        }
    }

    protected function getparentCategories($category)
    {
        $pathIds = array_reverse(explode(',', $category->getPathInStore()));
        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categories */
        $categories = $this->_categoryFactory->create()->getCollection();
        return $categories->setStore(
            $this->_storeManager->getStore()
        )->addAttributeToSelect(
            'name'
        )->addAttributeToSelect(
            'url_key'
        )->addFieldToFilter(
            'entity_id',
            ['in' => $pathIds]
        )->load()->getItems();
    }

    protected function _importCategories()
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        $this->registry->register('isSecureArea', true);

        $this->_output->writeln('Start import categories');
        try {
            $filepath = $this->_uploadFileAndGetName();
            if (!empty($filepath)) {
                $this->_output->writeln('File is exist.');

                chmod($filepath, 0777);
                $data = $this->_fileCsv->getData($filepath);
                if (isset($data[0]) && !empty($data[0])) {
                    $this->_output->writeln('Data are exist.');

                    $header = $data[0];
                    $categorieskey = array_search('categories', $header);
                    $categoryidkey = array_search('category_id', $header);
                    $storedata = array_search('store', $header);

                    $store = $this->_storeManager->getStore();
                    $storeId = $store->getStoreId();
                    $singlestoremode = $this->_storeManager->isSingleStoreMode();
                    $_stores = [];
                    $stores = $this->_storeManager->getStores();
                    $exist_categories_pathname = [];
                    $exist_categories_path = [];
                    $alreadyexist = [];
                    $exist_categories_name = [];
                    $roots = [];

                    if (!$singlestoremode) {
                        $this->_output->writeln('Start to analyze paths.');
                        foreach ($stores as $store) {
                            $_stores[$store->getCode()] = $store->getId();
                            $exist_categories_pathname[$store->getCode()] = [];
                            $exist_categories_name[$store->getCode()] = [];

                            $rootNodeId = $store->getRootCategoryId();
                            $rootCat = $this->_categoryFactory->create();
                            $cat_info = $rootCat->load($rootNodeId);
                            $roots[$store->getCode()] = $rootNodeId;

                            $categorycollection = $this->_categoryFactory->create()
                                ->getCollection()
                                ->addAttributeToSelect('*')
                                ->addAttributeToFilter('path', ['like' => "1/{$rootNodeId}/%"]);

                            foreach ($categorycollection as $value) {
                                $exist_categories_name[$store->getCode()][$value->getId()] = $value->getName();
                                $exist_categories_path[$value->getId()] = $value->getPath();
                                $checkcat = $this->_categoryFactory->create();
                                $categoryobj = $checkcat->load($value->getId());
                                $parentcatnames = [];
                                $parentid = '';
                                foreach ($this->getparentCategories($categoryobj) as $parentcate) {
                                    $parentcatnames[] = $parentcate->getName();
                                    $parentid = $parentcate->getId();
                                }
                                $parent_cat = implode('/', $parentcatnames);
                                if ($parent_cat && $parentid) {
                                    $exist_categories_pathname[$store->getCode()][$parentid] = $parent_cat;
                                }
                            }
                        }
                    }

                    $this->_output->writeln('Start to import categories.');

                    foreach ($data as $key => $categoryitem) {
                        if ($key != 0) {
                            $cat_data = $this->_getKeyValue($categoryitem, $header);
                            if (isset($cat_data['category_id'])) {
                                unset($cat_data['category_id']);
                            }
                            if (isset($categorieskey) && ($categorieskey != '' || $categorieskey === 0)) {
                                $array_key = array_search(
                                    $categoryitem[$categorieskey],
                                    $exist_categories_pathname[$cat_data['store']]
                                );
                                if ($array_key) {
                                    $alreadyexist[] = $categoryitem[$categorieskey];
                                } else {
                                    $strmark = strrpos($categoryitem[$categorieskey], '/');
                                    if ($strmark != false) {
                                        $parentpath = substr($categoryitem[$categorieskey], 0, ($strmark));
                                        $newcategory = substr($categoryitem[$categorieskey], ($strmark) + 1);
                                        $_parentid = array_search(
                                            $parentpath,
                                            $exist_categories_pathname[$cat_data['store']]
                                        );
                                    } else {
                                        $newcategory = $categoryitem[$categorieskey];
                                        $_parentid = $cat_info->getId();
                                    }
                                    if ($_parentid != '' && $newcategory != '') {
                                        $cateitem = $this->_categoryFactory->create();
                                        if (isset($cat_data['path'])) {
                                            unset($cat_data['path']);
                                        }
                                        $cateitem->setData($cat_data);
                                        $parentcategory = $this->_categoryFactory->create();
                                        $parentcategory->load($_parentid);
                                        if ($parentcategory->getId()) {
                                            $parentStoreIds = $parentcategory->getStoreIds();
                                            if (in_array($_stores[$cat_data['store']], $parentStoreIds)) {
                                                $cateitem->setParentId($_parentid);
                                                $cateitem->setPath($parentcategory->getPath());
                                            } else {
                                                $_parentid = $roots[$cat_data['store']];
                                                $parentcategory = $this->_categoryFactory->create();
                                                $parentcategory->load($_parentid);
                                                $cateitem->setParentId($_parentid);
                                                $cateitem->setPath($parentcategory->getPath());
                                            }
                                        }
                                        $cateitem->setAttributeSetId($cateitem->getDefaultAttributeSetId());
                                        $cateitem->setName($newcategory);
                                        $_url_key = str_replace(' ', '-', strtolower($newcategory));
                                        if (in_array($newcategory, $exist_categories_name[$cat_data['store']])) {
                                            $_url_key .= '-' . mt_rand(10, 99);
                                        }
                                        $cateitem->setUrlKey($_url_key);
                                        $cateitem->setStoreId($_stores[$cat_data['store']]);

                                        $cateitem->save();

                                        if ($cateitem->getId()) {
                                            $exist_categories_name[$cat_data['store']][$cateitem->getId()] = $cateitem->getName();
                                            $exist_categories_path[$cateitem->getId()] = $cateitem->getPath();
                                            $exist_categories_pathname[$cat_data['store']][$cateitem->getId()] = $categoryitem[$categorieskey];

                                            $this->_logger->debug(__('Category %1 was saved successfully.', $cateitem->getName()));
                                            $this->_output->writeln(__('Category %1 was saved successfully.', $cateitem->getName())->render());
                                        }
                                    }
                                }
                            } elseif (isset($categoryidkey) && ($categoryidkey != '' || $categoryidkey === 0) && $storedata != '') {
                                //update categories
                                $catemodel = $this->_categoryFactory->create();
                                if (!$singlestoremode && isset($_stores[$categoryitem[$storedata]])) {
                                    $catemodel->setStoreId($_stores[$categoryitem[$storedata]]);
                                } else {
                                    $catemodel->setStoreId($storeId);
                                }
                                $cateitem = $catemodel->load($categoryitem[$categoryidkey]);
                                $nocategoryfound = true;
                                if ($cateitem->getId()) {
                                    $nocategoryfound = false;
//                                    $attributesetid = $cateitem->getAttributeSetId();
                                    $_parentid = $cateitem->getParentId();
                                    foreach ($cat_data as $key => $value) {
                                        if (!in_array($key, ['url_key', 'category_id', 'url_path', 'path', 'level', 'children_count', 'full_path'])) {
                                            $cateitem->setData($key, $value);
                                        }
                                    }
                                    $parentid = $cateitem->getParentId();
                                    if ($parentid != $_parentid && $cateitem->getId() > 2) {
                                        $_catemodel = $this->_categoryFactory->create();
                                        $parentcat = $_catemodel->load($parentid);
                                        if ($parentcat->getId()) {
                                            $cateitem->setPath($parentcat->getPath() . '/' . $cateitem->getId());
                                        } else {
                                            $this->_logger->error('Parent category not Found.');
                                            $this->_output->writeln('Parent category not Found.');
                                        }
                                        $cateitem->move($parentid, false);

                                        $this->_logger->debug(__('Category %1 was moved.', $cateitem->getName()));
                                        $this->_output->writeln(__('Category %1 was moved.', $cateitem->getName())->render());
                                    }
                                    if ($cateitem->getId() <= 2) {
                                        $cateitem->unsetData('posted_products');
                                    }
                                    $cateitem->save();

                                    $this->_logger->debug(__('Category %1 was saved successfully.', $cateitem->getName()));
                                    $this->_output->writeln(__('Category %1 was saved successfully.', $cateitem->getName())->render());
                                }
                            } else {
                                $this->_logger->error('Data Column not Found.');
                                $this->_output->writeln('Data Column not Found.');
                            }
                        }
                    }
                    if (isset($alreadyexist) && !empty($alreadyexist)) {
                        $this->_logger->error(__('This categories are already exist: %1', implode(', ', $alreadyexist)));
                        $this->_logger->debug(__('Other categories has been imported successfully'));
                        $this->_output->writeln(__('This categories are already exist: %1', implode(', ', $alreadyexist))->render());
                        $this->_output->writeln('Other categories has been imported successfully');
                    } elseif (isset($categoryidkey) && $categoryidkey === 0) {
                        if ($nocategoryfound) {
                            $this->_logger->error(__('No Category Found.'));
                            $this->_output->writeln('No Category Found.');
                        } else {
                            $this->_logger->debug(__('Categories has been updated successfully'));
                            $this->_output->writeln('Categories has been updated successfully');
                        }
                    } else {
                        $this->_logger->debug(__('Categories has been imported successfully'));
                        $this->_output->writeln('Categories has been imported successfully');
                    }
//                    unlink($filepath);
                } else {
                    $this->_logger->error('Data Not Found.');
                    $this->_output->writeln('Data Not Found.');
                }
            } else {
                $this->_logger->error('File not Found.');
                $this->_output->writeln('File not Found.');
            }
        } catch (LocalizedException $e) {
            $this->_logger->error($e->getMessage());
            $this->_output->writeln($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->_logger->error($e->getMessage());
            $this->_output->writeln($e->getMessage());
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
            $this->_output->writeln($e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_output = $output;

        $filename = $input->getOption(self::FILENAME_PARAM);
        if (!empty($filename)) {
            $this->setFileName($filename);
        }

        $this->_importCategories();
    }
}