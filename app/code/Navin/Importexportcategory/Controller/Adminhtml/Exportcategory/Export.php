<?php
/**
* Navin Bhudiya
* Copyright (C) 2016 Navin Bhudiya <navindbhudiya@gmail.com>
*
* NOTICE OF LICENSE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
*
* @category Navin
* @package Navin_Importexportcategory
* @copyright Copyright (c) 2016 Mage Delight (http://www.navinbhudiya.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Navin Bhudiya <navindbhudiya@gmail.com>
*/
namespace Navin\Importexportcategory\Controller\Adminhtml\Exportcategory;

use Magento\Framework\App\ResourceConnection;

class Export extends \Magento\Backend\App\Action
{
    /**
     * Redirect result factory
     *
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $_resultForwardFactory;

    /**
     * Category collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollectionFactory;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_dbConnection;

    /**
     * constructor
     *
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $prodcollection,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Backend\App\Action\Context $context,
        ResourceConnection $resource
    ) {
    
        $this->_resultForwardFactory = $resultForwardFactory;
        $this->_storeManager = $storeManagerInterface;
        $this->_categoryFactory = $categoryFactory;
        $this->_productcollection = $prodcollection;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->fileFactory  = $fileFactory;
        $this->_dbConnection    = $resource->getConnection();

        parent::__construct($context);
    }

    /**
     * Get products by category id.
     *
     * @param $categoryId
     *
     * @return array
     */
    protected function _getCategoryProducts($categoryId)
    {
        $this->_dbConnection->beginTransaction();

        $result = $this->_dbConnection->fetchAll("SELECT `e`.`entity_id`
        FROM `catalog_product_entity` AS `e`
        INNER JOIN `catalog_category_product` AS `cat_index`
        ON cat_index.product_id = e.entity_id AND cat_index.category_id = '{$categoryId}'");

        if (empty($result)) {
            return [];
        }

        return array_column($result, 'entity_id');
    }

    public function execute()
    {
        $store_id = $this->getRequest()->getPost('store_id');
        $singlestoremode = $this->_storeManager->isSingleStoreMode();
        $_stores = [];
        if (!$singlestoremode) {
            $stores = $this->_storeManager->getStores();
            foreach ($stores as $key => $store) {
                $_stores[$store->getId()] = $store->getCode();
            }
        }
         $fileName = 'categories.csv';
        $content = '"categories"';
        $content .= ',"store"';
        $content .= ',"name","path","image","is_active","is_anchor","include_in_menu","meta_title","meta_keywords","meta_description","display_mode","custom_use_parent_settings","custom_apply_to_products","custom_design","custom_design_from","custom_design_to","default_sort_by","page_layout","description","products"'."\n";
        $store = $this->_storeManager->getStore($store_id);
        $rootCategoryId = $store->getRootCategoryId();
        $collection = $this->_categoryFactory->create()->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('path', array('like' => "1/{$rootCategoryId}/%"))
            ->addOrder('level', 'ASC')
            ->addOrder('position', 'ASC')
            ->addOrder('entity_id', 'ASC');
        foreach ($collection as $key => $cat) {
            $categoryitem = $this->_categoryFactory->create();
            if ($cat->getId()>=2) {
                $categoryitem->setStoreId($store_id);
                $categoryitem->load($cat->getId());
                if ($categoryitem->getId()) {
                    $prodids = '';
//                    $productids1 = $this->_productcollection->addCategoryFilter($categoryitem)->getAllIds();
                    $productids = $this->_getCategoryProducts($categoryitem->getId());
                    if (isset($productids) && !empty($productids)) {
                        $prodids = $productids = implode('|', $productids);
                    }
                    //$content .= '"'.$categoryitem->getId().'","'.$categoryitem->getParentId().'","';
                    //if ($categoryitem->getLevel() > 1) {
                        $parents = $this->getParentCategories($categoryitem);
                        if (count($parents)) {
                            $catName = array();
                            foreach ($parents as $cat) {
                                if ($cat->getId() != $categoryitem->getId()) {
                                    $catName[] =  $cat->getName();
                                }
                            }
                            $catName[] = $categoryitem->getName();
                            $content .= '"'.implode('/', $catName).'","';
                        } else {
                            $content .= '"'.$categoryitem->getName().'","';
                        }
                        $content .= $_stores[$categoryitem->getStoreId()].'","';
                        $description =   str_replace('"', '""', $categoryitem->getDescription());
                        $content .= $categoryitem->getName().'","'.$categoryitem->getPath().'","'.$categoryitem->getImage().'","'.$categoryitem->getIsActive().'","'.$categoryitem->getIsAnchor().'","'.$categoryitem->getIncludeInMenu().'","'.$categoryitem->getMetaTitle().'","'.$categoryitem->getMetaKeywords().'","'.$categoryitem->getMetaDescription().'","'.$categoryitem->getDisplayMode().'","'.$categoryitem->getCustomUseParentSettings().'","'.$categoryitem->getCustomApplyToProducts().'","'.$categoryitem->getCustomDesign().'","'.$categoryitem->getCustomDesignFrom().'","'.$categoryitem->getCustomDesignTo().'","'.$categoryitem->getDefaultSortBy().'","'.$categoryitem->getPageLayout().'","'.$description.'","'.$prodids.'"'."\n";
                    //}
                }
            }
        };
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Return parent categories of category
     *
     * @param \Magento\Catalog\Model\Category $category
     * @return \Magento\Framework\DataObject[]
     */
    public function getParentCategories($category)
    {
        $pathIds = array_reverse(explode(',', $category->getPathInStore()));
        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categories */
        $categories = $this->_categoryCollectionFactory->create();
        return $categories->setStore(
            $this->_storeManager->getStore()
        )->addAttributeToSelect(
            'name'
        )->addAttributeToSelect(
            'url_key'
        )->addFieldToFilter(
            'entity_id',
            ['in' => $pathIds]
        )->load()->getItems();
    }

    public function _prepareDownloadResponse($name, $content)
    {
        $fileName = $name;
        $this->fileFactory->create(
            $fileName,
            $content,
            'var',
            'text/csv', 
            strlen($content)
        );
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw;
    }
}
