/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/sidebar',
    'Magento_Catalog/js/price-utils'
], function ($, Component, quote, stepNavigator, sidebarModel, priceUtils) {
    'use strict';
    return function (Component) {
        return Component.extend({
            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },
            getShippingMethodTitle: function () {
                var shippingMethod = quote.shippingMethod(),
                    surcharge = this.getFormattedPrice(shippingMethod.base_amount);
                return shippingMethod ? shippingMethod.carrier_title + " - " + shippingMethod.method_title + " " + surcharge : '';
            }
        });
    }
});
