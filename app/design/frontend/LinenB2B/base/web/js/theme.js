/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'matchMedia',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'niceSelect',
    'slick',
    'mage/ie-class-fixer',
    'shorten',
    'domReady!',
    'mage/translate'
], function ($, mediaCheck, keyboardHandler ) {
    'use strict';

    // Init select box
    $('select:not(#region_id, #country)').niceSelect();

    $(document).on('click', '.filter-options-title', function(){
        console.log('test');
        $(this).parent().toggleClass('selected');
    });
    $('.nav-sections .ms-level0 a').click(function(e){
        if($(window).width()<980){
            if($(this).next().hasClass('label-arrow-right')) {
                $(this).next().trigger('click');
                e.preventDefault();
                return false;
            }
        }
    });
    if($('.blog-content').length != 0){
        $('.blog-content').shorten({
            showChars: 220,
            moreText: 'More',
            lessText: 'Less'
        });
    }
    if($('.category-description .all-content').length != 0){
        $('.category-description .all-content').shorten({
            showChars: 220,
            moreText: 'More',
            lessText: 'Less'
        });
    }

    var $wislistTitle = $('.wishlist-index-index .page-title'),
        $wislistTitleText = $wislistTitle.text(),
        $wislistSubtitle = $('.block-collapsible-nav-title strong'),
        $wislistSubtitleText = $wislistSubtitle.text();

    mediaCheck({
       media: '(max-width: 992px)',
       entry: function () {
           $wislistTitle.text($wislistSubtitleText);
           $wislistSubtitle.text($wislistTitleText);
       },

       exit: function () {
           $wislistTitle.text($wislistTitleText);
           $wislistSubtitle.text($wislistSubtitleText);
       }
   });


    //if($('.page-with-filter').length){
    //    var filterContainer = $('.catalog-category-view .block.filter');
    //    mediaCheck({
    //        media: '(min-width: 992px)',
    //        // Switch to Desktop Version
    //        entry: function () {
    //            filterContainer.prependTo('.products.wrapper');
    //        },
    //        // Switch to Mobile Version
    //        exit: function () {
    //            filterContainer.prependTo('.column.main');
    //        }
    //    });
    //}
//
    //if($('.catalogsearch-result-index').length){
    //    var filterContainer = $('.catalogsearch-result-index .block.filter');
    //    mediaCheck({
    //        media: '(min-width: 992px)',
    //        // Switch to Desktop Version
    //        entry: function () {
    //            filterContainer.prependTo('.products.wrapper');
    //        },
    //        // Switch to Mobile Version
    //        exit: function () {
    //            filterContainer.prependTo('.column.main');
    //        }
    //    });
    //}

    $('.footer-bottom-inner .title').on('click', function(){
        $(this).parent().toggleClass('open');
    });

    if($('.checkout-cart-index').length){
        var cartBreadcrumbs = $('.checkout-cart-index .breadcrumbs');
        cartBreadcrumbs.insertBefore('#maincontent');
    }

    if ($('.sales-order-view').length || $('.sales-order-invoice').length) {
        $('.breadcrumbs strong').append($('.page-title .order-number').text().trim());
    }

    // Instagram load more function
    function handleScrollInstagram() {
        if ($(window).scrollTop() > ($('.fs-next-page').offset().top - 400)) {
            $('.fs-next-page').trigger('click');
        }
    }

    if ($('.cms-instagram').length) {
        // Set a timer on the scroll event
        // Do the main work only
        var scrollTimer;
        $(window).scroll(function () {
            if (scrollTimer) {
                clearTimeout(scrollTimer);
            }
            scrollTimer = setTimeout(handleScrollInstagram, 200);
        });
    }

    // Product slider starts
    $('.category-view .widget .product-items, .products-related .product-items, .blog-recent-posts .recent-posts').each(function(){
        $(this).slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            responsive: [
                {
                    breakpoint: 860,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true
                    }
                }
            ]
        });
    });

    if($('.checkout-cart-index .product-items').children().length > 2){
        // Cart empty
        $('.checkout-cart-index .widget .product-items').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 2,
            arrows: false,
            responsive: [
                {
                    breakpoint: 860,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true
                    }
                }
            ]
        });

        $('.crosssell .product-items').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            responsive: [
                {
                    breakpoint: 860,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true
                    }
                }
            ]
        });
    }

    if($('.catalog-product-view .recent-posts, #blog_recent_posts .recent-posts').children().length > 3){
        $('.recent-posts').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 2,
            arrows: false,
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: true
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: true
                    }
                }
            ]
        });
    }
    // Product slider ends

    // Need to check is this is something we need to keep
    //$('.panel.header > .header.links').clone().appendTo('#store\\.links');

    // filter block animation helper
    (function () {
        var body = $('body');
        $(document).on('click','.filter-opener, .filter-overlay', function () {
            $(this).toggleClass('active');
            if (body.hasClass('filter-opened')) {
                body.removeClass('filter-opened');
                if($('.filter-opener').hasClass('active')){
                    $('.filter-opener').removeClass('active');
                }
            } else {
                body.addClass('filter-opened');
            }
        });
    })();


    // Check if url has hash then scroll to description
    // if clicked on more scroll to description and trigger click on tab
    (function(){
        var tabDescription = $('#tab-label-product\\.info\\.description'),
            contentDescription = $('#product.info.description');
        if(window.location.hash.indexOf('tab-label-product.info.description') == 1){
            $(document).ajaxComplete(function(event, xhr, settings) {
                jQuery('body,html').stop().animate({
                    scrollTop: tabDescription.offset().top
                }, 300);
                tabDescription.focus();
                return false;
            });
        }
        if($('.overview').length){
            $('.scroll-to').on('click', function(e){
                e.preventDefault();
                if(!tabDescription.hasClass('active')) {
                    tabDescription.trigger('click');
                }
                jQuery('body,html').stop().animate({
                    scrollTop: tabDescription.offset().top
                }, 800);
            });
        }
    })();

    $(document).on('keyup', '#super-product-table .input-text.qty', function(){
        var name = $(this).closest('tr').children('.product_name');
        if($(this).val() != 0 && $(this).val() != ''){
            $(name).removeClass('not-selected');
        }
        else{
            if(!$(name).hasClass('not-selected')){
                $(name).addClass('not-selected');
            }
        }
    });

    if($('.authorization-link.account').length){
        $('.authorization-link.account').on('click', function(){
            $(this).toggleClass('active');
        });

        $(document).on('click touchstart', function(e){
            if($('.authorization-link.account').hasClass('active')){
                var container = $('.customer-account'),
                    link = $('.authorization-link.account');
                // if the target of the click isn't the container nor a descendant of the container
                if ((container.has(e.target).length === 0 && !container.is(e.target)) && (link.has(e.target).length === 0 && !link.is(e.target)))
                {
                    $('.authorization-link.account').removeClass('active');
                }
            }
        });

    }

    if($('.item-actions').length) {
        mediaCheck({
            media: '(min-width: 768px)',
            // Switch to Desktop Version
            entry: function () {
                $('.item-actions').each(function() {
                    var _this = $(this);
                    var target1 = _this.prev().find('td.actions');
                    var target2 = _this.prev().find('.product-item-details');
                    _this.find('.action-delete').detach().prependTo(target1);
                    _this.find('.action-edit').detach().appendTo(target2);
                });
            },
            // Switch to Mobile Version
            exit: function () {
                $('.item-actions').each(function() {
                    var _this = $(this);
                    var target1 = _this.prev().find('.product-item-details');
                    var target2 = _this.find('.actions-toolbar');
                    _this.find('.action-delete').detach().prependTo(target1);
                    _this.find('.action-edit').detach().appendTo(target2);
                });
            }
        });
    }


    if($('.sidebar-main').length) {
        mediaCheck({
            media: '(min-width: 980px)',
            // Switch to Desktop Version
            entry: function () {
                $('.cms-page-view .sidebar.sidebar-main').insertAfter('.column.main');
            },
            // Switch to Mobile Version
            exit: function () {
                $('.cms-page-view .sidebar.sidebar-main').insertAfter('.page-title-wrapper');
            }
        });
    }


    if($('.aw-block-holder .opener a').length) {
        $('.aw-block-holder .opener a').on('click', function (event) {
            event.preventDefault();
            $(this).closest('.aw-block-holder').toggleClass('open');
        })
    }

    if($('.cms-menu').length) {
        var mobileMenuTitle = $('.cms-menu strong').text(),
            mobilePageTitle = $('.cms-menu .parent > a span').text();
        $('body').addClass('cms-hierarchy');
        $('.page-title').append('<span class="base mobile">' + mobilePageTitle + '</span>');

        if (!$('.cms-menu-holder').length) {
            $('.cms-menu').wrap('<div class=\'cms-menu-holder\'></div>').parent().prepend('<a class=\'opener\' href=\'#\'><strong>' + mobileMenuTitle + '</strong></a>');
        }
    }

    $('.cms-page-view .sidebar.sidebar-main').on('click', '.cms-menu-holder .opener', function (event) {
        event.preventDefault();
        $(this).closest('.cms-menu-holder').toggleClass('open');
    });

    if($('.sub-categorys-trigger').length){
        $(document).on('click', '.sub-categorys-trigger', function(e){
            e.preventDefault();
           $(this).toggleClass('active');
           $(this).next().toggleClass('active');
        });
    }

    if ($('body.account').length) {
        mediaCheck({
            media: '(max-width: 980px)',
            // Update account menu as per design
            entry: function () {
                var menuName = $('#block-collapsible-nav').find('.current').text();
                $('.block-collapsible-nav-title strong').text(menuName);
            },
            // Nothing todo
            exit: function () {

            }
        });
    }

    keyboardHandler.apply();

    $(".block-search.linen-au .block-title").click(function () {
        $(this).parents(".block-search").toggleClass("active");
        $(this).parents("body").toggleClass("show-search");
    });
    $(".block-search.linen-au .close-search").click(function () {
        $(this).parents(".block-search").removeClass("active");
        $(this).parents("body").removeClass("show-search");
    });

});

