/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!'
], function () {
    'use strict';

    if((document.location.href.indexOf('customer') > -1) || (document.location.href.indexOf('checkout') > -1)) {
        // indexOf will return the position of the first occurence of this string in the url
        // or -1 it it's not there.
        document.location.href = '/';
    }

});
