/*
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    deps: [
        "js/theme-extend"
    ]
};
