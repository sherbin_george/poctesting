/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'matchMedia',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'niceSelect',
    'slick',
    'shorten',
    'mage/ie-class-fixer',
    'domReady!'
], function ($, mediaCheck) {
    'use strict';

    $('.toogle-search').on('click', function(){
        $(this).toggleClass('active');
        $(this).parent().children('.block-content').toggleClass('active');
    });

    $('.mobile-blog').on('click', function(){
        $(this).toggleClass('active');
        $(this).parent().children('ul').toggleClass('active');
    });


    // Megamenu submenus toggler
    $('.mb-return, .label-arrow-right').unbind().on('click', function() {
        $(this).closest('.col-level, .ui-menu-item').first().toggleClass('active');
        return false;
    });

    $('.js-load-more').on('click', function() {
        $(this).closest('.block').find('.js-product-list').toggleClass('expanded');
    });

    $('.stock.unavailable').parent().addClass('unavailable');

    $(document).on('click','.filter-opener', function () {
        $('.minicart-wrapper.active').removeClass('active');
    });
    
    $('.pages-items .item').on('click', function () {
        setTimeout(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 300);
        },1000);
    });

    $('.instagram-gallery').slick({
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 860,
                settings: {
                    slidesToShow: 4,
                    arrows: true
                }
            }
        ]
    });

    var $cartProductItems = $('.checkout-cart-index:not(.no-items) .product-items');

    /**
     * Fix error triggered in some cases
     */
    try {
        $cartProductItems.slick('unslick');
    } catch (e) {
        console.log(e);
    }

    $cartProductItems.slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });

    // Blog recent Posts Homepage

    var $recent_posts = $('.cms-index-index .blog-recent-posts .recent-posts');

    if ($recent_posts.hasClass('slick-initialized')) {
        $recent_posts.slick('unslick');
    }

    if($('.section-seo-block .note').length != 0){

        var $block = $('.section-seo-block .note'),
            content = $block.html();

        $block.html(content);
        $block.shorten({
            showChars: 514,
            moreText: 'Show more',
            lessText: 'Less',
            force: true
        });
    }

    mediaCheck({
        media: '(max-width: 1024px)',
        entry: function () {
            $('.section-style .balance-box-wrapper').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            arrows: true
                        }
                    },

                    {
                        breakpoint: 640,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: true
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: true
                        }
                    }
                ]
            });
        },

        exit: function () {

        }
    });

    mediaCheck({
        media: '(min-width: 640px) and (max-width: 979px)',
        entry: function () {
            if($('.category-description .all-content').length != 0){

                var $block = $('.category-description > .all-content'),
                    content = $block.find('.all-content').html();

                $block.html(content);
                $block.shorten({
                    showChars: 180,
                    moreText: 'More',
                    lessText: 'Less',
                    force: true
                });
            }

            if($('.blog-content .all-content').length != 0){

                var $block = $('.blog-content'),
                    content = $('.blog-content > .all-content > .blog-text').html();

                $block.html('<div class="blog-text">' + content + '</div>');
                $block.shorten({
                    showChars: 180,
                    moreText: 'More',
                    lessText: 'Less',
                    force: true
                });
            }
        },

        exit: function () {

        }

    });

    mediaCheck({
        media: '(min-width: 1024px)',

        entry: function () {
            $('.message.success').on('click', function () {
                $('.message.success').css('display', 'none');
            });
        },
        exit: function () {

        }
    });
    mediaCheck({

        media: '(min-width: 980px)',

        entry: function () {
            if($('.category-description .all-content').length != 0){

                var $block = $('.category-description > .all-content'),
                    content = $block.find('.all-content').html();

                $block.html(content);
                $block.shorten({
                    showChars: 220,
                    moreText: 'More',
                    lessText: 'Less',
                    force: true
                });
            }

            if($('.blog-content .all-content').length != 0){

                var $block = $('.blog-content'),
                    content = $('.blog-content > .all-content > .blog-text').html();

                $block.html('<div class="blog-text">' + content + '</div>');
                $block.shorten({
                    showChars: 220,
                    moreText: 'More',
                    lessText: 'Less',
                    force: true
                });
            }
        },
        exit: function () {

        }

    });
    mediaCheck({
        media: '(max-width: 767px)',
        entry: function () {

            //HomePage Products Collection, BestSellers

            $('.collection').removeClass("section-new_season");

            $('.section-collection-block .block.widget .product-items, .section-bestsellers-block .block.widget .product-items').slick({
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: true
                        }
                    }
                ]
            });

        },

        exit: function () {}
    });


    mediaCheck({

        media: '(max-width: 639px)',

        entry: function () {
            if($('.category-description .all-content').length != 0){

                var $block = $('.category-description > .all-content'),
                    content = $block.find('.all-content').html();

                $block.html(content);
                $block.shorten({
                    showChars: 120,
                    moreText: 'More',
                    lessText: 'Less',
                    force: true
                });
            }

            if($('.blog-content .all-content').length != 0){

                var $block = $('.blog-content'),
                    content = $('.blog-content > .all-content > .blog-text').html();

                $block.html('<div class="blog-text">' + content + '</div>');
                $block.shorten({
                    showChars: 120,
                    moreText: 'More',
                    lessText: 'Less',
                    force: true
                });
            }
        },
        exit: function () {

        }

    });

});
