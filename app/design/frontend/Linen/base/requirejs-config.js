/*
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        "slick":      "js/vendor/slick.min",
        "lazyLoad":   "js/vendor/lazyload.min",
        "shorten":    "js/vendor/jquery.shorten",
        "niceSelect": "js/vendor/jquery.nice-select"

    },
    config: {
        mixins: {
            'Magento_Customer/js/password-strength-indicator': {
                'Magento_Customer/js/password-strength-indicator-extend': true
            },
            'Magento_Checkout/js/view/payment': {
                'Magento_Checkout/js/view/payment-extend': true
            },
            'Magento_Checkout/js/model/step-navigator': {
                'Magento_Checkout/js/model/step-navigator-extend': true
            },
            'Magento_Braintree/js/view/payment/method-renderer/paypal': {
                'Magento_Braintree/js/view/payment/method-renderer/paypal-mixin': true
            },
            'Magento_Braintree/js/view/payment/method-renderer/hosted-fields': {
                'Magento_Braintree/js/view/payment/method-renderer/hosted-fields-mixin': true
            }
        }
    },
    shim: {
        'slick': {
            deps: ['jquery']
        },
        'lazyLoad': {
            deps: ['jquery']
        },
        'shorten': {
            deps: ['jquery']
        },
        'niceSelect': {
            deps: ['jquery']
        }
    }
};