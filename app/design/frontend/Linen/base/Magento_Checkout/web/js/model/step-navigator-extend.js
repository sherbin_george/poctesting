/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'ko',
        'mage/translate'
    ],
    function($, ko, $t) {
        'use strict';
        return function (target) {
            // modify target
            var getActiveItemIndex = target.getActiveItemIndex;
            target.getActiveItemIndex = function() {
                if(getActiveItemIndex() == 1){
                    $('.page-wrapper').removeClass('shipping').addClass('payment');
                    $('.breadcrumbs strong').html($t('Payment'));
                }
                else{
                    $('.page-wrapper').removeClass('payment').addClass('shipping');
                    $('.breadcrumbs strong').html($t('Shipping Address '));
                }
                var result = getActiveItemIndex.apply(this, arguments);

                return  result;
            };
            return target;
        };
    }
);