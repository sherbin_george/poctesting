/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * jshint browser:true
 */
define([
    'jquery',
    'Magento_Customer/js/zxcvbn',
    'mage/translate',
    'mage/validation'
], function ($, zxcvbn, $t) {
    'use strict';
    return function (widget) {
        $.widget('mage.passwordStrengthIndicator', widget, {
            /**
             * {@inheritDoc}
             */
            _displayStrength: function (displayScore) {
                var strengthLabel = '',
                    className = this._getClassName(displayScore);

                switch (displayScore) {
                    case 0:
                        strengthLabel = $t('No Password');
                        break;

                    case 1:
                        strengthLabel = $t('Weak');
                        break;

                    case 2:
                        strengthLabel = $t('Okay');
                        break;

                    case 3:
                        strengthLabel = $t('Strong');
                        break;

                    case 4:
                        strengthLabel = $t('Very Strong');
                        break;
                }

                this.options.cache.meter
                    .removeClass()
                    .addClass(className);
                this.options.cache.label.text(strengthLabel);
            }
        });

        return $.mage.passwordStrengthIndicator;
    };

});