define([
    'jquery',
    'Magento_Braintree/js/validator',
    'Magento_Checkout/js/model/full-screen-loader',
    'mage/translate'
], function ($, validator, fullScreenLoader, $t) {
    'use strict';
    return function (Component) {
        return Component.extend({
            initialize: function () {
                this._super();
                return this;
            },
            /**
             * Get Braintree Hosted Fields
             * @returns {Object}
             */
            getHostedFields: function () {
                var self = this,
                    fields = {
                        number: {
                            selector: self.getSelector('cc_number')
                        },
                        expirationMonth: {
                            selector: self.getSelector('expirationMonth'),
                            placeholder: $t('Month'),
                            select:{
                                options: [
                                    '01 - January',
                                    '02 - February',
                                    '03 - March',
                                    '04 - April',
                                    '05 - May',
                                    '06 - June',
                                    '07 - July',
                                    '08 - August',
                                    '09 - September',
                                    '10 - October',
                                    '11 - November',
                                    '12 - December'
                                ]
                            }
                        },
                        expirationYear: {
                            selector: self.getSelector('expirationYear'),
                            placeholder: $t('Year'),
                            select: true
                        }
                    };

                if (self.hasVerification()) {
                    fields.cvv = {
                        selector: self.getSelector('cc_cid')
                    };
                }

                /**
                 * Triggers on Hosted Field changes
                 * @param {Object} event
                 * @returns {Boolean}
                 */
                fields.onFieldEvent = function (event) {
                    if (event.isEmpty === false) {
                        self.validateCardType();
                    }

                    if (event.type !== 'fieldStateChange') {

                        return false;
                    }

                    // Handle a change in validation or card type
                    if (event.target.fieldKey === 'number') {
                        self.selectedCardType(null);
                    }

                    if (event.target.fieldKey === 'number' && event.card) {
                        self.isValidCardNumber = event.isValid;
                        self.selectedCardType(
                            validator.getMageCardType(event.card.type, self.getCcAvailableTypes())
                        );
                    }
                };

                return fields;
            }
        });
    };
});
