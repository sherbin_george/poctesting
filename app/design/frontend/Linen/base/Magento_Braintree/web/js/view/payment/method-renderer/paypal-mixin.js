define([
    'jquery',
    'Magento_Checkout/js/model/quote'
], function ($, quote) {
    'use strict';
    return function (Component) {
        return Component.extend({
            initialize: function () {
                this._super()
            },
            getShippingAddress: function () {
                var address = quote.shippingAddress();
                var street;
                if (address.postcode === null) {

                    return {};
                }

                if(typeof address.street !== 'undefined'){
                    street = address.street[0];
                }
                else{
                    street = address.street;
                }

                return {
                    recipientName: address.firstname + ' ' + address.lastname,
                    streetAddress: street,
                    locality: address.city,
                    countryCodeAlpha2: address.countryId,
                    postalCode: address.postcode,
                    region: address.regionCode,
                    phone: address.telephone,
                    editable: this.isAllowOverrideShippingAddress()
                };
            }
        });
    };
});
