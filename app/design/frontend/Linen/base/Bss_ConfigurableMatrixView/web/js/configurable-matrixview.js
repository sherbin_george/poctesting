define([
    'jquery',
    'Magento_Catalog/js/price-utils',
    'mage/template',
    "mage/mage",
    'Bss_ConfigurableMatrixView/js/table',
    'Magento_Swatches/js/swatch-renderer'
], function ($, priceUtils, mageTemplate) {
    'use strict';
    $.widget('mage.ConfigurableMatrixView', {
        options: {
            info_html_child:{},
            childproduct:{},
            priceFormat:'',
            show_button_qty:'',
            cks_tier_price:'',
            tier_price_calculate:'',
            price_display_type:'',
            taxRate:'',
            priceConfig: null,
            tableSelector: '#bss-matrixview',
            totalPriceContainer: '.total-price-matrix'
        },

        _create: function () {
            localStorage.removeItem("option_amountMVT");
            var $widget = this;
            $widget._RenderTable();
            $widget._RenderProduct();
            $widget._EventListener();
        },

        _RenderTable: function () {
            var $widget = this;
            var info_html_child = $widget.options.info_html_child;
            if($('#bss-price-range').length > 0){
                if ($('.product-info-price>.price-box').length) {
                    $('.product-info-price>.price-box').html($('#bss-price-range').html()).css('width','auto');
                }

            }
            if ($('[data-role="tier-price-block"]').length) {
                $('[data-role="tier-price-block"]').attr('data-mattrixv', 'tier-price-block').removeAttr('data-role')
            }
            if ($widget.options.cks_tier_price === '1') {
                var  product_id = $('input[name="product"]').val();
                if (product_id != '') {
                    if ($('.product-info-main .prices-tier.items').length > $('.product-options-wrapper .prices-tier.items').length){
                        $('.product-info-main .prices-tier.items').remove();
                        $(info_html_child['tier_price'][product_id]).insertAfter(".product-info-main > .product-info-price");
                    }else{
                        if ($('[data-mattrixv="tier-price-block"]').length) {
                            $('[data-mattrixv="tier-price-block"]').html(info_html_child['tier_price'][product_id]);
                        }else{
                            var html_tier_price = '<div id="tier-price-'+ product_id +'" class="tier-price">'+ info_html_child['tier_price'][product_id] +'</div>';
                            $('.product-info-main .product-info-price').append(html_tier_price);
                        }
                    }
                }
            }
            // insert button +-
            if($widget.options.show_button_qty == 1){
                $(".child-product-matrix > .qty").spinner({
                    min: 0,
                    step: 1,
                }).parent().find(".ui-spinner-button").empty().append("<span class='custom-button-bss'></span>");
            }

            if ($(this.options.tableSelector).width() >= $(window).width()) {
                $(".block-bss-matrixview").width($(window).width() - 30);
            }
            $(window).resize(function(){
                if ($($widget.options.tableSelector).width() >= $(window).width()) {
                    $(".block-bss-matrixview").width($(window).width() - 30);
                }
            });

            setTimeout(function(){
                $($widget.options.tableSelector).tableCFMatrixView().css('visibility','visible');
                $widget._painDiagonalLine();
            },100)

            var href = $('#product_addtocart_form').attr('action');
            $('#product_addtocart_form').attr('action',href.replace('checkout/cart','matrixview/cart'));
        },

        _RenderProduct: function () {
            var $widget = this;
            var childproduct = $widget.options.childproduct;
            var cks_tier_price = $widget.options.cks_tier_price;
            var info_html_child = $widget.options.info_html_child;
            $(this.options.tableSelector).find('.child-product-matrix').each(function(){
                var i = 1;
                var products = [];
                $(this).find('.super_attribute_matrix').each(function(){
                    var option = $(this);
                    var attribute = childproduct["attributes"][$(this).data('attribute-id')]['options'];
                    $.each(attribute, function (index, value){
                        if (value['id'] == $(option).val()) {
                            products.push(value['products']);
                        }
                    });
                })
                var product = products.shift().filter(function(v) {
                    return products.every(function(a) {
                        return a.indexOf(v) !== -1;
                    });
                });
                if(product[0] && product.length == 1){
                    var name_qty = $(this).find('.qty').attr('name');
                    $(this).append('<input type="hidden" class="child-product" name="child_product'+ name_qty.replace('qty','') +'" value="'+ product[0] +'">')
                    if (info_html_child['final_price'][product[0]] != 'undefined') {
                        $(this).append(info_html_child['final_price'][product[0]]);
                    }


                    $(this).find('*').css('visibility','')
                    if (cks_tier_price == ''){
                        if (typeof info_html_child['tier_price'][product[0]] != 'undefined') {
                            if (info_html_child['tier_price'][product[0]].trim() !='') {
                                var html_tier_price = '<div id="tier-price-'+ product[0]+'" class="tier-price" style="display: none;">'+ info_html_child['tier_price'][product[0]] +'</div>'
                                $(this).append(html_tier_price);
                            }
                        }
                    }

                    if (typeof info_html_child['is_in_stock'][product[0]] != 'undefined' ) {
                        var tmpl = '';
                        if (info_html_child['is_in_stock'][product[0]] == '1') {
                            var stock = mageTemplate('#instock-mt');
                            tmpl = stock({
                                data: {
                                    id: product[0],
                                    qty: info_html_child['qty'][product[0]]
                                }
                            });
                        }
                        if (info_html_child['is_in_stock'][product[0]] == '0') {
                            var outofstock = mageTemplate('#outofstock-mt');
                            tmpl = outofstock({
                                data: {
                                    id: product[0]
                                }
                            });
                        }

                        $(this).append(tmpl);
                    }

                    if ($.inArray(product[0], info_html_child['product_allow']) != -1) {
                        $(this).find('.qty').removeAttr('disabled')
                        $(this).find('.ui-spinner-button').css('visibility','')
                    }else{
                        $(this).find('.qty').attr('disabled','disabled')
                        $(this).find('.ui-spinner-button').css('visibility','hidden')
                    }

                } else {
                    $(this).find('.qty').attr('disabled','disabled')
                    $(this).find('*').css('visibility','hidden')
                }
            })
        },

        _painDiagonalLine: function (){

            if ($('.label-attribute-f').length) {
                var he = $('.label-attribute-f').outerHeight();
                var wi = $('.label-attribute-f').outerWidth();
                $('#bsscanvas').attr('width',wi);
                $('#bsscanvas').attr('height',he);
                var c = document.getElementById("bsscanvas");
                var ctx = c.getContext("2d");
                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(wi, he);
                ctx.lineWidth = 1;
                ctx.strokeStyle = '#999';
                ctx.stroke();
                $('#bsscanvas').appendTo($('.label-attribute-f'));
            }
        },

        _EventListener: function () {
            var $widget = this;
            // hover box qty
            $('.child-product-matrix .qty').focusin(function(event){
                $widget._styleTooltipBeforeshow($(this));
                // $('.block-bss-matrixview').css('overflow','inherit')
                return false;
            });
            // hide tier price + error
            $('.child-product-matrix .qty').focusout(function(){
                $widget._hideTooltip($(this));
                // $('.block-bss-matrixview').css('overflow','auto')
            });

            // reset matrix view
            $(document).on('click','#reset-matrix-view',function(){
                $widget._Reset();
            })

            // select option
            $('input.super-attribute-select,select.super-attribute-select,textarea.super-attribute-select').on('change paste keyup', function(){
                var attribute_id = $(this).attr('name').match(/\d+/)[0];
                var option_value = $(this).val();
                if (option_value) {}
                $($widget.options.tableSelector + ' .rm-attribute').each(function() {
                    if ($(this).data('attribute-id') == attribute_id) {
                        $(this).remove();
                    }
                });

                $($widget.options.tableSelector + ' input.child-product,'
                    + $widget.options.tableSelector  + ' .price-box,'
                    + $widget.options.tableSelector + ' .prices-tier,'
                    + $widget.options.tableSelector + ' .stock-st').remove();

                var attribute_select = false;
                $($widget.options.tableSelector + ' .child-product-matrix').each(function() {
                    $(this).find('.super_attribute_matrix').each(function() {
                        if ($(this).val() == option_value && $(this).attr('data-attribute-id') == attribute_id) {
                            attribute_select = true;
                            return false;
                        }
                    });
                })

                if (!attribute_select) {
                    $($widget.options.tableSelector + ' .child-product-matrix').each(function(){
                        $(this).append('<input type="hidden" disabled="disabled" class="super_attribute_matrix rm-attribute" data-attribute-id="'+ attribute_id +'" value="'+ option_value+ '" />');
                    })
                    $widget._Reset();
                    setTimeout(function() {
                        $widget._painDiagonalLine();
                    },100)
                }

                $widget._RenderProduct();

            })

            $($widget.options.tableSelector + ' .qty,.ui-spinner-button,input.product-custom-option,select.product-custom-option ,textarea.product-custom-option').on("change paste keyup click", function(){
                setTimeout(function() {
                    var total_price = $widget.getTotalPrice();
                    var amount = 0;
                    if (parseInt($widget.options.price_display_type) == 3) {
                        amount = total_price * (1 - 1 / (1 + parseFloat($widget.options.taxRate)));
                        var total_price_ex = total_price - amount;
                        $($widget.options.totalPriceContainer).text($widget.getFormattedPrice(total_price) + '  ' + '(' + $widget.options.excl_text + ':'+ $widget.getFormattedPrice(total_price_ex) + ')');
                    }else{
                        $($widget.options.totalPriceContainer).text($widget.getFormattedPrice(total_price));
                    }
                },100)
            })
        },

        _showTooltip: function($this, posY, posX, posY_minus, posX_minus) {

            if ($this.parents('.child-product-matrix').find('.mess-err-bss').text() != '') {
                var top = posY - posY_minus - 10 - $this.parents('.child-product-matrix').find('.mess-err-bss-m').outerHeight();
                var left =  posX + $('.custom-button-bss').width() + 20 - posX_minus -($this.parents('.child-product-matrix').find('.mess-err-bss-m').outerWidth()/2);
                $this.parents('.child-product-matrix').find('.mess-err-bss-m').show().css({'top':top,'left':left});
            }else{
                if($this.parents('.child-product-matrix').find('.tier-price').length){
                     var top = posY - posY_minus - 10 - $this.parents('.child-product-matrix').find('.tier-price').outerHeight();
                     var left = posX + $('.custom-button-bss').width() + 10 - posX_minus -($this.parents('.child-product-matrix').find('.tier-price').outerWidth()/2);
                    $this.parents('.child-product-matrix').find('.tier-price').show().css({'top':top,'left':left});
                }
            }
        },

        _hideTooltip: function($this) {
            $this.parents('.child-product-matrix').css('zIndex', '9');
            $this.parents('.child-product-matrix').find('.tier-price').hide();
            $this.parents('.child-product-matrix').find('.mess-err-bss-m').hide();
        },

        _styleTooltipBeforeshow: function($this) {
            var $widget = this;
            var closestRelativeParent = $('.child-product-matrix').parents().filter(function() {
                var $prelative = $(this);
                return $prelative.is('body') || $prelative.css('position') == 'relative';
            }).slice(0,1);
            var posX_minus = $(closestRelativeParent).offset().left;
            var posY_minus = $(closestRelativeParent).offset().top;
            if ($this.parents('td').css('position') == 'relative') {
                $this.parent().css('zIndex', '999');
                posX_minus = $this.parent().offset().left;
                posY_minus = $this.parent().offset().top;
            }
            var posX = $this.offset().left;
            var posY = $this.offset().top;
            $widget._showTooltip($this, posY, posX, posY_minus, posX_minus);
            return false;
        },

        _Reset: function() {

            $('.child-product-matrix .qty').val(0);
            $(this.options.totalPriceContainer).text(this.getFormattedPrice(0));
        },

        getTotalPrice: function () {
            var $widget = this;
            var info_html_child = $widget.options.info_html_child;
            var tier_prices = info_html_child['tierPrices'];
            var cks_tier_price = $widget.options.cks_tier_price;
            var tier_price_calculate = $widget.options.tier_price_calculate;
            var childproduct =  $widget.options.childproduct;
            var total_qty = $widget.getTotalQty();
            var custom_price = 0;
            if (localStorage.getItem("option_amountMVT")) {
                custom_price = parseFloat(localStorage.getItem("option_amountMVT"));
            }
            var total_price = 0;
            $(this.options.tableSelector + ' .child-product-matrix').each(function() {
                var qty = 0 ;
                if ($(this).find('.qty').val() !='') {
                    qty = parseFloat($(this).find('.qty').val());
                }
                var child_product = $(this).find('input.child-product').val();
                var tier_price = 0;
                if (child_product && qty > 0) {
                    var unit_price = parseFloat(childproduct["optionPrices"][child_product]['finalPrice']["amount"]);
                    if (!$.isEmptyObject(tier_prices) && typeof tier_prices[child_product] !== 'undefined') {
                        if (tier_price_calculate == 1 && cks_tier_price !='') {
                            tier_price = parseFloat($widget.getTierPrice(tier_prices[child_product],total_qty));
                        }else{
                            tier_price = parseFloat($widget.getTierPrice(tier_prices[child_product],qty));
                        }
                    }

                    if (tier_price > 0 &&  tier_price < unit_price ) {
                        unit_price  = tier_price;
                    }
                    total_price += (custom_price*qty);
                    total_price += (unit_price*qty);
                }
            })
            return total_price;
        },

        getTierPrice: function  (tier_prices, qty){
            var prevQty = 1;
            var price = 0;
                $.each( tier_prices, function(price_qty, tier_price ) {
                    if (qty < tier_price["qty"]) return true;
                    if (tier_price["qty"] < prevQty) return true;
                    price = tier_price["price"];
                    prevQty = tier_price["qty"];
                });
            return price;
        },

        getTotalQty: function () {
            var total_qty = 0;
            $(this.options.tableSelector).find('.child-product-matrix .qty').each(function(){
                if ($(this).val() !='' && $(this).val() > 0) {
                    total_qty += parseFloat($(this).val());
                }
            });

            return total_qty;
        },

        getFormattedPrice: function (price) {
            return priceUtils.formatPrice(price, this.options.priceFormat);
        }
    });
    return $.mage.ConfigurableMatrixView;
});
