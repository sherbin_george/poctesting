/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'ko'
    ],
    function($, ko) {
        'use strict';
        return function (target) {
            // modify target
            var _RenderControls = target._RenderControls;
            target._RenderControls = function() {
                var $widget = this,
                    container = this.element,
                    classes = this.options.classes,
                    chooseText = this.options.jsonConfig.chooseText;

                $widget.optionsMap = {};

                $.each(this.options.jsonConfig.attributes, function () {
                    var item = this,
                        options = $widget._RenderSwatchOptions(item),
                        select = $widget._RenderSwatchSelect(item, chooseText),
                        input = $widget._RenderFormInput(item),
                        label = '';

                    // Show only swatch controls
                    if ($widget.options.onlySwatches && !$widget.options.jsonSwatchConfig.hasOwnProperty(item.id)) {
                        return;
                    }
                    if ($widget.options.enableControlLabel) {
                        if($('.attribute.size-guide').length) {
                            label +=
                                '<span class="' + classes.attributeLabelClass + '">' + item.label + ((item.label == 'Size')?'<span id="size-chat-button" class="toogle-action">size guide</span>':'') +'</span>' +
                                '<span class="' + classes.attributeSelectedOptionLabelClass + '"></span>';
                        }else {
                            label +=
                                '<span class="' + classes.attributeLabelClass + '">' + item.label + '</span>' +
                                '<span class="' + classes.attributeSelectedOptionLabelClass + '"></span>';
                        }
                    }

                    if ($widget.inProductList) {
                        $widget.productForm.append(input);
                        input = '';
                    }

                    // Create new control
                    container.append(
                        '<div class="' + classes.attributeClass + ' ' + item.code +
                        '" attribute-code="' + item.code +
                        '" attribute-id="' + item.id + '">' +
                        label +
                        '<div class="' + classes.attributeOptionsWrapper + ' clearfix">' +
                        options + select +
                        '</div>' + input +
                        '</div>'
                    );

                    $widget.optionsMap[item.id] = {};

                    // Aggregate options array to hash (key => value)
                    $.each(item.options, function () {
                        if (this.products.length > 0) {
                            $widget.optionsMap[item.id][this.id] = {
                                price: parseInt(
                                    $widget.options.jsonConfig.optionPrices[this.products[0]].finalPrice.amount,
                                    10
                                ),
                                products: this.products
                            };
                        }
                    });
                });

                // Connect Tooltip
                container
                    .find('[option-type="1"], [option-type="2"], [option-type="0"], [option-type="3"]')
                    .SwatchRendererTooltip();

                // Hide all elements below more button
                $('.' + classes.moreButton).nextAll().hide();

                // Handle events like click or change
                $widget._EventListener();

                // Rewind options
                $widget._Rewind(container);

                //Emulate click on all swatches from Request
                $widget._EmulateSelected($.parseQuery());
                $widget._EmulateSelected($widget._getSelectedAttributes());
                var result = _RenderControls.apply(this, arguments);

                return  result;
            };
            return target;
        };
    }
);
